
import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/BannerModel.dart';
import 'package:sapawarga/repositories/BannerRepository.dart';

main() {
  group('BannerRepository test', () {
    test('general test', () {

      List<BannerModel> list = [];

      BannerRepository repository = BannerRepository();
      var data = repository.fetchRecords();
      data.then((value){
        list = value;
      });

      assert(list != null);
      expect(list.isNotEmpty, false);
    });
  });

}