import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';

main() {
  group('AuthProfileRepository test', () {
    test('general test', () {
      UserInfoModel userInfo = UserInfoModel();

      AuthProfileRepository repository = AuthProfileRepository();
      var data = repository.fetchUserInfo();
      data.then((value) {
        userInfo = value;
      });

      repository.persistUserInfo(userInfo);
      repository.hasLocalUserInfo();
      repository.readLocalUserInfo();
      repository.getUserInfo();
      repository.updateProfile(userInfoModel: userInfo);
      repository.changePassword(
          oldPass: '123', newPass: '12345', confNewPass: '12345');
      repository.changeProfile(
          name: 'test',
          email: 'test@gmail.com',
          phone: '081222333444',
          address: 'test');
      repository.deleteLocalUserInfo();

      assert(userInfo != null);
      expect(userInfo == null, false);
    });
  });
}
