
import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/GamificationsModel.dart';
import 'package:sapawarga/repositories/GamificationsRepository.dart';

main() {
  group('GamificationRepository test', () {
    test('is correctly generated from Gamification', () {
      List<ItemGamification> list = [];

      GamificationsRepository gamificationsRepository = GamificationsRepository();
     var data = gamificationsRepository.getNewMissionDetail(id: 1);
     data.then((value){
       list.add(value);
     });
     
     gamificationsRepository.getMissionOnProgress(page: 1);
     gamificationsRepository.getNewMissionDetail(id: 1);
     gamificationsRepository.getMissionHistory(page: 1);
     gamificationsRepository.getDetailMission(id: 1);
     gamificationsRepository.getNewMissionDetail(id: 1);
     gamificationsRepository.takeMission(id: 1);
     gamificationsRepository.getMyBadge(page: 1);


      assert(list != null);
      expect(list.isNotEmpty, false);
    });
  });

}