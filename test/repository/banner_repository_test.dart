
import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/MessageModel.dart';
import 'package:sapawarga/repositories/BroadcastRepository.dart';

main() {
  group('BroadcastRepository test', () {
    test('general test', () {

      List<MessageModel> list = [];

      BroadcastRepository repository = BroadcastRepository();
      var data = repository.getRecords();
      data.then((value){
        list = value;
      });

      repository.fetchRecords(1);
      repository.getDetail(1);
      repository.deleteRecord('1');
      repository.deleteListRecord(list);
      repository.insertToDatabase(MessageModel());
      repository.hasLocalData();
      repository.getLocalData();
      repository.updateReadData('1');
      repository.delete('1');
      repository.MultipleDelete('1');
      repository.clearLocalData();


      assert(list != null);
      expect(list.isNotEmpty, false);
    });
  });

}