
import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/UsulanModel.dart';
import 'package:sapawarga/repositories/UsulanRepository.dart';

main() {
  group('UsulanRepository test', () {
    test('is correctly generated from PhoneNumberModel', () {
      List<UsulanModel> list = [];

      UsulanRepository usulanRepository = UsulanRepository();
     var data = usulanRepository.getUsulan(page: 1);
     data.then((value){
       list = value;
     });
     
     usulanRepository.deleteUsulan(idUsulan: 1);
     usulanRepository.likeUsulan(id: 1);
     usulanRepository.getMyUsulan(page: 1);
     usulanRepository.addPhoto(null);
     usulanRepository.getCategoryUsulan(page: 1);
     usulanRepository.getUsulanDetail(id: 1);
     usulanRepository.sendUsulan(title: '', description: '',status: '', categoryId: null,attachment: null, id: null);
     usulanRepository.getRecordsLocal();
     usulanRepository.getRecordsCategoryUsulan();
     usulanRepository.getLocalDataCategoryUsulan();
     usulanRepository.hasRecordsLocal();
     usulanRepository.hasLocalDataCategoryUsulan();

      assert(list != null);
      expect(list.isNotEmpty, false);
    });
  });

}