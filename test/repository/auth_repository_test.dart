
import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';

main() {
  group('AuthRepository test', () {
    test('general test', () {

      String token = '';

      AuthRepository repository = AuthRepository();
      var data = repository.authenticate(username: 'test', password: 'test');
      data.then((value){
        token = value;
      });

      repository.persistToken(token);
      repository.hasToken();
      repository.getToken();
      repository.requestResetPassword(email: 'test@gmail.com');
      repository.setOnboarding();
      repository.hasOnboarding();
      repository.unAuthenticate();
      repository.deleteToken();


      assert(token != null);
      expect(token == null, false);
    });
  });

}