
import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/CounterHoaxModel.dart';
import 'package:sapawarga/repositories/CounterHoaxRepository.dart';

main() {
  group('CounterHoaxRepository test', () {
    test('general test', () {

      List<CounterHoaxModel> list = [];

      CounterHoaxRepository repository = CounterHoaxRepository();
      var data = repository.fetchRecords(1);
      data.then((value){
        list = value;
      });

      repository.getDetail(1);


      assert(list != null);
      expect(list.isNotEmpty, false);
    });
  });

}