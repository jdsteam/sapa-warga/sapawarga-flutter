import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/banner/Bloc.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/BannerModel.dart';
import 'package:sapawarga/repositories/BannerRepository.dart';
import 'package:test/test.dart';

class MockBannerRepository extends Mock implements BannerRepository {}

void main() {
  MockBannerRepository mockBannerRepository;
  BannerListBloc bannerListBloc;

  setUp(() {
    mockBannerRepository = MockBannerRepository();
    bannerListBloc = BannerListBloc(bannerRepository: mockBannerRepository);
  });

  test('BannerList initial state is correct', () {
    expect(bannerListBloc.initialState, BannerListInitial());
  });

  test('BannerList dispose does not emit new states', () {
    expectLater(
      bannerListBloc,
      emitsInOrder([]),
    );
    bannerListBloc.close();
  });

  test('BannerListLoad to String', () {
    expect(BannerListLoad().toString(), 'Event BannerListLoad');
  });

  test('BannerListLoad props', () {
    expect(BannerListLoad().props, []);
  });

  test('BannerListLoading to String', () {
    expect(BannerListLoading().toString(), 'State BannerListLoading');
  });

  test('BannerListLoaded to String', () {
    expect(BannerListLoaded(records: []).toString(), 'State BannerListLoaded');
  });

  test('BannerListFailure to String', () {
    expect(BannerListFailure(error: '').toString(),
        'State BannerListFailure{error: }');
  });

  test('emits [BannerListInitial, BannerListLoading, BannerListLoaded]', () {
    List<BannerModel> records = [];

    when(mockBannerRepository.fetchRecords())
        .thenAnswer((_) => Future.value(records));

    final expectedResponse = [
      BannerListInitial(),
      BannerListLoading(),
      BannerListLoaded(records: records)
    ];

    expectLater(
      bannerListBloc,
      emitsInOrder(expectedResponse),
    );

    bannerListBloc.add(BannerListLoad());
  });

  test('emits [BannerListInitial, BannerListLoading, BannerListFailed]', () {
    when(mockBannerRepository.fetchRecords())
        .thenThrow(ErrorException.timeoutException);

    final expectedResponse = [
      BannerListInitial(),
      BannerListLoading(),
      BannerListFailure(
          error: CustomException.onConnectionException(
              ErrorException.timeoutException))
    ];

    expectLater(
      bannerListBloc,
      emitsInOrder(expectedResponse),
    );

    bannerListBloc.add(BannerListLoad());
  });
}
