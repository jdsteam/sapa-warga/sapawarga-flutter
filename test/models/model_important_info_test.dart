import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/CategoryModel.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';

main() {
  group('ImportantInfoModel', () {
    test('is correctly generated from ImportantInfoModel Object', () {

      Category category = Category(id: 1, name: 'test');

      List<Attachment> attachments = [];


      Map<String, Object> mock = {
        "id": 1,
        "title": "test",
        "category_id": 1,
        "category": category.toJson(),
        "content": "content",
        "image_path": "image",
        "image_path_url": "image",
        "source_url": "url",
        "status": 10,
        "attachments": List<dynamic>.from(attachments.map((x) => x.toJson())),
        "status_label": "status",
        "created_at": 1111,
        "updated_at": 1111,
        "created_by": 1111,
      };

      final response = importantInfoModelFromJson(jsonEncode(mock));

      assert(response != null);
      expect(response.id, 1);
      expect(response.title, 'test');
    });

    test('is correctly generated from ImportantInfoModel Array', () {
      Category categoryData = Category(id: 43, name: 'Berita');

      List<Attachment> attachments = [];

      ImportantInfoModel data1 = ImportantInfoModel(
        id: 1,
        title: "title",
        categoryId: 1,
        category: categoryData,
        content: "content",
        imagePath: "image_path",
        imagePathUrl: "image_path_url",
        sourceUrl: "source_url",
        status: 1,
        attachments: attachments,
        statusLabel: "status_label",
        createdAt: 1111,
        updatedAt: 1111,
        createdBy: 1111);

      ImportantInfoModel data2 = ImportantInfoModel(
          id: 12,
          title: "title2",
          categoryId: 2,
          category: categoryData,
          content: "content2",
          imagePath: "image_path2",
          imagePathUrl: "image_path_url2",
          sourceUrl: "source_url2",
          status: 1,
          attachments: attachments,
          statusLabel: "status_label2",
          createdAt: 1111222,
          updatedAt: 1111222,
          createdBy: 1111222);

      List<ImportantInfoModel> mock = List<ImportantInfoModel>();
      mock.add(data1);
      mock.add(data2);

      List<ImportantInfoModel> response = listImportantInfoModelFromJson(jsonEncode(mock));
      assert(response != null);
      expect(response.length, 2);
      expect(response[0].id, 1);
      expect(response[0].title, 'title');
      expect(response[1].id, 12);
      expect(response[1].title, 'title2');
    });
  });

  group('Attachment', () {
    test('is correctly generated from AttachmentModel Object', () {

      Map<String, Object> mock = {
        "id": 1,
        "name": "test",
        "file_path": "test",
        "file_url": "test",
      };

      final response = Attachment.fromJson(json.decode(jsonEncode(mock)));

      assert(response != null);
      expect(response.id, 1);
      expect(response.name, 'test');
    });

    test('is correctly generated from ImportantInfoModel Array', () {

      String attachmentModelToJson(Attachment data) => json.encode(data.toJson());

      Attachment attachment = Attachment(id: 1, name: "test", filePath: "test", fileUrl: "test");

      expect(attachmentModelToJson(attachment), jsonEncode(attachment.toJson()));

    });
  });
}
