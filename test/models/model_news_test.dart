import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/NewsModel.dart';

main() {
  group('News', () {
    test('Channel', () {
      Map<String, Object> mock = {
        "id": 2,
        "name": "Kompas",
        "icon_url":
            "https://asset.kompas.com/data/2017/wp/images/favicon2017.ico",
        "website": "https://kompas.com",
        "meta": null,
        "status": 10,
        "status_label": "Aktif",
        "created_at": 1562663406,
        "updated_at": 1562663406
      };

      final respon = channelFromJson(jsonEncode(mock));
      assert(respon != null);
      expect(mock['id'], 2);
      expect(mock['name'], 'Kompas');
      expect(mock['icon_url'], 'https://asset.kompas.com/data/2017/wp/images/favicon2017.ico');
      expect(mock['website'], 'https://kompas.com');
      expect(mock['meta'], null);
      expect(mock['status'], 10);
      expect(mock['status_label'], 'Aktif');
      expect(mock['created_at'], 1562663406);
      expect(mock['updated_at'], 1562663406);
    });

    test('Channel 2', () {
      Channel channel = Channel(
          id: 2,
          name: 'Kompas',
          iconUrl:
              'https://asset.kompas.com/data/2017/wp/images/favicon2017.ico',
          website: 'https://kompas.com',
          meta: null,
          status: 10,
          statusLabel: 'Aktif',
          createdAt: 1562663406,
          updatedAt: 1562663406);

      assert(channel != null);
      expect(channel.id, 2);
      expect(channel.name, 'Kompas');
      expect(channel.iconUrl, 'https://asset.kompas.com/data/2017/wp/images/favicon2017.ico');
      expect(channel.website, 'https://kompas.com');
      expect(channel.meta, null);
      expect(channel.status, 10);
      expect(channel.statusLabel, 'Aktif');
      expect(channel.createdAt, 1562663406);
      expect(channel.updatedAt, 1562663406);
    });

    test('Meta', () {
      Map<String, Object> mock = {
        "read_count": 2,
      };

      final respon = metaFromJson(jsonEncode(mock));
      assert(respon != null);
      expect(mock['read_count'], 2);
    });

    test('Meta', () {
      Meta meta = Meta(
          readCount: 2,
      );

      assert(meta != null);
      expect(meta.readCount, 2);
    });



    test('is correctly generated from News Object', () {
      DateTime sourceDate = DateTime.parse('2019-09-23');
      Map<String, Object> mock = {
        "id": 45,
        "title": 'Wagub Uu Beberkan Cara Menjaga Kuantitas Pertanian di Jabar',
        "content":
            'KOMPAS.com - Wakil Gubernur Jawa Barat (Jabar) Uu Ruzhanul Ulum mengatakan menjaga kuantitas',
        "featured": 1,
        "cover_path": '',
        "cover_path_url": '',
        "source_date":
            "${sourceDate.year.toString().padLeft(4, '0')}-${sourceDate.month.toString().padLeft(2, '0')}-${sourceDate.day.toString().padLeft(2, '0')}",
        "source_url": '',
        "channel_id": 1,
        "channel": {
          "id": 2,
          "name": "Kompas",
          "icon_url":
              "https://asset.kompas.com/data/2017/wp/images/favicon2017.ico",
          "website": "https://kompas.com",
          "meta": null,
          "status": 10,
          "status_label": "Aktif",
          "created_at": 1562663406,
          "updated_at": 1562663406
        },
        "kabkota_id": 1,
        "kabkota": 2,
        "total_viewers": 10,
        "meta": null,
        "seq": 10,
        "status": 0,
        "status_label": '',
        "created_at": 1570763169,
        "updated_at": 1570763169,
      };

      final respon = newsModelFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock['id'], 45);
      expect(mock['title'],
          'Wagub Uu Beberkan Cara Menjaga Kuantitas Pertanian di Jabar');
    });
  });
}
