import 'package:sapawarga/models/AuthorModel.dart';
import 'package:test/test.dart';

main() {
  group('Author', () {
    test('is correctly generated from BroadcastModel Array', () {
      Author data1 = Author(
          id: 1,
          name: "Admin",
        roleLabel: "admin");

      Author data2 = Author(
          id: 2,
          name: "Provinsi",
          roleLabel: "provinsi");

      List<Author> mock = List<Author>();
      mock.add(data1);
      mock.add(data2);

      expect(mock.length, 2);
      expect(mock[0].id, 1);
      expect(mock[0].name, 'Admin');
      expect(mock[0].roleLabel, 'admin');
      expect(mock[1].id, 2);
      expect(mock[1].name, 'Provinsi');
      expect(mock[1].roleLabel, 'provinsi');
    });
  });

}