import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/MessageModel.dart';

main() {
  group('Message ', () {
    test('is correctly generated from MessageModel Object', () {
      Map<String, Object> mock = {
        "id": "02kZB",
        "type": "broadcast",
        "message_id": 765,
        "sender_id": 186063,
        "sender_name": "Robot Provinsi Pertama",
        "recipient_id": 36,
        "category_name": "Sosialisasi",
        "title": "Kirim Pesan Broadcast [2]",
        "excerpt": null,
        "content": "Kirim Pesan Broadcast [2]",
        "status": 10,
        "meta": null,
        "read_at": null,
        "created_at": 1570612013,
        "updated_at": 1570612013
      };

      final respon = messageFromJson(jsonEncode(mock));

      assert(respon != null);
      expect(mock['id'], '02kZB');
      expect(mock['title'], 'Kirim Pesan Broadcast [2]');
    });

    test('is correctly generated from MessageModel Array', () {
      MessageModel data1 = MessageModel(
          id: "02kZB",
          type: "broadcast",
          messageId: 765,
          senderId: 186063,
          senderName: "Robot Provinsi Pertama",
          recipientId: 36,
          categoryName: "Sosialisasi",
          title: "Kirim Pesan Broadcast [2]",
          excerpt: null,
          content: "Kirim Pesan Broadcast [2]",
          status: 10,
          meta: null,
          readAt: null,
          createdAt: 1570612013,
          updatedAt: 1570612013);

      MessageModel data2 = MessageModel(
          id: "0221ZZB",
          type: "broadcast",
          messageId: 765,
          senderId: 186063,
          senderName: "Robot Provinsi Pertama",
          recipientId: 36,
          categoryName: "Sosialisasi",
          title: "Kirim Pesan Broadcast [221]",
          excerpt: null,
          content: "Kirim Pesan Broadcast [122]",
          status: 10,
          meta: null,
          readAt: null,
          createdAt: 1570612013,
          updatedAt: 1570612013);

      List<MessageModel> mock = List<MessageModel>();
      mock.add(data1);
      mock.add(data2);

      List<MessageModel> respon =
          listMessageFromJson(jsonEncode(mock));
      assert(respon != null);
      expect(respon.length, 2);
      expect(respon[0].id, '02kZB');
      expect(respon[0].title, 'Kirim Pesan Broadcast [2]');
      expect(respon[1].id, '0221ZZB');
      expect(respon[1].title, 'Kirim Pesan Broadcast [221]');
    });
  });
}
