import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/AddPhotoModel.dart';

main(){
  group('Add Photo',(){
    test('is correctly generated from Photo Object', () {

      Data data = Data(
        path: '',
        url: ''
      );

      AddPhotoModel addPhotoModel = AddPhotoModel(
        data: data
      );


      final respon = addPhotoModelFromJson(jsonEncode(addPhotoModel));

      assert(respon != null);
      expect(respon.data.path, data.path);
      expect(respon.data.url, data.url);
    });
  });
}