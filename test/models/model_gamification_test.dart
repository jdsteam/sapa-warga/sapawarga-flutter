import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/GamificationDetailOnProgressModel.dart';
import 'package:sapawarga/models/GamificationOnprogressModel.dart';
import 'package:sapawarga/models/GamificationsModel.dart';
import 'package:sapawarga/models/MetaResponseModel.dart';

main() {
  group('GamificationModel', () {
    test('is correctly generated from GamificationModel Object', () {
      Map<String, dynamic> mock = {
        "items": [
          {
            "id": 15,
            "title": "Memposting 10 kegiatan RW kamu",
            "title_badge": "RW teraktif",
            "description": "Memposting 10 kegiatan RW kamu",
            "object_type": "user_post",
            "object_event": "user_post_create",
            "total_hit": 10,
            "image_badge_path_url": "https://d2o6nohz2yihhc.cloudfront.net/general/1580718430-IW0TlvScM7GvdNbd5oG3vjsdPMWkXgDQ.jpg",
            "start_date": "2020-02-03",
            "end_date": "2020-02-13",
            "status": 10,
            "status_label": "Aktif",
            "created_at": 1580718647,
            "updated_at": 1580718647,
            "created_by": 1
          },
          {
            "id": 14,
            "title": "Misi membaca 10 berita",
            "title_badge": "RW terupdate",
            "description": "Misi membaca 10 berita deskripsi",
            "object_type": "news",
            "object_event": "news_view_detail",
            "total_hit": 5,
            "image_badge_path_url": "https://d2o6nohz2yihhc.cloudfront.net/general/1580718365-wdAVtNISMoUe9PJYumYWxrrNUVFg3gBB.jpg",
            "start_date": "2020-02-03",
            "end_date": "2020-02-10",
            "status": 10,
            "status_label": "Aktif",
            "created_at": 1580718406,
            "updated_at": 1580718406,
            "created_by": 1
          }
        ],
        "_meta": {
          "totalCount": 2,
          "pageCount": 1,
          "currentPage": 1,
          "perPage": 20
        }
      };

      final response = gamificationFromJson(jsonEncode(mock));
      response.toJson();

      assert(response != null);
      expect(mock['items'][0]['id'], 15);
    });

    test('is correctly generated from item GamificationModel Object', () {
      Map<String, dynamic> mock = {
        "id": 15,
        "title": "Memposting 10 kegiatan RW kamu",
        "title_badge": "RW teraktif",
        "description": "Memposting 10 kegiatan RW kamu",
        "object_type": "user_post",
        "object_event": "user_post_create",
        "total_hit": 10,
        "image_badge_path_url": "https://d2o6nohz2yihhc.cloudfront.net/general/1580718430-IW0TlvScM7GvdNbd5oG3vjsdPMWkXgDQ.jpg",
        "start_date": "2020-02-03",
        "end_date": "2020-02-13",
        "status": 10,
        "status_label": "Aktif",
        "created_at": 1580718647,
        "updated_at": 1580718647,
        "created_by": 1
      };

      final response = itemGamificationFromJson(jsonEncode(mock));
      response.toJson();

      assert(response != null);
      expect(mock['id'], 15);
      expect(mock['title'], 'Memposting 10 kegiatan RW kamu');
    });
  });

  group('GamificationOnProgressModel', () {
    test('is correctly generated from GamificationOnProgressModel Object', () {
      Map<String, dynamic> mock = {
        "items": [
          {
            "id": 1,
            "gamification_id": 14,
            "user_id": 186210,
            "user": {
              "id": 186210,
              "name": "anggaaaa",
              "email": "gaganugraha33@gmail.com",
              "kabkota": "KAB. BANDUNG",
              "kelurahan": "ANCOLMEKAR",
              "kecamatan": "ARJASARI",
              "rw": "001",
              "role_label": "staffRW"
            },
            "total_user_hit": 5,
            "created_at": 1580786194,
            "updated_at": 1580982420,
            "completed": true,
            "gamification": {
              "id": 14,
              "title": "Misi membaca 10 berita",
              "title_badge": "RW terupdate",
              "description": "Misi membaca 10 berita deskripsi",
              "object_type": "news",
              "object_event": "news_view_detail",
              "total_hit": 5,
              "image_badge_path_url": "https://d2o6nohz2yihhc.cloudfront.net/general/1580718365-wdAVtNISMoUe9PJYumYWxrrNUVFg3gBB.jpg",
              "start_date": "2020-02-03",
              "end_date": "2020-02-10",
              "status": 10,
              "status_label": "Aktif",
              "created_at": 1580718406,
              "updated_at": 1580718406,
              "created_by": 1
            }
          },
          {
            "id": 6,
            "gamification_id": 15,
            "user_id": 186210,
            "user": {
              "id": 186210,
              "name": "anggaaaa",
              "email": "gaganugraha33@gmail.com",
              "kabkota": "KAB. BANDUNG",
              "kelurahan": "ANCOLMEKAR",
              "kecamatan": "ARJASARI",
              "rw": "001",
              "role_label": "staffRW"
            },
            "total_user_hit": 2,
            "created_at": 1581058433,
            "updated_at": 1581059639,
            "completed": false,
            "gamification": {
              "id": 15,
              "title": "Memposting 10 kegiatan RW kamu",
              "title_badge": "RW teraktif",
              "description": "Memposting 10 kegiatan RW kamu",
              "object_type": "user_post",
              "object_event": "user_post_create",
              "total_hit": 10,
              "image_badge_path_url": "https://d2o6nohz2yihhc.cloudfront.net/general/1580718430-IW0TlvScM7GvdNbd5oG3vjsdPMWkXgDQ.jpg",
              "start_date": "2020-02-03",
              "end_date": "2020-02-13",
              "status": 10,
              "status_label": "Aktif",
              "created_at": 1580718647,
              "updated_at": 1580718647,
              "created_by": 1
            }
          }
        ],
        "_links": {
          "self": {
            "href": "http://sapawarga-staging.jabarprov.go.id/v1/gamifications/participant?page=1&limit=20"
          }
        },
        "_meta": {
          "totalCount": 2,
          "pageCount": 1,
          "currentPage": 1,
          "perPage": 20
        }
      };

      final response = gamificationOnProgressFromJson(jsonEncode(mock));
      response.toJson();

      assert(response != null);
      expect(mock['items'][0]['id'], 1);
    });
  });

  group('GamificationDetailOnProgressModel', () {
    test('is correctly generated from GamificationDetailOnProgressModel Object', () {
      Map<String, dynamic> mock = {
        "items": [
          {
            "id": 4,
            "gamification_id": 14,
            "object_id": 91,
            "user_id": 186210,
            "created_at": 1580797038,
            "updated_at": 1580797038,
            "total_hit": 5,
            "object_event": "news_view_detail"
          },
          {
            "id": 5,
            "gamification_id": 14,
            "object_id": 103,
            "user_id": 186210,
            "created_at": 1580797485,
            "updated_at": 1580797485,
            "total_hit": 5,
            "object_event": "news_view_detail"
          },
          {
            "id": 6,
            "gamification_id": 14,
            "object_id": 95,
            "user_id": 186210,
            "created_at": 1580980320,
            "updated_at": 1580980320,
            "total_hit": 5,
            "object_event": "news_view_detail"
          },
          {
            "id": 7,
            "gamification_id": 14,
            "object_id": 104,
            "user_id": 186210,
            "created_at": 1580982393,
            "updated_at": 1580982393,
            "total_hit": 5,
            "object_event": "news_view_detail"
          },
          {
            "id": 8,
            "gamification_id": 14,
            "object_id": 100,
            "user_id": 186210,
            "created_at": 1580982420,
            "updated_at": 1580982420,
            "total_hit": 5,
            "object_event": "news_view_detail"
          }
        ],
        "_links": {
          "self": {
            "href": "http://sapawarga-staging.jabarprov.go.id/v1/gamifications/my-task/14?page=1&per-page=0"
          }
        },
        "_meta": {
          "totalCount": 5,
          "pageCount": 1,
          "currentPage": 1,
          "perPage": 0
        }
      };

      final response = gamificationDetailFromJson(jsonEncode(mock));
      response.toJson();

      final responseMeta = metaResponseModelFromJson(jsonEncode(mock));


      assert(response != null);
      expect(mock['items'][0]['id'], 4);
      assert(responseMeta != null);
      expect(mock['_meta']['totalCount'], 5);
    });
  });

}
