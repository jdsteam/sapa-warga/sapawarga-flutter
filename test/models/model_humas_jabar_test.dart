import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/HumasJabarModel.dart';

main() {
  group('Humas Jabar', () {
    test('is correctly generated from HumasJabarModel', () {
      HumasJabarModel humasJabarModel = HumasJabarModel(
        id: 1,
        postTitle: 'test',
        thumbnail: '',
        slug: '',
        tglPublish: ''
      );


      Map<String, Object> mock = {
        'ID': 1,
        'post_title': 'test',
        'thumbnail': '',
        'slug': '',
        'tgl_publish': '',
      };

      HumasJabarModel.fromJson(mock);
      HumasJabarModel.fromDatabaseMap(mock);
      humasJabarModel.toJson();

      assert(humasJabarModel != null);

      expect(humasJabarModel.id, 1);
      expect(humasJabarModel.postTitle, 'test');
    });

  });
}
