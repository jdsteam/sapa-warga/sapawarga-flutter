import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/models/UpdateAppModel.dart';

main() {
    test('is correctly generated from Update App Model Object', () {
      Map<String, Object> mock = {
        "version": '1.0.0',
        "force_update": true,
      };

      final response = updateAppFromJson(jsonEncode(mock));

      assert(response != null);
      expect(mock['version'], '1.0.0');
      expect(mock['force_update'], true);
    });

    test('is correctly generated Update App Model to Json', () {
      UpdateAppModel data1 = UpdateAppModel(
          version: '1.0.0',
          forceUpdate: true);

      Map<String, dynamic> mock = {
        "version": '1.0.0',
        "force_update": true,
      };

      expect(data1.toJson(), mock);

    });
}
