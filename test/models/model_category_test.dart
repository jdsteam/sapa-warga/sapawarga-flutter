import 'package:sapawarga/models/CategoryModel.dart';
import 'package:test/test.dart';

main() {
  group('Category', () {
    test('is correctly generated from CategoryModel Array', () {
      Category data1 = Category(
          id: 1,
          name: "Informasi");

      Category data2 = Category(
          id: 2,
          name: "Kunjungan");

      List<Category> mock = List<Category>();
      mock.add(data1);
      mock.add(data2);

      assert(mock != null);
      expect(mock.length, 2);
      expect(mock[0].id, 1);
      expect(mock[0].name, 'Informasi');
      expect(mock[1].id, 2);
      expect(mock[1].name, 'Kunjungan');
    });
  });

}