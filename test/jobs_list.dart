import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/jobs_list/Bloc.dart';
import 'package:sapawarga/models/GeneralTitleModel.dart';
import 'package:sapawarga/repositories/JobRepository.dart';

class MockJobRepository extends Mock implements JobRepository {}

void main() {
  MockJobRepository mockJobRepository;
  JoblistBloc joblistBloc;

  setUp(() {
    mockJobRepository = MockJobRepository();
    joblistBloc = JoblistBloc(jobRepository: mockJobRepository);
  });

  group('Job List Test', () {
    test('JoblistBloc initial state is correct', () {
      expect(joblistBloc.initialState, InitialJoblistState());
    });

    test('JoblistBloc dispose does not emit new states', () {
      expectLater(
        joblistBloc,
        emitsInOrder([]),
      );
      joblistBloc.close();
    });

    test('Event EducationsLoad Test', () {
      expect(JobsLoad().toString(), 'Event JobsLoad');
    });

    test('State [InitialJoblistState, JobsListLoading, JobsLoaded] Test', () {
      List<GeneralTitleModel> record;

      when(mockJobRepository.fetchJobs())
          .thenAnswer((_) => Future.value(record));

      final expectedResponse = [
        InitialJoblistState(),
        JobsListLoading(),
        JobsLoaded(record: record)
      ];

      expectLater(
        joblistBloc,
        emitsInOrder(expectedResponse),
      );

      joblistBloc.add(JobsLoad());
    });
  });
}
