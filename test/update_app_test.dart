import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/update_app/Bloc.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/repositories/UpdateAppRepository.dart';
import 'package:test/test.dart';

class MockUpdateRepository extends Mock implements UpdateAppRepository {}

void main() {
  MockUpdateRepository mockUpdateRepository;
  UpdateAppBloc updateAppBloc;

  setUp(() {
    mockUpdateRepository = MockUpdateRepository();
    updateAppBloc = UpdateAppBloc(updateAppRepository: mockUpdateRepository);
  });

  test('UpdateApp initial state is correct', () {
    expect(updateAppBloc.initialState, UpdateAppInitial());
  });

  test('CheckUpdate to String', () {
    expect(CheckUpdate().toString(), 'CheckUpdate');
  });

  test('CheckUpdate props', () {
    expect(CheckUpdate().props, []);
  });

  test('UpdateApp dispose does not emit new states', () {
    expectLater(
      updateAppBloc,
      emitsInOrder([]),
    );
    updateAppBloc.close();
  });

  test('emits [UpdateAppInitial, UpdateAppUpdated]', () {

    when(mockUpdateRepository.checkUpdate())
        .thenAnswer((_) => Future.value(false));

    final expectedResponse = [
      UpdateAppInitial(),
      UpdateAppLoading(),
      UpdateAppUpdated(),
    ];

    expectLater(
      updateAppBloc,
      emitsInOrder(expectedResponse),
    );

    updateAppBloc.add(CheckUpdate());
  });

  test('emits [UpdateAppInitial, UpdateAppRequired]', () {

    when(mockUpdateRepository.checkUpdate())
        .thenAnswer((_) => Future.value(true));

    final expectedResponse = [
      UpdateAppInitial(),
      UpdateAppLoading(),
      UpdateAppRequired(),
    ];

    expectLater(
      updateAppBloc,
      emitsInOrder(expectedResponse),
    );

    updateAppBloc.add(CheckUpdate());
  });

  test('emits [UpdateAppInitial, UpdateAppFailure]', () {
    when(mockUpdateRepository.checkUpdate()).thenThrow('error');

    when(mockUpdateRepository.getCurrentVersion())
        .thenAnswer((_) => Future.value("1.0.0"));

    final expectedResponse = [
      UpdateAppInitial(),
      UpdateAppLoading(),
      UpdateAppFailure(error: Dictionary.somethingWrong),
    ];

    expectLater(
      updateAppBloc,
      emitsInOrder(expectedResponse),
    );

    updateAppBloc.add(CheckUpdate());
  });
}
