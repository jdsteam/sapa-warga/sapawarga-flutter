import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/forgot_password/Bloc.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';

class MockAuthRepository extends Mock implements AuthRepository {}

void main() {
  MockAuthRepository mockAuthRepository;
  ForgotPasswordBloc forgotPasswordBloc;

  setUp(() {
    mockAuthRepository = MockAuthRepository();
    forgotPasswordBloc = ForgotPasswordBloc(authRepository: mockAuthRepository);
  });

  test('ForgotPassword initial state is correct', () {
    expect(forgotPasswordBloc.initialState, ForgotPasswordInitial());
  });

  test('ForgotPassword dispose does not emit new states', () {
    expectLater(
      forgotPasswordBloc,
      emitsInOrder([]),
    );
    forgotPasswordBloc.close();
  });

  test('RequestForgotPassword to String', () {
    expect(RequestForgotPassword(email: 'test@gmail.com').toString(),
        'SendRequestForgotPassword{email: test@gmail.com}');
  });

  test('RequestForgotPassword props', () {
    expect(RequestForgotPassword(email: 'test@gmail.com').props,
        ['test@gmail.com']);
  });

  test('ValidationError to String', () {
    expect(ValidationError(errors: null).toString(), 'State ValidationError');
  });

  test(
      'emits [ForgotPasswordInitial, ForgotPasswordLoading, ForgotPasswordRequested]',
      () {
    when(mockAuthRepository.requestResetPassword(email: 'test@email.com'))
        .thenAnswer((_) => Future.value(true));

    final expectedResponse = [
      ForgotPasswordInitial(),
      ForgotPasswordLoading(),
      ForgotPasswordRequested(),
    ];

    expectLater(
      forgotPasswordBloc,
      emitsInOrder(expectedResponse),
    );

    forgotPasswordBloc.add(RequestForgotPassword(email: 'temail.com'));
  });

  test('emits [ForgotPasswordInitial, ForgotPasswordLoading, ValidationError]',
      () {
    String response = '{"email": ["Email tidak ditemukan."]}';

    Map<String, dynamic> error = json.decode(response);

    when(mockAuthRepository.requestResetPassword(email: 'test@email.com'))
        .thenThrow(ValidationException(error));

    final expectedResponse = [
      ForgotPasswordInitial(),
      ForgotPasswordLoading(),
      ValidationError(errors: error)
    ];

    expectLater(
      forgotPasswordBloc,
      emitsInOrder(expectedResponse),
    );

    forgotPasswordBloc.add(RequestForgotPassword(email: 'test@email.com'));
  });

  test(
      'emits [ForgotPasswordInitial, ForgotPasswordLoading, ForgotPasswordFailure]',
      () {
    when(mockAuthRepository.requestResetPassword(email: 'test@email.com'))
        .thenThrow(ErrorException.unauthorizedException);

    final expectedResponse = [
      ForgotPasswordInitial(),
      ForgotPasswordLoading(),
      ForgotPasswordFailure(error: CustomException.onConnectionException(ErrorException.unauthorizedException)),
    ];

    expectLater(
      forgotPasswordBloc,
      emitsInOrder(expectedResponse),
    );

    forgotPasswordBloc.add(RequestForgotPassword(email: 'test@email.com'));
  });
}
