import 'package:flutter_test/flutter_test.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:shared_preferences/shared_preferences.dart';

main() {
  group('SharedPreference', () {
    const String _key = 'test';
    const String _prefixedKey = 'flutter.' + _key;

    test('test Phonebook', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setPhoneBookPage(1);
      final value = await Preferences.getPhoneBookPage();
      expect(value, 1);
    });

    test('test MyUsulanStat', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setMyUsulanStat(true);
      final value = await Preferences.getMyUsulanStat();
      expect(value, true);
    });

    test('test UsulanTotalCount', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setTotalCount(0);
      final value = await Preferences.getTotalCount();
      expect(value, 0);
    });

    test('test UsulanTotalCountMe', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setUsulanTotalCountMe(0);
      final value = await Preferences.getUsulanTotalCountMe();
      expect(value, 0);
    });

    test('test VideoListJabar', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setVideoListJabar('test');
      final value = await Preferences.getVideoListJabar();
      expect(value, 'test');
    });

    test('test Usulan List', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setUsulanist('test');
      final value = await Preferences.getUsulanList();
      expect(value, 'test');
    });

    test('test hasvideoListJabar', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      final value = await Preferences.hasVideoListJabar();
      expect(value, false);
    });

    test('test deleteVideoListJabar', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.deleteVideoListJabar();
    });

    test('test videoListKokab', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setVideoListKokab('test');
      final value = await Preferences.getVideoListKokab();
      expect(value, 'test');
    });

    test('test hasvideoListKokab', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      final value = await Preferences.hasVideoListKokab();
      expect(value, false);
    });

    test('test deleteVideoListKokab', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.deleteVideoListKokab();
    });

    test('test hasUsulanList', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      final value = await Preferences.hasUsulanList();
      expect(value, false);
    });

    test('test showCaseJabar', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setShowcaseJabar(true);
      final value = await Preferences.hasShowcaseJabar();
      expect(value, true);
    });

    test('test BroadcastPage', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setBroadcastPage(0);
      final value = await Preferences.getBroadcastPage();
      expect(value, 0);
    });

    test('test Qna', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setShowcaseQna(false);
      final value = await Preferences.hasShowcaseQna();
      expect(value, false);
    });

    test('test Qna2', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setShowcaseQna2(false);
      final value = await Preferences.hasShowcaseQna2();
      expect(value, false);
    });

    test('test Total Count', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setRecordsTotalCount(0);
      final value = await Preferences.getRecordTotalCount();
      expect(value, 0);
    });

    test('test Total Count Qna', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setQnATotalCount(0);
      final value = await Preferences.getQnATotalCount();
      expect(value, 0);
    });

    test('test Show Help Qna', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setShowHelpQna(true);
      final value = await Preferences.hasShowHelpQna();
      expect(value, true);
    });

    test('test Show Help Rw Activities', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setShowHelpRwActivity(true);
      final value = await Preferences.hasShowHelpRwActivity();
      expect(value, true);
    });

    test('test Showcase Home', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setShowcaseHome(true);
      final value = await Preferences.hasShowcaseHome();
      expect(value, true);
    });

    test('test Showcase Important Info', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setShowcaseImportantInfo(true);
      final value = await Preferences.hasShowcaseImportantInfo();
      expect(value, true);
    });

    test('test get time education later ', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setTimeEducationLater(10);
      final value = await Preferences.getTimeEducationLater();
      expect(value, 10);
    });

    test('test Showcase Edit Education Level', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setShowcaseEditEducation(true);
      final value = await Preferences.hasShowcaseEditEducation();
      expect(value, true);
    });

    test('test get time rate later ', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setTimeRateLater(10);
      final value = await Preferences.getTimeRateLater();
      expect(value, 10);
    });

    test('test get time rate later ', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setHasRatedApp(true);
      final value = await Preferences.hasRatedApp();
      expect(value, true);
    });

    test('test get count rate later ', () async {
      SharedPreferences.setMockInitialValues(
          <String, dynamic>{_prefixedKey: 1});
      await Preferences.setCountRateLater(10);
      final value = await Preferences.getCountRateLater();
      expect(value, 10);
    });
  });
}
