import 'package:sapawarga/constants/Dictionary.dart';
import 'package:test/test.dart';

void main() {
  group('Dictionary', () {
    test('Usulan', () {
      expect(Dictionary.titleOnboarding2, Dictionary.importantInfo);
      expect(Dictionary.titleUsulan, Dictionary.aspiration);
      expect(Dictionary.dipublikasikan, Dictionary.dipublikasikan);
      expect(Dictionary.terkirim, Dictionary.terkirim);
      expect(Dictionary.ditolak, Dictionary.ditolak);
      expect(Dictionary.draft, Dictionary.draft);
    });
  });
}
