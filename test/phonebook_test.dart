import 'package:flutter/services.dart';
import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/administrasi/AdministrationDetailBloc.dart';
import 'package:sapawarga/blocs/administrasi/AdministrationDetailEvent.dart';
import 'package:sapawarga/blocs/administrasi/AdministrationDetailState.dart';
import 'package:sapawarga/blocs/nearby_locations/Bloc.dart';
import 'package:sapawarga/blocs/phonebook_detail/Bloc.dart';
import 'package:sapawarga/blocs/phonebook_list/Bloc.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/NearByLocationsModel.dart';
import 'package:sapawarga/models/PhoneBookModel.dart';
import 'package:sapawarga/repositories/PhoneBookRepository.dart';
import 'package:test/test.dart';

class MockPhoneBookRepository extends Mock implements PhoneBookRepository {}

void main() {
  MockPhoneBookRepository mockPhoneBookRepository;
  PhoneBookListBloc phoneBookListBloc;
  PhoneBookDetailBloc phoneBookDetailBloc;
  NearByLocationsBloc nearByLocationBloc;
  AdministrationDetailBloc administrationDetailBloc;

  setUp(() {
    mockPhoneBookRepository = MockPhoneBookRepository();
    phoneBookListBloc =
        PhoneBookListBloc(phoneBookRepository: mockPhoneBookRepository);
    phoneBookDetailBloc = PhoneBookDetailBloc();
    nearByLocationBloc =
        NearByLocationsBloc(phoneBookRepository: mockPhoneBookRepository);
    administrationDetailBloc =
        AdministrationDetailBloc(phoneBookRepository: mockPhoneBookRepository);
  });

  group('PhoneBookList', () {
    test('PhoneBookList initial state is correct', () {
      expect(phoneBookListBloc.initialState, PhoneBookListInitial());
    });

    test('PhoneBookListLoad to String', () {
      expect(PhoneBookListLoad(page: 1).toString(), 'Event PhoneBookListLoad');
    });

    test('PhoneBookListLoad props', () {
      expect(PhoneBookListLoad(page: 1, isFirstLoad: true).props, [1, true]);
    });

    test('PhoneBookListRefresh to String', () {
      expect(PhoneBookListRefresh().toString(), 'Event PhoneBookListRefresh');
    });

    test('PhoneBookListRefresh props', () {
      expect(PhoneBookListRefresh().props, []);
    });

    test('PhoneBookListSearch to String', () {
      expect(PhoneBookListSearch(keyword: 'a').toString(),
          'Event PhoneBookListSearch');
    });

    test('PhoneBookListSearch props', () {
      expect(PhoneBookListSearch(keyword: 'a', page: 1, isFirstLoad: true).props,
          ['a', 1, true]);
    });

    test('PhoneBookListFilter to String', () {
      expect(PhoneBookListFilter(keyword: 'a').toString(),
          'Event PhoneBookListFilter');
    });

    test('PhoneBookListFilter props', () {
      expect(PhoneBookListFilter(keyword: 'a').props,
          ['a']);
    });

    test('PhoneBookListFailure to String', () {
      expect(PhoneBookListFailure(error: 'a').toString(),
          'State PhoneBookListFailure');
    });
    test('PhoneBookList dispose does not emit new states', () {
      expectLater(
        phoneBookListBloc,
        emitsInOrder([]),
      );
      phoneBookListBloc.close();
    });

    test(
        'emits [PhoneBookListInitial, PhoneBookListLoading, PhoneBookListLoaded]',
        () {
      const MethodChannel('plugins.flutter.io/shared_preferences')
          .setMockMethodCallHandler((MethodCall methodCall) async {
        if (methodCall.method == 'getAll') {
          return <String, dynamic>{}; // set initial values here if desired
        }
        return null;
      });

      List<PhoneBookModel> records = [];

      when(mockPhoneBookRepository.getRecords(forceRefresh: true))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        PhoneBookListInitial(),
        PhoneBookListLoading(),
        PhoneBookListLoaded(records: records)
      ];

      expectLater(
        phoneBookListBloc,
        emitsInOrder(expectedResponse),
      );

      phoneBookListBloc.add(PhoneBookListLoad(page: 1, isFirstLoad: true));
    });

    test(
        'emits [PhoneBookListInitial, PhoneBookListLoading, PhoneBookListLoaded] => Refresh',
        () {
      List<PhoneBookModel> records = [];

      when(mockPhoneBookRepository.getRecords(page: 1, forceRefresh: true))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        PhoneBookListInitial(),
        PhoneBookListLoading(),
        PhoneBookListLoaded(records: records)
      ];

      expectLater(
        phoneBookListBloc,
        emitsInOrder(expectedResponse),
      );

      phoneBookListBloc.add(PhoneBookListRefresh());
    });

    test(
        'emits [PhoneBookListInitial, PhoneBookListLoading, PhoneBookListLoaded] => Search',
        () {
      List<PhoneBookModel> records = [];

      when(mockPhoneBookRepository.getRecords(keyword: 'this.keyword', page: 1))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        PhoneBookListInitial(),
        PhoneBookListLoading(),
        PhoneBookListLoaded(records: records)
      ];

      expectLater(
        phoneBookListBloc,
        emitsInOrder(expectedResponse),
      );

      phoneBookListBloc.add(PhoneBookListSearch(
          keyword: 'this.keyword', page: 1, isFirstLoad: true));
    });

    test(
        'emits [PhoneBookListInitial, PhoneBookListLoading, PhoneBookListLoaded] => Filter',
        () {
      List<PhoneBookModel> records = [];

      when(mockPhoneBookRepository.getRecords(filter: 'this.keyword'))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        PhoneBookListInitial(),
        PhoneBookListLoading(),
        PhoneBookListLoaded(records: records)
      ];

      expectLater(
        phoneBookListBloc,
        emitsInOrder(expectedResponse),
      );

      phoneBookListBloc.add(PhoneBookListFilter(keyword: 'this.keyword'));
    });

    test(
        'emits [PhoneBookListInitial, PhoneBookListLoading, PhoneBookListFailure]',
        () {
      when(mockPhoneBookRepository.getRecords(forceRefresh: true))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        PhoneBookListInitial(),
        PhoneBookListLoading(),
        PhoneBookListFailure(
            error: CustomException.onConnectionException(
                ErrorException.unauthorizedException))
      ];

      expectLater(
        phoneBookListBloc,
        emitsInOrder(expectedResponse),
      );

      phoneBookListBloc.add(PhoneBookListLoad(page: 1, isFirstLoad: true));
    });

    test(
        'emits [PhoneBookListInitial, PhoneBookListLoading, PhoneBookListFailure] => Refresh',
        () {
      when(mockPhoneBookRepository.getRecords(page: 1, forceRefresh: true))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        PhoneBookListInitial(),
        PhoneBookListLoading(),
        PhoneBookListFailure(
            error: CustomException.onConnectionException(
                ErrorException.unauthorizedException))
      ];

      expectLater(
        phoneBookListBloc,
        emitsInOrder(expectedResponse),
      );

      phoneBookListBloc.add(PhoneBookListRefresh());
    });

    test(
        'emits [PhoneBookListInitial, PhoneBookListLoading, PhoneBookListFailure] => Search',
        () {
      when(mockPhoneBookRepository.getRecords(keyword: 'this.keyword', page: 1))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        PhoneBookListInitial(),
        PhoneBookListLoading(),
        PhoneBookListFailure(
            error: CustomException.onConnectionException(
                ErrorException.unauthorizedException))
      ];

      expectLater(
        phoneBookListBloc,
        emitsInOrder(expectedResponse),
      );

      phoneBookListBloc.add(PhoneBookListSearch(
          keyword: 'this.keyword', page: 1, isFirstLoad: true));
    });

    test(
        'emits [PhoneBookListInitial, PhoneBookListLoading, PhoneBookListFailure] => Filter',
        () {
      when(mockPhoneBookRepository.getRecords(filter: 'this.keyword'))
          .thenThrow(Exception(ErrorException.unauthorizedException));

      final expectedResponse = [
        PhoneBookListInitial(),
        PhoneBookListLoading(),
        PhoneBookListFailure(
            error: CustomException.onConnectionException(
                ErrorException.unauthorizedException))
      ];

      expectLater(
        phoneBookListBloc,
        emitsInOrder(expectedResponse),
      );

      phoneBookListBloc.add(PhoneBookListFilter(keyword: 'this.keyword'));
    });
  });

  group('PhoneBookDetail', () {
    test('PhoneBookDetail initial state is correct', () {
      expect(phoneBookDetailBloc.initialState, PhoneBookDetailInitial());
    });

    test('PhoneBookDetailLoad to String ', () {
      expect(PhoneBookDetailLoad(record: null).toString(),
          'Event PhoneBookDetailLoad');
    });

    test('PhoneBookDetailLoad props ', () {
      expect(PhoneBookDetailLoad(record: null).props,
          [null]);
    });

    test('PhoneBookDetailFailure to String ', () {
      expect(
          PhoneBookDetailFailure().toString(), 'State PhoneBookDetailFailure');
    });

    test('PhoneBookDetailFailure props ', () {
      expect(
          PhoneBookDetailFailure().props, []);
    });

    test('PhoneBookDetail dispose does not emit new states', () {
      expectLater(
        phoneBookDetailBloc,
        emitsInOrder([]),
      );
      phoneBookDetailBloc.close();
    });

    test(
        'emits [PhoneBookDetailInitial, PhoneBookDetailLoading, PhoneBookDetailLoaded]',
        () {
      final expectedResponse = [
        PhoneBookDetailInitial(),
        PhoneBookDetailLoading(),
        PhoneBookDetailLoaded()
      ];

      expectLater(
        phoneBookDetailBloc,
        emitsInOrder(expectedResponse),
      );

      phoneBookDetailBloc.add(PhoneBookDetailLoad(record: null));
    });
  });

  group('NearByLocations', () {
    test('NearByLocations initial state is correct', () {
      expect(nearByLocationBloc.initialState, NearByLocationsInitial());
    });

    test('NearByLocations dispose does not emit new states', () {
      expectLater(
        nearByLocationBloc,
        emitsInOrder([]),
      );
      nearByLocationBloc.close();
    });

    test('NearByLocationsLoad to String', () {
      expect(NearByLocationsLoad(latitude: -6.0, longitude: 107.0).toString(),
          'Event NearByLocationsLoad');
    });

    test('NearByLocationsLoad props', () {
      expect(NearByLocationsLoad(latitude: -6.0, longitude: 107.0).props,
          [-6.0,107.0]);
    });

    test(
        'emits [NearByLocationsInitial, NearByLocationsLoading, NearByLocationsLoaded]',
        () {
      List<NearByLocationsModel> records = [];

      when(mockPhoneBookRepository.getNearByLocations(
              latitude: -6.0, longitude: 107.0))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        NearByLocationsInitial(),
        NearByLocationsLoading(),
        NearByLocationsLoaded(records: records)
      ];

      expectLater(
        nearByLocationBloc,
        emitsInOrder(expectedResponse),
      );

      nearByLocationBloc
          .add(NearByLocationsLoad(latitude: -6.0, longitude: 107.0));
    });

    test(
        'emits [NearByLocationsInitial, NearByLocationsLoading, NearByLocationsFailure]',
        () {
      when(mockPhoneBookRepository.getNearByLocations(
              latitude: -6.0, longitude: 107.0))
          .thenThrow(ErrorException.timeoutException);

      final expectedResponse = [
        NearByLocationsInitial(),
        NearByLocationsLoading(),
        NearByLocationsFailure(
            error: CustomException.onConnectionException(
                ErrorException.timeoutException))
      ];

      expectLater(
        nearByLocationBloc,
        emitsInOrder(expectedResponse),
      );

      nearByLocationBloc
          .add(NearByLocationsLoad(latitude: -6.0, longitude: 107.0));
    });
  });

  group('Administration', () {
    PhoneBookModel phoneBookModel = PhoneBookModel();

    test('Administration initial state is correct', () {
      expect(
          administrationDetailBloc.initialState, AdministrationDetailInitial());
    });

    test('AdministrationDetailLoaded to String', () {
      expect(
          AdministrationDetailLoaded(phoneBookModel: phoneBookModel).toString(),
          'State AdministrationDetailLoaded');
    });

    test('AdministrationDetailFailure to String', () {
      expect(
          AdministrationDetailFailure(error: Dictionary.errorUnauthorized)
              .toString(),
          'AdministrationDetailFailure{error: Izin akses ke server ditolak}');
    });

    test('AdministrationDetailLoad to String', () {
      expect(AdministrationDetailLoad(instansi: 'polisi').toString(),
          'Event AdministrationDetailLoad{instansi: polisi}');
    });

    test('AdministrationDetailLoad props', () {
      expect(AdministrationDetailLoad(instansi: 'polisi').props,
          ['polisi']);
    });

    test(
        'emits [PhoneBookDetailInitial, PhoneBookDetailLoading, PhoneBookDetailLoaded]',
        () {
      final expectedResponse = [
        AdministrationDetailInitial(),
        AdministrationDetailLoading(),
        AdministrationDetailLoaded()
      ];

      expectLater(
        administrationDetailBloc,
        emitsInOrder(expectedResponse),
      );

      administrationDetailBloc
          .add(AdministrationDetailLoad(instansi: 'polisi'));
    });

    test('Administration phone book data null', () {
      when(mockPhoneBookRepository.getNearByLocationInstansi(
              nameInstansi: 'polisi'))
          .thenThrow(ErrorException.timeoutException);

      final expectedResponse = [
        AdministrationDetailInitial(),
        AdministrationDetailLoading(),
        AdministrationDetailLoaded(phoneBookModel: null)
      ];

      expectLater(
        administrationDetailBloc,
        emitsInOrder(expectedResponse),
      );

      administrationDetailBloc
          .add(AdministrationDetailLoad(instansi: 'polisi'));
    });

    test('Administration phone book data socket exception', () {
      when(mockPhoneBookRepository.getNearByLocationInstansi(
              nameInstansi: 'polisi'))
          .thenThrow(ErrorException.socketException);

      final expectedResponse = [
        AdministrationDetailInitial(),
        AdministrationDetailLoading(),
        AdministrationDetailFailure(
            error: CustomException.onConnectionException(
                ErrorException.socketException))
      ];

      expectLater(
        administrationDetailBloc,
        emitsInOrder(expectedResponse),
      );

      administrationDetailBloc
          .add(AdministrationDetailLoad(instansi: 'polisi'));
    });
  });
}
