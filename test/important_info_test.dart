import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/important_info/important_info_comment/Bloc.dart';
import 'package:sapawarga/blocs/important_info/important_info_detail/Bloc.dart';
import 'package:sapawarga/blocs/important_info/important_info_list/Bloc.dart';
import 'package:sapawarga/blocs/important_info/improtant_info_home/Bloc.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/ImportantInfoCommentModel.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';
import 'package:sapawarga/models/MetaResponseModel.dart';
import 'package:sapawarga/repositories/ImportantInfoRepository.dart';

class MockImportantInfoRepository extends Mock implements ImportantInfoRepository {}

void main() {
  MockImportantInfoRepository mockRepo;
  ImportantInfoHomeBloc importantInfoHomeBloc;
  ImportantInfoListTopBloc importantInfoListTopBloc;
  ImportantInfoListOthersBloc importantInfoListOthersBloc;
  ImportantInfoDetailBloc importantInfoDetailBloc;
  ImportantInfoCommentListBloc importantInfoCommentListBloc;
  ImportantInfoCommentAddBloc importantInfoCommentAddBloc;

  setUp(() {
    mockRepo = MockImportantInfoRepository();
    importantInfoHomeBloc = ImportantInfoHomeBloc(importantInfoRepository: mockRepo);
    importantInfoListTopBloc = ImportantInfoListTopBloc(importantInfoRepository: mockRepo);
    importantInfoListOthersBloc = ImportantInfoListOthersBloc(importantInfoRepository: mockRepo);
    importantInfoDetailBloc = ImportantInfoDetailBloc(importantInfoRepository: mockRepo);
    importantInfoCommentListBloc = ImportantInfoCommentListBloc(mockRepo);
    importantInfoCommentAddBloc = ImportantInfoCommentAddBloc(mockRepo);
  });

  // IMPORTANT INFO HOME TEST
  group('ImportantInfoHome Test', () {
    test('ImportantInfoHome initial state is correct', () {
      expect(importantInfoHomeBloc.initialState, ImportantInfoHomeInitial());
    });

    test('ImportantInfoHomeLoad to String', () {
      expect(ImportantInfoHomeLoad().toString(), 'Event ImportantInfoHomeLoad');
    });

    test('ImportantInfoHomeLoad props', () {
      expect(ImportantInfoHomeLoad().props, []);
    });

    test('ImportantInfoDetailLike props  ', () {
      expect(ImportantInfoDetailLike(importantInfoId: 1).props, [1]);
    });

    test('ImportantInfoHome dispose does not emit new states', () {
      expectLater(
        importantInfoHomeBloc,
        emitsInOrder([]),
      );
      importantInfoHomeBloc.close();
    });

    test('emits [ImportantInfoHomeInitial, ImportantInfoHomeLoading, ImportantInfoHomeLoaded]', () {
      List<ImportantInfoModel> records = [];

      when(mockRepo.fetchRecords(0, limit: 3))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        ImportantInfoHomeInitial(),
        ImportantInfoHomeLoading(),
        ImportantInfoHomeLoaded(records)
      ];

      expectLater(
        importantInfoHomeBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoHomeBloc.add(ImportantInfoHomeLoad());
    });

    test('emits [ImportantInfoHomeInitial, ImportantInfoHomeLoading, ImportantInfoHomeFailure]', () {

      when(mockRepo.fetchRecords(0, limit: 3))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        ImportantInfoHomeInitial(),
        ImportantInfoHomeLoading(),
        ImportantInfoHomeFailure(error: CustomException.onConnectionException(ErrorException.timeoutException)),
      ];

      expectLater(
        importantInfoHomeBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoHomeBloc.add(ImportantInfoHomeLoad());
    });

  });


  // IMPORTANT INFO LIST TOP TEST
  group('ImportantInfoListTop Test', () {
    test('ImportantInfoListTop initial state is correct', () {
      expect(importantInfoListTopBloc.initialState, ImportantInfoListTopInitial());
    });

    test('ImportantInfoListTopLoad to String', () {
      expect(ImportantInfoListTopLoad(0).toString(), 'Event ImportantInfoListTopLoad');
    });

     test('ImportantInfoListTopLoadRefreshViewer to String', () {
      expect(ImportantInfoListTopLoadRefreshViewer(0).toString(), 'Event ImportantInfoListTopLoadRefreshViewer');
    });

    test('ImportantInfoListTopLoad props', () {
      expect(ImportantInfoListTopLoad(0).props, []);
    });

    test('ImportantInfoListTopLoadRefreshViewer props', () {
      expect(ImportantInfoListTopLoadRefreshViewer(0).props, []);
    });

    test('ImportantInfoListTop dispose does not emit new states', () {
      expectLater(
        importantInfoListTopBloc,
        emitsInOrder([]),
      );
      importantInfoListTopBloc.close();
    });
    

    test('emits [ImportantInfoListTopInitial, ImportantInfoListTopLoading, ImportantInfoListTopLoaded]', () {
      List<ImportantInfoModel> records = [];

      when(mockRepo.getTopFiveRecords(0))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        ImportantInfoListTopInitial(),
        ImportantInfoListTopLoading(),
        ImportantInfoListTopLoaded(records: records)
      ];

      expectLater(
        importantInfoListTopBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoListTopBloc.add(ImportantInfoListTopLoad(0));

    });

     test('emits [ImportantInfoListTopInitial, ImportantInfoListTopLoadingRefreshViewer, ImportantInfoListTopLoaded]', () {
      List<ImportantInfoModel> records = [];

      when(mockRepo.getTopFiveRecords(0))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        ImportantInfoListTopInitial(),
        ImportantInfoListTopLoadingRefreshViewer(),
        ImportantInfoListTopLoaded(records: records)
      ];

      expectLater(
        importantInfoListTopBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoListTopBloc.add(ImportantInfoListTopLoadRefreshViewer(0));

    });

    test('emits [ImportantInfoListTopInitial, ImportantInfoListTopLoading, ImportantInfoListTopFailure]', () {

      when(mockRepo.getTopFiveRecords(0))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        ImportantInfoListTopInitial(),
        ImportantInfoListTopLoading(),
        ImportantInfoListTopFailure(error: CustomException.onConnectionException(ErrorException.timeoutException)),
      ];

      expectLater(
        importantInfoListTopBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoListTopBloc.add(ImportantInfoListTopLoad(0));

    });

    test('emits [ImportantInfoListTopInitial, ImportantInfoListTopFailure]', () {

      when(mockRepo.getTopFiveRecords(0))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        ImportantInfoListTopInitial(),
        ImportantInfoListTopFailure(error: CustomException.onConnectionException(ErrorException.timeoutException)),
      ];

      expectLater(
        importantInfoListTopBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoListTopBloc.add(ImportantInfoListTopLoadRefreshViewer(0));

    });

  });

  // IMPORTANT INFO LIST OTHER TEST
  group('ImportantInfoListOthers Test', () {
    test('ImportantInfoListOthers initial state is correct', () {
      expect(importantInfoListOthersBloc.initialState, ImportantInfoListOthersInitial());
    });

    test('ImportantInfoListOthersLoad to String', () {
      expect(ImportantInfoListOthersLoad(0).toString(), 'Event ImportantInfoListOthersLoad');
    });

    test('ImportantInfoListOthersLoadRefreshViewer to String', () {
      expect(ImportantInfoListOthersLoadRefreshViewer(0).toString(), 'Event ImportantInfoListOthersLoadRefreshViewer');
    });

    test('ImportantInfoListOthersLoad props', () {
      expect(ImportantInfoListOthersLoad(0).props, []);
    });

    test('ImportantInfoListOthersLoadRefreshViewer props', () {
      expect(ImportantInfoListOthersLoadRefreshViewer(0).props, []);
    });

    test('ImportantInfoListOthers dispose does not emit new states', () {
      expectLater(
        importantInfoListOthersBloc,
        emitsInOrder([]),
      );
      importantInfoListOthersBloc.close();
    });

    test('emits [ImportantInfoListOthersInitial, ImportantInfoListOthersLoading, ImportantInfoListOthersLoaded]', () {
      List<ImportantInfoModel> records = [];

      when(mockRepo.getOtherRecords(0))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        ImportantInfoListOthersInitial(),
        ImportantInfoListOthersLoading(),
        ImportantInfoListOthersLoaded(records: records)
      ];

      expectLater(
        importantInfoListOthersBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoListOthersBloc.add(ImportantInfoListOthersLoad(0));

    });

    test('emits [ImportantInfoListOthersInitial, ImportantInfoListOthersLoadingRefreshViewer, ImportantInfoListOthersLoaded]', () {
      List<ImportantInfoModel> records = [];

      when(mockRepo.getOtherRecords(0))
          .thenAnswer((_) => Future.value(records));

      final expectedResponse = [
        ImportantInfoListOthersInitial(),
        ImportantInfoListOthersLoadingRefreshViewer(),
        ImportantInfoListOthersLoaded(records: records)
      ];

      expectLater(
        importantInfoListOthersBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoListOthersBloc.add(ImportantInfoListOthersLoadRefreshViewer(0));

    });

    test('emits [ImportantInfoListOthersInitial, ImportantInfoListOthersLoading, ImportantInfoListOthersFailure]', () {

      when(mockRepo.getOtherRecords(0))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        ImportantInfoListOthersInitial(),
        ImportantInfoListOthersLoading(),
        ImportantInfoListOthersFailure(error: CustomException.onConnectionException(ErrorException.timeoutException)),
      ];

      expectLater(
        importantInfoListOthersBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoListOthersBloc.add(ImportantInfoListOthersLoad(0));

    });

    test('emits [ImportantInfoListOthersInitial, ImportantInfoListOthersFailure]', () {

      when(mockRepo.getOtherRecords(0))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        ImportantInfoListOthersInitial(),
        ImportantInfoListOthersFailure(error: CustomException.onConnectionException(ErrorException.timeoutException)),
      ];

      expectLater(
        importantInfoListOthersBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoListOthersBloc.add(ImportantInfoListOthersLoadRefreshViewer(0));

    });

  });

  // IMPORTANT INFO DETAIL TEST
  group('ImportantInfoDetail Test', () {
    test('ImportantInfoDetail initial state is correct', () {
      expect(importantInfoDetailBloc.initialState, ImportantInfoDetailInitial());
    });

    test('ImportantInfoDetailLoad to String', () {
      expect(ImportantInfoDetailLoad().toString(), 'Event ImportantInfoDetailLoad');
    });

    test('ImportantInfoDetailLike to String', () {
      expect(ImportantInfoDetailLike(importantInfoId: 1).toString(),
          'Event ImportantInfoDetailLike{importantInfoId: 1}');
    });

    test('ImportantInfoDetailLoad props', () {
      expect(ImportantInfoDetailLoad(id: null).props, [null]);
    });

    test('ImportantInfoDetail dispose does not emit new states', () {
      expectLater(
        importantInfoDetailBloc,
        emitsInOrder([]),
      );
      importantInfoDetailBloc.close();
    });

    test('emits [ImportantInfoDetailInitial, ImportantInfoDetailLoading, ImportantInfoDetailLoaded] by id', () {
      ImportantInfoModel record = ImportantInfoModel(id: 1, title: 'abc', categoryId: 1, category: null, content: 'abc');

      when(mockRepo.getDetail(1))
          .thenAnswer((_) => Future.value(record));

      final expectedResponse = [
        ImportantInfoDetailInitial(),
        ImportantInfoDetailLoading(),
        ImportantInfoDetailLoaded(record: record)
      ];

      expectLater(
        importantInfoDetailBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoDetailBloc.add(ImportantInfoDetailLoad(id: 1));
    });

    test('emits [ImportantInfoDetailInitial, ImportantInfoDetailLoading, ImportantInfoDetailFailure]', () {

      when(mockRepo.getDetail(1))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        ImportantInfoDetailInitial(),
        ImportantInfoDetailLoading(),
        ImportantInfoDetailFailure(error: CustomException.onConnectionException(ErrorException.timeoutException)),
      ];

      expectLater(
        importantInfoDetailBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoDetailBloc.add(ImportantInfoDetailLoad(id: 1));
    });

    test('emits [ImportantInfoDetailInitial, ImportantInfoDetailLike]', () {
      ImportantInfoModel _data = ImportantInfoModel(id: 1, title: 'a');

      when(mockRepo.sendLike(id: _data.id))
          .thenAnswer((_) => Future.value(true));

      final expectedResponse = [
        ImportantInfoDetailInitial()
      ];

      expectLater(
        importantInfoDetailBloc,
        emitsInOrder(expectedResponse),
      );
      importantInfoDetailBloc.add(ImportantInfoDetailLike(importantInfoId: _data.id));
    });

    test('emits [ImportantInfoDetailInitial, ImportantInfoDetailLikeFailure]', () {
      ImportantInfoModel _data = ImportantInfoModel(id: 1, title: 'a');

      when(mockRepo.sendLike(id: _data.id))
          .thenAnswer((_) => Future.value(true));

      final expectedResponse = [
        ImportantInfoDetailInitial()
      ];

      expectLater(
        importantInfoDetailBloc,
        emitsInOrder(expectedResponse),
      );
      importantInfoDetailBloc.add(ImportantInfoDetailLike(importantInfoId: _data.id));
    });

  });

  group('ImportantInfoComment Test', () {
    test('ImportantInfoCommentList initial state is correct', () {
      expect(importantInfoCommentListBloc.initialState, ImportantInfoCommentListInitial());
    });

    test('ImportantInfoCommentListLoad to String', () {
      expect(ImportantInfoCommentListLoad(1,1).toString(), 'Event ImportantInfoCommentListLoad{id: 1, page: 1}');
    });

    test('ImportantInfoCommentListLoad props', () {
      expect(ImportantInfoCommentListLoad(null, null).props, [null, null]);
    });

    test('ImportantInfoCommentList dispose does not emit new states', () {
      expectLater(
        importantInfoCommentListBloc,
        emitsInOrder([]),
      );
      importantInfoCommentListBloc.close();
    });

    test('emits [ImportantInfoCommentListInitial, ImportantInfoCommentListLoading, ImportantInfoCommentListLoaded]', () {

      ItemImportantInfoComment item = ItemImportantInfoComment(
        id : 1,
        newsImportantId : 1,
        text : 'test',
        user : null ,
        createdAt : null,
        updatedAt : null,
        createdBy : null,
        updatedBy : null);

      MetaResponseModel meta = MetaResponseModel(totalCount: 1, pageCount: 1, currentPage: 1, perPage: 10);

      List<ItemImportantInfoComment> items = List<ItemImportantInfoComment>();
      items.add(item);

      ImportantInfoCommentModel record = ImportantInfoCommentModel(items: items, meta: meta);

      when(mockRepo.fetchCommentRecords(id: 1, page: 1))
          .thenAnswer((_) => Future.value(record));

      final expectedResponse = [
        ImportantInfoCommentListInitial(),
        ImportantInfoCommentListLoading(),
        ImportantInfoCommentListLoaded(record)
      ];

      expectLater(
        importantInfoCommentListBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoCommentListBloc.add(ImportantInfoCommentListLoad(1,1));
    });

    test('emits [ImportantInfoCommentListInitial, ImportantInfoCommentListLoading, ImportantInfoCommentListFailure]', () {

      when(mockRepo.fetchCommentRecords(id: 1, page: 1))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        ImportantInfoCommentListInitial(),
        ImportantInfoCommentListLoading(),
        ImportantInfoCommentListFailure(error: CustomException.onConnectionException(ErrorException.timeoutException)),
      ];

      expectLater(
        importantInfoCommentListBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoCommentListBloc.add(ImportantInfoCommentListLoad(1,1));
    });
  });

  group('ImportantInfoCommentAdd Test', () {
    test('ImportantInfoCommentAdd initial state is correct', () {
      expect(importantInfoCommentAddBloc.initialState, ImportantInfoCommentAddInitial());
    });

    test('ImportantInfoCommentAdd to String', () {
      expect(ImportantInfoCommentAdd(id: 1, text: 'test').toString(), 'Event ImportantInfoCommentAdd{id: 1, text: test}');
    });

    test('ImportantInfoCommentAdd props', () {
      expect(ImportantInfoCommentAdd(id: null, text: null).props, [null, null]);
    });

    test('ImportantInfoCommentAdded props', () {
      expect(ImportantInfoCommentAdded(null).props, [null]);
    });

    test('ImportantInfoCommentAdd dispose does not emit new states', () {
      expectLater(
        importantInfoCommentAddBloc,
        emitsInOrder([]),
      );
      importantInfoCommentAddBloc.close();
    });

    test('emits [ImportantInfoCommentAddInitial, ImportantInfoCommentAddLoading, ImportantInfoCommentAdded]', () {

      ItemImportantInfoComment item = ItemImportantInfoComment(
          id : 1,
          newsImportantId : 1,
          text : 'test',
          user : null ,
          createdAt : null,
          updatedAt : null,
          createdBy : null,
          updatedBy : null);

      when(mockRepo.postComment(id: 1, text: 'test'))
          .thenAnswer((_) => Future.value(item));

      final expectedResponse = [
        ImportantInfoCommentAddInitial(),
        ImportantInfoCommentAddLoading(),
        ImportantInfoCommentAdded(item)
      ];

      expectLater(
        importantInfoCommentAddBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoCommentAddBloc.add(ImportantInfoCommentAdd(id: 1, text: 'test'));
    });

    test('emits [ImportantInfoCommentAddInitial, ImportantInfoCommentAddLoading, ImportantInfoCommentAddFailure]', () {

      when(mockRepo.postComment(id: 1, text: 'test'))
          .thenThrow(Exception(ErrorException.timeoutException));

      final expectedResponse = [
        ImportantInfoCommentAddInitial(),
        ImportantInfoCommentAddLoading(),
        ImportantInfoCommentAddFailure(error: CustomException.onConnectionException(ErrorException.timeoutException)),
      ];

      expectLater(
        importantInfoCommentAddBloc,
        emitsInOrder(expectedResponse),
      );

      importantInfoCommentAddBloc.add(ImportantInfoCommentAdd(id: 1, text: 'test'));
    });
  });
}