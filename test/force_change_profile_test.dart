import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/force_change_profile/Bloc.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:test/test.dart';

class MockAuthProfileRepository extends Mock implements AuthProfileRepository {}

void main() {
  MockAuthProfileRepository mockAuthProfileRepository;
  ForceChangeProfileBloc forceChangePasswordBloc;

  setUp(() {
    mockAuthProfileRepository = MockAuthProfileRepository();
    forceChangePasswordBloc = ForceChangeProfileBloc(
        authProfileRepository: mockAuthProfileRepository);
  });

  group('Check Force Change Password Test', () {
    test('CheckForceChangeProfile toString', () {
      expect(CheckForceChangeProfile().toString(),
          'Event CheckForceChangeProfile');
    });

    test('CheckForceChangeProfile props', () {
      expect(CheckForceChangeProfile().props,
          []);
    });

    test('Initial state is correct', () {
      expect(forceChangePasswordBloc.initialState, ForceChangeProfileInitial());
    });

    test('Dispose does not emit new states', () {
      expectLater(
        forceChangePasswordBloc,
        emitsInOrder([]),
      );
      forceChangePasswordBloc.close();
    });

    test(
        'emits [ForceChangePasswordInitial, ForceChangePasswordLoading, ForceChangePasswordRequired]',
        () {
      UserInfoModel userInfo = UserInfoModel(passwordUpdatedAt: null);

      when(mockAuthProfileRepository.getUserInfo(forceFetch: true))
          .thenAnswer((_) => Future.value(userInfo));

      final expectedResponse = [
        ForceChangeProfileInitial(),
        ForceChangeProfileLoading(),
        ForceChangePasswordRequired()
      ];

      expectLater(
        forceChangePasswordBloc,
        emitsInOrder(expectedResponse),
      );

      forceChangePasswordBloc.add(CheckForceChangeProfile());
    });

    test(
        'emits [ForceChangePasswordInitial, ForceChangePasswordLoading, ForceChangeProfileRequired]',
        () {
      UserInfoModel userInfo =
          UserInfoModel(passwordUpdatedAt: 1568611049, profileUpdatedAt: null);

      when(mockAuthProfileRepository.getUserInfo(forceFetch: true))
          .thenAnswer((_) => Future.value(userInfo));

      final expectedResponse = [
        ForceChangeProfileInitial(),
        ForceChangeProfileLoading(),
        ForceChangeProfileRequired()
      ];

      expectLater(
        forceChangePasswordBloc,
        emitsInOrder(expectedResponse),
      );

      forceChangePasswordBloc.add(CheckForceChangeProfile());
    });

    test(
        'emits [ForceChangePasswordInitial, ForceChangePasswordLoading, ForceChangeProfileNotRequired]',
        () {
      UserInfoModel userInfo = UserInfoModel(
          passwordUpdatedAt: 1568611049, profileUpdatedAt: 1568611121);

      when(mockAuthProfileRepository.getUserInfo(forceFetch: true))
          .thenAnswer((_) => Future.value(userInfo));

      final expectedResponse = [
        ForceChangeProfileInitial(),
        ForceChangeProfileLoading(),
        ForceChangeProfileNotRequired()
      ];

      expectLater(
        forceChangePasswordBloc,
        emitsInOrder(expectedResponse),
      );

      forceChangePasswordBloc.add(CheckForceChangeProfile());
    });

    test(
        'emits [ForceChangePasswordInitial, ForceChangePasswordLoading, ForceChangePasswordFailure]',
        () {
      when(mockAuthProfileRepository.getUserInfo()).thenThrow('error');

      final expectedResponse = [
        ForceChangeProfileInitial(),
        ForceChangeProfileLoading(),
        ForceChangeProfileFailure(error: Dictionary.somethingWrong)
      ];

      expectLater(
        forceChangePasswordBloc,
        emitsInOrder(expectedResponse),
      );

      forceChangePasswordBloc.add(CheckForceChangeProfile());
    });
  });
}
