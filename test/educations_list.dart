import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/educations_list/Bloc.dart';
import 'package:sapawarga/models/GeneralTitleModel.dart';
import 'package:sapawarga/repositories/EducationRepository.dart';
import 'package:flutter_test/flutter_test.dart';

class MockEducationRepository extends Mock implements EducationRepository {}

void main() {
  MockEducationRepository mockEducationRepository;
  EducationsListBloc educationsListBloc;

  setUp(() {
    mockEducationRepository = MockEducationRepository();
    educationsListBloc =
        EducationsListBloc(educationRepository: mockEducationRepository);
  });

  group('Education List Test', () {
    test('EducationsListBloc initial state is correct', () {
      expect(educationsListBloc.initialState, InitialEducationsListState());
    });

    test('EducationsListBloc dispose does not emit new states', () {
      expectLater(
        educationsListBloc,
        emitsInOrder([]),
      );
      educationsListBloc.close();
    });

    test('Event EducationsLoad Test', () {
      expect(EducationsLoad().toString(), 'Event EducationsLoad');
    });

    test(
        'State [InitialEducationsListState, EducationsListLoading, EducationsLoaded] Test',
        () {
      List<GeneralTitleModel> record;

      when(mockEducationRepository.fetchEducations())
          .thenAnswer((_) => Future.value(record));

      final expectedResponse = [
        InitialEducationsListState(),
        EducationsListLoading(),
        EducationsLoaded(record: record)
      ];

      expectLater(
        educationsListBloc,
        emitsInOrder(expectedResponse),
      );

      educationsListBloc.add(EducationsLoad());
    });
  });
}
