import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/repositories/VideoRepository.dart';

class MockAuthRepository extends Mock implements AuthRepository {}

class MockAuthProfileRepository extends Mock implements AuthProfileRepository {}

class MockVideoRepository extends Mock implements VideoRepository {}

void main() {
  AuthenticationBloc authenticationBloc;
  MockAuthRepository authRepository;
  MockAuthProfileRepository authProfileRepository;
  MockVideoRepository videoRepository;

  setUp(() {
    authRepository = MockAuthRepository();
    authProfileRepository = MockAuthProfileRepository();
    videoRepository = MockVideoRepository();
    authenticationBloc = AuthenticationBloc(
        authRepository: authRepository,
        authProfileRepository: authProfileRepository,
        videoRepository: videoRepository);
  });

  test('initial state is correct', () {
    expect(authenticationBloc.initialState, AuthenticationUninitialized());
  });

  test('dispose does not emit new states', () {
    expectLater(
      authenticationBloc,
      emitsInOrder([]),
    );
    authenticationBloc.close();
  });

  test('AuthenticationBloc assertation', () {
    expect(AuthenticationBloc(authRepository: AuthRepository()).authRepository,
        isNot(null));
  });

  group('AppStarted', () {
    test('AppStarted to String', () {
      expect(AppStarted().toString(), 'AppStarted');
    });

    test('AppStarted props', () {
      expect(AppStarted().props, []);
    });

    test('emits [uninitialized, authenticated] for valid token', () {
      final expectedResponse = [
        AuthenticationUninitialized(),
        AuthenticationAuthenticated()
      ];

      when(authRepository.hasToken()).thenAnswer((_) => Future.value(true));

      expectLater(
        authenticationBloc,
        emitsInOrder(expectedResponse),
      );

      authenticationBloc.add(AppStarted());
    });

    test('emits [uninitialized, unauthenticated] for invalid token', () {
      final expectedResponse = [
        AuthenticationUninitialized(),
        AuthenticationUnauthenticated()
      ];

      when(authRepository.hasToken()).thenAnswer((_) => Future.value(false));

      expectLater(
        authenticationBloc,
        emitsInOrder(expectedResponse),
      );

      authenticationBloc.add(AppStarted());
    });
  });

  group('LoggedIn', () {
    test('LoggedIn to String', () {
      expect(LoggedIn(token: null).toString(), 'LoggedIn');
    });

    test('LoggedIn props', () {
      expect(LoggedIn(token: null).props, [null]);
    });

    test(
        'emits [uninitialized, loading, authenticated] when token is persisted',
        () {
      final expectedResponse = [
        AuthenticationUninitialized(),
        AuthenticationLoading(),
        AuthenticationAuthenticated(),
      ];

      expectLater(
        authenticationBloc,
        emitsInOrder(expectedResponse),
      );

      authenticationBloc.add(LoggedIn(
        token: 'instance.token',
      ));
    });
  });

  group('LoggedOut', () {
    test('LoggedOut to String', () {
      expect(LoggedOut().toString(), 'LoggedOut');
    });

    test('LoggedOut props', () {
      expect(LoggedOut().props, []);
    });

    test(
        'emits [uninitialized, loading, unauthenticated] when token is deleted',
        () {

      final expectedResponse = [
        AuthenticationUninitialized(),
        AuthenticationLoading(),
        AuthenticationUnauthenticated(hasOnBoarding: true),
      ];

      expectLater(
        authenticationBloc,
        emitsInOrder(expectedResponse),
      );

      authenticationBloc.add(LoggedOut());
    });
  });
}
