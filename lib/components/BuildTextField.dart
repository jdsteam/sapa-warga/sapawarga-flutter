import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BuildTextField extends StatelessWidget {
  final String title;
  final String hintText;
  final TextEditingController controller;
  final validation;
  final TextInputType textInputType;
  final TextStyle textStyle;
  final bool isEdit;
  final int limitCharacter;

  /// @params
  /// * [title] type String must not be null.
  /// * [hintText] type String must not be null.
  /// * [controller] type from class TextEditingController must not be null.
  /// * [validation] type from class Validation.
  /// * [textInputType] type from class TextInputType.
  /// * [textStyle] type from class TextStyle.
  /// * [isEdit] type bool.

  BuildTextField({
    Key key,
    this.title,
    this.hintText,
    this.controller,
    this.validation,
    this.textInputType,
    this.textStyle,
    this.isEdit,
    this.limitCharacter,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 16.0, right: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
          ),
          TextFormField(
            style:
                textStyle != null ? textStyle : TextStyle(color: Colors.black),
            enabled: isEdit != null ? false : true,
            validator: validation,
            inputFormatters: textInputType == TextInputType.number
                ? [FilteringTextInputFormatter.digitsOnly]
                : null,
            controller: controller,
            decoration: InputDecoration(hintText: hintText),
            maxLength: limitCharacter,
            keyboardType:
                textInputType != null ? textInputType : TextInputType.text,
          )
        ],
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('title', title));
    properties.add(StringProperty('hintText', hintText));
    properties.add(
        DiagnosticsProperty<TextEditingController>('controller', controller));
    properties.add(DiagnosticsProperty('validation', validation));
    properties.add(
        DiagnosticsProperty<TextInputType>('textInputType', textInputType));
    properties.add(DiagnosticsProperty<TextStyle>('textStyle', textStyle));
    properties.add(DiagnosticsProperty<bool>('isEdit', isEdit));
    properties.add(IntProperty('limitCharacter', limitCharacter));
  }
}
