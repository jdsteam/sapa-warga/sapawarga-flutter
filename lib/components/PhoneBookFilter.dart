import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';

typedef void PhoneBookFilterCallback(String result);

class PhoneBookFilter extends StatefulWidget {
  final String selectedFilter;
  final PhoneBookFilterCallback onSubmit;

  PhoneBookFilter({Key key, this.selectedFilter, this.onSubmit})
      : super(key: key);

  @override
  _PhoneBookFilterState createState() => _PhoneBookFilterState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('selectedFilter', selectedFilter));
    properties.add(
        ObjectFlagProperty<PhoneBookFilterCallback>.has('onSubmit', onSubmit));
  }
}

class _PhoneBookFilterState extends State<PhoneBookFilter> {
  String _tempSelected = "";

  @override
  void initState() {
    _tempSelected = widget.selectedFilter;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Dimens.padding),
        ),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        children: <Widget>[_dialogContent(context)]);
  }

  _dialogContent(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(Dimens.padding),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        // To make the card compact
        children: <Widget>[
          Center(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                'Filter',
                style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w700,
                    color: Colors.black),
              ),
            ),
          ),
          Divider(height: 1.0, color: Colors.black),
          const SizedBox(height: 16.0),
          Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Radio(
                    groupValue: _tempSelected,
                    onChanged: (value) =>
                        setState(() => this._tempSelected = value),
                    value: "1",
                  ),
                  Text(Dictionary.city)
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Radio(
                    groupValue: _tempSelected,
                    onChanged: (value) =>
                        setState(() => this._tempSelected = value),
                    value: "2",
                  ),
                  Text(Dictionary.subDistrict)
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Radio(
                    groupValue: _tempSelected,
                    onChanged: (value) =>
                        setState(() => this._tempSelected = value),
                    value: "3",
                  ),
                  Text(Dictionary.village)
                ],
              ),
            ],
          ),
          const SizedBox(height: 24.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pop(); // To close the dialog
                },
                child: Text(
                  Dictionary.cancel.toUpperCase(),
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.blue),
                ),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  widget.onSubmit(_tempSelected);
                },
                child: Text(
                  Dictionary.ok.toUpperCase(),
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.blue),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
