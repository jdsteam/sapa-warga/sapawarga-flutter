import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pedantic/pedantic.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sapawarga/blocs/geocoder/Bloc.dart';
import 'package:sapawarga/components/DialogRequestPermission.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/repositories/GeocoderRepository.dart';

class LocationPicker extends StatefulWidget {
  LocationPicker({Key key}) : super(key: key);

  @override
  _LocationPickerState createState() => _LocationPickerState();
}

class _LocationPickerState extends State<LocationPicker> {
  final Completer<GoogleMapController> _controller = Completer();
  GeocoderBloc _geocoderBloc;

  Map<String, Marker> markers = <String, Marker>{};
  LatLng _currentPosition = LatLng(-6.902735, 107.618782);

  String primaryAddress = '';
  String secondaryAddress = '';

  @override
  void initState() {
    _checkPermission();
    markers["baru"] = Marker(
      markerId: MarkerId("Bandung"),
      position: _currentPosition,
      icon: BitmapDescriptor.defaultMarker,
    );
    super.initState();
  }

  Future<void> _onMapCreated(GoogleMapController controller) async {
    await controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: _currentPosition, zoom: 15.5),
      ),
    );

    _controller.complete(controller);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        elevation: 0,
        title: Text('Profile', style: TextStyle(fontSize: 15)),
      ),
      body: BlocProvider<GeocoderBloc>(
        create: (context) => _geocoderBloc =
            GeocoderBloc(geocoderRepository: GeocoderRepository()),
        child: Stack(
          children: <Widget>[
            GoogleMap(
              mapType: MapType.normal,
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                target: _currentPosition,
                zoom: 16.0,
              ),
              markers: Set<Marker>.of(markers.values),
              myLocationButtonEnabled: true,
              myLocationEnabled: true,
              onCameraMove: _updatePosition,
              onCameraIdle: _cameraIdle,
            ),
            Positioned(
              bottom: 0.0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Card(
                  margin: const EdgeInsets.all(0.0),
                  elevation: 5.0,
                  child: Container(
                    padding: const EdgeInsets.only(
                        left: Dimens.padding, right: Dimens.padding, top: 10.0),
                    child: BlocBuilder<GeocoderBloc, GeocoderState>(
                      builder: (context, state) => Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          state is GeocoderLoading
                              ? CupertinoActivityIndicator()
                              : state is GeocoderLoaded
                                  ? _buildLocation(state)
                                  : state is GeocoderFailure
                                      ? Container(child: Text(state.error))
                                      : Container(),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            margin:
                                const EdgeInsets.only(bottom: 20.0, top: 10.0),
                            child: RaisedButton(
                              child: Text('Set Lokasi'),
                              color: clr.Colors.blue,
                              textColor: Colors.white,
                              onPressed: state is GeocoderLoading
                                  ? null
                                  : state is GeocoderLoaded
                                      ? () {
                                          Navigator.pop(
                                              context, _currentPosition);
                                        }
                                      : null,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _buildLocation(GeocoderLoaded state) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(state.primaryAddress,
            style: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w600,
                fontFamily: FontsFamily.sourceSansPro)),
        Text(state.secondaryAddress,
            style: TextStyle(
                fontSize: 12.0,
                fontFamily: FontsFamily.sourceSansPro,
                color: Colors.grey[600])),
      ],
    );
  }

  void _updatePosition(CameraPosition _position) {
    Position newMarkerPosition = Position(
        latitude: _position.target.latitude,
        longitude: _position.target.longitude);
    Marker marker = markers["baru"];

    setState(() {
      markers["baru"] = marker.copyWith(
          positionParam:
              LatLng(newMarkerPosition.latitude, newMarkerPosition.longitude));
      _currentPosition =
          LatLng(newMarkerPosition.latitude, newMarkerPosition.longitude);
    });
  }

  _cameraIdle() {
    _geocoderBloc.add(GeocoderGetLocation(coordinate: _currentPosition));
  }

  Future<void> _checkPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);

    if (permission == PermissionStatus.granted) {
      final GoogleMapController controller = await _controller.future;
      final Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);

      LatLng currentLocation = LatLng(position.latitude, position.longitude);

      await controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: currentLocation, zoom: 15.5),
        ),
      );
    } else {
      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/map_pin.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionLocationMap,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.location]).then(_onStatusRequested);
                },
              )));
    }
  }

  Future<void> _onStatusRequested(
      Map<PermissionGroup, PermissionStatus> statuses) async {
    final status = statuses[PermissionGroup.location];
    if (status == PermissionStatus.granted) {
      final GoogleMapController controller = await _controller.future;
      final Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);

      LatLng currentLocation = LatLng(position.latitude, position.longitude);

      await controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: currentLocation, zoom: 15.5),
        ),
      );

      _geocoderBloc.add(GeocoderGetLocation(coordinate: currentLocation));
    }
  }

  @override
  void dispose() {
    _geocoderBloc.close();
    super.dispose();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('primaryAddress', primaryAddress));
    properties.add(StringProperty('secondaryAddress', secondaryAddress));
    properties
        .add(DiagnosticsProperty<Map<String, Marker>>('markers', markers));
  }
}
