import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/UrlThirdParty.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:url_launcher/url_launcher.dart';

class DialogUpdateApp extends StatefulWidget {
  DialogUpdateApp({Key key}) : super(key: key);

  @override
  _DialogUpdateAppState createState() => _DialogUpdateAppState();
}

class _DialogUpdateAppState extends State<DialogUpdateApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Dimens.dialogRadius)),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: _dialogContent(context),
    );
  }

  _dialogContent(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(Dimens.dialogRadius),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min, // To make the card compact
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: 170.0,
                decoration: BoxDecoration(
                    color: Colors.blue[300],
                    borderRadius: BorderRadius.vertical(
                        top: Radius.circular(Dimens.dialogRadius))),
              ),
              Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.only(top: 30.0),
                  child: Image.asset(
                    '${Environment.imageAssets}popup-update.png',
                    height: 250.0,
                  )),
            ],
          ),
          Text(
            Dictionary.updateAppAvailable,
            style: TextStyle(fontSize: 15.0, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: RaisedButton(
              color: Colors.blue[300],
              textColor: Colors.white,
              child: Container(
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                height: 40.0,
                child: Text(
                  Dictionary.update,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              onPressed: () {
                _launchURL();
              },
            ),
          ),
        ],
      ),
    );
  }

  _launchURL() async {
    if (await canLaunch(UrlThirdParty.urlPlayStore)) {
      await launch(UrlThirdParty.urlPlayStore);
    } else {
      if (await canLaunch(UrlThirdParty.urlPlayStoreAlt)) {
        await launch(UrlThirdParty.urlPlayStoreAlt);
      } else {
        throw 'Could not launch ${UrlThirdParty.urlPlayStoreAlt}';
      }
    }
  }
}
