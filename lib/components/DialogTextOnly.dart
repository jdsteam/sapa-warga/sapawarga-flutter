import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;

class DialogTextOnly extends StatelessWidget {
  final String title, description, buttonText;
  final GestureTapCallback onOkPressed;
  final CrossAxisAlignment crossAxisAlignment;
  final AlignmentGeometry buttonAlignment;
  final Color descriptionColor, titleColor;
  final TextAlign textAlign;
  final bool isRaisedButton;

  DialogTextOnly(
      {Key key,
      this.title,
      this.titleColor,
      @required this.description,
      this.descriptionColor,
      @required this.buttonText,
      @required this.onOkPressed,
      this.textAlign,
      this.crossAxisAlignment,
      this.buttonAlignment,
      this.isRaisedButton = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: _dialogContent(context),
    );
  }

  _dialogContent(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
      child: Container(
        padding: EdgeInsets.all(Dimens.padding),
        decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(Dimens.padding),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 10.0),
            ),
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min, // To make the card compact
          crossAxisAlignment: crossAxisAlignment != null
              ? crossAxisAlignment
              : CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 14),
            title != null
                ? Text(
                    title,
                    textAlign: textAlign != null ? textAlign : TextAlign.left,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: FontsFamily.roboto,
                        color: titleColor),
                  )
                : Text(""),
            SizedBox(height: 14),
            Text(
              description != null ? description : "",
              textAlign: textAlign != null ? textAlign : TextAlign.left,
              style: TextStyle(
                  fontSize: 14.0,
                  color: descriptionColor,
                  fontFamily: FontsFamily.roboto),
            ),
            SizedBox(height: 24.0),
            !isRaisedButton
                ? Container(
                    alignment: buttonAlignment,
                    child: FlatButton(
                      onPressed: onOkPressed,
                      child: Text(
                        buttonText,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.blue),
                      ),
                    ),
                  )
                : RaisedButton(
                    color: clr.Colors.darkblue,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    child: Container(
                      alignment: Alignment.center,
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.symmetric(vertical: 13),
                      child: Text(
                        buttonText,
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: FontsFamily.roboto),
                      ),
                    ),
                    onPressed: onOkPressed)
          ],
        ),
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('description', description));
    properties.add(StringProperty('title', title));
    properties.add(StringProperty('buttonText', buttonText));
    properties.add(
        ObjectFlagProperty<GestureTapCallback>.has('onOkPressed', onOkPressed));
    properties.add(EnumProperty<CrossAxisAlignment>(
        'crossAxisAlignment', crossAxisAlignment));
    properties.add(DiagnosticsProperty<AlignmentGeometry>(
        'buttonAlignment', buttonAlignment));
    properties.add(ColorProperty('descriptionColor', descriptionColor));
    properties.add(ColorProperty('titleColor', titleColor));
    properties.add(EnumProperty<TextAlign>('textAlign', textAlign));
    properties.add(DiagnosticsProperty<bool>('isRaisedButton', isRaisedButton));
  }
}
