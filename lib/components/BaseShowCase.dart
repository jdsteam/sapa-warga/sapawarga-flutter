import 'package:flutter/material.dart';
import 'package:sapawarga/components/BubbleCustom.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:showcaseview/showcase.dart';

// ignore: must_be_immutable
class BaseShowCase {
  static Showcase showcaseWidget(
      {@required GlobalKey key,
      @required BuildContext context,
      @required List<Widget> widgets,
      @required Widget child,
      @required GestureTapCallback onOkTap,
      @required NipLocation nipLocation,
        String buttonText,
        double nipPaddingTopLeft = 0.0,
        double nipPaddingTopRight = 0.0,
        EdgeInsetsGeometry margin = const EdgeInsets.only(top: 10.0),
      }) {
    return Showcase.withWidget(
      width: MediaQuery.of(context).size.width,
      height: 100.0,
      key: key,
      container: Container(
        margin: margin,
        child: BubbleCustom(
          nipLocation: nipLocation,
          nipTopLeftPadding: nipPaddingTopLeft,
          nipTopRightPadding: nipPaddingTopRight,
          color: Colors.white,
          child: Container(
            width: MediaQuery.of(context).size.width - 50,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Column(
                  children: widgets,
                ),
                Container(
                    alignment: Alignment.topRight,
                    margin: EdgeInsets.only(top: 10.0, right: 10),
                    child: RoundedButton(
                        minWidth: 5,
                        height: 30,
                        color: clr.Colors.blue,
                        title: buttonText != null ? buttonText : Dictionary.next,
                        onPressed: onOkTap,
                        textStyle: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white)))
              ],
            ),
          ),
        ),
      ),
      child: child,
    );
  }
}
