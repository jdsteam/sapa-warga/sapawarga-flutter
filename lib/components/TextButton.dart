import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class TextButtonOnly extends StatelessWidget {
  final String title;
  final TextStyle textStyle;
  final GestureTapCallback onTap;
  final EdgeInsets padding;

  TextButtonOnly(
      {Key key,
      @required this.title,
      @required this.onTap,
      this.textStyle,
      this.padding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding != null ? padding : const EdgeInsets.all(0.0),
      child: GestureDetector(
        child: Text(
          title,
          style: textStyle,
        ),
        onTap: onTap,
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('title', title));
    properties.add(DiagnosticsProperty<TextStyle>('textStyle', textStyle));
    properties.add(ObjectFlagProperty<GestureTapCallback>.has('onTap', onTap));
    properties.add(DiagnosticsProperty<EdgeInsets>('padding', padding));
  }
}
