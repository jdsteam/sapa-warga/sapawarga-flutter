import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;

class CustomAppBar {
  AppBar DefaultAppBar(
      {Widget leading, @required String title, List<Widget> actions}) {
    return AppBar(
      leading: leading,
      title: Text(title,
          style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              fontFamily: FontsFamily.intro),
          maxLines: 1,
          overflow: TextOverflow.ellipsis),
      titleSpacing: 0.0,
      actions: actions,
    );
  }

  static AppBar SearchAppBar(
      BuildContext context, TextEditingController textController) {
    return AppBar(
      title: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(5.0)),
        child: Wrap(children: [
          Container(
              width: 25.0,
              height: 30.0,
              child: Icon(
                Icons.search,
                color: Colors.grey,
                size: 20.0,
              )),
          Container(
            width: MediaQuery.of(context).size.width - 100,
            height: 30.0,
            child: TextField(
              controller: textController,
              autofocus: true,
              maxLines: 1,
              minLines: 1,
              maxLength: 255,
              style: TextStyle(color: Colors.black, fontSize: 15.0, height: 2.0),
              decoration: InputDecoration(
                isDense: true,
                hintText: Dictionary.hintSearch,
                counterText: "",
                border: InputBorder.none,
                contentPadding: EdgeInsets.all(5.0),
              ),
            ),
          ),
        ]),
      ),
      titleSpacing: 0.0,
    );
  }

  static Widget buildSearchField(TextEditingController searchController,
      String hintText, ValueChanged<String> onChanged,
      {EdgeInsetsGeometry margin}) {
    return Container(
      margin: margin ?? EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20),
      height: 40.0,
      decoration: BoxDecoration(
          color: Colors.grey[100],
          shape: BoxShape.rectangle,
          border: Border.all(color: Colors.grey[400]),
          borderRadius: BorderRadius.circular(Dimens.borderRadius)),
      child: TextField(
        controller: searchController,
        autofocus: false,
        decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.search,
              color: clr.Colors.darkGrey,
            ),
            suffixIcon: searchController.text.isNotEmpty
                ? IconButton(
              icon: Icon(Icons.close),
              color: clr.Colors.darkGrey,
              onPressed: () {
                searchController.text = '';
              },
            )
                : null,
            hintText: hintText,
            border: InputBorder.none,
            hintStyle: TextStyle(
                color: clr.Colors.darkGrey,
                fontFamily: FontsFamily.roboto,
                fontSize: 16,),
            contentPadding:
            EdgeInsets.symmetric(horizontal: 15.0, vertical: 12.0)),
        style: TextStyle(color: Colors.black, fontSize: 16.0, fontFamily: FontsFamily.roboto),
        onChanged: onChanged,
      ),
    );
  }

}
