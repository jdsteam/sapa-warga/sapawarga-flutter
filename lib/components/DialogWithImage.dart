import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/FontsFamily.dart';
import 'RoundedButton.dart';

class DialogWithImage extends StatelessWidget {
  final String title, description, locationImage, buttonName;
  final GestureTapCallback onNextPressed;
  final double heightImage;

  DialogWithImage(
      {Key key,
      this.title,
      this.description,
      this.locationImage,
      this.buttonName,
      this.onNextPressed,
      this.heightImage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: _dialogContent(context),
    );
  }

  _dialogContent(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
      child: Stack(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.fromLTRB(
                Dimens.padding, 20.0, Dimens.padding, 20.0),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(Dimens.padding),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 10.0,
                  offset: const Offset(0.0, 10.0),
                ),
              ],
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image.asset(locationImage, height: heightImage ?? 250),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  title != null ? title : '',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600,
                      fontFamily: FontsFamily.roboto),
                ),
                const SizedBox(height: Dimens.padding),
                Text(
                  description != null ? description : '',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: FontsFamily.roboto,
                      color: clr.Colors.darkGrey),
                ),
                const SizedBox(height: 25.0),
                RoundedButton(
                  title: buttonName,
                  borderRadius: BorderRadius.circular(8.0),
                  color: clr.Colors.darkblue,
                  textStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 14.0,
                      fontFamily: FontsFamily.roboto),
                  onPressed: onNextPressed,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('description', description));
    properties.add(StringProperty('title', title));
    properties.add(StringProperty('buttonName', buttonName));
    properties.add(StringProperty('locationImage', locationImage));
    properties.add(ObjectFlagProperty<GestureTapCallback>.has(
        'onNextPressed', onNextPressed));
    properties.add(DoubleProperty('heightImage', heightImage));
  }
}
