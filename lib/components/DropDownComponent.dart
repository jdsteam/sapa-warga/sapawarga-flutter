import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/components/custom_dropdown.dart' as custom;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';

class DropDownComponent extends StatefulWidget {
  final String title;
  final String hintText;
  final List items;
  final TextEditingController controller;
  final bool isEmpty;
  final Function() onChanged;

  DropDownComponent(
      {Key key,
      this.title,
      this.hintText,
      this.items = const [],
      this.controller,
      this.isEmpty,
      this.onChanged})
      : super(key: key);

  @override
  _DropDownComponentState createState() => _DropDownComponentState();
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('title', title));
    properties.add(StringProperty('hintText', hintText));
    properties.add(IterableProperty<dynamic>('items', items));
    properties.add(
        DiagnosticsProperty<TextEditingController>('controller', controller));
    properties.add(DiagnosticsProperty<bool>('isEmpty', isEmpty));
    properties.add(DiagnosticsProperty<Function()>('onChanged', onChanged));
  }
}

class _DropDownComponentState extends State<DropDownComponent> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                widget.title,
                style: TextStyle(
                    fontSize: 12.0,
                    color: widget.items.isNotEmpty
                        ? clr.Colors.veryDarkGrey
                        : clr.Colors.disableText,
                    fontFamily: FontsFamily.roboto,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                Dictionary.requiredForm,
                style: TextStyle(
                    fontSize: 10.0,
                    color: clr.Colors.darkblue,
                    fontFamily: FontsFamily.roboto,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 60,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: Colors.grey[100],
                borderRadius: BorderRadius.circular(8),
                border: Border.all(
                    color: widget.isEmpty ? Colors.red : Colors.grey[400],
                    width: 1.5)),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: custom.DropdownButton<dynamic>(
                underline: const SizedBox(),
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  color: widget.items.isNotEmpty
                      ? clr.Colors.netralGrey
                      : clr.Colors.disableText,
                  size: 30,
                ),
                isExpanded: true,
                hint: Text(
                  widget.hintText,
                  style: TextStyle(
                      color: widget.items.isNotEmpty
                          ? clr.Colors.netralGrey
                          : clr.Colors.disableText,
                      fontFamily: FontsFamily.roboto,
                      fontSize: 14),
                ),
                items: widget.items.map(
                  (item) {
                    String name = '';
                    try {
                      name = item.name;
                    } catch (_) {
                      name = item.title;
                    }

                    return custom.DropdownMenuItem(
                      child: Text(
                        name,
                        style: TextStyle(
                            fontFamily: FontsFamily.roboto,
                            fontSize: 14,
                            color: clr.Colors.veryDarkGrey),
                      ),
                      value: item.id.toString(),
                    );
                  },
                ).toList(),
                onChanged: (value) {
                  FocusScope.of(context).requestFocus(FocusNode());
                  setState(
                    () {
                      widget.controller.text = value;
                    },
                  );
                  if (widget.onChanged != null) {
                    widget.onChanged();
                  }
                },
                value: widget.controller.text == ''
                    ? null
                    : widget.controller.text,
              ),
            ),
          ),
          widget.isEmpty
              ? const SizedBox(
                  height: 10,
                )
              : Container(),
          widget.isEmpty
              ? Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Text(
                    widget.title + Dictionary.pleaseCompleteAllField,
                    style: TextStyle(color: Colors.red, fontSize: 12),
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}
