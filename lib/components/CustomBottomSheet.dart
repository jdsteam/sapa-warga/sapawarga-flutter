import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/FontsFamily.dart';

import 'RoundedButton.dart';

/// Shows a success modal material design bottom sheet.
void showCustomBottomSheet(
    {@required BuildContext context,
    @required GestureTapCallback onPressed,
    @required String image,
    @required String title,
    @required String message,
    @required String buttonText,
    bool isDismissible = true}) {
  showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: const Radius.circular(8.0),
          topRight: const Radius.circular(8.0),
        ),
      ),
      isDismissible: isDismissible,
      builder: (context) {
        return Container(
          margin: const EdgeInsets.all(Dimens.padding),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.only(bottom: 15),
                child: Image.asset(
                  image,
                  fit: BoxFit.fitWidth,
                  width: 150,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: FontsFamily.roboto,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                message,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: FontsFamily.roboto,
                    fontSize: 14.0,
                    color: Colors.grey[600]),
              ),
              const SizedBox(height: 24.0),
              RoundedButton(
                  title: buttonText,
                  textStyle: TextStyle(
                      fontFamily: FontsFamily.roboto,
                      fontSize: 14.0,
                      color: Colors.white),
                  color: clr.Colors.darkblue,
                  onPressed: onPressed)
            ],
          ),
        );
      });
}
