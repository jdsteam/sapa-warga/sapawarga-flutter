import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final double minWidth, height;
  final String title;
  final GestureTapCallback onPressed;
  final Color color;
  final BorderRadiusGeometry borderRadius;
  final TextStyle textStyle;

  RoundedButton(
      {Key key,
      @required this.title,
      @required this.onPressed,
      this.minWidth,
      this.height = 45.0,
      this.borderRadius,
      this.color,
      this.textStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      minWidth: minWidth != null ? minWidth : MediaQuery.of(context).size.width,
      height: height,
      child: RaisedButton(
          color: color,
          shape: RoundedRectangleBorder(
              borderRadius: borderRadius != null
                  ? borderRadius
                  : BorderRadius.circular(8.0)),
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 13),
            child: Text(title, style: textStyle),
          ),
          onPressed: onPressed),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DoubleProperty('minWidth', minWidth));
    properties.add(DoubleProperty('height', height));
    properties.add(StringProperty('title', title));
    properties.add(
        ObjectFlagProperty<GestureTapCallback>.has('onPressed', onPressed));
    properties.add(ColorProperty('color', color));
    properties.add(DiagnosticsProperty<BorderRadiusGeometry>(
        'borderRadius', borderRadius));
    properties.add(DiagnosticsProperty<TextStyle>('textStyle', textStyle));
  }
}
