import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/FontsFamily.dart';

class PasswordFormField extends StatefulWidget {
  final TextEditingController controller;
  final bool showIcon;
  final bool showLabel;
  final String labelText;
  final String hintText;
  final String errorText;
  final String Function(String) validator;
  final int limitCharacter;

  PasswordFormField(
      {Key key,
      this.labelText,
      this.showIcon = true,
      this.showLabel = true,
      this.hintText,
      this.errorText,
      this.controller,
      this.validator,
      this.limitCharacter})
      : super(key: key);

  _PasswordFormFieldState createState() => _PasswordFormFieldState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(
        DiagnosticsProperty<TextEditingController>('controller', controller));
    properties.add(DiagnosticsProperty<bool>('showIcon', showIcon));
    properties.add(StringProperty('labelText', labelText));
    properties.add(StringProperty('hintText', hintText));
    properties.add(StringProperty('errorText', errorText));
    properties.add(DiagnosticsProperty<String Function(String p1)>(
        'validator', validator));
    properties.add(DiagnosticsProperty<bool>('showLabel', showLabel));
    properties.add(IntProperty('limitCharacter', limitCharacter));
  }
}

class _PasswordFormFieldState extends State<PasswordFormField> {
  TextEditingController get _controller => widget.controller;

  String get _labelText => widget.labelText;

  bool get _showLabel => widget.showLabel;

  String get _hintText => widget.hintText;

  String get _errorText => widget.errorText;

  String Function(String) get _validator => widget.validator;

  bool get _showIcon => widget.showIcon;

  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        TextFormField(
            controller: _controller,
            obscureText: _obscureText,
            maxLength: widget.limitCharacter,
            decoration: InputDecoration(
                errorMaxLines: 2,
                fillColor: Colors.grey[100],
                hintText: _hintText,
                filled: true,
                hintStyle: TextStyle(
                    color: clr.Colors.netralGrey,
                    fontFamily: FontsFamily.roboto,
                    fontSize: 14),
                icon: _showIcon ? Icon(Icons.lock) : null,
                contentPadding: const EdgeInsets.only(
                    left: 12.0, right: 15.0, top: 14.0, bottom: 14.0),
                labelText: _showLabel == true ? _labelText : null,
                errorText: _errorText,
                errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(color: Colors.red, width: 1.5)),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide:
                        BorderSide(color: Colors.grey[400], width: 1.5)),
                disabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide:
                        BorderSide(color: Colors.grey[400], width: 1.5)),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide:
                        BorderSide(color: Colors.grey[400], width: 1.5))),
            validator: _validator),
        Positioned(
          top: 12.0,
          right: 12.0,
          child: GestureDetector(
            child: SizedBox(
              height: 24.0,
              child: Icon(
                _obscureText ? Icons.visibility_off : Icons.visibility,
                color: Colors.grey[700],
              ),
            ),
            onTap: () {
              setState(() {
                _obscureText = !_obscureText;
              });
            },
          ),
        )
      ],
    );
  }
}
