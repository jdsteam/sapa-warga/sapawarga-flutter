import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:sapawarga/models/RWActivityModel.dart';

class HeroImagePreview extends StatefulWidget {
  final String heroTag;
  final String imageUrl;
  final ItemRWActivity galleryItems;
  final int initialIndex;
  final PageController pageController;

  HeroImagePreview(this.heroTag,
      {Key key, this.imageUrl, this.galleryItems, this.initialIndex})
      : pageController = PageController(initialPage: initialIndex ?? 0),
        super(key: key);

  @override
  _HeroImagePreviewState createState() => _HeroImagePreviewState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('heroTag', heroTag));
    properties.add(StringProperty('imageUrl', imageUrl));
    properties
        .add(DiagnosticsProperty<ItemRWActivity>('galleryItems', galleryItems));
    properties.add(IntProperty('initialIndex', initialIndex));
    properties.add(
        DiagnosticsProperty<PageController>('pageController', pageController));
  }
}

class _HeroImagePreviewState extends State<HeroImagePreview> {
  int currentIndex;

  @override
  initState() {
    if (widget.initialIndex != null) {
      currentIndex = widget.initialIndex;
    }

    super.initState();
  }

  void onPageChanged(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: widget.imageUrl == null
          ? PhotoViewGallery.builder(
              scrollPhysics: const BouncingScrollPhysics(),
              builder: (BuildContext context, int index) {
                return PhotoViewGalleryPageOptions(
                  minScale: 0.3,
                  imageProvider:
                      NetworkImage(widget.galleryItems.images[index].url),
                  heroAttributes: PhotoViewHeroAttributes(tag: widget.heroTag),
                  onTapUp: (context, tapDetail, controller) {
                    Navigator.of(context).pop();
                  },
                );
              },
              itemCount: widget.galleryItems.images.length,
              backgroundDecoration: BoxDecoration(color: Colors.white),
              pageController: widget.pageController,
              onPageChanged: onPageChanged,
              scrollDirection: Axis.horizontal,
            )
          : PhotoView(
              imageProvider: NetworkImage(widget.imageUrl),
              minScale: 0.3,
              backgroundDecoration: BoxDecoration(color: Colors.white),
              heroAttributes: PhotoViewHeroAttributes(tag: widget.heroTag),
              onTapUp: (context, tapDetail, controller) {
                Navigator.of(context).pop();
              },
            ),
    );
  }

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    super.dispose();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(IntProperty('currentIndex', currentIndex));
  }
}
