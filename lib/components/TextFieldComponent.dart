import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';

class TextFieldComponent extends StatefulWidget {
  ///Component for build custom textfield component

  final String title;
  final TextEditingController controller;
  final String hintText;
  final TextInputType textInputType;
  final TextStyle textStyle;
  final bool isEdit;
  final int maxLines;
  final FocusNode focusNode;
  final bool isPassword;
  final dynamic validation;
  final int limitCharacter;

  TextFieldComponent(
      {Key key,
      this.title,
      this.controller,
      this.validation,
      this.hintText,
      this.textInputType,
      this.textStyle,
      this.isEdit,
      this.maxLines,
      this.focusNode,
      this.isPassword = false,
      this.limitCharacter})
      : super(key: key);

  @override
  _TextFieldComponentState createState() => _TextFieldComponentState();
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('title', title));
    properties.add(
        DiagnosticsProperty<TextEditingController>('controller', controller));
    properties.add(StringProperty('hintText', hintText));
    properties.add(
        DiagnosticsProperty<TextInputType>('textInputType', textInputType));
    properties.add(DiagnosticsProperty<TextStyle>('textStyle', textStyle));
    properties.add(DiagnosticsProperty<bool>('isEdit', isEdit));
    properties.add(IntProperty('maxLines', maxLines));
    properties.add(DiagnosticsProperty<FocusNode>('focusNode', focusNode));
    properties.add(DiagnosticsProperty<bool>('isPassword', isPassword));
    properties.add(DiagnosticsProperty('validation', validation));
    properties.add(IntProperty('limitCharacter', limitCharacter));
  }
}

class _TextFieldComponentState extends State<TextFieldComponent> {
  bool _isObscure = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const SizedBox(
            height: 10,
          ),
          Row(
            children: <Widget>[
              Text(
                widget.title,
                style: TextStyle(
                    fontSize: 12.0,
                    color: clr.Colors.veryDarkGrey,
                    fontFamily: FontsFamily.roboto,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                Dictionary.requiredForm,
                style: TextStyle(
                    fontSize: 10.0,
                    color: clr.Colors.darkblue,
                    fontFamily: FontsFamily.roboto,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          TextFormField(
            maxLines: widget.maxLines != null ? widget.maxLines : 1,
            style: widget.isEdit
                ? TextStyle(
                    color: Colors.black,
                    fontFamily: FontsFamily.roboto,
                    fontSize: 14)
                : TextStyle(
                    color: clr.Colors.disableText,
                    fontFamily: FontsFamily.roboto,
                    fontSize: 14),
            enabled: widget.isEdit,
            focusNode: widget.focusNode,
            validator: widget.validation,
            maxLength: widget.limitCharacter,
            textCapitalization: TextCapitalization.words,
            controller: widget.controller,
            inputFormatters: widget.textInputType == TextInputType.number
                ? [FilteringTextInputFormatter.digitsOnly]
                : null,
            decoration: InputDecoration(
              suffixIcon: widget.isPassword
                  ? IconButton(
                      icon: Icon(
                        _isObscure ? Icons.visibility_off : Icons.visibility,
                        color: clr.Colors.netralGrey,
                      ),
                      onPressed: () {
                        setState(() {
                          _isObscure = !_isObscure;
                        });
                      },
                    )
                  : null,
              fillColor: Colors.grey[100],
              errorMaxLines: 2,
              filled: true,
              hintText: widget.hintText,
              hintStyle: TextStyle(
                  color: clr.Colors.netralGrey,
                  fontFamily: FontsFamily.roboto,
                  fontSize: 12),
              errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Colors.red, width: 1.5)),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Colors.grey[400], width: 1.5)),
              disabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Colors.grey[400], width: 1.5)),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Colors.grey[400], width: 1.5)),
            ),
            obscureText: widget.isPassword ? _isObscure : false,
            keyboardType: widget.textInputType != null
                ? widget.textInputType
                : widget.isPassword
                    ? TextInputType.visiblePassword
                    : TextInputType.text,
          ),
        ],
      ),
    );
  }
}
