import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/models/RadioModel.dart';

class RadioItem extends StatelessWidget {
  final RadioModel item;

  RadioItem({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            height: 30.0,
            child: Center(
              child: Text(item.buttonText,
                  style: TextStyle(
                    fontFamily: FontsFamily.sourceSansPro,
                    fontSize: 14.0,
                    color: item.isSelected ? Colors.white : Colors.white,
                  )),
            ),
            decoration: BoxDecoration(
              color: item.isSelected ? clr.Colors.blue : Colors.grey,
              border: Border.all(
                  width: 1.0,
                  color: item.isSelected ? clr.Colors.blue : Colors.grey),
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
            ),
          ),
          item.text != null
              ? Container(
                  margin: const EdgeInsets.only(left: 10.0),
                  child: Text(item.text),
                )
              : Container()
        ],
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<RadioModel>('item', item));
  }
}
