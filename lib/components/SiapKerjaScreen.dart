import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/siap_kerja/generate_token_totp/Bloc.dart';
import 'package:sapawarga/blocs/siap_kerja/get_access_token/Bloc.dart';
import 'package:sapawarga/blocs/siap_kerja/send_profile_user/Bloc.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/SiapKerjaRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:webview_flutter/webview_flutter.dart';

class SiapKerjaScreen extends StatefulWidget {
  final String url;
  final UserInfoModel userInfoModel;
  final Map<String, dynamic> secretEnvSiapKerja;

  SiapKerjaScreen(
      {Key key,
      @required this.url,
      this.userInfoModel,
      this.secretEnvSiapKerja})
      : super(key: key);

  @override
  _SiapKerjaScreenState createState() => _SiapKerjaScreenState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('url', url));
    properties.add(
        DiagnosticsProperty<UserInfoModel>('userInfoModel', userInfoModel));
    properties.add(DiagnosticsProperty<Map<String, dynamic>>(
        'secretEnvSiapKerja', secretEnvSiapKerja));
  }
}

class _SiapKerjaScreenState extends State<SiapKerjaScreen> {
  bool isLoading = false;
  bool showLoading = false;
  GenerateTotpBloc _generateTotpBloc;
  AccessTokenBloc _accessTokenBloc;
  SendProfileUserBloc _sendProfileUserBloc;
  String accessToken = '';
  WebViewController _webViewController;

  @override
  void initState() {
    if (widget.userInfoModel != null) {
      _checkSiapKerjaToken();
    }
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    SystemChannels.textInput.invokeMethod('TextInput.hide');

    AnalyticsHelper.setLogEvent(Analytics.EVENT_SIAP_KERJA);

    super.initState();
  }

  _checkSiapKerjaToken() async {
    accessToken = await Preferences.getAccessTokenSiapKerja();
    if (accessToken.isEmpty) {
      _generateTotpBloc
          .add(GenerateTotp(secretEnvSiapKerja: widget.secretEnvSiapKerja));
    } else {
      _sendProfileUserBloc.add(SendProfileUser(
          userInfoModel: widget.userInfoModel,
          secretEnvSiapKerja: widget.secretEnvSiapKerja));
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () => _exitWebView(context),
      child: Scaffold(
        body: MultiBlocProvider(
          providers: [
            BlocProvider<GenerateTotpBloc>(
              create: (BuildContext context) =>
                  _generateTotpBloc = GenerateTotpBloc(
                siapKerjaRepository: SiapKerjaRepository(),
              ),
            ),
            BlocProvider<AccessTokenBloc>(
              create: (BuildContext context) =>
                  _accessTokenBloc = AccessTokenBloc(
                siapKerjaRepository: SiapKerjaRepository(),
              ),
            ),
            BlocProvider<SendProfileUserBloc>(
              create: (BuildContext context) =>
                  _sendProfileUserBloc = SendProfileUserBloc(
                siapKerjaRepository: SiapKerjaRepository(),
              ),
            ),
          ],
          child: MultiBlocListener(
            listeners: [
              BlocListener<GenerateTotpBloc, GenerateTotpState>(
                bloc: _generateTotpBloc,
                listener: (context, state) async {
                  if (state is GenerateTotpLoading) {
                  } else if (state is GenerateTotpSuccess) {
                    _accessTokenBloc.add(GetAccessToken(
                        userInfoModel: widget.userInfoModel,
                        tOtpToken: state.tOtpToken,
                        secretEnvSiapKerja: widget.secretEnvSiapKerja));
                  }
                },
              ),
              BlocListener<SendProfileUserBloc, SendProfileUserState>(
                bloc: _sendProfileUserBloc,
                listener: (context, state) async {
                  if (state is SendProfileUserSuccess) {
                    print(state.isUpdate.toString());
                  }
                },
              ),
              BlocListener<AccessTokenBloc, AccessTokenState>(
                bloc: _accessTokenBloc,
                listener: (context, state) async {
                  if (state is AccessTokenLoading) {
                  } else if (state is AccessTokenSuccess) {
                    setState(() {
                      accessToken = state.accessToken;
                    });
                    _sendProfileUserBloc.add(SendProfileUser(
                        userInfoModel: widget.userInfoModel,
                        secretEnvSiapKerja: widget.secretEnvSiapKerja));
                    await Preferences.setAccessTokenSiapKerja(
                        state.accessToken);
                  }
                },
              ),
            ],
            child: Container(
              width: size.width,
              height: size.height,
              child: Stack(
                children: <Widget>[
                  SafeArea(
                    child: usingWebview(),
                  ),
                  Positioned(
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    child: !isLoading
                        ? Container(
                            color: clr.Colors.blueCardColor,
                            height: size.height,
                            width: size.width,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: 80.0,
                                  child: Image.asset(
                                    'assets/icons/icon.png',
                                    fit: BoxFit.contain,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(Dictionary.waiting,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 16,
                                        height: 1.5,
                                        fontFamily: FontsFamily.roboto))
                              ],
                            ),
                          )
                        : Container(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  usingWebview() {
    return GestureDetector(
      onTap: () {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: WebView(
        initialUrl: widget.url,
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) async {
          setState(() {
            _webViewController = webViewController;
          });
        },
        javascriptChannels: <JavascriptChannel>[
          _toasterJavascriptChannel(context),
        ].toSet(),
        navigationDelegate: (NavigationRequest request) {
          return NavigationDecision.navigate;
        },
        onPageFinished: (String url) async {
          setState(() {
            isLoading = true;
          });
          await _webViewController.evaluateJavascript(
              "window.localStorage.setItem('${widget.secretEnvSiapKerja['projectsId']}_user_token', '7009a624-82be-4cc6-85e0-8f65c5ef9e40');");
          String token = await _webViewController.evaluateJavascript(
              "localStorage.getItem('${widget.secretEnvSiapKerja['projectsId']}_user_token')");
          print(token);
        },
      ),
    );
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: Dictionary.exit,
        onMessageReceived: (JavascriptMessage message) {
          // ignore: deprecated_member_use
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        });
  }

  Future<bool> _exitWebView(BuildContext context) async {
    if (await _webViewController.canGoBack()) {
      await _webViewController.goBack();
      return Future.value(false);
    } else {
      return Future.value(true);
    }
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<bool>('showLoading', showLoading));
    properties.add(DiagnosticsProperty<bool>('isLoading', isLoading));
    properties.add(StringProperty('accessToken', accessToken));
  }
}
