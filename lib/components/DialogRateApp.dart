import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;

class DialogRateApp extends StatelessWidget {
  final String title, description;
  final GestureTapCallback onOkPressed;
  final GestureTapCallback onCancelPressed;

  DialogRateApp(
      {Key key,
      this.title,
      this.description,
      this.onOkPressed,
      this.onCancelPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: _dialogContent(context),
    );
  }

  _dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(right: 10.0, top: 10.0),
          padding: const EdgeInsets.fromLTRB(
              Dimens.padding, 20.0, Dimens.padding, 20.0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(Dimens.padding),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                title != null ? title : '',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600),
              ),
              const SizedBox(height: Dimens.padding),
              Text(
                description != null ? description : '',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600),
              ),
              const SizedBox(height: 40.0),
              ButtonTheme(
                buttonColor: clr.Colors.blue,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: RaisedButton(
                  onPressed: onOkPressed,
                  child: Text(
                    Dictionary.rateIt,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ),
        Positioned(
          right: 0.0,
          child: GestureDetector(
            child: Container(
                width: 30.0,
                height: 30.0,
                decoration: BoxDecoration(
                  color: Colors.grey,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(35.0),
                ),
                child: Icon(
                  Icons.close,
                  color: Colors.white,
                  size: 20.0,
                )),
            onTap: onCancelPressed != null
                ? onCancelPressed
                : () {
                    Navigator.of(context).pop();
                  },
          ),
        )
      ],
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('description', description));
    properties.add(StringProperty('title', title));
    properties.add(
        ObjectFlagProperty<GestureTapCallback>.has('onOkPressed', onOkPressed));
    properties.add(ObjectFlagProperty<GestureTapCallback>.has(
        'onCancelPressed', onCancelPressed));
  }
}
