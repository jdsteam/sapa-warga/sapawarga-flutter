import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:timeline_list/timeline_model.dart';

  TimelineModel timelineConmponent(
      String title, String desc, IconData iconData, Color colors) {
    return TimelineModel(
        Container(
          margin: EdgeInsets.only(top: 25, bottom: 25),
          child: Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                child: Text(
                  title,
                  style:
                      TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
                ),
              ),

              desc != null || desc.isNotEmpty
                  ? Container(
                      width: double.infinity,
                      child: Text(
                        desc,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.grey),
                      ),
                    )
                  : Container()
            ],
          ),
        ),
        position: TimelineItemPosition.left,
        iconBackground: colors,
        icon: Icon(iconData, color: Colors.white, size: 10));
  }
