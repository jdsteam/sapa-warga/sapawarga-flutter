import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';
import 'package:sapawarga/environment/Environment.dart';

class LoadingScreen extends StatefulWidget {
  LoadingScreen({Key key}) : super(key: key);

  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.white));

    return Scaffold(
        backgroundColor: Colors.white,
        body: Center(
            child: Container(
                width: MediaQuery.of(context).size.width / 2,
                child: Lottie.asset(
                  '${Environment.dataAssets}sapawargaloader.json',
                  alignment: Alignment.center,
                  fit: BoxFit.contain,
                ))));
  }

  @override
  void dispose() {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: clr.Colors.blue));

    super.dispose();
  }
}
