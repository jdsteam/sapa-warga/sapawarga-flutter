import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:webview_flutter/webview_flutter.dart' as webView;

class BrowserScreen extends StatefulWidget {
  final String url;
  final bool useInAppWebView;

  BrowserScreen({Key key, @required this.url, this.useInAppWebView = true})
      : super(key: key);

  @override
  _BrowserScreenState createState() => _BrowserScreenState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('url', url));
    properties
        .add(DiagnosticsProperty<bool>('useInAppWebView', useInAppWebView));
  }
}

class _BrowserScreenState extends State<BrowserScreen> {
  webView.WebViewController _webViewController;
  InAppWebViewController _inAppWebViewController;

  double progress = 0.0;
  bool showLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _exitWebView,
      child: Scaffold(
        appBar: AppBar(
          title: Text(Dictionary.appName,
              style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: FontsFamily.intro),
              maxLines: 1,
              overflow: TextOverflow.ellipsis),
        ),
        body: Column(
          children: <Widget>[
            widget.useInAppWebView && (progress != 1.0)
                ? LinearProgressIndicator(value: progress)
                : Container(),
            Expanded(
              child: Container(
                child: widget.useInAppWebView == false
                    ? _buildWebView()
                    : _buildInAppWebView(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildWebView() {
    return Stack(children: <Widget>[
      webView.WebView(
        initialUrl: widget.url,
      )
    ]);
  }

  InAppWebView _buildInAppWebView() {
    return InAppWebView(
      initialUrl: widget.url,
      initialOptions: InAppWebViewGroupOptions(
          crossPlatform: InAppWebViewOptions(debuggingEnabled: true)),
      onWebViewCreated: (InAppWebViewController controller) {
        _inAppWebViewController = controller;
      },
      onProgressChanged: (InAppWebViewController controller, int progress) {
        setState(() {
          this.progress = progress / 100;
        });
      },
    );
  }

  Future<bool> _exitWebView() async {
    if (widget.useInAppWebView) {
      if (await _inAppWebViewController.canGoBack()) {
        await _inAppWebViewController.goBack();
        return Future.value(false);
      } else {
        return Future.value(true);
      }
    } else {
      if (await _webViewController.canGoBack()) {
        await _webViewController.goBack();
        return Future.value(false);
      } else {
        return Future.value(true);
      }
    }
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DoubleProperty('progress', progress));
    properties.add(DiagnosticsProperty<bool>('showLoading', showLoading));
  }
}
