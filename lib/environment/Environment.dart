class Environment {
  static String imageAssets = 'assets/images/';
  static String logoAssets = 'assets/logo/';
  static String iconAssets = 'assets/icons/';
  static String flareAssets = 'assets/flares/';
  static String dataAssets = 'assets/data/';

  static Map<String, String> headerPost = {};

  static Duration requestTimeout = Duration(minutes: 1);

  // production
  static String apiProd = 'https://sapawarga.jabarprov.go.id/api/v1';
  static String apiProdStorage =
      'https://sapawarga.jabarprov.go.id/api/storage'; //
  static String databaseNameProd = 'SapawargaDB.db';

  // staging
  static String apiStaging = 'https://sapawarga-staging.jabarprov.go.id/api/v1';
  static String apiStagingStorage =
      'https://sapawarga-staging.jabarprov.go.id/api/storage';
  static String databaseNameStaging = 'SapawargaDBStaging.db';

  // android download storage
  static String downloadStorage = '/storage/emulated/0/Download';

  // mock
  static String apiMock = 'http://52.74.74.33:3000/v1';

  static String googleApiKey = '%GOOGLE_API_KEY%';
  static String defaultPassword = '123456';
  static String saberHoaxPhone = '+6282118670700';
  static String csPhone = '+6281312227212';
  static String laporPhone = '1708';

  static String urlImage = 'https://d2o6nohz2yihhc.cloudfront.net/';

  static String sentryDSN = '%SENTRY_DNS%';
}
