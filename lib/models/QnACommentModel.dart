// To parse this JSON data, do
//
//     final qnACommentModel = qnACommentModelFromJson(jsonString);

import 'dart:convert';

import 'package:sapawarga/models/UserModel.dart';

List<QnACommentModel> listQnACommentModelFromJson(String str) => List<QnACommentModel>.from(json.decode(str).map((x) => QnACommentModel.fromJson(x)));

QnACommentModel qnaCommentModelFromJson(String str) => QnACommentModel.fromJson(json.decode(str));

class QnACommentModel {
  int id;
  int questionId;
  String text;
  User user;
  bool isFlagged;
  int createdAt;
  int updatedAt;
  int createdBy;
  int updatedBy;

  QnACommentModel({
    this.id,
    this.questionId,
    this.text,
    this.user,
    this.isFlagged,
    this.createdAt,
    this.updatedAt,
    this.createdBy,
    this.updatedBy,
  });

  factory QnACommentModel.fromJson(Map<String, dynamic> json) => QnACommentModel(
    id: json["id"],
    questionId: json["question_id"],
    text: json["text"],
    user: User.fromJson(json["user"]),
    isFlagged: json["is_flagged"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    createdBy: json["created_by"],
    updatedBy: json["updated_by"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "question_id": questionId,
    "text": text,
    "user": user.toJson(),
    "is_flagged": isFlagged,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "created_by": createdBy,
    "updated_by": updatedBy,
  };
}
