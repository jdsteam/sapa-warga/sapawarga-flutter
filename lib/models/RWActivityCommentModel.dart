import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/MetaResponseModel.dart';
import 'package:sapawarga/models/UserModel.dart';

RWActivityCommentModel rwActivityCommentModelFromJson(String str) =>
    RWActivityCommentModel.fromJson(json.decode(str));

ItemRwActivityComment itemRWActivityCommentFromJson(String str) =>
    ItemRwActivityComment.fromJson(json.decode(str));

class RWActivityCommentModel {
  List<ItemRwActivityComment> items;
  MetaResponseModel meta;

  RWActivityCommentModel({
    this.items,
    this.meta,
  });

  factory RWActivityCommentModel.fromJson(Map<String, dynamic> json) =>
      RWActivityCommentModel(
        items: List<ItemRwActivityComment>.from(
            json["items"].map((x) => ItemRwActivityComment.fromJson(x))),
        meta: MetaResponseModel.fromJson(json["_meta"]),
      );

  Map<String, dynamic> toJson() => {
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
        "_meta": meta.toJson(),
      };
}

// ignore: must_be_immutable
class ItemRwActivityComment extends Equatable {
  int id;
  int userPostId;
  String text;
  User user;
  int createdAt;
  int updatedAt;
  int createdBy;
  int updatedBy;

  ItemRwActivityComment({
    this.id,
    this.userPostId,
    this.text,
    this.user,
    this.createdAt,
    this.updatedAt,
    this.createdBy,
    this.updatedBy,
  });

  factory ItemRwActivityComment.fromJson(Map<String, dynamic> json) =>
      ItemRwActivityComment(
        id: json["id"],
        userPostId: json["user_post_id"],
        text: json["text"],
        user: User.fromJson(json["user"]),
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_post_id": userPostId,
        "text": text,
        "user": user.toJson(),
        "created_at": createdAt,
        "updated_at": updatedAt,
        "created_by": createdBy,
        "updated_by": updatedBy,
      };

  @override
  List<Object> get props => [id];
}
