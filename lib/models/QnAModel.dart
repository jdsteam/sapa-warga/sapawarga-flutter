import 'dart:convert';

import 'package:sapawarga/models/UserModel.dart';

List<QnAModel> listQnAModelFromJson(String str) =>
    List<QnAModel>.from(json.decode(str).map((x) => QnAModel.fromJson(x)));

QnAModel qnaModelFromJson(String str) => QnAModel.fromJson(json.decode(str));

class QnAModel {
  int id;
  String text;
  int likesCount;
  int commentsCount;
  int answerId;
  Answer answer;
  int status;
  String statusLabel;
  int createdAt;
  int updatedAt;
  int createdBy;
  int isFlagged;
  bool isLiked;
  bool isHighlight;
  User user;

  QnAModel({
    this.id,
    this.text,
    this.likesCount,
    this.commentsCount,
    this.answerId,
    this.answer,
    this.status,
    this.statusLabel,
    this.createdAt,
    this.updatedAt,
    this.createdBy,
    this.isFlagged,
    this.isLiked,
    this.isHighlight,
    this.user,
  });

  factory QnAModel.fromJson(Map<String, dynamic> json) => QnAModel(
        id: json["id"],
        text: json["text"],
        likesCount: json["likes_count"],
        commentsCount: json["comments_count"],
        answerId: json["answer_id"] == null ? null : json["answer_id"],
        answer: json["answer"] == null ? null : Answer.fromJson(json["answer"]),
        status: json["status"],
        statusLabel: json["status_label"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        createdBy: json["created_by"],
        isFlagged: json["is_flagged"],
        isLiked: json["is_liked"],
        isHighlight: false,
        user: User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "text": text,
        "likes_count": likesCount,
        "comments_count": commentsCount,
        "answer_id": answerId == null ? null : answerId,
        "answer": answer?.toJson(),
        "status": status,
        "status_label": statusLabel,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "created_by": createdBy,
        "is_flagged": isFlagged,
        "is_liked": isLiked,
        "user": user.toJson(),
        "is_highlight": isHighlight,
      };
}

class Answer {
  int id;
  int questionId;
  String text;
  User user;
  bool isFlagged;
  int createdAt;
  int updatedAt;
  int createdBy;
  int updatedBy;

  Answer({
    this.id,
    this.questionId,
    this.text,
    this.user,
    this.isFlagged,
    this.createdAt,
    this.updatedAt,
    this.createdBy,
    this.updatedBy,
  });

  factory Answer.fromJson(Map<String, dynamic> json) => Answer(
        id: json["id"],
        questionId: json["question_id"],
        text: json["text"],
        user: User.fromJson(json["user"]),
        isFlagged: json["is_flagged"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "question_id": questionId,
        "text": text,
        "user": user.toJson(),
        "is_flagged": isFlagged,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "created_by": createdBy,
        "updated_by": updatedBy,
      };
}
