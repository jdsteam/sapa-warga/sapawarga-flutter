// To parse this JSON data, do
//
//     final statisticsPikobarModel = statisticsPikobarModelFromJson(jsonString);

import 'dart:convert';

StatisticsPikobarModel statisticsPikobarModelFromJson(String str) =>
    StatisticsPikobarModel.fromJson(json.decode(str));

class StatisticsPikobarModel {
  StatisticsPikobarModel({
    this.metadata,
    this.data,
  });

  Metadata metadata;
  List<Datum> data;

  factory StatisticsPikobarModel.fromJson(Map<String, dynamic> json) =>
      StatisticsPikobarModel(
        metadata: Metadata.fromJson(json["metadata"]),
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "metadata": metadata.toJson(),
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.pcrIndividuDate,
    this.kodeProv,
    this.closecontactTotalPertumbuhan,
    this.pcrIndividuInvalid,
    this.probableTotal,
    this.pcrIndividuNegatif,
    this.confirmationTotal,
    this.confirmationSelesaiPertumbuhan,
    this.closecontactDikarantina,
    this.probableDiisolasiPertumbuhan,
    this.confirmationMeninggal,
    this.namaProv,
    this.closecontactDikarantinaPertumbuhan,
    this.closecontactDiscardedPertumbuhan,
    this.suspectMeninggal,
    this.probableMeninggal,
    this.rdtTanggal,
    this.suspectTotal,
    this.confirmationTotalPertumbuhan,
    this.pcrTotal,
    this.confirmationSelesai,
    this.pcrNegatif,
    this.rdtTotal,
    this.pcrDate,
    this.pcrIndividuPositif,
    this.confirmationDiisolasiPertumbuhan,
    this.pcrIndividuTotal,
    this.probableDiisolasi,
    this.probableTotalPertumbuhan,
    this.rdtPositif,
    this.suspectDiisolasi,
    this.confirmationMeninggalPertumbuhan,
    this.suspectDiscardedPertumbuhan,
    this.closecontactTotal,
    this.probableDiscardedPertumbuhan,
    this.suspectDiisolasiPertumbuhan,
    this.pcrPositif,
    this.confirmationDiisolasi,
    this.probableMeninggalPertumbuhan,
    this.suspectDiscarded,
    this.probableDiscarded,
    this.rdtInvalid,
    this.pcrInvalid,
    this.suspectTotalPertumbuhan,
    this.rdtNegatif,
    this.closecontactDiscarded,
    this.suspectMeninggalPertumbuhan,
  });

  DateTime pcrIndividuDate;
  String kodeProv;
  int closecontactTotalPertumbuhan;
  int pcrIndividuInvalid;
  int probableTotal;
  int pcrIndividuNegatif;
  int confirmationTotal;
  int confirmationSelesaiPertumbuhan;
  int closecontactDikarantina;
  int probableDiisolasiPertumbuhan;
  int confirmationMeninggal;
  String namaProv;
  int closecontactDikarantinaPertumbuhan;
  int closecontactDiscardedPertumbuhan;
  int suspectMeninggal;
  int probableMeninggal;
  DateTime rdtTanggal;
  int suspectTotal;
  int confirmationTotalPertumbuhan;
  int pcrTotal;
  int confirmationSelesai;
  int pcrNegatif;
  int rdtTotal;
  DateTime pcrDate;
  int pcrIndividuPositif;
  int confirmationDiisolasiPertumbuhan;
  int pcrIndividuTotal;
  int probableDiisolasi;
  int probableTotalPertumbuhan;
  int rdtPositif;
  int suspectDiisolasi;
  int confirmationMeninggalPertumbuhan;
  int suspectDiscardedPertumbuhan;
  int closecontactTotal;
  int probableDiscardedPertumbuhan;
  int suspectDiisolasiPertumbuhan;
  int pcrPositif;
  int confirmationDiisolasi;
  int probableMeninggalPertumbuhan;
  int suspectDiscarded;
  int probableDiscarded;
  int rdtInvalid;
  int pcrInvalid;
  int suspectTotalPertumbuhan;
  int rdtNegatif;
  int closecontactDiscarded;
  int suspectMeninggalPertumbuhan;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        pcrIndividuDate: DateTime.parse(json["pcr_individu_date"]),
        kodeProv: json["kode_prov"],
        closecontactTotalPertumbuhan: json["closecontact_total_pertumbuhan"],
        pcrIndividuInvalid: json["pcr_individu_invalid"],
        probableTotal: json["probable_total"],
        pcrIndividuNegatif: json["pcr_individu_negatif"],
        confirmationTotal: json["confirmation_total"],
        confirmationSelesaiPertumbuhan:
            json["confirmation_selesai_pertumbuhan"],
        closecontactDikarantina: json["closecontact_dikarantina"],
        probableDiisolasiPertumbuhan: json["probable_diisolasi_pertumbuhan"],
        confirmationMeninggal: json["confirmation_meninggal"],
        namaProv: json["nama_prov"],
        closecontactDikarantinaPertumbuhan:
            json["closecontact_dikarantina_pertumbuhan"],
        closecontactDiscardedPertumbuhan:
            json["closecontact_discarded_pertumbuhan"],
        suspectMeninggal: json["suspect_meninggal"],
        probableMeninggal: json["probable_meninggal"],
        rdtTanggal: DateTime.parse(json["rdt_tanggal"]),
        suspectTotal: json["suspect_total"],
        confirmationTotalPertumbuhan: json["confirmation_total_pertumbuhan"],
        pcrTotal: json["pcr_total"],
        confirmationSelesai: json["confirmation_selesai"],
        pcrNegatif: json["pcr_negatif"],
        rdtTotal: json["rdt_total"],
        pcrDate: DateTime.parse(json["pcr_date"]),
        pcrIndividuPositif: json["pcr_individu_positif"],
        confirmationDiisolasiPertumbuhan:
            json["confirmation_diisolasi_pertumbuhan"],
        pcrIndividuTotal: json["pcr_individu_total"],
        probableDiisolasi: json["probable_diisolasi"],
        probableTotalPertumbuhan: json["probable_total_pertumbuhan"],
        rdtPositif: json["rdt_positif"],
        suspectDiisolasi: json["suspect_diisolasi"],
        confirmationMeninggalPertumbuhan:
            json["confirmation_meninggal_pertumbuhan"],
        suspectDiscardedPertumbuhan: json["suspect_discarded_pertumbuhan"],
        closecontactTotal: json["closecontact_total"],
        probableDiscardedPertumbuhan: json["probable_discarded_pertumbuhan"],
        suspectDiisolasiPertumbuhan: json["suspect_diisolasi_pertumbuhan"],
        pcrPositif: json["pcr_positif"],
        confirmationDiisolasi: json["confirmation_diisolasi"],
        probableMeninggalPertumbuhan: json["probable_meninggal_pertumbuhan"],
        suspectDiscarded: json["suspect_discarded"],
        probableDiscarded: json["probable_discarded"],
        rdtInvalid: json["rdt_invalid"],
        pcrInvalid: json["pcr_invalid"],
        suspectTotalPertumbuhan: json["suspect_total_pertumbuhan"],
        rdtNegatif: json["rdt_negatif"],
        closecontactDiscarded: json["closecontact_discarded"],
        suspectMeninggalPertumbuhan: json["suspect_meninggal_pertumbuhan"],
      );

  Map<String, dynamic> toJson() => {
        "pcr_individu_date":
            "${pcrIndividuDate.year.toString().padLeft(4, '0')}-${pcrIndividuDate.month.toString().padLeft(2, '0')}-${pcrIndividuDate.day.toString().padLeft(2, '0')}",
        "kode_prov": kodeProv,
        "closecontact_total_pertumbuhan": closecontactTotalPertumbuhan,
        "pcr_individu_invalid": pcrIndividuInvalid,
        "probable_total": probableTotal,
        "pcr_individu_negatif": pcrIndividuNegatif,
        "confirmation_total": confirmationTotal,
        "confirmation_selesai_pertumbuhan": confirmationSelesaiPertumbuhan,
        "closecontact_dikarantina": closecontactDikarantina,
        "probable_diisolasi_pertumbuhan": probableDiisolasiPertumbuhan,
        "confirmation_meninggal": confirmationMeninggal,
        "nama_prov": namaProv,
        "closecontact_dikarantina_pertumbuhan":
            closecontactDikarantinaPertumbuhan,
        "closecontact_discarded_pertumbuhan": closecontactDiscardedPertumbuhan,
        "suspect_meninggal": suspectMeninggal,
        "probable_meninggal": probableMeninggal,
        "rdt_tanggal":
            "${rdtTanggal.year.toString().padLeft(4, '0')}-${rdtTanggal.month.toString().padLeft(2, '0')}-${rdtTanggal.day.toString().padLeft(2, '0')}",
        "suspect_total": suspectTotal,
        "confirmation_total_pertumbuhan": confirmationTotalPertumbuhan,
        "pcr_total": pcrTotal,
        "confirmation_selesai": confirmationSelesai,
        "pcr_negatif": pcrNegatif,
        "rdt_total": rdtTotal,
        "pcr_date":
            "${pcrDate.year.toString().padLeft(4, '0')}-${pcrDate.month.toString().padLeft(2, '0')}-${pcrDate.day.toString().padLeft(2, '0')}",
        "pcr_individu_positif": pcrIndividuPositif,
        "confirmation_diisolasi_pertumbuhan": confirmationDiisolasiPertumbuhan,
        "pcr_individu_total": pcrIndividuTotal,
        "probable_diisolasi": probableDiisolasi,
        "probable_total_pertumbuhan": probableTotalPertumbuhan,
        "rdt_positif": rdtPositif,
        "suspect_diisolasi": suspectDiisolasi,
        "confirmation_meninggal_pertumbuhan": confirmationMeninggalPertumbuhan,
        "suspect_discarded_pertumbuhan": suspectDiscardedPertumbuhan,
        "closecontact_total": closecontactTotal,
        "probable_discarded_pertumbuhan": probableDiscardedPertumbuhan,
        "suspect_diisolasi_pertumbuhan": suspectDiisolasiPertumbuhan,
        "pcr_positif": pcrPositif,
        "confirmation_diisolasi": confirmationDiisolasi,
        "probable_meninggal_pertumbuhan": probableMeninggalPertumbuhan,
        "suspect_discarded": suspectDiscarded,
        "probable_discarded": probableDiscarded,
        "rdt_invalid": rdtInvalid,
        "pcr_invalid": pcrInvalid,
        "suspect_total_pertumbuhan": suspectTotalPertumbuhan,
        "rdt_negatif": rdtNegatif,
        "closecontact_discarded": closecontactDiscarded,
        "suspect_meninggal_pertumbuhan": suspectMeninggalPertumbuhan,
      };
}

class Metadata {
  Metadata({
    this.lastUpdate,
    this.dataSource,
  });

  DateTime lastUpdate;
  String dataSource;

  factory Metadata.fromJson(Map<String, dynamic> json) => Metadata(
        lastUpdate: DateTime.parse(json["last_update"]),
        dataSource: json["data_source"],
      );

  Map<String, dynamic> toJson() => {
        "last_update":
            "${lastUpdate.year.toString().padLeft(4, '0')}-${lastUpdate.month.toString().padLeft(2, '0')}-${lastUpdate.day.toString().padLeft(2, '0')}",
        "data_source": dataSource,
      };
}
