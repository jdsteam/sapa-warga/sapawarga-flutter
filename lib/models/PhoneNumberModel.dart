import 'package:sapawarga/enums/PhoneNumberTypeEnum.dart';

class PhoneNumberModel {
  final PhoneNumberTypeEnum type;
  final String phoneNumber;

  PhoneNumberModel({this.type, this.phoneNumber});

  factory PhoneNumberModel.fromMap(Map<String, dynamic> json) {
    return PhoneNumberModel(
      type: json['type'] == 'phone'
          ? PhoneNumberTypeEnum.phone
          : PhoneNumberTypeEnum.message,
      phoneNumber: json['phone_number'],
    );
  }

  Map<String, dynamic> toJson() => {
        'type': type,
        'phone_number': phoneNumber,
      };
}
