//     final ramadanModel = ramadanModelFromJson(jsonString);

import 'dart:convert';

List<RamadanModel> ramadanModelFromJson(String str) => List<RamadanModel>.from(
    json.decode(str).map((x) => RamadanModel.fromJson(x)));

class RamadanModel {
  RamadanModel({
    this.kabkot,
    this.items,
  });

  String kabkot;
  List<Item> items;

  factory RamadanModel.fromJson(Map<String, dynamic> json) => RamadanModel(
        kabkot: json["Kabkot"],
        items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Kabkot": kabkot,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class Item {
  Item({
    this.tanggal,
    this.ramadhan,
    this.imsak,
    this.subuh,
    this.terbit,
    this.duha,
    this.zuhur,
    this.asar,
    this.magrib,
    this.isya,
  });

  int tanggal;
  String ramadhan;
  String imsak;
  String subuh;
  String terbit;
  String duha;
  String zuhur;
  String asar;
  String magrib;
  String isya;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        tanggal: json["Tanggal"],
        ramadhan: json["Ramadhan"],
        imsak: json["IMSAK"],
        subuh: json["SUBUH"],
        terbit: json["TERBIT"],
        duha: json["DUHA"],
        zuhur: json["ZUHUR"],
        asar: json["ASAR"],
        magrib: json["MAGRIB"],
        isya: json["ISYA"],
      );

  Map<String, dynamic> toJson() => {
        "Tanggal": tanggal,
        "Ramadhan": ramadhan,
        "IMSAK": imsak,
        "SUBUH": subuh,
        "TERBIT": terbit,
        "DUHA": duha,
        "ZUHUR": zuhur,
        "ASAR": asar,
        "MAGRIB": magrib,
        "ISYA": isya,
      };
}
