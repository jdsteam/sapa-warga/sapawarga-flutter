import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pedantic/pedantic.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sapawarga/blocs/nearby_locations/Bloc.dart';
import 'package:sapawarga/components/DialogRequestPermission.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/models/PhoneBookModel.dart';
import 'package:sapawarga/repositories/PhoneBookRepository.dart';
import 'package:sapawarga/screens/phonebook/PhoneBookDetailScreen.dart';

class NearByLocationsScreen extends StatelessWidget {
  NearByLocationsScreen({Key key}) : super(key: key);

  final PhoneBookRepository phoneBookRepository = PhoneBookRepository();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NearByLocationsBloc>(
      create: (BuildContext context) =>
          NearByLocationsBloc(phoneBookRepository: phoneBookRepository),
      child: NearByLocations(),
    );
  }
}

class NearByLocations extends StatefulWidget {
  NearByLocations({Key key}) : super(key: key);

  @override
  _NearByLocationsState createState() => _NearByLocationsState();
}

class _NearByLocationsState extends State<NearByLocations> {
  NearByLocationsBloc _nearByLocationsBloc;

  final Completer<GoogleMapController> _controller = Completer();

  static const LatLng _center = LatLng(-6.902735, 107.618782);

  double _latitude;
  double _longitude;

  BitmapDescriptor myIcon;

  final Set<Marker> _markers = Set();

  @override
  void initState() {
    _nearByLocationsBloc = BlocProvider.of<NearByLocationsBloc>(context);
    _checkPermission();
    super.initState();
  }

  Future<void> _onMapCreated(GoogleMapController controller) async {
    LatLng currentLocation = LatLng(_latitude, _longitude);

    await controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: currentLocation, zoom: 15.5),
      ),
    );

    _controller.complete(controller);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<NearByLocationsBloc, NearByLocationsState>(
      listener: (context, state) async {
        if (state is NearByLocationsLoaded) {
          for (final data in state.records) {
            _markers.add(Marker(
                markerId: MarkerId(data.id),
                icon: BitmapDescriptor.fromBytes(
                    await _markerIcon(data.category)),
                draggable: false,
                infoWindow: InfoWindow(
                    title: data.name,
                    snippet: data.address,
                    onTap: () {
                      _onTapItem(
                          context,
                          PhoneBookModel(
                              id: int.tryParse(data.id),
                              name: data.name,
                              address: data.address,
                              description: data.description,
                              latitude: data.latitude,
                              longitude: data.longitude,
                              phoneNumbers: data.phoneNumbers));
                    }),
                position: LatLng(double.tryParse(data.latitude),
                    double.tryParse(data.longitude))));

            if (_markers.length == state.records.length) {
              setState(() {});
            }
          }
        }
      },
      child: BlocBuilder<NearByLocationsBloc, NearByLocationsState>(
          bloc: _nearByLocationsBloc,
          builder: (context, state) {
            return Scaffold(
              appBar: AppBar(
                title: Text(Dictionary.nearbyLocation),
              ),
              body: state is NearByLocationsLoading
                  ? Center(child: CircularProgressIndicator())
                  : state is NearByLocationsLoaded
                      ? buildGoogleMap()
                      : state is NearByLocationsFailure
                          ? Center(child: Text(state.error))
                          : Container(),
            );
          }),
    );
  }

  GoogleMap buildGoogleMap() {
    return GoogleMap(
      myLocationEnabled: true,
      gestureRecognizers: Set()
        ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
        ..add(Factory<VerticalDragGestureRecognizer>(
            () => VerticalDragGestureRecognizer())),
      onMapCreated: _onMapCreated,
      initialCameraPosition: CameraPosition(
        target: _center,
        zoom: 15.0,
      ),
      markers: _markers,
    );
  }

  Future<Uint8List> _markerIcon(String category) async {
    String uriIcon;
    switch (category.toLowerCase()) {
      case 'kesehatan':
        uriIcon = 'assets/icons/kesehatan.png';
        break;
      case 'ekonomi':
        uriIcon = 'assets/icons/ekonomi.png';
        break;
      case 'keamanan':
        uriIcon = 'assets/icons/keamanan.png';
        break;
      case 'transportasi':
        uriIcon = 'assets/icons/transport.png';
        break;
      case 'sosial':
        uriIcon = 'assets/icons/sosial.png';
        break;
      case 'layanan':
        uriIcon = 'assets/icons/pelayanan.png';
        break;
      default:
        uriIcon = 'assets/icons/pelayanan.png';
        break;
    }

    Uint8List markerIcon = await getBytesFromAsset(uriIcon, 100);

    return markerIcon;
  }

  Future<void> _onStatusRequested(
      Map<PermissionGroup, PermissionStatus> statuses) async {
    final status = statuses[PermissionGroup.location];
    if (status == PermissionStatus.granted) {
      final GoogleMapController controller = await _controller.future;
      final Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);

      LatLng currentLocation = LatLng(position.latitude, position.longitude);
      _latitude = position.latitude;
      _longitude = position.longitude;

      await controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: currentLocation, zoom: 15.5),
        ),
      );
    }
  }

  Future<void> _checkPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);

    if (permission == PermissionStatus.granted) {
      final Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);

      _latitude = position.latitude;
      _longitude = position.longitude;

      _nearByLocationsBloc
          .add(NearByLocationsLoad(latitude: _latitude, longitude: _longitude));
    } else {
      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/map_pin.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionPhoneBookMap,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.location]).then(_onStatusRequested);
                },
              )));
    }
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  void _onTapItem(BuildContext context, PhoneBookModel record) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PhoneBookDetailScreen(record: record),
      ),
    );
  }

  @override
  void dispose() {
    _nearByLocationsBloc.close();
    super.dispose();
  }
}
