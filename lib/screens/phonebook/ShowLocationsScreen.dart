import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pedantic/pedantic.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sapawarga/components/DialogRequestPermission.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/models/PhoneBookModel.dart';
import 'package:sapawarga/screens/phonebook/PhoneBookDetailScreen.dart';

class ShowLocationsScreen extends StatelessWidget {
  final PhoneBookModel phoneBookModel;

  ShowLocationsScreen({Key key, this.phoneBookModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ShowLocations(phoneBookModel: phoneBookModel);
  }
}

class ShowLocations extends StatefulWidget {
  final PhoneBookModel phoneBookModel;

  ShowLocations({Key key, this.phoneBookModel}) : super(key: key);

  @override
  _ShowLocationsState createState() => _ShowLocationsState();
}

class _ShowLocationsState extends State<ShowLocations> {
  final Completer<GoogleMapController> _controller = Completer();

  LatLng _center;

  double _latitude;
  double _longitude;

  BitmapDescriptor myIcon;

  final Set<Marker> _markers = Set();

  @override
  void initState() {
    _center = LatLng(double.tryParse(widget.phoneBookModel.latitude),
        double.tryParse(widget.phoneBookModel.longitude));
    _checkPermission();
    super.initState();
  }

  Future<void> _onMapCreated(GoogleMapController controller) async {
    LatLng currentLocation = LatLng(_latitude, _longitude);

    await controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: currentLocation, zoom: 15.5),
      ),
    );

    _controller.complete(controller);
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(milliseconds: 500), () async {
      _markers.add(Marker(
          markerId: MarkerId(widget.phoneBookModel.id.toString()),
          icon: BitmapDescriptor.fromBytes(await _markerIcon(
              widget.phoneBookModel.category.name.toString())),
          draggable: false,
          infoWindow: InfoWindow(
              title: widget.phoneBookModel.name,
              snippet: widget.phoneBookModel.address,
              onTap: () {
                _onTapItem(
                    context,
                    PhoneBookModel(
                        id: widget.phoneBookModel.id,
                        name: widget.phoneBookModel.name,
                        address: widget.phoneBookModel.address,
                        description: widget.phoneBookModel.description,
                        latitude: widget.phoneBookModel.latitude,
                        longitude: widget.phoneBookModel.longitude,
                        phoneNumbers: widget.phoneBookModel.phoneNumbers));
              }),
          position: LatLng(double.tryParse(widget.phoneBookModel.latitude),
              double.tryParse(widget.phoneBookModel.longitude))));
    });

    return Scaffold(
        appBar: AppBar(
          title: Text(Dictionary.nearbyLocation),
        ),
        body: buildGoogleMap());
  }

  GoogleMap buildGoogleMap() {
    return GoogleMap(
      myLocationEnabled: true,
      gestureRecognizers: Set()
        ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
        ..add(Factory<VerticalDragGestureRecognizer>(
            () => VerticalDragGestureRecognizer())),
      onMapCreated: _onMapCreated,
      initialCameraPosition: CameraPosition(
        target: _center,
        zoom: 15.0,
      ),
      markers: _markers,
    );
  }

  Future<Uint8List> _markerIcon(String category) async {
    String uriIcon;
    switch (category.toLowerCase()) {
      case 'kesehatan':
        uriIcon = 'assets/icons/kesehatan.png';
        break;
      case 'ekonomi':
        uriIcon = 'assets/icons/ekonomi.png';
        break;
      case 'keamanan':
        uriIcon = 'assets/icons/keamanan.png';
        break;
      case 'transportasi':
        uriIcon = 'assets/icons/transport.png';
        break;
      case 'sosial':
        uriIcon = 'assets/icons/sosial.png';
        break;
      case 'layanan':
        uriIcon = 'assets/icons/pelayanan.png';
        break;
      default:
        uriIcon = 'assets/icons/pelayanan.png';
        break;
    }

    Uint8List markerIcon = await getBytesFromAsset(uriIcon, 100);

    return markerIcon;
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  void _onTapItem(BuildContext context, PhoneBookModel record) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PhoneBookDetailScreen(record: record),
      ),
    );
  }

  Future<void> _onStatusRequested(
      Map<PermissionGroup, PermissionStatus> statuses) async {
    final status = statuses[PermissionGroup.location];
    if (status == PermissionStatus.granted) {
      final GoogleMapController controller = await _controller.future;
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);

      LatLng currentLocation = LatLng(position.latitude, position.longitude);
      _latitude = position.latitude;
      _longitude = position.longitude;

      await controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: currentLocation, zoom: 15.5),
        ),
      );
    }
  }

  Future<void> _checkPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);

    if (permission == PermissionStatus.granted) {
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);

      _latitude = position.latitude;
      _longitude = position.longitude;

      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/map_pin.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionPhoneBookMap,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.location]).then(_onStatusRequested);
                },
              )));
    }
  }
}
