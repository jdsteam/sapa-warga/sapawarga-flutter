import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pedantic/pedantic.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sapawarga/MainStaging.dart';
import 'package:sapawarga/blocs/activation_account/ActivationAccountBloc.dart';
import 'package:sapawarga/blocs/activation_account/ActivationAccountEvent.dart';
import 'package:sapawarga/blocs/activation_account/Bloc.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/login/Bloc.dart';
import 'package:sapawarga/blocs/remote_home/Bloc.dart';
import 'package:sapawarga/blocs/remote_home/RemoteHomeBloc.dart';
import 'package:sapawarga/blocs/update_app/Bloc.dart';
import 'package:sapawarga/components/BlockCircleLoading.dart';
import 'package:sapawarga/components/BrowserScreen.dart';
import 'package:sapawarga/components/DialogRequestPermission.dart';
import 'package:sapawarga/components/DialogUpdateApp.dart';
import 'package:sapawarga/components/TextButton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/constants/UrlThirdParty.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/repositories/FirebaseRepository.dart';
import 'package:sapawarga/repositories/RegisterRepository.dart';
import 'package:sapawarga/repositories/UpdateAppRepository.dart';
import 'package:sapawarga/screens/login/LoginForm.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';

class LoginScreen extends StatefulWidget {
  final AuthRepository authRepository;
  final AppState appState;

  LoginScreen({Key key, @required this.authRepository, this.appState})
      : assert(authRepository != null),
        super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(
        DiagnosticsProperty<AuthRepository>('authRepository', authRepository));
    properties.add(DiagnosticsProperty<AppState>('appState', appState));
  }
}

class _LoginScreenState extends State<LoginScreen> {
  AuthRepository get _authRepository => widget.authRepository;
  UpdateAppBloc _updateAppBloc;
  ActivationAccountBloc _activationAccountBloc;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  String _versionText = Dictionary.version;
  bool _isLoading = false;

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.LOGIN);
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_LOGIN);

    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        _versionText = packageInfo.version != null
            ? packageInfo.version
            : Dictionary.version;
      });
    });

    sendTokenActivation();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<UpdateAppBloc>(
            create: (context) => _updateAppBloc = UpdateAppBloc(
              updateAppRepository: UpdateAppRepository(),
            )..add(CheckUpdate()),
          ),
          BlocProvider<ActivationAccountBloc>(
            create: (BuildContext context) =>
                _activationAccountBloc = ActivationAccountBloc(
              registerRepository: RegisterRepository(),
            ),
          ),
          BlocProvider<RemoteHomeBloc>(
              create: (BuildContext context) =>
                  RemoteHomeBloc(firebaseRepository: FirebaseRepository())
                    ..add(RemoteHomeLoad())),
        ],
        child: MultiBlocListener(
          listeners: [
            BlocListener<UpdateAppBloc, UpdateAppState>(
              bloc: _updateAppBloc,
              listener: (context, state) {
                if (state is UpdateAppRequired) {
                  showDialog(
                      context: context,
                      builder: (context) => WillPopScope(
                          onWillPop: () {
                            return;
                          },
                          child: DialogUpdateApp()),
                      barrierDismissible: false);
                }
              },
            ),
            BlocListener<ActivationAccountBloc, ActivationAccountState>(
              bloc: _activationAccountBloc,
              listener: (context, state) async {
                if (state is ActivationAccountSuccess) {
                  if (_isLoading) {
                    _isLoading = false;
                    Navigator.of(context, rootNavigator: true).pop();
                  }

                  await Preferences.setHasActivationAccount(true);
                  await Preferences.setHasTokenActivation('');

                  scaffoldKey.currentState.showSnackBar(
                    SnackBar(
                      content: Text(Dictionary.infoRegister),
                    ),
                  );
                } else if (state is ActivationAccountLoading) {
                  _isLoading = true;
                  await blockCircleLoading(context: context);
                } else if (state is ActivationAccountFailure) {
                  if (_isLoading) {
                    _isLoading = false;
                    Navigator.of(context, rootNavigator: true).pop();
                  }

                  await Preferences.setHasTokenActivation('');

                  await Navigator.of(context).pushNamed(
                    NavigationConstrants.RegisterErrorPage,
                    arguments: [
                      state.error.toString(),
                      '${Environment.imageAssets}image_error_registration.png',
                    ],
                  );
                } else {
                  if (_isLoading) {
                    _isLoading = false;
                    Navigator.of(context, rootNavigator: true).pop();
                  }
                  await Preferences.setHasTokenActivation('');
                }
              },
            ),
          ],
          child: Scaffold(
            key: scaffoldKey,
            body: KeyboardDismissOnTap(
              child: MultiBlocProvider(
                providers: [
                  BlocProvider<LoginBloc>(
                    create: (BuildContext context) => LoginBloc(
                      authenticationBloc:
                          BlocProvider.of<AuthenticationBloc>(context),
                      authRepository: _authRepository,
                    ),
                  )
                ],
                child: Stack(
                  children: <Widget>[
                    // Login Form
                    Container(
                        child: Padding(
                      padding: const EdgeInsets.only(top: 40.0),
                      child: LoginForm(),
                    )),

                    KeyboardVisibilityBuilder(
                      builder: (context, visible) {
                        return !visible
                            ? // Footer
                            Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        TextButtonOnly(
                                          padding: const EdgeInsets.all(5.0),
                                          title: Dictionary.termOfService,
                                          textStyle: TextStyle(
                                              fontSize: 12,
                                              fontFamily: FontsFamily.roboto,
                                              color: clr.Colors.netralGrey),
                                          onTap: () {
                                            AnalyticsHelper.setLogEvent(Analytics
                                                .EVENT_VIEW_TERMS_OF_SERVICE_LOGIN);
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      BrowserScreen(
                                                        url: UrlThirdParty
                                                            .termOfService,
                                                      )),
                                            );
                                          },
                                        ),
                                        Text(' | '),
                                        TextButtonOnly(
                                          padding: const EdgeInsets.all(5.0),
                                          title: Dictionary.privacyPolicy,
                                          textStyle: TextStyle(
                                              fontFamily: FontsFamily.roboto,
                                              fontSize: 12,
                                              color: clr.Colors.netralGrey),
                                          onTap: () {
                                            AnalyticsHelper.setLogEvent(Analytics
                                                .EVENT_VIEW_PRIVACY_POLICY_LOGIN);
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      BrowserScreen(
                                                        url: UrlThirdParty
                                                            .privacyPolicy,
                                                      )),
                                            );
                                          },
                                        ),
                                      ],
                                    ),
                                  ),

                                  GestureDetector(
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                          left: 5.0, right: 5.0, bottom: 10.0),
                                      child: Text(
                                        Dictionary.downloadGuideBook,
                                        style: TextStyle(
                                          fontFamily: FontsFamily.roboto,
                                          fontSize: 12,
                                          color: clr.Colors.netralGrey,
                                        ),
                                      ),
                                    ),
                                    onTap: _downloadGuideBook,
                                  ),

                                  // TODO: Don't delete this code. It's possible to be used again
                                  // Container(
                                  //   width: MediaQuery.of(context).size.width,
                                  //   height: 1.5,
                                  //   color: Color.fromRGBO(180, 180, 180, 0.5),
                                  // ),
                                  // Container(
                                  //   width: MediaQuery.of(context).size.width,
                                  //   height: 50.0,
                                  //   color: Colors.white,
                                  //   child: Center(
                                  //     child: Column(
                                  //       mainAxisAlignment: MainAxisAlignment.center,
                                  //       children: <Widget>[
                                  //         Text('Version ' + _versionText,
                                  //             style: TextStyle(fontSize: 12.0)),
                                  //         Text(Dictionary.copyRight,
                                  //             style: TextStyle(fontSize: 12.0)),
                                  //       ],
                                  //     ),
                                  //   ),
                                  // )
                                ],
                              )
                            : Container();
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  Future<void> _downloadGuideBook() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);

    if (permission != PermissionStatus.granted) {
      unawaited(showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/folder.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionDownloadManualBook,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.storage]).then(_onStatusRequested);
                },
              )));
    } else {
      unawaited(Fluttertoast.showToast(
          msg: Dictionary.downloadingFile,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          fontSize: 16.0));

      try {
        await FlutterDownloader.enqueue(
          url: UrlThirdParty.manualBook,
          savedDir: Environment.downloadStorage,
          showNotification: true,
          // show download progress in status bar (for Android)
          openFileFromNotification:
              true, // click on notification to open downloaded file (for Android)
        );
      } catch (e) {
        String dir = (await getExternalStorageDirectory()).path + '/download';
        await FlutterDownloader.enqueue(
          url: UrlThirdParty.manualBook,
          savedDir: dir,
          showNotification: true,
          // show download progress in status bar (for Android)
          openFileFromNotification:
              true, // click on notification to open downloaded file (for Android)
        );
      }

      await AnalyticsHelper.setLogEvent(
          Analytics.EVENT_DOWNLOAD_GUIDEBOOK_LOGIN);
    }
  }

  loadInfoActivation(String activationToken) async {
    _activationAccountBloc
        .add(ActivationAccount(activationToken: activationToken));
  }

  void _onStatusRequested(Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.storage];
    if (status == PermissionStatus.granted) {
      _downloadGuideBook();
    }
  }

  sendTokenActivation() async {
    String activationToken = await Preferences.hasTokenActivation();

    if (activationToken != null && activationToken.isNotEmpty) {
      WidgetsBinding.instance.addPostFrameCallback(
        (_) => loadInfoActivation(activationToken),
      );
    }
  }

  @override
  void dispose() {
    _updateAppBloc.close();
    super.dispose();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<GlobalKey<ScaffoldState>>(
        'scaffoldKey', scaffoldKey));
  }
}
