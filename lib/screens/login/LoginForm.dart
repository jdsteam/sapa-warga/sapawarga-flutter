import 'dart:convert';
import 'package:device_apps/device_apps.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:sapawarga/blocs/login/Bloc.dart';
import 'package:sapawarga/blocs/remote_home/Bloc.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/DialogWidgetContent.dart';
import 'package:sapawarga/components/DialogWithImage.dart';
import 'package:sapawarga/components/PasswordFormField.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/components/TextButton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/Validations.dart';
import 'package:sapawarga/utilities/WhatsAppHelper.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginForm extends StatefulWidget {
  LoginForm({Key key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  LoginBloc _loginBloc;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  String token;
  List<dynamic> csPhoneNumbers;
  dynamic csInfoText;
  bool isShowTitle = false;
  int failedLoginCount = 0;

  @override
  void initState() {
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _firebaseMessaging.getToken().then((token) {
      this.token = token;
    });
    csPhoneNumbers = json.decode(FirebaseConfig.callCenterNumbersValue);
    csInfoText = Dictionary.helpAdminWA;
    super.initState();
  }

  Future<RemoteConfig> _initializeRemoteConfig() async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    await remoteConfig.setDefaults(<String, dynamic>{
      FirebaseConfig.callCenterNumbersKey: FirebaseConfig.callCenterNumbersValue
    });

    try {
      await remoteConfig.fetch(expiration: Duration(minutes: 30));
      await remoteConfig.activateFetched();
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }

    return remoteConfig;
  }

  @override
  Widget build(BuildContext context) {
    _onLoginButtonPressed() {
      AnalyticsHelper.setLogEvent(Analytics.EVENT_TAPPED_BUTTON_LOGIN);

      FocusScope.of(context).requestFocus(FocusNode()); // Hide Keyboard
      if (_formKey.currentState.validate()) {
        _loginBloc.add(LoginButtonPressed(
          username: _usernameController.text,
          password: _passwordController.text,
          fcmToken: token,
        ));
      } else {
        print("Validate Error");
      }
    }

    return ListView(
      children: [
        KeyboardDismissOnTap(
          child: BlocListener<LoginBloc, LoginState>(
            listener: (context, state) async {
              if (state is LoginFailure) {
                failedLoginCount++;
                await AnalyticsHelper.setLogEvent(
                    Analytics.EVENT_FAILED_LOGIN, <String, dynamic>{
                  'username': '${_usernameController.text}',
                  'message': '${state.error.toString()}'
                });
                if (failedLoginCount < 3) {
                  await showDialog(
                      context: context,
                      builder: (BuildContext context) => DialogTextOnly(
                            description: state.error.toString(),
                            buttonText: "OK",
                            onOkPressed: () {
                              Navigator.of(context)
                                  .pop(); // To close the dialog
                            },
                          ));
                } else {
                  failedLoginCount = 0;
                  await showDialog(
                      context: context,
                      barrierDismissible: true,
                      builder: (BuildContext context) => DialogWithImage(
                            title: Dictionary.titleFailedLogin,
                            description: Dictionary.descFailedLogin,
                            locationImage:
                                '${Environment.imageAssets}hotline_sapawarga.png',
                            buttonName: Dictionary.helpHotline,
                            onNextPressed: () async {
                              csPhoneNumbers.length > 1
                                  ? _openDialogContact()
                                  : WhatsAppHelper.checkNumber(
                                      csPhoneNumbers[0], csInfoText);

                              await AnalyticsHelper.setLogEvent(
                                  Analytics.EVENT_CALL_HOTLINE);
                              Navigator.of(context).pop();
                            },
                          ));
                }

                Scaffold.of(context).hideCurrentSnackBar();
              } else if (state is LoginLoading) {
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    content: Row(
                      children: <Widget>[
                        CircularProgressIndicator(),
                        Container(
                          margin: const EdgeInsets.only(left: 15.0),
                          child: Text(Dictionary.loadingLogin),
                        )
                      ],
                    ),
                    duration: Duration(minutes: 1),
                  ),
                );
              } else if (state is LoginValidationError) {
                failedLoginCount++;
                if (state.errors.containsKey('password') &&
                    state.errors['password'][0].toString().contains('salah')) {
                  await AnalyticsHelper.setLogEvent(
                      Analytics.EVENT_FAILED_LOGIN, <String, dynamic>{
                    'username': '${_usernameController.text}',
                    'message': '${state.errors['password'][0].toString()}'
                  });
                  if (failedLoginCount < 3) {
                    await showDialog(
                        context: context,
                        builder: (BuildContext context) => DialogTextOnly(
                              description:
                                  state.errors['password'][0].toString(),
                              buttonText: "OK",
                              onOkPressed: () {
                                Navigator.of(context)
                                    .pop(); // To close the dialog
                              },
                            ));
                  } else {
                    failedLoginCount = 0;
                    await showDialog(
                        context: context,
                        barrierDismissible: true,
                        builder: (BuildContext context) => DialogWithImage(
                              title: Dictionary.titleFailedLogin,
                              description: Dictionary.descFailedLogin,
                              locationImage:
                                  '${Environment.imageAssets}hotline_sapawarga.png',
                              buttonName: Dictionary.helpHotline,
                              onNextPressed: () async {
                                csPhoneNumbers.length > 1
                                    ? _openDialogContact()
                                    : WhatsAppHelper.checkNumber(
                                        csPhoneNumbers[0], csInfoText);

                                await AnalyticsHelper.setLogEvent(
                                    Analytics.EVENT_CALL_HOTLINE);
                                Navigator.of(context).pop();
                              },
                            ));
                  }

                  Scaffold.of(context).hideCurrentSnackBar();
                } else if (state.errors.containsKey('status')) {
                  await AnalyticsHelper.setLogEvent(
                      Analytics.EVENT_FAILED_LOGIN, <String, dynamic>{
                    'username': '${_usernameController.text}',
                    'message': Dictionary.errorStatusUnActive
                  });
                  await showDialog(
                      context: context,
                      builder: (BuildContext context) => DialogTextOnly(
                            description: state.errors['status'][0].toString(),
                            buttonText: "OK",
                            onOkPressed: () {
                              Navigator.of(context)
                                  .pop(); // To close the dialog
                            },
                          ));
                  Scaffold.of(context).hideCurrentSnackBar();
                }
              } else {
                Scaffold.of(context).hideCurrentSnackBar();
              }
            },
            child: BlocBuilder<LoginBloc, LoginState>(
              bloc: _loginBloc,
              builder: (
                BuildContext context,
                LoginState state,
              ) {
                return Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Row(
                          children: [
                            SizedBox(
                              height: 80.0,
                              child: Image.asset(
                                'assets/icons/icon.png',
                                fit: BoxFit.contain,
                              ),
                            ),
                            KeyboardVisibilityBuilder(
                              builder: (context, visible) {
                                return visible
                                    ? Expanded(
                                        child: Text(
                                        Dictionary.titleLogin,
                                        style: TextStyle(
                                            fontSize: 24,
                                            fontFamily: FontsFamily.roboto,
                                            fontWeight: FontWeight.bold),
                                      ))
                                    : Container();
                              },
                            ),
                          ],
                        ),
                      ),

                      KeyboardVisibilityBuilder(
                        builder: (context, visible) {
                          return !visible
                              ? Container(
                                  margin: const EdgeInsets.only(top: 10),
                                  padding: const EdgeInsets.only(
                                      left: 40, right: 40),
                                  child: Text(
                                    Dictionary.titleLogin,
                                    style: TextStyle(
                                        fontSize: 32,
                                        fontFamily: FontsFamily.roboto,
                                        fontWeight: FontWeight.bold),
                                  ),
                                )
                              : Container();
                        },
                      ),

                      Container(
                        padding: const EdgeInsets.only(
                            top: 20.0, bottom: 20.0, left: 40.0, right: 40.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              Dictionary.labelUsernameOrPhoneNumber,
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: clr.Colors.veryDarkGrey,
                                  fontFamily: FontsFamily.roboto,
                                  fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            TextFormField(
                              controller: _usernameController,
                              obscureText: false,
                              autofocus: false,
                              decoration: InputDecoration(
                                  // icon: Icon(Icons.person),
                                  fillColor: Colors.grey[100],
                                  filled: true,
                                  hintText: Dictionary
                                      .labelInputUsernameOrPhoneNumber,
                                  hintStyle: TextStyle(
                                      color: clr.Colors.netralGrey,
                                      fontFamily: FontsFamily.roboto,
                                      fontSize: 14),
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 12, vertical: 14),
                                  errorText: (state is LoginValidationError &&
                                          state.errors.containsKey('username'))
                                      ? state.errors['username'][0]
                                      : null,
                                  errorBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8),
                                      borderSide: BorderSide(
                                          color: Colors.red, width: 1.5)),
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8),
                                      borderSide: BorderSide(
                                          color: Colors.grey[400], width: 1.5)),
                                  disabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8),
                                      borderSide: BorderSide(
                                          color: Colors.grey[400], width: 1.5)),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8),
                                      borderSide: BorderSide(
                                          color: Colors.grey[400],
                                          width: 1.5))),
                              validator: (val) =>
                                  Validations.usernameValidationLogin(val),
                            ),
                            const SizedBox(height: 10.0),
                            Text(
                              Dictionary.inputIdUserDesc,
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: clr.Colors.netralGrey,
                                  fontFamily: FontsFamily.roboto),
                            ),
                            const SizedBox(height: 18.0),
                            Text(
                              Dictionary.labelPassword,
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: clr.Colors.veryDarkGrey,
                                  fontFamily: FontsFamily.roboto,
                                  fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            PasswordFormField(
                              showIcon: false,
                              showLabel: false,
                              controller: _passwordController,
                              labelText: Dictionary.labelPassword,
                              hintText: Dictionary.labelHintPassword,
                              errorText: (state is LoginValidationError &&
                                      state.errors.containsKey('password') &&
                                      !state.errors['password'][0]
                                          .toString()
                                          .contains('salah'))
                                  ? state.errors['password'][0]
                                  : null,
                              validator: (val) =>
                                  Validations.passwordValidation(val, true),
                            ),
                            TextButtonOnly(
                              padding: const EdgeInsets.only(
                                  right: 10.0, top: 10.0, bottom: 10.0),
                              title: Dictionary.forgotPasswordLogin,
                              textStyle: TextStyle(
                                  fontSize: 14,
                                  fontFamily: FontsFamily.roboto,
                                  color: clr.Colors.darkblue),
                              onTap: () {
                                // showBottomForgotAccount(context: context);
                                csPhoneNumbers.length > 1
                                    ? _openDialogContact()
                                    : _launchWhatsApp(
                                        csPhoneNumbers[0], csInfoText);

                                AnalyticsHelper.setLogEvent(
                                    Analytics.EVENT_CALL_HOTLINE);
                              },
                            ),
                            const SizedBox(height: 20),
                            RoundedButton(
                              title: Dictionary.login,
                              borderRadius: BorderRadius.circular(5.0),
                              color: clr.Colors.darkblue,
                              textStyle: TextStyle(
                                  fontSize: 16,
                                  fontFamily: FontsFamily.roboto,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              onPressed: state is! LoginLoading
                                  ? _onLoginButtonPressed
                                  : null,
                            ),
                            const SizedBox(height: 12),
                            // BlocBuilder<RemoteHomeBloc, RemoteHomeState>(
                            //   builder: (context, state) {
                            //     return state is RemoteHomeLoaded
                            //         ? _buildRegisterButton(state.remoteConfig)
                            //         : Container();
                            //   },
                            // ),
                          ],
                        ),
                      ),
                      const SizedBox(height: Dimens.padding),
                      FutureBuilder<RemoteConfig>(
                        future: _initializeRemoteConfig(),
                        builder: (BuildContext context,
                            AsyncSnapshot<RemoteConfig> snapshot) {
                          if (snapshot.hasData) {
                            final dataPhoneNumbers = snapshot.data
                                .getString(FirebaseConfig.callCenterNumbersKey);
                            if (dataPhoneNumbers != null) {
                              csPhoneNumbers = json.decode(dataPhoneNumbers);
                            }
                            try {
                              final dataInfoText = snapshot.data.getString(
                                  FirebaseConfig.callCenterTextInfoKey);
                              if (dataInfoText != null) {
                                csInfoText = json.decode(dataInfoText);
                              }
                            } catch (e) {
                              print(e.toString());
                            }
                          }
                          return Container();
                        },
                      ),
                      // SizedBox(height: Dimens.dialogRadius),
                    ],
                  ),
                );
              },
            ),
          ),
        )
      ],
    );
  }

  _buildRegisterButton(RemoteConfig remoteConfig) {
    Map<String, dynamic> menuConfig =
        json.decode(remoteConfig.getString(FirebaseConfig.menuConfig));

    return menuConfig['RegisterAccount']['enabled']
        ? RichText(
            text: TextSpan(children: [
            TextSpan(
                text: Dictionary.confirmDoesntHaveAccount,
                style: TextStyle(
                    fontSize: 14,
                    fontFamily: FontsFamily.roboto,
                    color: clr.Colors.netralGrey)),
            TextSpan(
                text: Dictionary.register,
                style: TextStyle(
                    fontSize: 14,
                    fontFamily: FontsFamily.roboto,
                    decoration: TextDecoration.underline,
                    color: clr.Colors.darkblue),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    Navigator.of(context)
                        .pushNamed(NavigationConstrants.Register);
                  })
          ]))
        : Container();
  }

  _openDialogContact() {
    showDialog(
        context: context,
        builder: (BuildContext context) => DialogWidgetContent(
              title: Dictionary.callCenterList,
              child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount:
                    csPhoneNumbers.length > 5 ? 5 : csPhoneNumbers.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    child: Container(
                      margin:
                          index != 0 ? const EdgeInsets.only(top: 10.0) : null,
                      padding: const EdgeInsets.fromLTRB(
                          Dimens.padding, 10.0, Dimens.padding, 10.0),
                      decoration: BoxDecoration(
                        color: Color(0xffebf8ff),
                        shape: BoxShape.rectangle,
                        border: Border.all(color: clr.Colors.blue),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Wrap(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: Image.asset(
                                '${Environment.iconAssets}whatsapp.png',
                                width: 20,
                                height: 20,
                                color: clr.Colors.green),
                          ),
                          Text(csPhoneNumbers[index],
                              style: TextStyle(
                                  fontFamily: FontsFamily.sourceSansPro,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w600,
                                  color: clr.Colors.green))
                        ],
                      ),
                    ),
                    onTap: () {
                      WhatsAppHelper.checkNumber(
                          csPhoneNumbers[index], csInfoText);
                      Navigator.of(context).pop();
                    },
                  );
                },
              ),
              buttonText: Dictionary.close.toUpperCase(),
              onOkPressed: () {
                Navigator.of(context).pop(); // To close the dialog
              },
            ));
  }

  _buildButtonForgotAccount(
      {GestureTapCallback onPressed, String title, String imageIcon}) {
    return RaisedButton(
      onPressed: onPressed,
      highlightColor: clr.Colors.blueCardColor,
      elevation: 0,
      padding: const EdgeInsets.all(0.0),
      color: clr.Colors.greyContainer,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  imageIcon,
                  height: 48,
                  width: 48,
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  title,
                  style: TextStyle(
                      fontSize: 18,
                      fontFamily: FontsFamily.roboto,
                      color: clr.Colors.greyText,
                      fontWeight: FontWeight.w300),
                ),
                const SizedBox(
                  height: 8,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void showBottomForgotAccount(
      {@required BuildContext context, bool isDismissible = true}) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: const Radius.circular(8.0),
            topRight: const Radius.circular(8.0),
          ),
        ),
        isDismissible: isDismissible,
        builder: (context) {
          return Container(
            margin: const EdgeInsets.all(Dimens.padding),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 20.0),
                  child: Text(
                    Dictionary.forgotAccountDesc,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: FontsFamily.roboto,
                      fontSize: 16.0,
                      color: clr.Colors.greyText,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _buildButtonForgotAccount(
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                            NavigationConstrants.ForgotUsernameLogin,
                          );
                        },
                        title: 'Username',
                        imageIcon:
                            '${Environment.iconAssets}username_image.png'),
                    const SizedBox(
                      width: 20,
                    ),
                    _buildButtonForgotAccount(
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                            NavigationConstrants.ForgotPasswordLogin,
                          );
                        },
                        title: 'Kata Sandi',
                        imageIcon:
                            '${Environment.iconAssets}password_image.png'),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
              ],
            ),
          );
        });
  }

  _launchWhatsApp(String csPhoneNumber, String csInfoTex) async {
    String phoneNumber = '';
    try {
      bool isInstalled = await DeviceApps.isAppInstalled('com.whatsapp');
      phoneNumber = csPhoneNumber ?? Environment.csPhone;

      if (phoneNumber[0] == '0') {
        phoneNumber = phoneNumber.replaceFirst('0', '+62');
      }

      if (isInstalled) {
        String urlWhatsApp =
            'https://wa.me/${phoneNumber}?text=${Uri.encodeFull(csInfoTex)}%0A%0A';
        if (await canLaunch(urlWhatsApp)) {
          await launch(urlWhatsApp);

          await AnalyticsHelper.setLogEvent(
              Analytics.EVENT_OPEN_WA_ADMIN_CHANGE_PASSWORD);
        } else {
          await launch('tel://${phoneNumber}');
          await AnalyticsHelper.setLogEvent(
              Analytics.EVENT_OPEN_TELP_ADMIN_CHANGE_PASSWORD);
        }
      } else {
        await launch('tel://${phoneNumber}');
        await AnalyticsHelper.setLogEvent(
            Analytics.EVENT_OPEN_TELP_ADMIN_CHANGE_PASSWORD);
      }
    } catch (_) {
      await launch('tel://${phoneNumber}');
      await AnalyticsHelper.setLogEvent(
          Analytics.EVENT_OPEN_TELP_ADMIN_CHANGE_PASSWORD);
    }
  }

  @override
  void dispose() {
    _loginBloc.close();
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
