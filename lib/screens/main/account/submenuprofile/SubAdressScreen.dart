import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/account_profile/AccountProfileBloc.dart';
import 'package:sapawarga/blocs/account_profile/AccountProfileEvent.dart';
import 'package:sapawarga/blocs/account_profile/AccountProfileState.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/components/BuildTextField.dart';
import 'package:sapawarga/components/CustomAppBar.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/screens/main/account/subeditprofile/SubEditAddressScreen.dart';
import 'package:sapawarga/utilities/Validations.dart';

class SubAdressScreen extends StatefulWidget {
  SubAdressScreen({Key key}) : super(key: key);

  @override
  _SubAdressScreenState createState() => _SubAdressScreenState();
}

class _SubAdressScreenState extends State<SubAdressScreen> {
  UserInfoModel userInfoModel;
  final _addressController = TextEditingController();
  final _kabkotaController = TextEditingController();
  final _kecamatanController = TextEditingController();
  final _kelurahanController = TextEditingController();
  final _rtController = TextEditingController();
  final _rwController = TextEditingController();
  AuthenticationBloc _authenticationBloc;
  AccountProfileBloc _accountProfileBloc;
  var result;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final RefreshController _mainRefreshController = RefreshController();

  @override
  void initState() {
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: CustomAppBar().DefaultAppBar(title: 'Alamat', actions: <Widget>[
          GestureDetector(
            child: Container(
              padding: EdgeInsets.all(20),
              child: Text(
                'Ubah',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: FontsFamily.intro),
              ),
            ),
            onTap: () async {
              _navigateResult(context);
            },
          )
        ]),
        key: _scaffoldKey,
        body: Container(
            child: BlocProvider<AccountProfileBloc>(
          create: (context) => _accountProfileBloc = AccountProfileBloc.profile(
              authProfileRepository: AuthProfileRepository())
            ..add(AccountProfileLoad()),
          child: BlocListener<AccountProfileBloc, AccountProfileState>(
            bloc: _accountProfileBloc,
            listener: (context, state) {
              if (state is AccountProfileLoaded) {
                userInfoModel = state.record;

                _addressController.text = state.record.address;
                _kabkotaController.text = state.record.kabkota.name;
                _kecamatanController.text = state.record.kecamatan.name;
                _kelurahanController.text = state.record.kelurahan.name;
                _rtController.text = state.record.rt;
                _rwController.text = state.record.rw;

                setState(() {});
              } else if (state is AccountProfileFailure) {
                if (state.error.contains(Dictionary.errorUnauthorized)) {
                  _authenticationBloc.add(LoggedOut());
                  Navigator.of(context).pop();
                }
              }
            },
            child: BlocBuilder<AccountProfileBloc, AccountProfileState>(
              bloc: _accountProfileBloc,
              builder: (context, state) => SmartRefresher(
                  controller: _mainRefreshController,
                  enablePullDown: true,
                  header: WaterDropMaterialHeader(),
                  onRefresh: () async {
                    _accountProfileBloc.add(AccountProfileLoad());
                    _mainRefreshController.refreshCompleted();
                  },
                  child: state is AccountProfileLoading
                      ? _buildLoading()
                      : state is AccountProfileLoaded
                          ? state.record != null
                              ? _buildContent(state)
                              : EmptyData(
                                  message: Dictionary.emptyDataRWActivity)
                          : state is AccountProfileFailure
                              ? ErrorContent(error: state.error)
                              : _buildLoading()),
            ),
          ),
        )),
      ),
    );
  }

  _buildLoading() {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(10),
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              const SizedBox(height: 15),
              Skeleton(width: MediaQuery.of(context).size.width, height: 20.0),
              Divider(),
              const SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              const SizedBox(height: 15),
              Skeleton(width: MediaQuery.of(context).size.width, height: 20.0),
              Divider(),
              const SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              const SizedBox(height: 15),
              Skeleton(width: MediaQuery.of(context).size.width, height: 20.0),
              Divider(),
              const SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              const SizedBox(height: 15),
              Skeleton(width: MediaQuery.of(context).size.width, height: 20.0),
              Divider(),
              const SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              const SizedBox(height: 15),
              Skeleton(width: MediaQuery.of(context).size.width, height: 20.0),
              Divider(),
            ],
          ),
        )
      ],
    );
  }

  Future<bool> _onWillPop() async {
    Navigator.pop(context, result);
    return false;
  }

  _navigateResult(BuildContext context) async {
    result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SubEditAddressScreen(authUserInfo: userInfoModel),
      ),
    );
    _accountProfileBloc.add(AccountProfileLoad());
    if (result == Dictionary.successSaveProfile) {
      await showDialog(
        context: context,
        builder: (BuildContext context) => DialogTextOnly(
          title: Dictionary.congratulation,
          description: Dictionary.successSaveProfile,
          buttonText: Dictionary.ok,
          onOkPressed: () {
            Navigator.of(context).pop(); // To close the dialog
          },
        ),
      );
    }
  }

  _buildContent(AccountProfileLoaded state) {
    return ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    const SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.fullAddress,
                      hintText: Dictionary.placeHolderAddress,
                      controller: _addressController,
                      validation: Validations.addressValidation,
                      textInputType: null,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    const SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.kabkota,
                      hintText: '',
                      controller: _kabkotaController,
                      validation: null,
                      textInputType: null,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    const SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.kecamatan,
                      hintText: '',
                      controller: _kecamatanController,
                      validation: null,
                      textInputType: null,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    const SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.kelurahan,
                      hintText: '',
                      controller: _kelurahanController,
                      validation: null,
                      textInputType: null,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    const SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.rt.toUpperCase(),
                      hintText: Dictionary.placeHolderRt,
                      controller: _rtController,
                      validation: Validations.rtValidation,
                      textInputType: TextInputType.number,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    const SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.rw.toUpperCase(),
                      hintText: '',
                      controller: _rwController,
                      validation: null,
                      textInputType: TextInputType.number,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    const SizedBox(height: 20),
                  ],
                ),
              ),
            )
          ],
        ),
      ],
    );
  }
}
