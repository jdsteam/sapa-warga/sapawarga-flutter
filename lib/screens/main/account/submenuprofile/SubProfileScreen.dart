import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/account_profile/AccountProfileBloc.dart';
import 'package:sapawarga/blocs/account_profile/AccountProfileEvent.dart';
import 'package:sapawarga/blocs/account_profile/AccountProfileState.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/components/BuildTextField.dart';
import 'package:sapawarga/components/CustomAppBar.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/screens/main/account/subeditprofile/SubEditProfileScreen.dart';
import 'package:sapawarga/utilities/Validations.dart';

class SubProfileScreen extends StatefulWidget {
  SubProfileScreen({Key key}) : super(key: key);

  @override
  _SubProfileScreenState createState() => _SubProfileScreenState();
}

class _SubProfileScreenState extends State<SubProfileScreen> {
  UserInfoModel userInfoModel;
  final _nameController = TextEditingController();
  final _nikController = TextEditingController();
  final _birthDateController = TextEditingController();
  final _educationLevelIdController = TextEditingController();
  final _jobIdController = TextEditingController();
  final _usernameController = TextEditingController();
  String urlImageSK = '';
  AuthenticationBloc _authenticationBloc;
  AccountProfileBloc _accountProfileBloc;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  var result;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final RefreshController _mainRefreshController = RefreshController();

  @override
  void initState() {
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: CustomAppBar().DefaultAppBar(title: 'Profil', actions: <Widget>[
          GestureDetector(
            child: Container(
              padding: const EdgeInsets.all(20),
              child: Text(
                Dictionary.change,
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: FontsFamily.intro),
              ),
            ),
            onTap: () async {
              _navigateResult(context);
            },
          )
        ]),
        key: _scaffoldKey,
        body: Container(
            child: BlocProvider<AccountProfileBloc>(
          create: (context) => _accountProfileBloc = AccountProfileBloc.profile(
              authProfileRepository: AuthProfileRepository())
            ..add(AccountProfileLoad()),
          child: BlocListener<AccountProfileBloc, AccountProfileState>(
            bloc: _accountProfileBloc,
            listener: (context, state) {
              if (state is AccountProfileLoaded) {
                userInfoModel = state.record;

                _nameController.text = state.record.name;
                _nikController.text = state.record.nik;
                _birthDateController.text = state.record.birthDate != null
                    ? DateFormat('dd/MM/yyyy', 'id')
                        .format(state.record.birthDate)
                    : null;
                _educationLevelIdController.text =
                    state.record.educationLevel != null
                        ? state.record.educationLevel.title.toString()
                        : '';
                _jobIdController.text = state.record.jobType != null
                    ? state.record.jobType.title.toString()
                    : '';
                _usernameController.text = state.record.username;
                urlImageSK = state.record.fileSKUrl ?? '';

                setState(() {});
              } else if (state is AccountProfileFailure) {
                if (state.error.contains(Dictionary.errorUnauthorized)) {
                  _authenticationBloc.add(LoggedOut());
                  Navigator.of(context).pop();
                }
              }
            },
            child: BlocBuilder<AccountProfileBloc, AccountProfileState>(
              bloc: _accountProfileBloc,
              builder: (context, state) => SmartRefresher(
                  controller: _mainRefreshController,
                  enablePullDown: true,
                  header: WaterDropMaterialHeader(),
                  onRefresh: () async {
                    _accountProfileBloc.add(AccountProfileLoad());
                    _mainRefreshController.refreshCompleted();
                  },
                  child: state is AccountProfileLoading
                      ? _buildLoading()
                      : state is AccountProfileLoaded
                          ? state.record != null
                              ? _buildContent(state)
                              : EmptyData(
                                  message: Dictionary.emptyDataRWActivity)
                          : state is AccountProfileFailure
                              ? ErrorContent(error: state.error)
                              : _buildLoading()),
            ),
          ),
        )),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    Navigator.pop(context, result);
    return false;
  }

  _buildLoading() {
    final Size size = MediaQuery.of(context).size;
    return ListView(
      children: <Widget>[
        Container(
          padding: const EdgeInsets.all(10),
          width: size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              const SizedBox(height: 15),
              Skeleton(width: size.width, height: 20.0),
              Divider(),
              const SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              const SizedBox(height: 15),
              Skeleton(width: size.width, height: 20.0),
              Divider(),
              const SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              const SizedBox(height: 15),
              Skeleton(width: size.width, height: 20.0),
              Divider(),
              const SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              const SizedBox(height: 15),
              Skeleton(width: size.width, height: 20.0),
              Divider(),
              const SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              const SizedBox(height: 15),
              Skeleton(width: size.width, height: 20.0),
              Divider(),
              const SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              const SizedBox(height: 15),
              Skeleton(width: size.width, height: 20.0),
              Divider(),
              const SizedBox(height: 15),
              Skeleton(width: 50, height: 20.0),
              const SizedBox(height: 15),
              Skeleton(
                child: Container(
                  margin: const EdgeInsets.only(top: 5.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(25.0),
                    child: Container(
                        height: size.height / 3.5, color: Colors.grey[300]),
                  ),
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  _navigateResult(BuildContext context) async {
    result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SubEditProfileScreen(authUserInfo: userInfoModel),
      ),
    );
    _accountProfileBloc.add(AccountProfileLoad());
    if (result == Dictionary.successSaveProfile) {
      await showDialog(
        context: context,
        builder: (BuildContext context) => DialogTextOnly(
          title: Dictionary.congratulation,
          description: Dictionary.successSaveProfile,
          buttonText: Dictionary.ok,
          onOkPressed: () {
            Navigator.of(context).pop(); // To close the dialog
          },
        ),
      );
    }
  }

  _buildContent(AccountProfileLoaded state) {
    final Size size = MediaQuery.of(context).size;

    return ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    const SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.name,
                      hintText: Dictionary.placeHolderName,
                      controller: _nameController,
                      validation: Validations.nameValidation,
                      textInputType: null,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    const SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.nik,
                      hintText: Dictionary.placeHolderNik,
                      controller: _nikController,
                      validation: Validations.nikValidation,
                      textInputType: TextInputType.number,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    const SizedBox(height: 20),
                    InkWell(
                      child: BuildTextField(
                        title: Dictionary.birthDate,
                        hintText: Dictionary.placeHolderBirthday,
                        controller: _birthDateController,
                        validation: null,
                        textInputType: null,
                        textStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        isEdit: false,
                      ),
                      onTap: showDatePicker,
                    ),
                    const SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.education,
                      hintText: Dictionary.placeHolderEducation,
                      controller: _educationLevelIdController,
                      validation: null,
                      textInputType: null,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    const SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.job,
                      hintText: Dictionary.placeHolderJob,
                      controller: _jobIdController,
                      validation: null,
                      textInputType: null,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    const SizedBox(height: 20),
                    BuildTextField(
                      title: Dictionary.username,
                      hintText: Dictionary.placeHolderUsername,
                      controller: _usernameController,
                      validation: Validations.usernameValidation,
                      textInputType: null,
                      textStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      isEdit: false,
                    ),
                    const SizedBox(height: 20),
                    userInfoModel.roleLabel == "RW"
                        ? Column(
                            children: [
                              Container(
                                margin:
                                    const EdgeInsets.only(left: 20, right: 20),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  Dictionary.fileSk,
                                  style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              const SizedBox(height: 20),
                              Container(
                                padding:
                                    const EdgeInsets.only(left: 20, right: 20),
                                width: size.width,
                                height: 200,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(8.0),
                                  child: CachedNetworkImage(
                                    useOldImageOnUrlChange: true,
                                    imageUrl: urlImageSK,
                                    fit: BoxFit.fitWidth,
                                    placeholder: (context, url) => Center(
                                        heightFactor: 8.2,
                                        child: CupertinoActivityIndicator()),
                                    errorWidget: (context, url, error) => Container(
                                        height: size.height / 3.3,
                                        color: Colors.grey[200],
                                        child: Image.asset(
                                            '${Environment.imageAssets}placeholder.png',
                                            fit: BoxFit.fitWidth)),
                                  ),
                                ),
                              ),
                              const SizedBox(height: 20),
                            ],
                          )
                        : Container()
                  ],
                ),
              ),
            )
          ],
        ),
      ],
    );
  }
}
