import 'dart:convert';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/send_otp_change_username/Bloc.dart';
import 'package:sapawarga/components/BlockCircleLoading.dart';
import 'package:sapawarga/components/CustomAppBar.dart';
import 'package:sapawarga/components/DialogConfirmation.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/DialogWidgetContent.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/Validations.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:sapawarga/utilities/WhatsAppHelper.dart';

class UpdateUsernameScreen extends StatefulWidget {
  UpdateUsernameScreen({Key key}) : super(key: key);

  @override
  _UpdateUsernameScreenState createState() => _UpdateUsernameScreenState();
}

class _UpdateUsernameScreenState extends State<UpdateUsernameScreen> {
  SendOtpBloc _sendOtpBloc;
  final _userNameTextController = TextEditingController();
  final _phoneTextController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  List<dynamic> csPhoneNumbers;
  bool showDesc = true;
  dynamic csInfoText;
  bool _isLoading = false;

  @override
  void initState() {
    csPhoneNumbers = json.decode(FirebaseConfig.callCenterNumbersValue);
    csInfoText = Dictionary.helpAdminWA;
    super.initState();
  }

  Future<RemoteConfig> _initializeRemoteConfig() async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    await remoteConfig.setDefaults(<String, dynamic>{
      FirebaseConfig.callCenterNumbersKey: FirebaseConfig.callCenterNumbersValue
    });

    try {
      await remoteConfig.fetch(expiration: Duration(minutes: 30));
      await remoteConfig.activateFetched();
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }

    return remoteConfig;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: KeyboardDismissOnTap(
        child: Scaffold(
          appBar: CustomAppBar().DefaultAppBar(title: Dictionary.appName),
          backgroundColor: Colors.white,
          body: BlocProvider<SendOtpBloc>(
            create: (BuildContext context) => _sendOtpBloc =
                SendOtpBloc(authProfileRepository: AuthProfileRepository()),
            child: BlocListener<SendOtpBloc, SendOtpState>(
              bloc: _sendOtpBloc,
              listener: (context, state) {
                if (state is SendOtpLoading) {
                  _isLoading = true;
                  blockCircleLoading(context: context);
                } else if (state is SendOtpSuccess) {
                  if (_isLoading) {
                    _isLoading = false;
                    Navigator.of(context, rootNavigator: true).pop();
                  }

                  AnalyticsHelper.setLogEvent(
                      Analytics.EVENT_SUCCESS_SEND_OTP_CHANGE_USERNAME);

                  _sendOtpDialogSuccess();
                } else if (state is SendOtpFailure) {
                  if (_isLoading) {
                    _isLoading = false;
                    Navigator.of(context, rootNavigator: true).pop();
                  }

                  AnalyticsHelper.setLogEvent(
                      Analytics.EVENT_FAILED_SEND_OTP_CHANGE_USERNAME,
                      <String, dynamic>{
                        'message': '${state.error.toString()}'
                      });

                  showDialog(
                      context: context,
                      builder: (BuildContext context) => DialogTextOnly(
                            description: state.error.toString().isNotEmpty
                                ? state.error.toString()
                                : Dictionary.failedSendOtp,
                            buttonText: Dictionary.ok,
                            onOkPressed: () {
                              Navigator.of(context, rootNavigator: true)
                                  .pop(); // To close the dialog
                            },
                          ));
                } else if (state is ValidationSendOtpError) {
                  if (_isLoading) {
                    _isLoading = false;
                    Navigator.of(context, rootNavigator: true).pop();
                  }

                  if (state.errors.toString().contains('generic') &&
                      state.errors['generic'][0].toString() ==
                          Dictionary.infoOtpSend) {
                    _sendOtpDialogSuccess();
                  } else {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) => DialogTextOnly(
                              description: state.errors
                                      .toString()
                                      .contains('username')
                                  ? state.errors['username'][0].toString()
                                  : state.errors.toString().contains('phone')
                                      ? state.errors['phone'][0].toString()
                                      : state.errors
                                              .toString()
                                              .contains('generic')
                                          ? state.errors['generic'][0]
                                          : state.errors.toString(),
                              buttonText: Dictionary.ok,
                              onOkPressed: () {
                                Navigator.of(context, rootNavigator: true)
                                    .pop(); // To close the dialog
                              },
                            ));
                  }
                } else {
                  if (_isLoading) {
                    _isLoading = false;
                    Navigator.of(context, rootNavigator: true).pop();
                  }
                }
              },
              child: BlocBuilder<SendOtpBloc, SendOtpState>(
                bloc: _sendOtpBloc,
                builder: (context, state) => Container(
                  child: Form(
                    key: _formKey,
                    child: ListView(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: const EdgeInsets.all(20),
                          child: Column(
                            children: [
                              const SizedBox(height: 10),
                              Text(
                                Dictionary.updateUsernameTitle,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w600,
                                    fontFamily: FontsFamily.roboto),
                              ),
                              const SizedBox(height: 20),
                              KeyboardVisibilityBuilder(
                                builder: (context, visible) {
                                  return !visible
                                      ? Text(
                                          Dictionary.updateUsernameDesc,
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontSize: 14.0,
                                              fontFamily: FontsFamily.roboto,
                                              color: clr.Colors.darkGrey),
                                        )
                                      : Container();
                                },
                              ),
                              buildTextField(
                                  title: Dictionary.username,
                                  hintText: Dictionary.inputNewUsername,
                                  controller: _userNameTextController,
                                  // focusNode: _focusUsernameFieldText,
                                  validation: Validations.usernameValidation,
                                  isEdit: true),
                              buildTextField(
                                  title: Dictionary.inputWhatsappNumber,
                                  hintText: Dictionary.inputYourWhatsappNumber,
                                  controller: _phoneTextController,
                                  textInputType: TextInputType.number,
                                  // focusNode: _focusPhoneFieldText,
                                  validation:
                                      Validations.whatsappNumberValidation,
                                  isEdit: true),
                              const SizedBox(height: 40),
                              RaisedButton(
                                  color: clr.Colors.darkblue,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  child: Container(
                                    alignment: Alignment.center,
                                    width: MediaQuery.of(context).size.width,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 13),
                                    child: Text(
                                      Dictionary.updateNow,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: FontsFamily.roboto),
                                    ),
                                  ),
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      _sendOtpBloc.add(SendOtpChangedUsername(
                                          username:
                                              _userNameTextController.text,
                                          phone: _phoneTextController.text));
                                    }
                                  }),
                            ],
                          ),
                        ),
                        const SizedBox(height: 10),
                        Container(
                          height: 1,
                          width: MediaQuery.of(context).size.width,
                          color: clr.Colors.grey,
                        ),
                        const SizedBox(height: 25),
                        FutureBuilder<RemoteConfig>(
                          future: _initializeRemoteConfig(),
                          builder: (BuildContext context,
                              AsyncSnapshot<RemoteConfig> snapshot) {
                            if (snapshot.hasData) {
                              final dataPhoneNumbers = snapshot.data.getString(
                                  FirebaseConfig.callCenterNumbersKey);
                              if (dataPhoneNumbers != null) {
                                csPhoneNumbers = json.decode(dataPhoneNumbers);
                              }
                              try {
                                final dataInfoText = snapshot.data.getString(
                                    FirebaseConfig.callCenterTextInfoKey);
                                if (dataInfoText != null) {
                                  csInfoText = json.decode(dataInfoText);
                                }
                              } catch (e) {
                                print(e.toString());
                              }
                            }

                            return Container(
                              margin:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: RaisedButton(
                                  color: clr.Colors.lightGreen,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                      side: BorderSide(
                                          width: 2,
                                          color: clr.Colors.darkGrey)),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 13),
                                    child: Text(
                                      Dictionary.needHelp,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontFamily: FontsFamily.roboto),
                                    ),
                                  ),
                                  onPressed: () {
                                    csPhoneNumbers.length > 1
                                        ? _openDialogContact()
                                        : WhatsAppHelper.checkNumber(
                                            csPhoneNumbers[0], csInfoText);
                                  }),
                            );
                          },
                        ),
                        const SizedBox(height: 25),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return DialogConfirmation(
          title: Dictionary.confirm,
          body: Dictionary.confirmExitRegister,
          buttonOkText: Dictionary.yes,
          onOkPressed: () async {
            Navigator.pop(context, false);
            Navigator.pop(context, false);
          },
          buttonCancelText: Dictionary.no,
        );
      },
    );
    return false;
  }

  Widget buildTextField(
      {String title,
      TextEditingController controller,
      String hintText,
      validation,
      TextInputType textInputType,
      TextStyle textStyle,
      bool isEdit,
      int maxLines,
      FocusNode focusNode}) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const SizedBox(
            height: 10,
          ),
          Row(
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                    fontSize: 12.0,
                    color: clr.Colors.veryDarkGrey,
                    fontFamily: FontsFamily.roboto,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          TextFormField(
            maxLines: maxLines != null ? maxLines : 1,
            style: isEdit
                ? TextStyle(
                    color: Colors.black,
                    fontFamily: FontsFamily.roboto,
                    fontSize: 14)
                : TextStyle(
                    color: clr.Colors.disableText,
                    fontFamily: FontsFamily.roboto,
                    fontSize: 14),
            enabled: isEdit,
            focusNode: focusNode,
            validator: validation,
            inputFormatters: textInputType == TextInputType.number
                ? [FilteringTextInputFormatter.digitsOnly]
                : null,
            textCapitalization: TextCapitalization.words,
            controller: controller,
            decoration: InputDecoration(
              errorMaxLines: 2,
              fillColor: clr.Colors.greyContainer,
              filled: true,
              hintText: hintText,
              hintStyle: TextStyle(
                  color: clr.Colors.netralGrey,
                  fontFamily: FontsFamily.roboto,
                  fontSize: 12),
              errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Colors.red, width: 1.5)),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide:
                      BorderSide(color: clr.Colors.greyBorder, width: 1.5)),
              disabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide:
                      BorderSide(color: clr.Colors.greyBorder, width: 1.5)),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide:
                      BorderSide(color: clr.Colors.greyBorder, width: 1.5)),
            ),
            keyboardType: textInputType ?? TextInputType.text,
          )
        ],
      ),
    );
  }

  _openDialogContact() {
    showDialog(
        context: context,
        builder: (BuildContext context) => DialogWidgetContent(
              title: Dictionary.callCenterList,
              child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount:
                    csPhoneNumbers.length > 5 ? 5 : csPhoneNumbers.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    child: Container(
                      margin:
                          index != 0 ? const EdgeInsets.only(top: 10.0) : null,
                      padding: const EdgeInsets.fromLTRB(
                          Dimens.padding, 10.0, Dimens.padding, 10.0),
                      decoration: BoxDecoration(
                        color: Color(0xffebf8ff),
                        shape: BoxShape.rectangle,
                        border: Border.all(color: clr.Colors.blue),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Wrap(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: Image.asset(
                                '${Environment.iconAssets}whatsapp.png',
                                width: 20,
                                height: 20,
                                color: clr.Colors.green),
                          ),
                          Text(csPhoneNumbers[index],
                              style: TextStyle(
                                  fontFamily: FontsFamily.sourceSansPro,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w600,
                                  color: clr.Colors.green))
                        ],
                      ),
                    ),
                    onTap: () {
                      WhatsAppHelper.checkNumber(
                          csPhoneNumbers[index], csInfoText);
                      Navigator.of(context).pop();
                    },
                  );
                },
              ),
              buttonText: Dictionary.close.toUpperCase(),
              onOkPressed: () {
                Navigator.of(context).pop(); // To close the dialog
              },
            ));
  }

  _updateComplete() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => WillPopScope(
            child: DialogTextOnly(
              crossAxisAlignment: CrossAxisAlignment.center,
              textAlign: TextAlign.center,
              isRaisedButton: true,
              title: Dictionary.usernameHasbeenUpdatedTitle,
              buttonText: Dictionary.okUnderstand,
              onOkPressed: () {
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
              description: '',
            ),
            onWillPop: () {
              Navigator.of(context).popUntil((route) => route.isFirst);
              return Future.value(true);
            }));
  }

  _sendOtpDialogSuccess() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => WillPopScope(
            child: DialogTextOnly(
              crossAxisAlignment: CrossAxisAlignment.center,
              textAlign: TextAlign.center,
              descriptionColor: clr.Colors.darkGrey,
              isRaisedButton: true,
              title: Dictionary.sendVerificationCode,
              description: Dictionary.sendVerificationCodeDesc +
                  ' ' +
                  _phoneTextController.text,
              buttonText: Dictionary.okUnderstand,
              onOkPressed: () {
                verificationPage();
              },
            ),
            onWillPop: () {
              verificationPage();
              return Future.value(true);
            }));
  }

  verificationPage() async {
    var result = await Navigator.of(context)
        .pushNamed(NavigationConstrants.VerificationCode, arguments: [
      _userNameTextController.text,
      _phoneTextController.text,
    ]);
    if (result) {
      Navigator.of(context, rootNavigator: true).pop();
      _updateComplete();
    } else {
      Navigator.of(context, rootNavigator: true).pop();
    }
  }

  @override
  void dispose() {
    _userNameTextController.dispose();
    _phoneTextController.dispose();
    _sendOtpBloc.close();
    super.dispose();
  }
}
