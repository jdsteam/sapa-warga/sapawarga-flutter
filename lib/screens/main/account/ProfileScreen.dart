import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:package_info/package_info.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/account_profile/AccountProfileBloc.dart';
import 'package:sapawarga/blocs/account_profile/AccountProfileEvent.dart';
import 'package:sapawarga/blocs/account_profile/AccountProfileState.dart';
import 'package:sapawarga/blocs/account_profile/Bloc.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/gamifications/gamification_my_badge/Bloc.dart';
import 'package:sapawarga/components/BrowserScreen.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as color;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/constants/UrlThirdParty.dart';
import 'package:sapawarga/enums/ChangePasswordType.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/GamificationsRepository.dart';
import 'package:sapawarga/repositories/HumasJabarRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/ImagePickerHelper.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';

class ProfileScreen extends StatelessWidget {
  ProfileScreen({Key key}) : super(key: key);

  final AuthProfileRepository authProfileRepository = AuthProfileRepository();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AccountProfileBloc>(
        create: (context) => AccountProfileBloc.profile(
            authProfileRepository: authProfileRepository),
        child: _Profile());
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<AuthProfileRepository>(
        'authProfileRepository', authProfileRepository));
  }
}

class _Profile extends StatefulWidget {
  @override
  __ProfileState createState() => __ProfileState();
}

class __ProfileState extends State<_Profile> {
  final RefreshController _mainRefreshController = RefreshController();
  final GamificationsRepository gamificationsRepository =
      GamificationsRepository();

  AccountProfileBloc _accountProfileBloc;
  AccountProfileEditBloc _blocProfile;
  AuthenticationBloc _authenticationBloc;
  GamificationsMyBadgeBloc _gamificationsMyBadgeBloc;
  AccountProfileLoaded stateLoadProfile;
  String _versionText = Dictionary.version;
  ProgressDialog loadingDialog;
  ImagePickerHelper _imagePickerHelper;

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.ACCOUNT);
    AnalyticsHelper.setLogEvent(Analytics.EVENT_DETAIL_ACCOUNT);
    _imagePickerHelper = ImagePickerHelper(context);
    _imagePickerHelper.streamController.stream.listen((event) {
      setState(() {
        _blocProfile.add(AccountProfileEditPhotoSubmit(
            image: event, userInfoModel: stateLoadProfile.record));
      });
    });

    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(Dictionary.profile,
              style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: FontsFamily.intro),
              maxLines: 1,
              overflow: TextOverflow.ellipsis),
        ),
        body: MultiBlocProvider(
            providers: [
              BlocProvider<AccountProfileBloc>(
                create: (context) =>
                    _accountProfileBloc = AccountProfileBloc.profile(
                        authProfileRepository: AuthProfileRepository())
                      ..add(AccountProfileLoad()),
              ),
              BlocProvider<AccountProfileEditBloc>(
                create: (context) => _blocProfile = AccountProfileEditBloc(
                    authProfileRepository: AuthProfileRepository()),
              ),
              BlocProvider<GamificationsMyBadgeBloc>(
                create: (context) => _gamificationsMyBadgeBloc =
                    GamificationsMyBadgeBloc(
                        gamificationsRepository: gamificationsRepository),
              ),
            ],
            child: MultiBlocListener(
              listeners: [
                BlocListener<AccountProfileBloc, AccountProfileState>(
                    bloc: _accountProfileBloc,
                    listener: (context, state) {
                      if (state is AccountProfileLoaded) {
                        _gamificationsMyBadgeBloc
                            .add(GamificationsMyBadgeLoad(page: 1));
                      }
                    }),
                BlocListener<AccountProfileEditBloc, AccountProfileEditState>(
                    bloc: _blocProfile,
                    listener: (context, state) {
                      if (state is AccountProfileEditFailure) {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) => DialogTextOnly(
                                  description: state.error,
                                  buttonText: "OK",
                                  onOkPressed: () {
                                    Navigator.of(context)
                                        .pop(); // To close the dialog
                                  },
                                ));
                      }

                      if (state is AccountProfileEditValidationError) {
                        if (state.errors.containsKey('email')) {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) => DialogTextOnly(
                                    description:
                                        state.errors['email'][0].toString(),
                                    buttonText: "OK",
                                    onOkPressed: () {
                                      Navigator.of(context)
                                          .pop(); // To close the dialog
                                    },
                                  ));
                        }
                      }

                      if (state is AccountProfileEditPhotoLoading) {
                        _onWidgetDidBuild(() {
                          showLoading();
                        });
                      }

                      if (state is AccountProfileEditPhotoUpdated) {
                        hideLoading();
                        _onWidgetDidBuild(() {
                          showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Text(Dictionary.updatePhotoTitle),
                                  content: Text(Dictionary.successSavePhoto),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text(Dictionary.ok),
                                      onPressed: () {
                                        _accountProfileBloc
                                            .add(AccountProfileChanged());
                                        Navigator.pop(context);
                                      },
                                    ),
                                  ],
                                );
                              });
                        });
                      }
                    })
              ],
              child: BlocBuilder<AccountProfileBloc, AccountProfileState>(
                  bloc: _accountProfileBloc,
                  builder: (context, state) {
                    return SmartRefresher(
                        controller: _mainRefreshController,
                        enablePullDown: true,
                        header: WaterDropMaterialHeader(),
                        onRefresh: () async {
                          _accountProfileBloc.add(AccountProfileChanged());
                          _mainRefreshController.refreshCompleted();
                        },
                        child: Container(
                            child: state is AccountProfileLoading
                                ? _buildLoading()
                                : state is AccountProfileLoaded
                                    ? _buildContent(state)
                                    : state is AccountProfileFailure
                                        ? _buildFailure(state)
                                        : Container()));
                  }),
            )));
  }

  _buildLoadingMyBadge() {
    return Skeleton(
      child: Container(
        padding: const EdgeInsets.only(top: 10),
        child: Row(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(left: 10, right: 10),
              height: 86,
              width: MediaQuery.of(context).size.width / 4,
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(18),
                      bottomLeft: Radius.circular(18))),
              child: Column(
                children: <Widget>[
                  Text(
                    Dictionary.missionDone,
                    style: TextStyle(
                        color: Colors.grey[600],
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600),
                    textAlign: TextAlign.left,
                  ),
                  Text(
                    '0',
                    style: TextStyle(
                        color: Colors.grey[600],
                        fontSize: 45.0,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(right: 10),
              height: 86,
              width: MediaQuery.of(context).size.width / 1.55,
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(8),
                      bottomRight: Radius.circular(8))),
              child: Column(
                children: <Widget>[
                  Text(
                    Dictionary.missionDone,
                    style: TextStyle(
                        color: Colors.grey[600],
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600),
                    textAlign: TextAlign.left,
                  ),
                  Text(
                    '0',
                    style: TextStyle(
                        color: Colors.grey[600],
                        fontSize: 45.0,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildMyBadge() {
    return BlocBuilder<GamificationsMyBadgeBloc, GamificationsMyBadgeState>(
      bloc: _gamificationsMyBadgeBloc,
      builder: (context, state) => state is GamificationsMyBadgeLoaded
          ? _buildMyBadgeContet(state)
          : state is GamificationsMyBadgeLoading
              ? _buildLoadingMyBadge()
              : state is GamificationsMyBadgeFailure
                  ? Text(state.error)
                  : Container(),
    );
  }

  _buildMyBadgeContet(GamificationsMyBadgeLoaded state) {
    return Container(
      padding: const EdgeInsets.only(top: 10),
      child: Row(children: <Widget>[
        Container(
          margin: const EdgeInsets.only(right: 10, left: 10),
          child: Stack(
            children: <Widget>[
              Image.asset(
                  '${Environment.imageAssets}profile_mission_background1.png',
                  height: 90,
                  fit: BoxFit.fitWidth),
              Positioned(
                  bottom: 0,
                  top: 10,
                  left: 5,
                  right: 0,
                  child: Column(
                    children: <Widget>[
                      Text(
                        Dictionary.missionDone,
                        style: TextStyle(
                            color: Colors.grey[600],
                            fontSize: 16.0,
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.left,
                      ),
                      Text(
                        state.records.meta.totalCount.toString(),
                        style: TextStyle(
                            color: Colors.grey[600],
                            fontSize: 45.0,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ))
            ],
          ),
        ),
        Stack(
          children: <Widget>[
            Image.asset(
                '${Environment.imageAssets}profile_mission_background2.png',
                height: 90,
                width: MediaQuery.of(context).size.width - 150,
                fit: BoxFit.fitWidth),
            Positioned(
              bottom: 0,
              top: 10,
              left: 5,
              right: 0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(
                      Dictionary.badge,
                      style: TextStyle(
                          color: Colors.grey[600],
                          fontSize: 16.0,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Expanded(
                      child: Container(
                    alignment: Alignment.topLeft,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          padding: const EdgeInsets.all(10),
                          child: CachedNetworkImage(
                            imageUrl: state.records.items[index].gamification
                                .imageBadgePathUrl,
                            fit: BoxFit.fill,
                            width: 30,
                            placeholder: (context, url) =>
                                Center(child: CupertinoActivityIndicator()),
                            errorWidget: (context, url, error) => Container(
                                height:
                                    MediaQuery.of(context).size.height / 2.5,
                                width: MediaQuery.of(context).size.width / 3.3,
                                color: Colors.grey[200],
                                child: Image.asset(
                                    '${Environment.imageAssets}placeholder.png',
                                    fit: BoxFit.fitWidth)),
                          ),
                        );
                      },
                      itemCount: state.records.items.length,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                    ),
                  ))
                ],
              ),
            )
          ],
        ),
      ]),
    );
  }

  _buildLoading() {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height / 5.5,
            child: Stack(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height * 0.1,
                  child: Skeleton(
                    width: MediaQuery.of(context).size.width,
                  ),
                ),
                Positioned(
                  top: 15,
                  left: Dimens.padding,
                  child: Container(
                    width: 97,
                    height: 97,
                    child: CircleAvatar(
                      minRadius: 90,
                      maxRadius: 150,
                      backgroundImage:
                          ExactAssetImage('${Environment.imageAssets}user.png'),
                    ),
                  ),
                ),
                Positioned(
                  top: 95,
                  bottom: 2.0,
                  left: 130.0,
                  right: Dimens.padding,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 5.5,
                    child: Row(
                      children: <Widget>[
                        Skeleton(
                          child: Container(
                            margin: const EdgeInsets.only(right: 24.0),
                            height: 30.0,
                            width: 24.0,
                            child: Icon(
                              FontAwesomeIcons.instagram,
                              size: 27,
                            ),
                          ),
                        ),
                        Skeleton(
                            child: Container(
                          margin: const EdgeInsets.only(right: 24.0),
                          height: 30.0,
                          width: 24.0,
                          child: Icon(
                            FontAwesomeIcons.facebookSquare,
                            size: 27,
                          ),
                        )),
                        Skeleton(
                            child: Container(
                          margin: const EdgeInsets.only(right: 24.0),
                          height: 30.0,
                          width: 24.0,
                          child: Icon(
                            FontAwesomeIcons.twitter,
                            size: 27,
                          ),
                        )),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          _buildLoadingMyBadge(),
          Container(
              padding: const EdgeInsets.all(Dimens.padding),
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  _buildButtonProfileLoading(
                    '${Environment.iconAssets}icon_profile.png',
                    Dictionary.profile,
                    18,
                    null,
                  ),
                  _buildButtonProfileLoading(
                      '${Environment.iconAssets}icon_contact.png',
                      Dictionary.contact,
                      14,
                      13),
                  _buildButtonProfileLoading(
                      '${Environment.iconAssets}icon_address.png',
                      Dictionary.address,
                      21,
                      7),
                  _buildButtonProfileLoading(
                      '${Environment.iconAssets}icon_mission.png',
                      Dictionary.mission,
                      18,
                      null),
                  _buildButtonProfileLoading(
                      '${Environment.iconAssets}icon_privacy_policy.png',
                      Dictionary.privacyPolicy,
                      18,
                      null),
                  _buildButtonProfileLoading(
                      '${Environment.iconAssets}icon_app_version.png',
                      Dictionary.appVersion,
                      18,
                      null),
                  _buildButtonProfileLoading(
                      '${Environment.iconAssets}icon_key_password.png',
                      Dictionary.changePasswordProfile,
                      18,
                      null),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ))
        ],
      ),
    );
  }

  _buildContent(AccountProfileLoaded state) {
    stateLoadProfile = state;

    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        _versionText = packageInfo.version != null
            ? packageInfo.version.contains('-staging')
                ? packageInfo.version.replaceFirst('-staging', '')
                : packageInfo.version.replaceFirst('-production', '')
            : Dictionary.version;
      });
    });

    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            _buildHeader(state),
            state.record.roleLabel == 'RW' ? _buildMyBadge() : Container(),
            _buildListForm(state),
          ],
        ),
      ),
    );
  }

  _buildFailure(AccountProfileFailure state) {
    return ErrorContent(error: state.error);
  }

  Widget _buildHeader(AccountProfileLoaded state) {
    return Container(
      height: MediaQuery.of(context).size.height / 5.6,
      child: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * 0.11,
          ),
          Positioned(
              top: Dimens.padding,
              left: Dimens.padding,
              child: Row(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        width: 98,
                        height: 98,
                        child: CircleAvatar(
                          minRadius: 90,
                          maxRadius: 150,
                          backgroundImage: state.record.photoUrl != null
                              ? NetworkImage(state.record.photoUrl)
                              : ExactAssetImage(
                                  '${Environment.imageAssets}user.png'),
                        ),
                      ),
                      Positioned(
                          right: -24,
                          bottom: 0,
                          child: GestureDetector(
                            child: Container(
                              margin: const EdgeInsets.only(right: 24.0),
                              height: 24.0,
                              width: 24.0,
                              child: Image.asset(
                                  '${Environment.iconAssets}icon_addImage.png'),
                            ),
                            onTap: () {
                              // _cameraBottomSheet(context);
                              _imagePickerHelper.openDialog();
                            },
                          ))
                    ],
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: const EdgeInsets.only(left: 10, bottom: 40),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          state.record.name,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600),
                          textAlign: TextAlign.left,
                        ),
                        const SizedBox(height: 5),
                        Text(
                          "RW " +
                              state.record.rw +
                              ", " +
                              state.record.kecamatan.name +
                              " " +
                              state.record.kelurahan.name,
                          style: TextStyle(color: Colors.grey, fontSize: 14.0),
                          textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                  )
                ],
              )),
          Positioned(
            bottom: 2.0,
            left: 130.0,
            right: Dimens.padding,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 17.5,
              child: Row(
                children: <Widget>[
                  GestureDetector(
                    child: Container(
                      margin: const EdgeInsets.only(right: 24.0),
                      height: 24.0,
                      width: 24.0,
                      child:
                          Image.asset('${Environment.iconAssets}instagram.png'),
                    ),
                    onTap: () {
                      state.record.instagram != null &&
                              state.record.instagram.toString().isNotEmpty
                          ? Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => BrowserScreen(
                                        url: UrlThirdParty.urlPathInstagram +
                                            state.record.instagram,
                                      )),
                            )
                          : Scaffold.of(context).showSnackBar(SnackBar(
                              content:
                                  Text(Dictionary.loadInstagramUrlFailed)));
                    },
                  ),
                  GestureDetector(
                    child: Container(
                      margin: const EdgeInsets.only(right: 24.0),
                      height: 24.0,
                      width: 24.0,
                      child:
                          Image.asset('${Environment.iconAssets}facebook.png'),
                    ),
                    onTap: () {
                      state.record.facebook != null &&
                              state.record.facebook.toString().isNotEmpty
                          ? Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => BrowserScreen(
                                        url: UrlThirdParty.urlPathFacebook +
                                            state.record.facebook,
                                      )),
                            )
                          : Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text(Dictionary.loadFacebookUrlFailed)));
                    },
                  ),
                  GestureDetector(
                    child: Container(
                      margin: const EdgeInsets.only(right: 45.0),
                      height: 24.0,
                      width: 24.0,
                      child:
                          Image.asset('${Environment.iconAssets}twitter.png'),
                    ),
                    onTap: () {
                      state.record.twitter != null &&
                              state.record.twitter.toString().isNotEmpty
                          ? Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => BrowserScreen(
                                        url: UrlThirdParty.urlPathTwitter +
                                            state.record.twitter,
                                      )),
                            )
                          : Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text(Dictionary.loadTwitterUrlFailed)));
                    },
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildListForm(AccountProfileLoaded state) {
    return Container(
        padding: const EdgeInsets.all(Dimens.padding),
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            _buildButtonProfile(
                '${Environment.iconAssets}icon_profile.png',
                Dictionary.profile,
                18,
                NavigationConstrants.SubProfile,
                state.record,
                null,
                null),
            _buildButtonProfile(
                '${Environment.iconAssets}icon_contact.png',
                Dictionary.contact,
                14,
                NavigationConstrants.SubContact,
                state.record,
                null,
                13),
            _buildButtonProfile(
                '${Environment.iconAssets}icon_address.png',
                Dictionary.address,
                21,
                NavigationConstrants.SubAddress,
                state.record,
                null,
                7),
            state.record.roleLabel == 'RW'
                ? _buildButtonProfile(
                    '${Environment.iconAssets}icon_mission.png',
                    Dictionary.mission,
                    18,
                    NavigationConstrants.Mission,
                    null,
                    null,
                    null)
                : Container(),
            _buildButtonProfile(
                '${Environment.iconAssets}icon_privacy_policy.png',
                Dictionary.privacyPolicy,
                18,
                NavigationConstrants.Browser,
                UrlThirdParty.privacyPolicy,
                null,
                null),
            _buildButtonProfile('${Environment.iconAssets}icon_app_version.png',
                Dictionary.appVersion, 18, '', null, _versionText, null),
            _buildButtonProfile(
                '${Environment.iconAssets}icon_key_password.png',
                Dictionary.changePasswordProfile,
                18,
                NavigationConstrants.ChangePassword,
                ChangePasswordType.profile,
                null,
                null),
            const SizedBox(
              height: 20,
            ),
            RoundedButton(
              title: 'Keluar',
              borderRadius: BorderRadius.circular(5.0),
              color: color.Colors.darkRed,
              textStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
              onPressed: () {
                showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text(Dictionary.confirmExitTitle),
                        content: Text(Dictionary.confirmExit),
                        actions: <Widget>[
                          FlatButton(
                            child: Text(Dictionary.yes),
                            onPressed: () async {
                              await AuthProfileRepository()
                                  .deleteLocalUserInfo();
                              await HumasJabarRepository().truncateLocal();
                              await Preferences.setStatusTermCondition(false);
                              _authenticationBloc.add(LoggedOut());
                              Navigator.of(context).pop();
                            },
                          ),
                          FlatButton(
                            child: Text(Dictionary.cancel),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          )
                        ],
                      );
                    });
              },
            ),
          ],
        ));
  }

  Widget _buildButtonProfile(
      String iconProfile,
      String title,
      double sizeWidthIcon,
      String navigationName,
      Object userInfoModel,
      String versionName,
      double leftPadding) {
    return GestureDetector(
      child: Container(
        color: Colors.white,
        child: Stack(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.only(top: 18, bottom: 18),
                  child: Row(
                    children: <Widget>[
                      Image.asset(
                        iconProfile,
                        fit: BoxFit.fitWidth,
                        width: sizeWidthIcon,
                      ),
                      Container(
                        padding: EdgeInsets.only(left: leftPadding ?? 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.only(left: 8.0),
                              width: 150.0,
                              child: Text(
                                title,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16.0,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                versionName == null
                    ? Image.asset(
                        '${Environment.iconAssets}direct_page.png',
                        fit: BoxFit.fitWidth,
                        width: 8,
                      )
                    : Container(
                        child: Text(
                          versionName,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16.0,
                          ),
                        ),
                      )
              ],
            ),
            Positioned(
              bottom: 0,
              child: Container(
                margin: const EdgeInsets.only(left: 30, top: 15),
                color: color.Colors.grey,
                height: 1,
                width: MediaQuery.of(context).size.width,
              ),
            )
          ],
        ),
      ),
      onTap: () async {
        var result;
        if (userInfoModel != null) {
          result = await Navigator.of(context)
              .pushNamed(navigationName, arguments: userInfoModel);
        } else if (navigationName.isNotEmpty) {
          result = await Navigator.of(context).pushNamed(navigationName);
        }

        if (result != null) {
          if (result == Dictionary.successSaveProfile) {
            _accountProfileBloc.add(AccountProfileLoad());
          }
        }
      },
    );
  }

  Widget _buildButtonProfileLoading(String iconProfile, String title,
      double sizeWidthIcon, double leftPadding) {
    return Container(
      child: Stack(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.only(top: 18, bottom: 18),
                child: Row(
                  children: <Widget>[
                    Skeleton(
                        child: Container(
                      height: 21,
                      width: 21,
                      decoration: BoxDecoration(
                          color: Colors.grey[300],
                          borderRadius: BorderRadius.circular(4)),
                    )),
                    Container(
                      padding: EdgeInsets.only(left: leftPadding ?? 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: const EdgeInsets.only(left: 8.0),
                            width: 150.0,
                            child: Skeleton(
                              child: Text(
                                title,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16.0,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Skeleton(
                  child: Icon(
                Icons.arrow_forward_ios,
                size: 17,
              ))
            ],
          ),
          Positioned(
            bottom: 0,
            child: Skeleton(
              child: Container(
                margin: const EdgeInsets.only(left: 30, top: 15),
                color: color.Colors.grey,
                height: 1,
                width: MediaQuery.of(context).size.width,
              ),
            ),
          )
        ],
      ),
    );
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  void hideLoading() {
    loadingDialog.hide().then((isHidden) {});
  }

  // TODO: harus dibuat component supaya bisa di reuse
  void showLoading() {
    loadingDialog = ProgressDialog(context);
    loadingDialog.style(
        message: 'Silahkan Tunggu...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    loadingDialog.show();
  }

  @override
  void dispose() {
    _accountProfileBloc.close();
    _imagePickerHelper.streamController.close();
    _blocProfile.close();
    _gamificationsMyBadgeBloc.close();
    super.dispose();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<GamificationsRepository>(
        'gamificationsRepository', gamificationsRepository));
    properties.add(DiagnosticsProperty<AccountProfileLoaded>(
        'stateLoadProfile', stateLoadProfile));
    properties.add(
        DiagnosticsProperty<ProgressDialog>('loadingDialog', loadingDialog));
  }
}
