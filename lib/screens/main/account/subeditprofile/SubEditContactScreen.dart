import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:sapawarga/blocs/account_profile/Bloc.dart';
import 'package:pedantic/pedantic.dart';
import 'package:sapawarga/components/BuildTextField.dart';
import 'package:sapawarga/components/CustomAppBar.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/EducationRepository.dart';
import 'package:sapawarga/repositories/JobRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/Connection.dart';
import 'package:sapawarga/utilities/Validations.dart';

class SubEditContactScreen extends StatelessWidget {
  final UserInfoModel authUserInfo;
  final AuthProfileRepository authProfileRepository = AuthProfileRepository();
  final EducationRepository educationRepository = EducationRepository();
  final JobRepository jobRepository = JobRepository();

  SubEditContactScreen({Key key, @required this.authUserInfo})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AccountProfileEditBloc>(
          create: (context) => AccountProfileEditBloc(
              authProfileRepository: authProfileRepository),
        ),
      ],
      child: SubEditContact(
        userInfoModel: authUserInfo,
      ),
    );
  }
}

class SubEditContact extends StatefulWidget {
  final UserInfoModel userInfoModel;

  SubEditContact({Key key, this.userInfoModel}) : super(key: key);

  @override
  _SubEditContactState createState() => _SubEditContactState();
}

class _SubEditContactState extends State<SubEditContact> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();
  final _instagramController = TextEditingController();
  final _facebookController = TextEditingController();
  final _twitterController = TextEditingController();

  bool isShowDialog = false;
  ProgressDialog loadingDialog;
  GoogleSignIn _googleSignIn;
  AccountProfileEditBloc _blocProfile;
  DateTime _birthDate;

  @override
  void initState() {
    _googleSignIn = GoogleSignIn(
      scopes: ['email'],
    );
    _googleSignIn.signInSilently();
    _emailController.text = widget.userInfoModel.email;
    _phoneController.text = widget.userInfoModel.phone;
    _instagramController.text = widget.userInfoModel.instagram;
    _facebookController.text = widget.userInfoModel.facebook;
    _twitterController.text = widget.userInfoModel.twitter;
    _blocProfile = BlocProvider.of<AccountProfileEditBloc>(context);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _blocProfile,
      listener: (context, state) {
        if (state is AccountProfileEditFailure) {
          _onWidgetDidBuild(() {
            showDialog(
                context: context,
                builder: (BuildContext context) => DialogTextOnly(
                      description: state.error,
                      buttonText: "OK",
                      onOkPressed: () {
                        Navigator.of(context).pop(); // To close the dialog
                      },
                    ));
          });
        }

        if (state is AccountProfileEditValidationError) {
          if (state.errors.containsKey('email')) {
            _onWidgetDidBuild(() {
              showDialog(
                  context: context,
                  builder: (BuildContext context) => DialogTextOnly(
                        description: state.errors['email'][0].toString(),
                        buttonText: "OK",
                        onOkPressed: () {
                          Navigator.of(context).pop(); // To close the dialog
                        },
                      ));
            });
          }
        }
      },
      child: BlocBuilder(
          bloc: _blocProfile,
          builder: (BuildContext context, AccountProfileEditState state) {
            if (state is AccountProfileEditUpdated) {
              _onWidgetDidBuild(() {
                hideLoading();
                Navigator.pop(context, Dictionary.successSaveProfile);
              });
            }

            if (state is AccountProfileEditPhotoLoading) {
              _onWidgetDidBuild(() {
                showLoading();
              });
            }

            if (state is AccountProfileEditPhotoUpdated) {
              hideLoading();
              _onWidgetDidBuild(() {
                if (isShowDialog) {
                  showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text(Dictionary.updatePhotoTitle),
                          content: Text(Dictionary.successSavePhoto),
                          actions: <Widget>[
                            FlatButton(
                              child: Text(Dictionary.ok),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        );
                      });
                  isShowDialog = false;
                }
              });
            }

            if (state is AccountProfileEditLoading) {
              _onWidgetDidBuild(() {
                showLoading();
              });
            }

            return Scaffold(
              appBar: CustomAppBar().DefaultAppBar(title: 'Ubah Kontak'),
              body: ListView(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              const SizedBox(height: 20),
                              BuildTextField(
                                title: Dictionary.email,
                                hintText: Dictionary.placeHolderEmail,
                                controller: _emailController,
                                validation: Validations.emailValidation,
                                textInputType: TextInputType.emailAddress,
                                textStyle: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                              const SizedBox(height: 20),
                              BuildTextField(
                                title: Dictionary.noTelp,
                                hintText: Dictionary.placeHolderNoTelp,
                                controller: _phoneController,
                                validation: Validations.phoneValidation,
                                textInputType: TextInputType.number,
                                textStyle: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                              const SizedBox(height: 20),
                              BuildTextField(
                                title: Dictionary.twitter,
                                hintText: Dictionary.placeHolderTwitter,
                                controller: _twitterController,
                                validation: Validations.twitterlidation,
                                textInputType: null,
                                textStyle: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                              const SizedBox(height: 20),
                              BuildTextField(
                                title: Dictionary.instagram,
                                hintText: Dictionary.placeHolderInstagram,
                                controller: _instagramController,
                                validation: Validations.instagramValidation,
                                textInputType: null,
                                textStyle: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                              const SizedBox(height: 20),
                              BuildTextField(
                                title: Dictionary.facebook,
                                hintText: Dictionary.placeHolderFacebook,
                                controller: _facebookController,
                                validation: Validations.facebooklidation,
                                textInputType: null,
                                textStyle: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                              const SizedBox(height: 20),
                              Container(
                                padding: const EdgeInsets.only(
                                  left: 16.0,
                                  right: 16.0,
                                ),
                                alignment: Alignment.topLeft,
                                child: Text(Dictionary.useYourEmail),
                              ),
                              const SizedBox(height: 10),
                              Container(
                                padding: const EdgeInsets.only(
                                  left: 16.0,
                                  right: 16.0,
                                ),
                                child: ButtonTheme(
                                  minWidth: MediaQuery.of(context).size.width,
                                  height: 45.0,
                                  child: OutlineButton(
                                    child: Wrap(
                                      crossAxisAlignment:
                                          WrapCrossAlignment.center,
                                      children: <Widget>[
                                        Image.asset(
                                          '${Environment.iconAssets}google.png',
                                          width: 24.0,
                                          height: 24.0,
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 10.0),
                                          child: Text(
                                            Dictionary.useGoogleAccount,
                                            style: TextStyle(
                                                color: Colors.grey[800]),
                                          ),
                                        ),
                                      ],
                                    ),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0)),
                                    borderSide: BorderSide(
                                      color: Colors.grey,
                                      //Color of the border
                                      style: BorderStyle.solid,
                                      //Style of the border
                                      width: 1, //width of the border
                                    ),
                                    onPressed: _handleSignIn,
                                  ),
                                ),
                              ),
                              const SizedBox(height: 10),
                              Container(
                                padding: const EdgeInsets.only(
                                  left: 16.0,
                                  right: 16.0,
                                ),
                                child: Material(
                                  borderRadius: BorderRadius.circular(8.0),
                                  color: Colors.blue,
                                  child: MaterialButton(
                                    padding: const EdgeInsets.all(0),
                                    minWidth: MediaQuery.of(context).size.width,
                                    child: Text(Dictionary.saveProfile,
                                        style: TextStyle(
                                            fontFamily: FontsFamily.productSans,
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600,
                                            fontSize: 18.0)),
                                    onPressed: _onSaveProfileButtonPressed,
                                  ),
                                ),
                              ),
                              const SizedBox(height: 20),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            );
          }),
    );
  }

  // TODO: harus dibuat component supaya bisa di reuse
  void showLoading() {
    loadingDialog = ProgressDialog(context);
    loadingDialog.style(
        message: 'Silahkan Tunggu...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    loadingDialog.show();
  }

  void hideLoading() {
    loadingDialog.hide().then((isHidden) {});
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  _onSaveProfileButtonPressed() async {
    if (_formKey.currentState.validate()) {
      try {
        bool isConnected =
            await Connection().checkConnection('https://www.google.com');
        if (isConnected) {
          _blocProfile.add(
            AccountProfileEditSubmit(
              userInfoModel: UserInfoModel(
                name: widget.userInfoModel.name,
                username: widget.userInfoModel.username,
                educationLevelId: widget.userInfoModel.educationLevelId,
                jobTypeId: widget.userInfoModel.jobTypeId,
                email: _emailController.text,
                address: widget.userInfoModel.address,
                lat: widget.userInfoModel.lat,
                lon: widget.userInfoModel.lon,
                phone: _phoneController.text,
                rt: widget.userInfoModel.rt,
                facebook: _facebookController.text,
                instagram: _instagramController.text,
                twitter: _twitterController.text,
                birthDate: _birthDate,
              ),
            ),
          );

          await AnalyticsHelper.setLogEvent(Analytics.EVENT_EDIT_ACCOUNT);
        }
      } catch (_) {
        await showDialog(
            context: context,
            builder: (BuildContext context) => DialogTextOnly(
                  description: Dictionary.errorConnection,
                  buttonText: "OK",
                  onOkPressed: () {
                    Navigator.of(context).pop(); // To close the dialog
                  },
                ));
      }
    }
  }

  Future<void> _handleSignIn() async {
    try {
      bool isConnected =
          await Connection().checkConnection('https://www.google.com');
      if (isConnected) {
        try {
          await _googleSignIn.signIn();

          if (_googleSignIn.currentUser != null &&
              _googleSignIn.currentUser.email != null) {
            _emailController.text = _googleSignIn.currentUser.email;
            unawaited(_googleSignIn.disconnect());
          }
        } catch (error) {
          await showDialog(
              context: context,
              builder: (BuildContext context) => DialogTextOnly(
                    description: Dictionary.errorGetEmail,
                    buttonText: "OK",
                    onOkPressed: () {
                      Navigator.of(context).pop(); // To close the dialog
                    },
                  ));
        }
      }
    } catch (_) {
      await showDialog(
        context: context,
        builder: (BuildContext context) => DialogTextOnly(
          description: Dictionary.errorConnection,
          buttonText: "OK",
          onOkPressed: () {
            Navigator.of(context).pop(); // To close the dialog
          },
        ),
      );
    }
  }

  @override
  void dispose() {
    _emailController.dispose();
    _phoneController.dispose();
    _instagramController.dispose();
    _facebookController.dispose();
    _twitterController.dispose();

    _blocProfile.close();

    super.dispose();
  }
}
