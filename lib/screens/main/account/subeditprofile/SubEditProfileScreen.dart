import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:sapawarga/blocs/account_profile/Bloc.dart';
import 'package:sapawarga/blocs/educations_list/Bloc.dart';
import 'package:sapawarga/blocs/jobs_list/Bloc.dart';
import 'package:sapawarga/components/BaseShowCase.dart';
import 'package:sapawarga/components/BubbleCustom.dart';
import 'package:sapawarga/components/BuildTextField.dart';
import 'package:sapawarga/components/CustomAppBar.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/EducationRepository.dart';
import 'package:sapawarga/repositories/JobRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/Connection.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:sapawarga/utilities/Validations.dart';
import 'package:sapawarga/components/custom_dropdown.dart' as custom;
import 'package:showcaseview/showcaseview.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';

class SubEditProfileScreen extends StatelessWidget {
  final UserInfoModel authUserInfo;
  final AuthProfileRepository authProfileRepository = AuthProfileRepository();
  final EducationRepository educationRepository = EducationRepository();
  final JobRepository jobRepository = JobRepository();

  SubEditProfileScreen({Key key, @required this.authUserInfo})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AccountProfileEditBloc>(
          create: (context) => AccountProfileEditBloc(
              authProfileRepository: authProfileRepository),
        ),
        BlocProvider<EducationsListBloc>(
          create: (context) =>
              EducationsListBloc(educationRepository: educationRepository),
        ),
        BlocProvider<JoblistBloc>(
          create: (context) => JoblistBloc(jobRepository: jobRepository),
        ),
      ],
      child: SubEditProfile(
        userInfoModel: authUserInfo,
      ),
    );
  }
}

class SubEditProfile extends StatefulWidget {
  final UserInfoModel userInfoModel;

  SubEditProfile({Key key, this.userInfoModel}) : super(key: key);

  @override
  _SubEditProfileState createState() => _SubEditProfileState();
}

class _SubEditProfileState extends State<SubEditProfile> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey _showcaseOne = GlobalKey();

  final _nameController = TextEditingController();
  final _birthDateController = TextEditingController();
  final _educationLevelIdController = TextEditingController();
  final _jobIdController = TextEditingController();
  final _usernameController = TextEditingController();
  final _nikController = TextEditingController();
  final String _format = 'dd/MMMM/yyyy';
  String minDate = '1900-01-01';
  final DateTimePickerLocale _locale = DateTimePickerLocale.id;

  bool isShowDialog = false;
  ProgressDialog loadingDialog;

  BuildContext showcaseContext;
  AccountProfileEditBloc _blocProfile;
  EducationsListBloc _blocEducationsList;
  JoblistBloc _blocJoblist;
  DateTime _birthDate;

  @override
  void initState() {
    _blocProfile = BlocProvider.of<AccountProfileEditBloc>(context);
    _blocEducationsList = BlocProvider.of<EducationsListBloc>(context);
    _blocJoblist = BlocProvider.of<JoblistBloc>(context);

    _blocEducationsList.add(EducationsLoad());
    _blocJoblist.add(JobsLoad());

    _nameController.text = widget.userInfoModel.name;
    _nikController.text = widget.userInfoModel.nik;
    _birthDateController.text = widget.userInfoModel.birthDate != null
        ? DateFormat('dd/MM/yyyy', 'id').format(widget.userInfoModel.birthDate)
        : null;
    _educationLevelIdController.text =
        widget.userInfoModel.educationLevelId.toString();
    _jobIdController.text = widget.userInfoModel.jobTypeId.toString();
    _usernameController.text = widget.userInfoModel.username;
    super.initState();
  }

  _initializeShowcase() async {
    bool hasShown = await Preferences.hasShowcaseEditEducation();

    if (hasShown == null || hasShown == false) {
      Future.delayed(
          Duration(milliseconds: 200),
          () =>
              ShowCaseWidget.of(showcaseContext).startShowCase([_showcaseOne]));
    }
  }

  @override
  Widget build(BuildContext context) {
    _initializeShowcase();

    return Listener(
      onPointerDown: (_) {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus &&
            currentFocus.focusedChild != null) {
          currentFocus.focusedChild.unfocus();
        }
      },
      child: BlocListener(
        bloc: _blocProfile,
        listener: (context, state) {
          if (state is AccountProfileEditFailure) {
            hideLoading();
            _onWidgetDidBuild(() {
              showDialog(
                  context: context,
                  builder: (BuildContext context) => DialogTextOnly(
                        description: state.error,
                        buttonText: "OK",
                        onOkPressed: () {
                          Navigator.of(context).pop(); // To close the dialog
                        },
                      ));
            });
          }

          if (state is AccountProfileEditValidationError) {
            hideLoading();
            _onWidgetDidBuild(() {});
            if (state.errors.containsKey('email')) {
              showDialog(
                  context: context,
                  builder: (BuildContext context) => DialogTextOnly(
                        description: state.errors['email'][0].toString(),
                        buttonText: "OK",
                        onOkPressed: () {
                          Navigator.of(context).pop(); // To close the dialog
                        },
                      ));
            } else if (state.errors.containsKey('birth_date')) {
              showDialog(
                  context: context,
                  builder: (BuildContext context) => DialogTextOnly(
                        description: state.errors['birth_date'][0].toString(),
                        buttonText: "OK",
                        onOkPressed: () {
                          Navigator.of(context).pop(); // To close the dialog
                        },
                      ));
            }
          }
        },
        child: BlocBuilder(
            bloc: _blocProfile,
            builder: (BuildContext context, AccountProfileEditState state) {
              if (state is AccountProfileEditUpdated) {
                _onWidgetDidBuild(() {
                  hideLoading();
                  Navigator.pop(context, Dictionary.successSaveProfile);
                });
              }

              if (state is AccountProfileEditPhotoLoading) {
                _onWidgetDidBuild(() {
                  showLoading();
                });
              }

              if (state is AccountProfileEditPhotoUpdated) {
                hideLoading();
                _onWidgetDidBuild(() {
                  if (isShowDialog) {
                    showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text(Dictionary.updatePhotoTitle),
                            content: Text(Dictionary.successSavePhoto),
                            actions: <Widget>[
                              FlatButton(
                                child: Text(Dictionary.ok),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                            ],
                          );
                        });
                    isShowDialog = false;
                  }
                });
              }

              if (state is AccountProfileEditLoading) {
                _onWidgetDidBuild(() {
                  showLoading();
                });
              }

              return ShowCaseWidget(onFinish: () async {
                await Preferences.setShowcaseEditEducation(true);
              }, builder: Builder(builder: (context) {
                showcaseContext = context;
                return Scaffold(
                  appBar: CustomAppBar().DefaultAppBar(title: 'Ubah Profil'),
                  body: ListView(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            child: Form(
                              key: _formKey,
                              child: Column(
                                children: [
                                  const SizedBox(height: 20),
                                  BuildTextField(
                                    title: Dictionary.name,
                                    hintText: Dictionary.placeHolderName,
                                    controller: _nameController,
                                    validation: Validations.nameValidation,
                                    textStyle: TextStyle(
                                      color: Colors.black,
                                    ),
                                  ),
                                  const SizedBox(height: 20),
                                  BuildTextField(
                                    title: Dictionary.nik,
                                    hintText: Dictionary.placeHolderNik,
                                    controller: _nikController,
                                    validation: Validations.nikValidation,
                                    textInputType: TextInputType.number,
                                    textStyle: TextStyle(
                                      color: Colors.black,
                                    ),
                                    limitCharacter: 16,
                                  ),
                                  const SizedBox(height: 20),
                                  InkWell(
                                    child: BuildTextField(
                                      isEdit: false,
                                      title: Dictionary.birthDate,
                                      hintText: Dictionary.placeHolderBirthday,
                                      controller: _birthDateController,
                                      textStyle: TextStyle(
                                        color: Colors.black,
                                      ),
                                    ),
                                    onTap: showDatePicker,
                                  ),
                                  const SizedBox(height: 20),
                                  BaseShowCase.showcaseWidget(
                                    key: _showcaseOne,
                                    context: context,
                                    nipLocation: NipLocation.TOP,
                                    buttonText: Dictionary.ok,
                                    widgets: <Widget>[
                                      Column(
                                        children: <Widget>[
                                          Text(
                                            Dictionary.showcaseEditEducation,
                                            style: TextStyle(
                                                color: Colors.grey[800],
                                                fontSize: 15.0),
                                          ),
                                        ],
                                      )
                                    ],
                                    onOkTap: () {
                                      ShowCaseWidget.of(showcaseContext)
                                          .completed(_showcaseOne);
                                    },
                                    child: Column(
                                      children: <Widget>[
                                        BlocBuilder<EducationsListBloc,
                                            EducationsListState>(
                                          bloc: _blocEducationsList,
                                          builder: (context, state) {
                                            return state is EducationsLoaded
                                                ? buildDropdownField(
                                                    Dictionary.education,
                                                    Dictionary
                                                        .placeHolderEducation,
                                                    state.record,
                                                    _educationLevelIdController,
                                                  )
                                                : state is EducationsFailure
                                                    ? Text(state.error)
                                                    : buildSkeleton(context);
                                          },
                                        ),
                                        const SizedBox(height: 20),
                                        BlocBuilder<JoblistBloc, JoblistState>(
                                          bloc: _blocJoblist,
                                          builder: (context, state) {
                                            return state is JobsLoaded
                                                ? buildDropdownField(
                                                    Dictionary.job,
                                                    Dictionary.placeHolderJob,
                                                    state.record,
                                                    _jobIdController,
                                                  )
                                                : state is JobsFailure
                                                    ? Text(state.error)
                                                    : buildSkeleton(context);
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(height: 20),
                                  BuildTextField(
                                    title: Dictionary.username,
                                    hintText: Dictionary.placeHolderUsername,
                                    controller: _usernameController,
                                    validation: Validations.usernameValidation,
                                    textStyle: TextStyle(
                                      color: Colors.black,
                                    ),
                                  ),
                                  const SizedBox(height: 20),
                                  Container(
                                    padding: const EdgeInsets.only(
                                      left: 16.0,
                                      right: 16.0,
                                    ),
                                    child: Material(
                                      borderRadius: BorderRadius.circular(8.0),
                                      color: Colors.blue,
                                      child: MaterialButton(
                                        padding: const EdgeInsets.all(0),
                                        minWidth:
                                            MediaQuery.of(context).size.width,
                                        child: Text(Dictionary.saveProfile,
                                            style: TextStyle(
                                                fontFamily:
                                                    FontsFamily.productSans,
                                                color: Colors.white,
                                                fontWeight: FontWeight.w600,
                                                fontSize: 18.0)),
                                        onPressed: _onSaveProfileButtonPressed,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 20),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                );
              }));
            }),
      ),
    );
  }

  // TODO: harus dibuat component supaya bisa di reuse
  void showLoading() {
    loadingDialog = ProgressDialog(context);
    loadingDialog.style(
        message: 'Silahkan Tunggu...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    loadingDialog.show();
  }

  void hideLoading() {
    loadingDialog.hide().then((isHidden) {});
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  _onSaveProfileButtonPressed() async {
    if (_formKey.currentState.validate()) {
      try {
        bool isConnected =
            await Connection().checkConnection('https://www.google.com');
        if (isConnected) {
          _blocProfile.add(
            AccountProfileEditSubmit(
              userInfoModel: UserInfoModel(
                  name: _nameController.text,
                  username: _usernameController.text,
                  educationLevelId: _educationLevelIdController.text != 'null'
                      ? int.parse(_educationLevelIdController.text)
                      : null,
                  jobTypeId: _jobIdController.text != 'null'
                      ? int.parse(_jobIdController.text)
                      : null,
                  email: widget.userInfoModel.email,
                  address: widget.userInfoModel.address,
                  lat: widget.userInfoModel.lat,
                  lon: widget.userInfoModel.lon,
                  phone: widget.userInfoModel.phone,
                  rt: widget.userInfoModel.rt,
                  facebook: widget.userInfoModel.facebook,
                  instagram: widget.userInfoModel.instagram,
                  twitter: widget.userInfoModel.twitter,
                  birthDate: _birthDate,
                  nik: _nikController.text,
                  fileSKUrl: widget.userInfoModel.fileSKUrl,
                  fileSKUrlUploadedAt: widget.userInfoModel.fileSKUrlUploadedAt,
                  isFirstActivation: widget.userInfoModel.isFirstActivation,
                  isFirstVerified: widget.userInfoModel.isFirstVerified,
                  isUsernameUpdated: widget.userInfoModel.isUsernameUpdated,
                  isVerified: widget.userInfoModel.isVerified,
                  isVerifiedAt: widget.userInfoModel.isVerifiedAt),
            ),
          );

          await AnalyticsHelper.setLogEvent(Analytics.EVENT_EDIT_ACCOUNT);
        }
      } catch (_) {
        await showDialog(
            context: context,
            builder: (BuildContext context) => DialogTextOnly(
                  description: Dictionary.errorConnection,
                  buttonText: "OK",
                  onOkPressed: () {
                    Navigator.of(context).pop(); // To close the dialog
                  },
                ));
      }
    }
  }

  Skeleton buildSkeleton(BuildContext context) {
    return Skeleton(
      child: Container(
        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 70,
              height: 30.0,
              color: Colors.grey[300],
            ),
            const SizedBox(height: 5.0),
            Container(
                width: MediaQuery.of(context).size.width,
                height: 30.0,
                color: Colors.grey[300]),
          ],
        ),
      ),
    );
  }

  Widget buildDropdownField(String title, String hintText, List items,
      TextEditingController controller,
      [validation]) {
    return Container(
      padding: const EdgeInsets.only(left: 16.0, right: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600),
          ),
          custom.DropdownButton<String>(
            isExpanded: true,
            height: 320,
            hint: Text(hintText),
            items: items.map((item) {
              return custom.DropdownMenuItem(
                child: Text(item.title),
                value: item.id.toString(),
              );
            }).toList(),
            onChanged: (String value) {
              setState(() {
                controller.text = value;
              });
            },
            value: controller.text == 'null' ? null : controller.text,
          )
        ],
      ),
    );
  }

  void showDatePicker() {
    DatePicker.showDatePicker(
      context,
      pickerTheme: DateTimePickerTheme(
        showTitle: true,
        confirm: Text(Dictionary.save, style: TextStyle(color: Colors.red)),
        cancel: Text(Dictionary.cancel, style: TextStyle(color: Colors.cyan)),
      ),
      minDateTime: DateTime.parse(minDate),
      maxDateTime: DateTime.now(),
      initialDateTime: _birthDateController.text == ''
          ? DateTime.now()
          : DateTime.parse(DateFormat('yyyy-MM-dd', 'id')
              .format(widget.userInfoModel.birthDate)
              .toString()),
      dateFormat: _format,
      locale: _locale,
      onClose: () {
        setState(() {
          _birthDateController.text = _birthDateController.text;
        });
      },
      onCancel: () {
        setState(() {
          _birthDateController.text = _birthDateController.text;
        });
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          _birthDateController.text =
              DateFormat('dd/MM/yyyy', 'id').format(dateTime);
          _birthDate = dateTime;
        });
      },
    );
  }

  @override
  void dispose() {
    _nameController.dispose();
    _usernameController.dispose();
    _nikController.dispose();
    _birthDateController.dispose();
    _jobIdController.dispose();
    _educationLevelIdController.dispose();

    _blocProfile.close();
    _blocEducationsList.close();
    _blocJoblist.close();

    super.dispose();
  }
}
