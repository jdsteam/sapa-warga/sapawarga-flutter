import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/account_profile/AccountProfileBloc.dart';
import 'package:sapawarga/blocs/account_profile/Bloc.dart';
import 'package:sapawarga/blocs/banner/Bloc.dart';
import 'package:sapawarga/blocs/dismiss_change_username/Bloc.dart';
import 'package:sapawarga/blocs/humasjabar_list/Bloc.dart';
import 'package:sapawarga/blocs/important_info/improtant_info_home/Bloc.dart';
import 'package:sapawarga/blocs/notification/Bloc.dart';
import 'package:sapawarga/blocs/popup_information/Bloc.dart';
import 'package:sapawarga/blocs/ramadan/bloc/ramadan_bloc.dart';
import 'package:sapawarga/blocs/remote_home/Bloc.dart';
import 'package:sapawarga/blocs/showcase_home/Bloc.dart';
import 'package:sapawarga/blocs/statistics/Bloc.dart';
import 'package:sapawarga/blocs/video_list/Bloc.dart';
import 'package:sapawarga/components/BaseShowCase.dart';
import 'package:sapawarga/components/BrowserScreen.dart';
import 'package:sapawarga/components/BubbleCustom.dart';
import 'package:sapawarga/components/CustomBottomSheet.dart';
import 'package:sapawarga/components/DialogWithImage.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/components/TextButton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as color;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/constants/UrlThirdParty.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/BannerRepository.dart';
import 'package:sapawarga/repositories/FirebaseRepository.dart';
import 'package:sapawarga/repositories/HumasJabarRepository.dart';
import 'package:sapawarga/repositories/ImportantInfoRepository.dart';
import 'package:sapawarga/repositories/StatisticsRepository.dart';
import 'package:sapawarga/repositories/VideoRepository.dart';
import 'package:sapawarga/repositories/ramadan_repository.dart';
import 'package:sapawarga/screens/importantInformation/ImportantInfoListHome.dart';
import 'package:sapawarga/screens/main/account/subeditprofile/SubEditProfileScreen.dart';
import 'package:sapawarga/screens/main/home/Announcement.dart';
import 'package:sapawarga/screens/main/home/BannerListSlider.dart';
import 'package:sapawarga/screens/main/home/HumasJabarList.dart';
import 'package:sapawarga/screens/main/home/Statistics.dart';
import 'package:sapawarga/screens/main/home/VerifikasiBansos.dart';
import 'package:sapawarga/screens/main/home/VideoListJabar.dart';
import 'package:sapawarga/screens/main/home/VideoListKokab.dart';
import 'package:sapawarga/screens/main/home/ramadan.dart';
import 'package:sapawarga/screens/news/NewsListScreen.dart';
import 'package:sapawarga/screens/rwActivities/RWActivityScreen.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/BasicUtils.dart';
import 'package:sapawarga/utilities/RateAppHelper.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:showcaseview/showcaseview.dart';

class HomeScreen extends StatefulWidget {
  final ShowcaseHomeBloc showcaseHomeBloc;
  final PopupInformationBloc popupInformationBloc;

  HomeScreen({Key key, this.popupInformationBloc, this.showcaseHomeBloc})
      : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<ShowcaseHomeBloc>(
        'showcaseHomeBloc', showcaseHomeBloc));
    properties.add(DiagnosticsProperty<PopupInformationBloc>(
        'popupInformationBloc', popupInformationBloc));
  }
}

class _HomeScreenState extends State<HomeScreen> {
  NotificationBadgeBloc _notificationBadgeBloc;

  ShowcaseHomeBloc get _showcaseHomeBloc => widget.showcaseHomeBloc;

  PopupInformationBloc get _popupBloc => widget.popupInformationBloc;

  final GlobalKey _showcaseOne = GlobalKey();
  final GlobalKey _showcaseTwo = GlobalKey();
  final GlobalKey _showcaseThree = GlobalKey();
  BuildContext showcaseContext;
  bool showingShowcase = false;
  BannerListBloc _bannerListBloc;
  RemoteHomeBloc _remoteHomeBloc;
  StatisticsBloc _statisticsBloc;
  ImportantInfoHomeBloc _importantInfoHomeBloc;
  HumasJabarListBloc _humasJabarListBloc;
  VideoListJabarBloc _videoListJabarBloc;
  VideoListKokabBloc _videoListKokabBloc;
  AccountProfileBloc _accountProfileBloc;
  int isVerify = 0;
  bool isUserSubmitVerification = false;
  Map<String, dynamic> ramadhanWidget;

  // ignore: unused_field
  RamadanBloc _ramadanBloc;
  DismissChangeUsernameBloc _dismissChangeUsernameBloc;
  final RefreshController _mainRefreshController = RefreshController();

  final authProfileRepository = AuthProfileRepository();
  UserInfoModel userInfo;

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.HOME,
        screenClassOverride: 'BerandaScreen');
    _notificationBadgeBloc = BlocProvider.of<NotificationBadgeBloc>(context);
    _notificationBadgeBloc.add(CheckNotificationBadge());
    _getShowcaseStat();

    super.initState();
  }

  /// get info user
  _getUserInfo() async {
    print('calll this info');
    userInfo = await authProfileRepository.getUserInfo();

    setState(() {});
  }

  _getShowcaseStat() async {
    showingShowcase = await Preferences.hasShowcaseHome();
    setState(() {});
  }

  _loadDialogChangeUsername() async {
    var result;
    await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) => DialogWithImage(
              title: Dictionary.updateNumberAndUsernameTitle,
              description: Dictionary.updateNumberAndUsernameDesc,
              locationImage: '${Environment.imageAssets}update_profile.png',
              buttonName: Dictionary.updateNow,
              onNextPressed: () async {
                Navigator.of(context).pop();
                result = false;
                result = await Navigator.of(context)
                    .pushNamed(NavigationConstrants.UpdateUsername);
                if (result != null) {
                  if (!result) {
                    await AnalyticsHelper.setLogEvent(
                        Analytics.EVENT_DISMISS_CHANGE_USERNAME);
                    _dismissChangeUsernameBloc
                        .add(DismissSendChangedUsername());
                  }
                }
              },
            )).then((val) {
      if (result == null) {
        AnalyticsHelper.setLogEvent(Analytics.EVENT_DISMISS_CHANGE_USERNAME);
        _dismissChangeUsernameBloc.add(DismissSendChangedUsername());
      }
    });
  }

  _loadInfoAccountUserActive(UserInfoModel userInfoModel) async {
    await showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) => DialogWithImage(
        title: Dictionary.infoAccountActive,
        description: userInfoModel.roleLabel == 'RW'
            ? Dictionary.descAccountActive
            : Dictionary.descAccountActivePublic,
        heightImage: 150,
        locationImage: '${Environment.imageAssets}activation_image.png',
        buttonName: Dictionary.updateProfileNow,
        onNextPressed: () async {
          Navigator.of(context).pop();
          var result = await Navigator.of(context).pushNamed(
              NavigationConstrants.RegisterVerification,
              arguments: userInfoModel);
          if (result != null) {
            if (mounted) {
              setState(() {
                isUserSubmitVerification = result;
              });
            }
          }
        },
      ),
    );
  }

  _buildButtonColumn(String iconPath, String label, String route) {
    return Expanded(
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.all(2.0),
            margin: const EdgeInsets.only(left: 10.0, right: 10.0),
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                blurRadius: 6.0,
                color: Colors.black.withOpacity(.2),
                offset: Offset(2.0, 4.0),
              ),
            ], borderRadius: BorderRadius.circular(12.0), color: Colors.white),
            child: IconButton(
              color: Theme.of(context).textTheme.bodyText1.color,
              icon: Image.asset(iconPath),
              onPressed: () {
                if (route != null) {
                  Navigator.pushNamed(context, route);

                  if (route == NavigationConstrants.infoPKB) {
                    AnalyticsHelper.setCurrentScreen(Analytics.INFO_PKB,
                        screenClassOverride: 'InfoPKB');
                    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_INFO_PKB);
                  }

                  if (route == NavigationConstrants.Pikobar) {
                    AnalyticsHelper.setCurrentScreen(Analytics.PIKOBAR,
                        screenClassOverride: 'InfoCorona');
                    AnalyticsHelper.setLogEvent(
                        Analytics.EVENT_VIEW_INFO_CORONA);
                  }
                }
              },
            ),
          ),
          const SizedBox(height: 5.0),
          Container(
            child: Text(label,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 13,
                  color: Theme.of(context).textTheme.bodyText1.color,
                )),
          )
        ],
      ),
    );
  }

  _buildButtonColumnLayananLain(String iconPath, String label) {
    return Expanded(
        child: Column(
      children: [
        Container(
          padding: const EdgeInsets.all(2.0),
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              blurRadius: 10.0,
              color: Colors.black.withOpacity(.2),
              offset: Offset(2.0, 2.0),
            ),
          ], borderRadius: BorderRadius.circular(12.0), color: Colors.white),
          child: IconButton(
            color: Theme.of(context).textTheme.bodyText1.color,
            icon: Image.asset(iconPath),
            onPressed: () {
              _mainHomeLayananBottomSheet(context);
            },
          ),
        ),
        const SizedBox(height: 5.0),
        Text(label,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 12,
              color: Theme.of(context).textTheme.bodyText1.color,
            ))
      ],
    ));
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<BannerListBloc>(
          create: (BuildContext context) => _bannerListBloc =
              BannerListBloc(bannerRepository: BannerRepository())
                ..add(BannerListLoad()),
        ),
        BlocProvider<RemoteHomeBloc>(
          create: (BuildContext context) => _remoteHomeBloc =
              RemoteHomeBloc(firebaseRepository: FirebaseRepository())
                ..add(RemoteHomeLoad()),
        ),
        BlocProvider<StatisticsBloc>(
          create: (BuildContext context) => _statisticsBloc =
              StatisticsBloc(statisticsRepository: StatisticsRepository()),
        ),
        BlocProvider<ImportantInfoHomeBloc>(
          create: (BuildContext context) =>
              _importantInfoHomeBloc = ImportantInfoHomeBloc(
                  importantInfoRepository: ImportantInfoRepository())
                ..add(ImportantInfoHomeLoad()),
        ),
        BlocProvider<HumasJabarListBloc>(
            create: (BuildContext context) => _humasJabarListBloc =
                HumasJabarListBloc(humasJabarRepository: HumasJabarRepository())
                  ..add(HumasJabarLoad())),
        BlocProvider<VideoListKokabBloc>(
          create: (BuildContext context) =>
              _videoListKokabBloc = VideoListKokabBloc(
                  videoRepository: VideoRepository(),
                  authProfileRepository: AuthProfileRepository())
                ..add(VideoListKokabLoad()),
        ),
        BlocProvider<VideoListJabarBloc>(
          create: (BuildContext context) => _videoListJabarBloc =
              VideoListJabarBloc(videoRepository: VideoRepository())
                ..add(VideoListJabarLoad()),
        ),
        BlocProvider<RamadanBloc>(
          create: (BuildContext context) => _ramadanBloc = RamadanBloc(
            ramadanRepository: RamadanRepository(),
          ),
        ),
        BlocProvider<DismissChangeUsernameBloc>(
          create: (BuildContext context) =>
              _dismissChangeUsernameBloc = DismissChangeUsernameBloc(
            authProfileRepository: AuthProfileRepository(),
          ),
        ),
        BlocProvider<AccountProfileBloc>(
          create: (BuildContext context) =>
              _accountProfileBloc = AccountProfileBloc.profile(
            authProfileRepository: AuthProfileRepository(),
          )..add(
                  AccountProfileLoad(),
                ),
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener(
            bloc: _popupBloc,
            listener: (context, state) {
              if (state is PopupInformationHasShown) {
                _showProfile();
              }
            },
          ),
          BlocListener(
            bloc: _showcaseHomeBloc,
            listener: (context, state) async {
              if (state is ShowcaseHomeLoaded) {
                if (!showingShowcase) {
                  _initializeShowcase();
                }
              }
            },
          ),
          BlocListener<RemoteHomeBloc, RemoteHomeState>(
            bloc: _remoteHomeBloc,
            listener: (context, state) {
              if (state is RemoteHomeLoaded) {
                _statisticsBloc.add(StatisticsLoad());

                setState(() {
                  ramadhanWidget = json.decode(state.remoteConfig
                      .getString(FirebaseConfig.ramadhanWidget));
                });

                if (ramadhanWidget['enabled']) {
                  _ramadanBloc.add(RamadanLoad(url: ramadhanWidget['url']));
                }
              }
            },
          ),
          BlocListener<RamadanBloc, RamadanState>(
            listener: (context, state) {
              if (state is RamadanLoaded) {
                _getUserInfo();
              }
            },
          ),
          BlocListener<AccountProfileBloc, AccountProfileState>(
            bloc: _accountProfileBloc,
            listener: (context, state) async {
              if (state is AccountProfileLoaded) {
                setState(() {
                  userInfo = state.record;
                  isVerify = state.record.isVerified ?? 0;
                  if ((state.record.isVerified == 0 ||
                          state.record.isVerified == null) &&
                      state.record.fileSKUrl != null) {
                    isUserSubmitVerification = true;
                  }
                });
                // if (showingShowcase) {
                //   _infoAfterLoadProfile(userInfo);
                // }
              }
            },
          ),
        ],
        child: ShowCaseWidget(
          onFinish: () async {
            await Preferences.setShowcaseHome(true);
            Future.delayed(Duration(milliseconds: 500), () => _showProfile());
          },
          builder: Builder(
            builder: (context) {
              showcaseContext = context;
              return Scaffold(
                  backgroundColor: Colors.white,
                  appBar: AppBar(
                    elevation: 0.0,
                    backgroundColor: color.Colors.blue,
                    title: Row(
                      children: <Widget>[
                        Container(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              Dictionary.appName,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                                fontFamily: FontsFamily.intro,
                              ),
                            ))
                      ],
                    ),
                    actions: <Widget>[
                      BlocBuilder<NotificationBadgeBloc,
                              NotificationBadgeState>(
                          bloc: _notificationBadgeBloc,
                          builder: (context, state) {
                            return state is NotificationBadgeShow
                                ? Container(
                                    width:
                                        MediaQuery.of(context).size.width / 3,
                                    child: IconButton(
                                      icon: Stack(children: <Widget>[
                                        Positioned(
                                            right: 5.0,
                                            top: 5.0,
                                            child: Icon(
                                                Icons.notifications_active,
                                                size: 25.0)),
                                        Positioned(
                                          right: 0.0,
                                          top: 0.0,
                                          child: Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(25.0),
                                                color: Colors.redAccent),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      5.0, 3.0, 5.0, 3.0),
                                              child: Text(
                                                state.count.toString(),
                                                style:
                                                    TextStyle(fontSize: 10.0),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ]),
                                      onPressed: () {
                                        Navigator.pushNamed(
                                            context,
                                            NavigationConstrants
                                                .NotificationList);
                                      },
                                    ))
                                : IconButton(
                                    icon: Icon(Icons.notifications_active,
                                        size: 25.0, color: Colors.white),
                                    onPressed: () {
                                      Navigator.pushNamed(
                                          context,
                                          NavigationConstrants
                                              .NotificationList);
                                    },
                                  );
                          }),
                    ],
                  ),
                  body: Stack(
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height * 0.16,
                        color: color.Colors.blue,
                      ),
                      Column(
                        children: <Widget>[
                          Expanded(
                            child: SmartRefresher(
                              controller: _mainRefreshController,
                              enablePullDown: true,
                              header: WaterDropMaterialHeader(),
                              onRefresh: () async {
                                _bannerListBloc.add(BannerListLoad());
                                _remoteHomeBloc.add(RemoteHomeLoad());
                                _importantInfoHomeBloc
                                    .add(ImportantInfoHomeLoad());
                                _humasJabarListBloc.add(HumasJabarLoad());
                                _videoListJabarBloc.add(VideoListJabarLoad());
                                _videoListKokabBloc.add(VideoListKokabLoad());
                                _mainRefreshController.refreshCompleted();
                              },
                              child: ListView(children: [
                                Container(
                                  margin: const EdgeInsets.fromLTRB(
                                      0.0, 21.0, 0.0, 10.0),
                                  child: BannerListSlider(userInfo: userInfo),
                                ),
                                Announcement(),
                                BlocBuilder<AccountProfileBloc,
                                        AccountProfileState>(
                                    bloc: _accountProfileBloc,
                                    builder: (context, state) {
                                      return state is AccountProfileLoading
                                          ? _buildLoading()
                                          : state is AccountProfileLoaded
                                              ? _buildContent(state)
                                              : state is AccountProfileFailure
                                                  ? _buildFailure(state)
                                                  : Container();
                                    }),
                                userInfo != null
                                    ? VerificationSection(userInfo: userInfo)
                                    : Container(),
                                ramadhanWidget != null &&
                                        ramadhanWidget['enabled']
                                    ? Ramadan(userInfo: userInfo)
                                    : Container(),
                                Statistics(statisticsBloc: _statisticsBloc),
                                SizedBox(
                                  height: 8.0,
                                  child: Container(
                                    color: Color(0xFFE5E5E5),
                                  ),
                                ),
                                Container(
                                  color: Colors.white,
                                  child: Column(
                                    children: <Widget>[
                                      ImportantInfoListHome(
                                          userInfoModel: userInfo),
                                      Container(
                                        padding: const EdgeInsets.all(15.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(
                                              Dictionary.titleHumasJabar,
                                              style: TextStyle(
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily:
                                                      FontsFamily.productSans),
                                            ),
                                            TextButtonOnly(
                                              title: Dictionary.viewAll,
                                              textStyle: TextStyle(
                                                  color: Colors.green,
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 13.0),
                                              onTap: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        BrowserScreen(
                                                      url: UrlThirdParty
                                                          .newsHumasJabarTerkini,
                                                    ),
                                                  ),
                                                );

                                                AnalyticsHelper.setLogEvent(
                                                  Analytics
                                                      .EVENT_VIEW_LIST_HUMAS,
                                                );
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                      HumasJabarListScreen(),
                                      Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            ListTile(
                                              leading: Text(
                                                Dictionary.news,
                                                style: TextStyle(
                                                    fontSize: 16.0,
                                                    fontWeight: FontWeight.bold,
                                                    fontFamily: FontsFamily
                                                        .productSans),
                                              ),
                                            ),
                                            SingleChildScrollView(
                                              scrollDirection: Axis.horizontal,
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  NewsListScreen(
                                                      isIdKota: false),
                                                  NewsListScreen(isIdKota: true)
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        padding:
                                            const EdgeInsets.only(top: 16.0),
                                        child: VideoListJabar(),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 16.0),
                                        child: VideoListKokab(),
                                      )
                                    ],
                                  ),
                                )
                              ]),
                            ),
                          )
                        ],
                      )
                    ],
                  ));
            },
          ),
        ),
      ),
    );
  }

  _buildLoading() {
    return Column(
      children: [
        Card(
          margin: const EdgeInsets.only(left: 20, right: 20, top: 20),
          shadowColor: Colors.black.withOpacity(0.4),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
              side: BorderSide(width: 1, color: Colors.grey[300])),
          child: Skeleton(
            child: Container(
              padding: const EdgeInsets.all(20),
              child: Row(
                children: <Widget>[
                  Image.asset('${Environment.imageAssets}verify_info_image.png',
                      height: 46, width: 46),
                  const SizedBox(width: Dimens.sizedBoxHeight),
                  Skeleton(width: 220, height: 20.0),
                ],
              ),
            ),
          ),
        ),
        Container(
          alignment: Alignment.topCenter,
          padding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.white.withOpacity(0.05),
                offset: Offset(0.0, 0.05),
              ),
            ],
          ),
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: Skeleton(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _showcaseImportantInfo(),
                  _buildButtonColumn(
                      '${Environment.iconAssets}administrasi-1.png',
                      Dictionary.administration,
                      NavigationConstrants.AdministrationList),
                  _buildButtonColumn(
                      '${Environment.iconAssets}icon_esamsat.png',
                      Dictionary.infoPKB,
                      NavigationConstrants.infoPKB),
                  _buildButtonColumnLayananLain(
                      '${Environment.iconAssets}other.png',
                      Dictionary.otherMenus),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  _buildFailure(AccountProfileFailure state) {
    return Container(child: Text(state.error));
  }

  _buildContent(AccountProfileLoaded state) {
    Widget firstRowShortcuts = Container(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _showcaseRWActivity(),
          _buildButtonColumn('${Environment.iconAssets}survey.png',
              Dictionary.survey, NavigationConstrants.Survey),
          _buildButtonColumn('${Environment.iconAssets}polling.png',
              Dictionary.polling, NavigationConstrants.Polling),
          _showcaseImportantInfo()
        ],
      ),
    );

    Widget notVerifyRowShortcuts = Container(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _showcaseImportantInfo(),
          _buildButtonColumn(
              '${Environment.iconAssets}administrasi-1.png',
              Dictionary.administration,
              NavigationConstrants.AdministrationList),
          _buildButtonColumn('${Environment.iconAssets}icon_esamsat.png',
              Dictionary.infoPKB, NavigationConstrants.infoPKB),
          _buildButtonColumnLayananLain(
              '${Environment.iconAssets}other.png', Dictionary.otherMenus),
        ],
      ),
    );

    secondRowShortcuts() {
      return Container(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildButtonColumn(
                '${Environment.iconAssets}administrasi-1.png',
                Dictionary.administration,
                NavigationConstrants.AdministrationList),
            _buildButtonColumn('${Environment.iconAssets}aspirasi.png',
                Dictionary.aspiration, NavigationConstrants.Aspirasi),
            _buildButtonColumn('${Environment.iconAssets}nomorpenting.png',
                Dictionary.phoneBook, NavigationConstrants.Phonebook),
            _buildButtonColumnLayananLain(
                '${Environment.iconAssets}other.png', Dictionary.otherMenus),
          ],
        ),
      );
    }

    topContainer(UserInfoModel record) {
      return Container(
        alignment: Alignment.topCenter,
        padding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
            color: Colors.white.withOpacity(0.05),
            offset: Offset(0.0, 0.05),
          ),
        ]),
        child: record.roleLabel == 'Pengguna'
            ? notVerifyRowShortcuts
            : Column(
                children: <Widget>[
                  firstRowShortcuts,
                  const SizedBox(
                    height: 8.0,
                  ),
                  secondRowShortcuts()
                ],
              ),
      );
    }

    ;

    return Column(
      children: [
        (state.record.isVerified == null || state.record.isVerified == 0) &&
                state.record.roleLabel == 'RW'
            ? isUserSubmitVerification
                ? _afterSubmitVerifyInfo()
                : _verifyInfo(state.record)
            : Container(),
        topContainer(state.record),
      ],
    );
  }

  _initializeShowcase() async {
    bool hasShown = await Preferences.hasShowcaseHome();

    if (hasShown == null || hasShown == false) {
      Future.delayed(
          Duration(milliseconds: 200),
          () => ShowCaseWidget.of(showcaseContext)
              .startShowCase([_showcaseOne, _showcaseTwo, _showcaseThree]));
    } else {
      Future.delayed(Duration(milliseconds: 500), () => _showProfile());
    }
  }

  _verifyInfo(UserInfoModel record) {
    return GestureDetector(
      child: Card(
        margin: const EdgeInsets.only(left: 20, right: 20, top: 20),
        shadowColor: Colors.black.withOpacity(0.4),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: BorderSide(width: 1, color: Colors.grey[300])),
        child: Container(
          padding: const EdgeInsets.all(20),
          child: Row(
            children: <Widget>[
              Image.asset('${Environment.imageAssets}verify_info_image.png',
                  height: 46, width: 46),
              const SizedBox(width: Dimens.sizedBoxHeight),
              Flexible(
                child: Text(
                  Dictionary.infoUpdateAccount,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: 12,
                      height: 1.5,
                      fontFamily: FontsFamily.roboto,
                      color: Colors.grey[600]),
                ),
              )
            ],
          ),
        ),
      ),
      onTap: () async {
        var result = await Navigator.of(context).pushNamed(
            NavigationConstrants.RegisterVerification,
            arguments: record);

        if (result != null) {
          if (mounted) {
            setState(() {
              isUserSubmitVerification = result;
            });
          }
        }
      },
    );
  }

  _afterSubmitVerifyInfo() {
    return Card(
      margin: const EdgeInsets.only(left: 20, right: 20, top: 20),
      shadowColor: Colors.black.withOpacity(0.4),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
          side: BorderSide(width: 1, color: color.Colors.darkblue)),
      child: Container(
        color: color.Colors.blueCardColor,
        padding: const EdgeInsets.all(20),
        child: Row(
          children: <Widget>[
            Image.asset('${Environment.imageAssets}image_info_verification.png',
                height: 60, width: 60),
            const SizedBox(width: Dimens.sizedBoxHeight),
            Flexible(
              child: Text(
                Dictionary.infoAfterSubmitVerification,
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 12,
                    height: 1.5,
                    fontFamily: FontsFamily.roboto,
                    color: color.Colors.darkblue),
              ),
            )
          ],
        ),
      ),
    );
  }

  //TODO: perlu di refactor kembali
  void _mainHomeLayananBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10.0),
            topRight: Radius.circular(10.0),
          ),
        ),
        elevation: 60.0,
        builder: (BuildContext context) {
          return Container(
            margin: const EdgeInsets.only(bottom: 20.0),
            child: Wrap(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(top: 14.0),
                      color: Colors.black,
                      height: 1.5,
                      width: 40.0,
                    ),
                  ],
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin:
                      const EdgeInsets.only(left: Dimens.padding, top: 10.0),
                  child: Text(
                    Dictionary.otherMenus,
                    style:
                        TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600),
                  ),
                ),
                Container(
                  alignment: Alignment.topCenter,
                  padding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                  decoration: BoxDecoration(boxShadow: [
                    BoxShadow(
                      color: Colors.white.withOpacity(0.05),
                      offset: Offset(0.0, 0.05),
                    ),
                  ]),
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: userInfo != null &&
                                userInfo.roleLabel == 'Pengguna'
                            ? Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  _buildButtonColumn(
                                      '${Environment.iconAssets}survey.png',
                                      Dictionary.survey,
                                      NavigationConstrants.Survey),
                                  _buildButtonColumn(
                                      '${Environment.iconAssets}polling.png',
                                      Dictionary.polling,
                                      NavigationConstrants.Polling),
                                  _buildButtonColumn(
                                      '${Environment.iconAssets}nomorpenting.png',
                                      Dictionary.phoneBook,
                                      NavigationConstrants.Phonebook),
                                  _buildButtonColumn(
                                      '${Environment.iconAssets}saber_hoax.png',
                                      Dictionary.saberHoax,
                                      NavigationConstrants.SaberHoax)
                                ],
                              )
                            : Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  _buildButtonColumn(
                                      '${Environment.iconAssets}icon_esamsat.png',
                                      Dictionary.infoPKB,
                                      NavigationConstrants.infoPKB),
                                  _buildButtonColumn(
                                      '${Environment.iconAssets}saber_hoax.png',
                                      Dictionary.saberHoax,
                                      NavigationConstrants.SaberHoax)
                                ],
                              ),
                      ),

                      const SizedBox(
                        height: 8.0,
                      ),
                      // secondRowShortcuts
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }

  _showcaseRWActivity() {
    return Expanded(
      child: BaseShowCase.showcaseWidget(
        key: _showcaseOne,
        context: context,
        nipLocation: NipLocation.TOP_LEFT,
        nipPaddingTopLeft: 35.0,
        widgets: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(bottom: 5),
                alignment: Alignment.topLeft,
                child: Text(
                  Dictionary.rwActivities,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                Dictionary.showcaseHome1,
                style: TextStyle(color: Colors.grey[800]),
              ),
            ],
          )
        ],
        onOkTap: () async {
          ShowCaseWidget.of(showcaseContext).completed(_showcaseOne);
        },
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(2.0),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 6.0,
                      color: Colors.black.withOpacity(.2),
                      offset: Offset(2.0, 4.0),
                    ),
                  ],
                  borderRadius: BorderRadius.circular(12.0),
                  color: Colors.white),
              child: IconButton(
                color: Theme.of(context).textTheme.bodyText1.color,
                icon: Image.asset(
                    '${Environment.iconAssets}icon_kegiatan_rw.png'),
                onPressed: () async {
                  final result = await Navigator.push(
                    context,
                    // Create the SelectionScreen in the next step.
                    MaterialPageRoute(
                        builder: (context) =>
                            RWActivityScreen(userInfoModel: userInfo)),
                  );
                  if (result == null || result != null) {
                    RateAppHelper.showRateApp(context);
                  }
                },
              ),
            ),
            const SizedBox(height: 5.0),
            Text(Dictionary.rwActivities,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 13,
                  color: Theme.of(context).textTheme.bodyText1.color,
                ))
          ],
        ),
      ),
    );
  }

  _showcaseImportantInfo() {
    return Expanded(
      child: BaseShowCase.showcaseWidget(
        key: _showcaseTwo,
        context: context,
        nipLocation: NipLocation.TOP_RIGHT,
        nipPaddingTopRight: 35.0,
        margin: const EdgeInsets.only(top: 10.0, left: 25.0),
        widgets: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(bottom: 5),
                alignment: Alignment.topLeft,
                child: Text(
                  Dictionary.importantInfo,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                Dictionary.showcaseHome2,
                style: TextStyle(color: Colors.grey[800]),
              ),
            ],
          )
        ],
        onOkTap: () async {
          ShowCaseWidget.of(showcaseContext).completed(_showcaseTwo);
          showingShowcase = true;
          await Preferences.setShowcaseHome(true);

          setState(() {
            isVerify = userInfo.isVerified ?? 0;
            if ((userInfo.isVerified == 0 || userInfo.isVerified == null) &&
                userInfo.fileSKUrl != null) {
              isUserSubmitVerification = true;
            }
          });

          // _infoAfterLoadProfile(userInfo);
        },
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(2.0),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 6.0,
                      color: Colors.black.withOpacity(.2),
                      offset: Offset(2.0, 4.0),
                    ),
                  ],
                  borderRadius: BorderRadius.circular(12.0),
                  color: Colors.white),
              child: IconButton(
                color: Theme.of(context).textTheme.bodyText1.color,
                icon:
                    Image.asset('${Environment.iconAssets}important_info.png'),
                onPressed: () {
                  Navigator.pushNamed(
                      context, NavigationConstrants.importanInformation,
                      arguments: userInfo);
                },
              ),
            ),
            const SizedBox(height: 5.0),
            Text(Dictionary.importantInfo,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 13,
                  color: Theme.of(context).textTheme.bodyText1.color,
                ))
          ],
        ),
      ),
    );
  }

  _infoAfterLoadProfile(UserInfoModel userInfoModel) async {
    var currentTime = (DateTime.now().millisecondsSinceEpoch / 1000);
    if (!userInfoModel.isUsernameUpdated &&
        currentTime > userInfoModel.usernameUpdatePopupAt) {
      _loadDialogChangeUsername();
    }

    bool hasActiveAccount = await Preferences.hasActivationAccount();
    if (hasActiveAccount) {
      await Preferences.setHasActivationAccount(false);
      _loadInfoAccountUserActive(userInfoModel);
    }

    bool hasFirstVerificationAccount =
        await Preferences.hasFirstVerificationAccount();

    if (userInfoModel.isVerified == 1 && hasFirstVerificationAccount) {
      await Preferences.setHasFirstVerificationAccount(false);
      showCustomBottomSheet(
          context: context,
          image: '${Environment.imageAssets}verification_succes_image.png',
          title: Dictionary.verificationSuccess,
          message: Dictionary.verificationSuccessDesc +
              userInfoModel.rw +
              ', ' +
              Dictionary.villageDesc +
              ' ' +
              StringUtils.capitalizeWord(userInfoModel.kelurahan.name) +
              ', ' +
              Dictionary.kecamatan +
              ' ' +
              StringUtils.capitalizeWord(userInfoModel.kecamatan.name) +
              ', ' +
              Dictionary.city +
              ' ' +
              StringUtils.capitalizeWord(userInfoModel.kabkota.name) +
              Dictionary.verificationSuccessDesc2,
          // message: Dictionary.verificationSuccessDesc,
          onPressed: () async {
            Navigator.of(context).pop(true);
          },
          buttonText: Dictionary.okUnderstand);
    }
  }

  _showProfile() async {
    UserInfoModel userInfo = await AuthProfileRepository().getUserInfo();
    int oldTime = await Preferences.getTimeEducationLater();

    if (userInfo.educationLevelId == null || userInfo.jobTypeId == null) {
      if (oldTime == null) {
        await Preferences.setTimeEducationLater(
            DateTime.now().millisecondsSinceEpoch);

        await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SubEditProfileScreen(authUserInfo: userInfo),
          ),
        );
      } else {
        int hours = DateTime.now()
            .difference(DateTime.fromMillisecondsSinceEpoch(oldTime))
            .inHours;

        if (hours >= 24) {
          await Preferences.setTimeEducationLater(
              DateTime.now().millisecondsSinceEpoch);

          await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  SubEditProfileScreen(authUserInfo: userInfo),
            ),
          );
        }
      }
    }
  }

  @override
  void deactivate() {
    _notificationBadgeBloc.add(CheckNotificationBadge());
    super.deactivate();
  }

  @override
  void dispose() {
    _humasJabarListBloc.close();
    _bannerListBloc.close();
    _remoteHomeBloc.close();
    _statisticsBloc.close();
    _importantInfoHomeBloc.close();
    _videoListJabarBloc.close();
    _videoListKokabBloc.close();
    super.dispose();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<AuthProfileRepository>(
        'authProfileRepository', authProfileRepository));
    properties.add(DiagnosticsProperty<UserInfoModel>('userInfo', userInfo));
    properties.add(
        DiagnosticsProperty<BuildContext>('showcaseContext', showcaseContext));
    properties
        .add(DiagnosticsProperty<bool>('showingShowcase', showingShowcase));
    properties.add(IntProperty('isVerify', isVerify));
    properties.add(DiagnosticsProperty<bool>(
        'isUserSubmitVerification', isUserSubmitVerification));
    properties.add(DiagnosticsProperty<Map<String, dynamic>>(
        'ramadhanWidget', ramadhanWidget));
  }
}
