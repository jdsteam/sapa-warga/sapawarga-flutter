import 'dart:convert';

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/remote_home/Bloc.dart';
import 'package:sapawarga/components/OpenChromeSapariBrowser.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/BasicUtils.dart';
import 'package:pedantic/pedantic.dart';
import 'dart:math' as math;

class Announcement extends StatefulWidget {
  Announcement({Key key}) : super(key: key);

  @override
  _AnnouncementState createState() => _AnnouncementState();
}

class _AnnouncementState extends State<Announcement> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RemoteHomeBloc, RemoteHomeState>(
      builder: (context, state) {
        return state is RemoteHomeLoading
            ? _buildLoading()
            : state is RemoteHomeLoaded
                ? _buildContent(state.remoteConfig)
                : Container();
      },
    );
  }

  _buildContent(RemoteConfig remoteConfig) {
    Map<String, dynamic> dataAnnouncement =
        json.decode(remoteConfig.getString(FirebaseConfig.announcementKey));

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: dataAnnouncement['enabled']
          ? Container(
              width: (MediaQuery.of(context).size.width),
              padding: const EdgeInsets.all(10.0),
              margin: const EdgeInsets.only(left: 10, right: 10),
              decoration: BoxDecoration(
                  color: Color(0xFFF9EFD0),
                  borderRadius: BorderRadius.circular(8.0)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  dataAnnouncement['title'] != null &&
                          dataAnnouncement['title'].toString().isNotEmpty
                      ? Padding(
                          padding: const EdgeInsets.only(bottom: 5.0),
                          child: Text(dataAnnouncement['title'],
                              style: TextStyle(
                                  fontFamily: FontsFamily.productSans,
                                  fontSize: 13.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[600])),
                        )
                      : Container(),
                  RichText(
                    text: TextSpan(children: [
                      TextSpan(
                        text: dataAnnouncement['content'] != null
                            ? dataAnnouncement['content']
                            : '',
                        style: TextStyle(
                            fontSize: 13.0,
                            color: Colors.grey[600],
                            fontFamily: FontsFamily.productSans),
                      ),
                      dataAnnouncement['action_url'].toString().isNotEmpty
                          ? TextSpan(
                              text: Dictionary.moreDetail,
                              style: TextStyle(
                                  color: clr.Colors.green,
                                  fontFamily: FontsFamily.productSans,
                                  fontWeight: FontWeight.bold),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () async {
                                  String url =
                                      await StringUtils.userDataUrlAppend(
                                          dataAnnouncement['action_url']);
                                  openChromeSafariBrowser(url: url);

                                  String name = dataAnnouncement['name'];
                                  unawaited(AnalyticsHelper.setLogEvent(
                                      Analytics.EVENT_VIEW_ANNOUNCEMENT,
                                      <String, dynamic>{
                                        'name': name.substring(
                                            0, math.min(name.length, 80))
                                      }));
                                })
                          : TextSpan(text: '')
                    ]),
                  ),
                ],
              ),
            )
          : Container(),
    );
  }

  _buildLoading() {
    return Skeleton(
      child: Container(
        width: (MediaQuery.of(context).size.width),
        height: 74.0,
        padding: const EdgeInsets.all(10.0),
        margin: const EdgeInsets.only(left: 10, right: 10),
        decoration: BoxDecoration(
            color: Color(0xFFF9EFD0), borderRadius: BorderRadius.circular(8.0)),
      ),
    );
  }
}
