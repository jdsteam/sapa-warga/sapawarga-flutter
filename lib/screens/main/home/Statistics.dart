import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:sapawarga/blocs/statistics/Bloc.dart';
import 'package:sapawarga/components/OpenChromeSapariBrowser.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/UrlThirdParty.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/statistics_pikobar_model.dart';

class Statistics extends StatefulWidget {
  final StatisticsBloc statisticsBloc;

  Statistics({Key key, this.statisticsBloc}) : super(key: key);

  @override
  _StatisticsState createState() => _StatisticsState();
}

class _StatisticsState extends State<Statistics> {
  final formatter = NumberFormat("#,###");

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StatisticsBloc, StatisticsState>(
        bloc: widget.statisticsBloc,
        builder: (context, state) {
          if (state is StatisticsLoading) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 8.0,
                  child: Container(
                    color: Color(0xFFE5E5E5),
                  ),
                ),
                _buildLoading(),
                const SizedBox(height: 10.0)
              ],
            );
          } else if (state is StatisticsLoaded) {
            return _buildStatistics(state.record);
          } else {
            return Container();
          }
        });
  }

  _buildStatistics(StatisticsPikobarModel record) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 8.0,
          child: Container(
            color: Color(0xFFE5E5E5),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(Dimens.padding),
          decoration: BoxDecoration(color: Colors.white, boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.1),
                offset: Offset(0.0, 1),
                blurRadius: 4.0),
          ]),
          child: Column(
            children: <Widget>[
              _buildContent(record),
              const SizedBox(
                height: 10.0,
              )
            ],
          ),
        ),
      ],
    );
  }

  _buildLoading() {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.all(16.0),
      child: Skeleton(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              Dictionary.statistics,
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontFamily: FontsFamily.productSans,
                  fontSize: 12.0),
            ),
            const SizedBox(height: Dimens.padding),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _buildContainer('', Dictionary.positif, Dictionary.positif, '-',
                    3, Dictionary.people, Colors.grey[600], Colors.grey[600]),
                _buildContainer('', Dictionary.recover, Dictionary.recover, '-',
                    3, Dictionary.people, Colors.grey[600], Colors.grey[600]),
                _buildContainer('', Dictionary.die, Dictionary.die, '-', 3,
                    Dictionary.people, Colors.grey[600], Colors.grey[600]),
              ],
            ),
            const SizedBox(height: Dimens.padding),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _buildContainer(
                    '',
                    Dictionary.probable,
                    Dictionary.probable,
                    '-',
                    2,
                    Dictionary.people,
                    Colors.grey[600],
                    Colors.grey[600]),
                _buildContainer('', Dictionary.suspect, Dictionary.suspect, '-',
                    2, Dictionary.people, Colors.grey[600], Colors.grey[600]),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Container _buildContent(StatisticsPikobarModel data) {
    final record = data.data.first;

    final DateFormat format = DateFormat.yMMMMd('id');
    final String lastUpdate = format.format(DateTime.now());

    if (data.data.isEmpty) {
      return Container(
        child: Center(
          child: Text(Dictionary.errorStatisticsNotExists),
        ),
      );
    }

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Text(
                  Dictionary.statistics,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      fontFamily: FontsFamily.productSans,
                      fontSize: 12.0),
                ),
              ),
              Text(
                lastUpdate,
                style: TextStyle(
                    color: Colors.grey[650],
                    fontFamily: FontsFamily.productSans,
                    fontSize: 12.0),
              ),
            ],
          ),
          const SizedBox(height: Dimens.padding),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildContainer(
                  '${Environment.imageAssets}bg-positif.png',
                  Dictionary.positif,
                  Dictionary.positif,
                  record.confirmationTotal.toString(),
                  3,
                  Dictionary.people,
                  Colors.white,
                  Colors.white),
              _buildContainer(
                  '${Environment.imageAssets}bg-sembuh.png',
                  Dictionary.recover,
                  Dictionary.recover,
                  record.confirmationSelesai.toString(),
                  3,
                  Dictionary.people,
                  Colors.white,
                  Colors.white),
              _buildContainer(
                  '${Environment.imageAssets}bg-meninggal.png',
                  Dictionary.die,
                  Dictionary.die,
                  record.confirmationMeninggal.toString(),
                  3,
                  Dictionary.people,
                  Colors.white,
                  Colors.white),
            ],
          ),
          const SizedBox(height: Dimens.padding),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildContainer(
                  '',
                  Dictionary.suspect,
                  Dictionary.opdDesc,
                  record.suspectTotal.toString(),
                  2,
                  Dictionary.people,
                  Colors.grey[600],
                  clr.Colors.green),
              _buildContainer(
                  '',
                  Dictionary.probable,
                  Dictionary.pdpDesc,
                  record.probableTotal.toString(),
                  2,
                  Dictionary.people,
                  Colors.grey[600],
                  clr.Colors.green),
            ],
          )
        ],
      ),
    );
  }

  String getDataProcess(int totalData, int dataDone) {
    int processData = totalData - dataDone;
    return processData.toString();
  }

  String getDataProcessPercent(int totalData, int dataDone) {
    double processData =
        100 - num.parse(((dataDone / totalData) * 100).toStringAsFixed(2));

    return '(' + processData.toString() + '%)';
  }

  _buildContainer(String image, String title, String description, String count,
      int length, String label, Color colorTextTitle, Color colorNumber) {
    String countStatistic = count;
    if (countStatistic != null &&
        countStatistic.isNotEmpty &&
        countStatistic != '-') {
      try {
        countStatistic =
            formatter.format(int.parse(countStatistic)).replaceAll(',', '.');
      } catch (e) {
        print(e.toString());
      }
    }

    return Expanded(
      child: InkWell(
        child: Container(
          width: (MediaQuery.of(context).size.width / length),
          padding:
              const EdgeInsets.only(left: 5.0, right: 5.0, top: 15, bottom: 15),
          margin: const EdgeInsets.symmetric(horizontal: 2.5),
          decoration: BoxDecoration(
              image: image != '' && image != null
                  ? DecorationImage(fit: BoxFit.fill, image: AssetImage(image))
                  : null,
              border: image == null || image == ''
                  ? Border.all(color: Colors.grey[400])
                  : null,
              borderRadius: BorderRadius.circular(8.0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(left: 5.0),
                child: Text(title,
                    style: TextStyle(
                        fontSize: 13.0,
                        color: colorTextTitle,
                        fontWeight: FontWeight.bold,
                        fontFamily: FontsFamily.productSans)),
              ),
              Container(
                margin: const EdgeInsets.only(top: Dimens.padding, left: 5.0),
                child: Text(countStatistic,
                    style: TextStyle(
                        fontSize: 22.0,
                        color: colorNumber,
                        fontWeight: FontWeight.bold,
                        fontFamily: FontsFamily.productSans)),
              )
            ],
          ),
        ),
        onTap: () {
          openChromeSafariBrowser(url: UrlThirdParty.urlPikobar);
        },
      ),
    );
  }
}
