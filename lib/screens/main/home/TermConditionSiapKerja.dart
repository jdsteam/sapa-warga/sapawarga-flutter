import 'package:flutter/material.dart';
import 'package:sapawarga/components/SiapKerjaScreen.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/utilities/SharedPreferences.dart';

class TermConditionSiapKerja extends StatefulWidget {
  final UserInfoModel userInfoModel;
  final String url;
  final bool isAdEmail;
  final String termCondition;
  final Map<String, dynamic> secretEnvSiapKerja;

  const TermConditionSiapKerja(
      {Key key,
      this.userInfoModel,
      this.url,
      this.isAdEmail,
      this.termCondition,
      this.secretEnvSiapKerja})
      : super(key: key);

  @override
  _TermConditionSiapKerjaState createState() => _TermConditionSiapKerjaState();
}

class _TermConditionSiapKerjaState extends State<TermConditionSiapKerja> {
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.only(top: 10),
                alignment: Alignment.topRight,
                child: IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.pop(context, false);
                    if (widget.isAdEmail) {
                      Navigator.pop(context, false);
                    }
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      Dictionary.titleTermCondition,
                      style: TextStyle(
                          fontSize: 20.0,
                          color: clr.Colors.veryDarkGrey,
                          fontFamily: FontsFamily.roboto,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      Dictionary.descTermCondition,
                      style: TextStyle(
                          fontSize: 14.0,
                          wordSpacing: 1,
                          height: 1.5,
                          fontFamily: FontsFamily.roboto,
                          color: clr.Colors.veryDarkGrey),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Scrollbar(
                      isAlwaysShown: true,
                      controller: _scrollController,
                      child: Container(
                        color: clr.Colors.greyBackground,
                        height: 320,
                        child: ListView(
                          controller: _scrollController,
                          padding: const EdgeInsets.all(10.0),
                          children: [
                            Html(
                                data: widget.termCondition,
                                defaultTextStyle: TextStyle(
                                    fontSize: 14.0,
                                    wordSpacing: 1,
                                    height: 1.5,
                                    fontFamily: FontsFamily.roboto,
                                    color: clr.Colors.greyText2),
                                customTextAlign: (dom.Node node) {
                                  return TextAlign.justify;
                                }),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      bottomSheet: Container(
        margin: const EdgeInsets.only(left: 10, right: 10, bottom: 30),
        height: 45,
        child: RaisedButton(
          color: clr.Colors.darkblue,
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          child: Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.symmetric(vertical: 13),
            child: Text(
              Dictionary.agree,
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: FontsFamily.roboto,
                  fontWeight: FontWeight.w600),
            ),
          ),
          onPressed: () async {
            await Preferences.setStatusTermCondition(true);

            var record = await Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => SiapKerjaScreen(
                  url: widget.url,
                  userInfoModel: widget.userInfoModel,
                  secretEnvSiapKerja: widget.secretEnvSiapKerja,
                ),
              ),
            );

            if (record == null) {
              Navigator.pop(context, false);
              if (widget.isAdEmail) {
                Navigator.pop(context, false);
              }
            }
            // openChromeSafariBrowser(url: widget.url);
          },
        ),
      ),
    );
  }
}
