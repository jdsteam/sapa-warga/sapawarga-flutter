import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/utilities/BasicUtils.dart';

class RamadanDetail extends StatelessWidget {
  final List items;

  const RamadanDetail({Key key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: _dialogContent(context),
    );
  }

  _dialogContent(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Dimens.padding),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(4.0),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      child: ListView.separated(
        padding: const EdgeInsets.all(8),
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                StringUtils.capitalizeWord(items[index]['title']),
                style: TextStyle(
                  color: Colors.grey[600],
                  fontSize: 16.0,
                ),
              ),
              Text(
                items[index]['time'].toString(),
                style: TextStyle(
                  color: Colors.grey[700],
                  fontSize: 20.0,
                ),
              )
            ],
          );
        },
        separatorBuilder: (context, index) => Divider(
          color: Colors.grey[200],
          thickness: 1,
        ),
        itemCount: items.length,
      ),
    );
  }
}
