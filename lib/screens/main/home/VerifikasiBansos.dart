import 'dart:async';
import 'dart:convert';
import 'dart:math' as math;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pedantic/pedantic.dart';
import 'package:sapawarga/blocs/remote_home/Bloc.dart';
import 'package:sapawarga/components/BrowserScreen.dart';
import 'package:sapawarga/components/OpenChromeSapariBrowser.dart';
import 'package:sapawarga/components/SiapKerjaScreen.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/CookingOilDistributionHelper.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:url_launcher/url_launcher.dart';

class VerificationSection extends StatefulWidget {
  final UserInfoModel userInfo;

  VerificationSection({Key key, this.userInfo}) : super(key: key);

  @override
  _VerificationSectionState createState() => _VerificationSectionState();
}

class _VerificationSectionState extends State<VerificationSection> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RemoteHomeBloc, RemoteHomeState>(
      builder: (context, state) {
        return state is RemoteHomeLoading
            ? _buildLoading()
            : state is RemoteHomeLoaded
                ? _buildContent(state.remoteConfig)
                : Container();
      },
    );
  }

  _buildContent(RemoteConfig remoteConfig) {
    Map<String, dynamic> secretEnvSiapKerja =
        json.decode(remoteConfig.getString(FirebaseConfig.secretEnvSiapKerja));
    Map<String, dynamic> data =
        json.decode(remoteConfig.getString(FirebaseConfig.highlightMenuKey));
    var dataTermCondition =
        json.decode(remoteConfig.getString(FirebaseConfig.descTermCondition));

    List<dynamic> items = data['items'];

    return data['enabled']
        ? Container(
            color: Color(0xFFE5E5E5),
            padding: const EdgeInsets.only(top: 8.0),
            child: Container(
                color: Colors.white,
                padding: const EdgeInsets.symmetric(
                    vertical: 8.0, horizontal: Dimens.padding),
                child: ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: items.length,
                    itemBuilder: (context, index) {
                      return items[index]['enabled'] &&
                              items[index]['cover-image'] != null
                          ? Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: CachedNetworkImage(
                                  imageUrl: items[index]['cover-image'],
                                  imageBuilder: (context, imageProvider) =>
                                      GestureDetector(
                                        onTap: () async {
                                          String feature_name =
                                              items[index]['feature_name'];
                                          unawaited(AnalyticsHelper.setLogEvent(
                                              Analytics
                                                  .EVENT_VIEW_HIGHLIGHT_MENU,
                                              <String, dynamic>{
                                                'name': feature_name.substring(
                                                    0,
                                                    math.min(
                                                        feature_name.length,
                                                        80))
                                              }));
                                          //special condition for menu cooking oil distribution
                                          String url = await _userDataUrlAppend(
                                              items[index]['feature_name'] ==
                                                      Dictionary
                                                          .cookingOilDistribution
                                                  ? items[index]['url'] +
                                                      CookingOilDistributionHelper
                                                          .paramCookingOil(
                                                              widget.userInfo,
                                                              remoteConfig)
                                                  : items[index]['url']);

                                          bool statusTermCondition =
                                              await Preferences
                                                  .getStatusTermCondition();
                                          String email =
                                              widget.userInfo.email ?? '';
                                          if (items[index]['isTermCondition'] &&
                                              !statusTermCondition) {
                                            if (email.isNotEmpty) {
                                              await Navigator.of(context)
                                                  .pushNamed(
                                                NavigationConstrants
                                                    .termConditionSiapKerja,
                                                arguments: [
                                                  widget.userInfo,
                                                  url,
                                                  false,
                                                  dataTermCondition,
                                                  secretEnvSiapKerja
                                                ],
                                              );
                                            } else {
                                              await Navigator.of(context)
                                                  .pushNamed(
                                                NavigationConstrants.addEmail,
                                                arguments: [
                                                  widget.userInfo,
                                                  url,
                                                  dataTermCondition,
                                                  secretEnvSiapKerja
                                                ],
                                              );
                                            }
                                          } else {
                                            switch (items[index]['mode']) {
                                              case 'webview':
                                                await Navigator.of(context)
                                                    .push(
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        BrowserScreen(
                                                            url: url,
                                                            useInAppWebView:
                                                                false),
                                                  ),
                                                );
                                                break;
                                              case 'inappwebview':
                                                items[index]['feature_name'] ==
                                                        'JMSC'
                                                    ? email.isNotEmpty
                                                        ? await Navigator.of(
                                                                context)
                                                            .push(
                                                            MaterialPageRoute(
                                                              builder: (context) =>
                                                                  SiapKerjaScreen(
                                                                url: url,
                                                                userInfoModel:
                                                                    widget
                                                                        .userInfo,
                                                                secretEnvSiapKerja:
                                                                    secretEnvSiapKerja,
                                                              ),
                                                            ),
                                                          )
                                                        : await Navigator.of(
                                                                context)
                                                            .pushNamed(
                                                            NavigationConstrants
                                                                .addEmail,
                                                            arguments: [
                                                              widget.userInfo,
                                                              url,
                                                              dataTermCondition,
                                                              secretEnvSiapKerja
                                                            ],
                                                          )
                                                    : await Navigator.of(
                                                            context)
                                                        .push(
                                                        MaterialPageRoute(
                                                          builder: (context) =>
                                                              BrowserScreen(
                                                            url: url,
                                                          ),
                                                        ),
                                                      );
                                                break;
                                              case 'chrome':
                                                openChromeSafariBrowser(
                                                    url: url);
                                                break;
                                              case 'redirect':
                                                _launchURL(url);
                                                break;
                                              default:
                                                openChromeSafariBrowser(
                                                    url: url);
                                            }
                                          }
                                        },
                                        child: Image(
                                            image: imageProvider,
                                            fit: BoxFit.fitWidth),
                                      ),
                                  errorWidget: (context, url, error) =>
                                      Container()),
                            )
                          : Container();
                    })))
        : Container();
  }

  _buildLoading() {
    return Container(
        color: Color(0xFFE5E5E5),
        padding: const EdgeInsets.only(top: 8.0),
        child: Container(
            color: Colors.white,
            padding: const EdgeInsets.all(Dimens.padding),
            child: Skeleton(
              child: Container(
                width: (MediaQuery.of(context).size.width),
                height: 80.0,
                padding: const EdgeInsets.all(10.0),
                margin: const EdgeInsets.only(left: 10, right: 10),
                decoration: BoxDecoration(
                    color: Color(0xFFF9EFD0),
                    borderRadius: BorderRadius.circular(8.0)),
              ),
            )));
  }

  Future<String> _userDataUrlAppend(String url) async {
    String token = await AuthRepository().getToken();
    UserInfoModel user = await AuthProfileRepository().getUserInfo();

    if (url == null) {
      return url;
    } else {
      Map<String, String> usrMap = {'_userToken_': '', '_userID_': ''};
      String link = '';

      if (user != null) {
        usrMap = {'_userToken_': token, '_userID_': user.id.toString()};
      }

      usrMap.forEach((key, value) {
        link = url.replaceAll(key, value);
      });

      return link;
    }
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
