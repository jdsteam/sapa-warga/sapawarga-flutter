import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/account_profile/Bloc.dart';
import 'package:sapawarga/components/BlockCircleLoading.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/SiapKerjaScreen.dart';
import 'package:sapawarga/components/TextFieldComponent.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:sapawarga/utilities/Validations.dart';

class AddEmailScreen extends StatefulWidget {
  final UserInfoModel userInfoModel;
  final String url;
  final String termCondition;
  final Map<String, dynamic> secretEnvSiapKerja;

  const AddEmailScreen(
      {Key key,
      this.userInfoModel,
      this.url,
      this.termCondition,
      this.secretEnvSiapKerja})
      : super(key: key);

  @override
  _AddEmailScreenState createState() => _AddEmailScreenState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(
        DiagnosticsProperty<UserInfoModel>('userInfoModel', userInfoModel));
    properties.add(StringProperty('url', url));
    properties.add(StringProperty('termCondition', termCondition));
    properties.add(DiagnosticsProperty<Map<String, String>>(
        'secretEnvSiapKerja', secretEnvSiapKerja));
  }
}

class _AddEmailScreenState extends State<AddEmailScreen> {
  final _emailTextController = TextEditingController();
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  AccountProfileEditBloc _accountProfileEditBloc;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '',
        ),
        backgroundColor: Colors.white,
        elevation: 1.0,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: BlocProvider<AccountProfileEditBloc>(
        create: (BuildContext context) => _accountProfileEditBloc =
            AccountProfileEditBloc(
                authProfileRepository: AuthProfileRepository()),
        child: BlocListener<AccountProfileEditBloc, AccountProfileEditState>(
          bloc: _accountProfileEditBloc,
          listener: (context, state) async {
            if (state is AccountProfileEditLoading) {
              _isLoading = true;
              await blockCircleLoading(context: context);
            } else if (state is AccountProfileEditUpdated) {
              if (_isLoading) {
                _isLoading = false;
                Navigator.of(context, rootNavigator: true).pop();
              }

              bool statusTermCondition =
                  await Preferences.getStatusTermCondition();

              if (!statusTermCondition) {
                await Navigator.of(context).pushNamed(
                    NavigationConstrants.termConditionSiapKerja,
                    arguments: [
                      widget.userInfoModel,
                      widget.url,
                      true,
                      widget.termCondition,
                      widget.secretEnvSiapKerja
                    ]);
              } else {
                var record = await Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => SiapKerjaScreen(
                      url: widget.url,
                      userInfoModel: widget.userInfoModel,
                      secretEnvSiapKerja: widget.secretEnvSiapKerja,
                    ),
                  ),
                );

                if (record == null) {
                  Navigator.pop(context, false);
                }
              }
            } else if (state is AccountProfileEditFailure) {
              if (_isLoading) {
                _isLoading = false;
                Navigator.of(context, rootNavigator: true).pop();
              }

              await AnalyticsHelper.setLogEvent(
                  Analytics.EVENT_EDIT_ACCOUNT_FAILED,
                  <String, dynamic>{'message': '${state.error.toString()}'});

              await showDialog(
                  context: context,
                  builder: (BuildContext context) => DialogTextOnly(
                        description: state.error.toString().isNotEmpty
                            ? state.error.toString()
                            : Dictionary.updateProfileFailed,
                        buttonText: Dictionary.ok,
                        onOkPressed: () {
                          Navigator.of(context, rootNavigator: true)
                              .pop(); // To close the dialog
                        },
                      ));
            } else if (state is AccountProfileEditValidationError) {
              if (_isLoading) {
                _isLoading = false;
                Navigator.of(context, rootNavigator: true).pop();
              }

              await showDialog(
                context: context,
                builder: (BuildContext context) => DialogTextOnly(
                  description: state.errors.toString().contains('email')
                      ? state.errors['email'][0].toString()
                      : state.errors.toString(),
                  buttonText: Dictionary.ok,
                  onOkPressed: () {
                    Navigator.of(context, rootNavigator: true)
                        .pop(); // To close the dialog
                  },
                ),
              );
            } else {
              if (_isLoading) {
                _isLoading = false;
                Navigator.of(context, rootNavigator: true).pop();
              }
            }
          },
          child: BlocBuilder<AccountProfileEditBloc, AccountProfileEditState>(
            bloc: _accountProfileEditBloc,
            builder: (context, state) => Form(
              key: _formKey,
              child: Container(
                margin: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      Dictionary.addEmail,
                      style: TextStyle(
                          fontSize: 20.0,
                          color: clr.Colors.veryDarkGrey,
                          fontFamily: FontsFamily.roboto,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      Dictionary.addEmailDesc,
                      style: TextStyle(
                          fontSize: 14.0,
                          wordSpacing: 1,
                          height: 1.5,
                          fontFamily: FontsFamily.roboto,
                          color: clr.Colors.darkGrey),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextFieldComponent(
                        title: Dictionary.email,
                        hintText: Dictionary.descEmail,
                        controller: _emailTextController,
                        validation: Validations.emailValidation,
                        textInputType: TextInputType.emailAddress,
                        isEdit: true),
                    const SizedBox(
                      height: 20,
                    ),
                    RaisedButton(
                      color: clr.Colors.darkblue,
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.symmetric(vertical: 13),
                        child: Text(
                          Dictionary.send,
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: FontsFamily.roboto,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          UserInfoModel userInfo = widget.userInfoModel;
                          userInfo.email = _emailTextController.text;
                          _accountProfileEditBloc.add(
                            AccountProfileEditSubmit(userInfoModel: userInfo),
                          );
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
