import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:sapawarga/blocs/ramadan/bloc/ramadan_bloc.dart';
import 'package:sapawarga/blocs/remote_home/Bloc.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/models/ramadan_model.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/screens/main/home/ramadan_detail.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';

class Ramadan extends StatefulWidget {
  final UserInfoModel userInfo;

  const Ramadan({Key key, this.userInfo}) : super(key: key);

  @override
  _RamadanState createState() => _RamadanState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<UserInfoModel>('userInfo', userInfo));
  }
}

class _RamadanState extends State<Ramadan> {
  final authProfileRepository = AuthProfileRepository();
  RamadanBloc ramadanBloc;

  @override
  void initState() {
    ramadanBloc = BlocProvider.of<RamadanBloc>(context);

    super.initState();
  }

  /// format date timestamp to date format
  _formatDate(int date) {
    Timestamp stamp = Timestamp.fromMillisecondsSinceEpoch(date);
    DateFormat format = DateFormat('EEEE, dd MMMM yyyy', 'id');
    var dateTimeString = format.format(stamp.toDate());

    return dateTimeString;
  }

  get _getKabkota => widget.userInfo?.kabkota?.name ?? '';

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RemoteHomeBloc, RemoteHomeState>(
      builder: (context, state) {
        return state is RemoteHomeLoaded
            ? _buildContent(state.remoteConfig)
            : Container();
      },
    );
  }

  _buildContent(RemoteConfig remoteConfig) {
    // get data remote config ramadan

    return BlocBuilder<RamadanBloc, RamadanState>(
      bloc: ramadanBloc,
      builder: (context, state) {
        return state is RamadanLoading
            ? CupertinoActivityIndicator()
            : state is RamadanLoaded
                ? _buildContentRamadan(state.item)
                : Container();
      },
    );
  }

  _buildContentRamadan(Item item) {
    return Column(
      children: [
        SizedBox(
          height: 8.0,
          child: Container(
            color: Color(0xFFE5E5E5),
          ),
        ),
        Card(
          elevation: 6.0,
          margin: const EdgeInsets.all(Dimens.padding),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4.0),
          ),
          shadowColor: Colors.black.withOpacity(0.4),
          child: Padding(
            padding: const EdgeInsets.all(Dimens.padding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          _getKabkota,
                          style: TextStyle(
                            color: Color(0xff006430),
                            fontSize: 20.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        const SizedBox(height: 4),
                        Text(
                          _formatDate(item.tanggal),
                          style: TextStyle(
                            color: Colors.black.withOpacity(0.5),
                            fontSize: 14.0,
                          ),
                        ),
                      ],
                    ),
                    IconButton(
                      alignment: Alignment.topRight,
                      padding: const EdgeInsets.all(0),
                      icon: Icon(
                        Icons.open_in_new,
                        color: Color(0xff006430),
                        size: 20.0,
                      ),
                      onPressed: () {
                        _openDetail(item);
                      },
                    )
                  ],
                ),
                const SizedBox(height: Dimens.padding),
                ..._filter(item),
                const SizedBox(height: 8.0),
                Text(
                  'Sumber: Bimas Islam Kementerian Agama',
                  style: TextStyle(
                    color: Colors.black.withOpacity(0.5),
                    fontSize: 11.0,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  List<Widget> _filter(Item item) {
    List<Widget> listWidget = List<Widget>();

    // get data Imsak & Magrib
    List items = [
      {"title": "Imsak", "time": item.imsak},
      {"title": "Magrib", "time": item.magrib},
    ];

    for (int i = 0; i < items.length; i++) {
      listWidget.add(
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                items[i]["title"],
                style: TextStyle(
                  color: Color(0xff008444),
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Text(
                items[i]["time"],
                style: TextStyle(
                  color: Colors.green[900],
                  fontSize: 20.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
        ),
      );
    }

    return listWidget;
  }

  _openDetail(Item item) {
    List items = List();
    Map<String, dynamic> itemsJson = item.toJson();

    List exceptItems = ['tanggal', 'ramadhan', 'terbit', 'duha'];

    itemsJson.forEach((key, value) {
      // filter items exception
      bool isExceptItem = exceptItems.contains(key.toLowerCase());

      if (!isExceptItem) {
        items.add({"title": key, "time": value});
      }
    });

    // show dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return RamadanDetail(items: items);
      },
    );

    // add event open detail
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_DETAIL_RAMADAN);
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty<RamadanBloc>('ramadanBloc', ramadanBloc));
    properties.add(DiagnosticsProperty<AuthProfileRepository>(
        'authProfileRepository', authProfileRepository));
  }
}
