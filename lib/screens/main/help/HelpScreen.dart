import 'dart:convert';

import 'package:device_apps/device_apps.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;
import 'package:sapawarga/components/BrowserScreen.dart';
import 'package:sapawarga/components/DialogWidgetContent.dart';
import 'package:sapawarga/components/Expandable.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/UrlThirdParty.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/HelpModel.dart';
import 'package:sapawarga/repositories/HelpRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/WhatsAppHelper.dart';
import 'package:url_launcher/url_launcher.dart';

class HelpScreen extends StatefulWidget {
  HelpScreen({Key key}) : super(key: key);

  @override
  _HelpScreenState createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> {
  final HelpRepository _helpRepository = HelpRepository();
  List<dynamic> csPhoneNumbers;
  dynamic csInfoText;

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.HELP);
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_LIST_HELP);

    csPhoneNumbers = json.decode(FirebaseConfig.callCenterNumbersValue);
    csInfoText = Dictionary.helpAdminWA;

    super.initState();
  }

  Future<RemoteConfig> _initializeRemoteConfig() async {
    final defaultHelpData =
        await rootBundle.loadString('assets/data/helpData.json');
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    await remoteConfig.setDefaults(<String, dynamic>{
      FirebaseConfig.callCenterNumbersKey:
          FirebaseConfig.callCenterNumbersValue,
      FirebaseConfig.helpContentKey: defaultHelpData
    });

    try {
      await remoteConfig.fetch(expiration: Duration(minutes: 30));
      await remoteConfig.activateFetched();
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }

    return remoteConfig;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Dictionary.help,
            style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                fontFamily: FontsFamily.intro),
            maxLines: 1,
            overflow: TextOverflow.ellipsis),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.info),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => BrowserScreen(
                      url: UrlThirdParty.urlCommunityGuideLine,
                    ),
                  ),
                );

                AnalyticsHelper.setLogEvent(
                    Analytics.EVENT_OPEN_GUIDELINES_HELP);
              })
        ],
      ),
      body: FutureBuilder<RemoteConfig>(
        future: _initializeRemoteConfig(),
        builder: (BuildContext context, AsyncSnapshot<RemoteConfig> snapshot) {
          if (snapshot.hasData) {
            final helpData =
                snapshot.data.getString(FirebaseConfig.helpContentKey);

            final dataPhoneNumbers =
                snapshot.data.getString(FirebaseConfig.callCenterNumbersKey);
            if (dataPhoneNumbers != null) {
              csPhoneNumbers = json.decode(dataPhoneNumbers);
            }

            try {
              final dataInfoText =
                  snapshot.data.getString(FirebaseConfig.callCenterTextInfoKey);
              if (dataInfoText != null) {
                csInfoText = json.decode(dataInfoText);
              }
            } catch (e) {
              print(e.toString());
            }

            List<HelpModel> data = _helpRepository.fetchRecords(helpData);

            return ListView.builder(
              padding: const EdgeInsets.only(bottom: 80.0),
              itemCount: data.length,
              itemBuilder: (context, index) => _cardContent(data[index]),
            );
          } else {
            return Center(
              child: _buildLoading(),
            );
          }
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: clr.Colors.blue,
        onPressed: () {
          csPhoneNumbers.length > 1
              ? _openDialogContact()
              : WhatsAppHelper.checkNumber(csPhoneNumbers[0], csInfoText);
        },
        label: Text(Dictionary.contactAdmin),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Widget _buildLoading() {
    return ListView.builder(
      itemCount: 5,
      itemBuilder: (context, index) => Card(
        margin: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
        child: ListTile(
          title: Skeleton(
            width: MediaQuery.of(context).size.width - 140,
            height: 20.0,
          ),
          subtitle: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Skeleton(
              width: MediaQuery.of(context).size.width - 190,
              height: 20.0,
            ),
          ),
          trailing: Skeleton(
            width: 20.0,
            height: 20.0,
          ),
        ),
      ),
    );
  }

  Widget _cardContent(HelpModel dataHelp) {
    return ExpandableNotifier(
      child: ScrollOnExpand(
        scrollOnExpand: false,
        scrollOnCollapse: true,
        child: Card(
          margin: const EdgeInsets.only(top: 10, left: 10, right: 10),
          clipBehavior: Clip.antiAlias,
          child: ScrollOnExpand(
            scrollOnExpand: true,
            scrollOnCollapse: false,
            child: ExpandablePanel(
              tapHeaderToExpand: true,
              tapBodyToCollapse: true,
              headerAlignment: ExpandablePanelHeaderAlignment.center,
              header: Padding(
                padding: const EdgeInsets.all(10),
                child: Text(
                  dataHelp.title,
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                ),
              ),
              expanded: Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Html(
                  data: dataHelp.description.replaceAll('\n', '</br>'),
                  defaultTextStyle:
                      TextStyle(color: Colors.black, fontSize: 14.0),
                  customTextAlign: (dom.Node node) {
                    return TextAlign.justify;
                  },
                  onLinkTap: _launchUrl,
                ),
              ),
              builder: (_, collapsed, expanded) {
                return Padding(
                  padding:
                      const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  child: Expandable(
                    collapsed: collapsed,
                    expanded: expanded,
                    crossFadePoint: 0,
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  _openDialogContact() {
    showDialog(
        context: context,
        builder: (BuildContext context) => DialogWidgetContent(
              title: Dictionary.callCenterList,
              child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount:
                    csPhoneNumbers.length > 5 ? 5 : csPhoneNumbers.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    child: Container(
                      margin:
                          index != 0 ? const EdgeInsets.only(top: 10.0) : null,
                      padding: const EdgeInsets.fromLTRB(
                          Dimens.padding, 10.0, Dimens.padding, 10.0),
                      decoration: BoxDecoration(
                        color: Color(0xffebf8ff),
                        shape: BoxShape.rectangle,
                        border: Border.all(color: clr.Colors.blue),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Wrap(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: Image.asset(
                                '${Environment.iconAssets}whatsapp.png',
                                width: 20,
                                height: 20,
                                color: clr.Colors.green),
                          ),
                          Text(csPhoneNumbers[index],
                              style: TextStyle(
                                  fontFamily: FontsFamily.sourceSansPro,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w600,
                                  color: clr.Colors.green))
                        ],
                      ),
                    ),
                    onTap: () {
                      WhatsAppHelper.checkNumber(
                          csPhoneNumbers[index], csInfoText);
                      Navigator.of(context).pop();
                    },
                  );
                },
              ),
              buttonText: Dictionary.close.toUpperCase(),
              onOkPressed: () {
                Navigator.of(context).pop(); // To close the dialog
              },
            ));
  }

  _launchUrl(String url) async {
    String link = url;

    try {
      bool isInstalled = await DeviceApps.isAppInstalled('com.whatsapp');

      if (url.contains('tel')) {
        link = url.replaceAll('tel:', '');
        if (url[0] == '0') {
          link = url.replaceFirst('0', '+62');
        }

        if (isInstalled) {
          String urlWhatsApp = 'https://wa.me/$link';
          if (await canLaunch(urlWhatsApp)) {
            await launch(urlWhatsApp);

            await AnalyticsHelper.setLogEvent(
                Analytics.EVENT_OPEN_WA_ADMIN_HELP);
          } else {
            await launch('tel://$link');
          }
        } else {
          await launch('tel://$link');

          await AnalyticsHelper.setLogEvent(
              Analytics.EVENT_OPEN_TELP_ADMIN_HELP);
        }
      } else {
        if (await canLaunch(link)) {
          await launch(link);
          if (link.contains('mailto')) {
            await AnalyticsHelper.setLogEvent(
                Analytics.EVENT_OPEN_EMAIL_ADMIN_HELP);
          }
        }
      }
    } catch (_) {
      if (await canLaunch(link)) {
        await launch(link);
      }
    }
  }
}
