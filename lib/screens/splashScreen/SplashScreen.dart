import 'package:flutter/material.dart';
import 'package:sapawarga/environment/Environment.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: Image.asset('${Environment.logoAssets}logo_2.png',
              width: 150.0, height: 150.0),
        ));
  }
}
