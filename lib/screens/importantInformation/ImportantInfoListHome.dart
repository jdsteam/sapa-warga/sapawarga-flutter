import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/important_info/improtant_info_home/Bloc.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/screens/importantInformation/ImportantInfoDetailScreen.dart';

class ImportantInfoListHome extends StatefulWidget {
  final UserInfoModel userInfoModel;

  ImportantInfoListHome({Key key, this.userInfoModel}) : super(key: key);

  @override
  _ImportantInfoListHomeState createState() => _ImportantInfoListHomeState();
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(
        DiagnosticsProperty<UserInfoModel>('userInfoModel', userInfoModel));
  }
}

class _ImportantInfoListHomeState extends State<ImportantInfoListHome> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ImportantInfoHomeBloc, ImportantInfoHomeState>(
      builder: (context, state) {
        return state is ImportantInfoHomeLoading
            ? _buildLoading()
            : state is ImportantInfoHomeLoaded
                ? state.records.isNotEmpty
                    ? _buildContent(state)
                    : Container()
                : Container();
      },
    );
  }

  _buildLoading() {
    return Container(
      margin: const EdgeInsets.only(top: 10.0),
      padding:
          const EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 10.0),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Skeleton(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(Dictionary.importantInfo,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: FontsFamily.productSans)),
                Text(Dictionary.viewAll,
                    style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.bold,
                        color: clr.Colors.green)),
              ],
            ),
          ),
          ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: 3,
              itemBuilder: (context, index) {
                return Skeleton(
                    child: Container(
                        height: 80.0,
                        margin: const EdgeInsets.only(top: 10.0),
                        padding:
                            const EdgeInsets.fromLTRB(10.0, 10.0, 75.0, 10.0),
                        alignment: Alignment.centerLeft,
                        decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius: BorderRadius.circular(5.0))));
              }),
        ],
      ),
    );
  }

  _buildContent(ImportantInfoHomeLoaded state) {
    return Container(
      margin: const EdgeInsets.only(top: 10.0),
      padding:
          const EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 10.0),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(Dictionary.importantInfo,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: FontsFamily.productSans)),
              GestureDetector(
                child: Text(Dictionary.viewAll,
                    style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.bold,
                        color: clr.Colors.green)),
                onTap: _openList,
              ),
            ],
          ),
          const SizedBox(height: 10.0),
          ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: state.records.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  child: Container(
                      height: 80.0,
                      margin: const EdgeInsets.only(top: 10.0),
                      padding:
                          const EdgeInsets.fromLTRB(10.0, 10.0, 75.0, 10.0),
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                              colors: clr.Colors.gradientBlue),
                          borderRadius: BorderRadius.circular(5.0)),
                      child: Text(
                        state.records[index].title,
                        maxLines: 3,
                        overflow: TextOverflow.clip,
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      )),
                  onTap: () {
                    _openDetail(state.records[index]);
                  },
                );
              }),
        ],
      ),
    );
  }

  _openDetail(ImportantInfoModel record) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => ImportantInfoDetailScreen(
            id: record.id, userInfoModel: widget.userInfoModel)));
  }

  _openList() {
    Navigator.of(context).pushNamed(NavigationConstrants.ImportantInfoList);
  }
}
