import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/important_info/important_info_comment/Bloc.dart';
import 'package:sapawarga/components/BlockCircleLoading.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/ImportantInfoCommentModel.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/ImportantInfoRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/FormatDate.dart';
import 'package:sapawarga/utilities/Validations.dart';

class ImportantInfoDetailComment extends StatefulWidget {
  final int id;
  final ImportantInfoCommentListBloc commentListBloc;
  final ScrollController scrollController;
  final Key scaffoldKey;
  final UserInfoModel userInfoModel;

  ImportantInfoDetailComment(
      {Key key,
      this.scaffoldKey,
      this.id,
      this.commentListBloc,
      this.scrollController,
      this.userInfoModel})
      : super(key: key);

  @override
  _ImportantInfoDetailCommentState createState() =>
      _ImportantInfoDetailCommentState();
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(IntProperty('id', id));
    properties.add(DiagnosticsProperty<ImportantInfoCommentListBloc>(
        'commentListBloc', commentListBloc));
    properties.add(DiagnosticsProperty<ScrollController>(
        'scrollController', scrollController));
    properties.add(DiagnosticsProperty<Key>('scaffoldKey', scaffoldKey));
    properties.add(
        DiagnosticsProperty<UserInfoModel>('userInfoModel', userInfoModel));
  }
}

class _ImportantInfoDetailCommentState
    extends State<ImportantInfoDetailComment> {
  GlobalKey<ScaffoldState> get _scaffoldKey => widget.scaffoldKey;
  final _formKey = GlobalKey<FormState>();
  final _textController = TextEditingController();

  ImportantInfoCommentListBloc get _commentListBloc => widget.commentListBloc;
  ImportantInfoCommentAddBloc _commentAddBloc;

  ScrollController get _scrollController => widget.scrollController;
  List<ItemImportantInfoComment> _listComments =
      List<ItemImportantInfoComment>();

  Timer _debounce;

  int _pageCount = 0;
  int _currentPage = 1;
  int _itemCount = 0;
  bool _isLoading = false;
  final _scrollThreshold = 200.0;

  @override
  void initState() {
    _scrollController.addListener(() {
      _onScroll();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _commentAddBloc =
          ImportantInfoCommentAddBloc(ImportantInfoRepository()),
      child: MultiBlocListener(
        listeners: [
          BlocListener<ImportantInfoCommentListBloc,
              ImportantInfoCommentListState>(
            bloc: _commentListBloc,
            listener: (context, state) {
              if (state is ImportantInfoCommentListLoaded) {
                _pageCount = state.records.meta.pageCount;
                _currentPage = state.records.meta.currentPage + 1;
                _itemCount = state.records.meta.totalCount;
                _listComments.addAll(state.records.items);
                _listComments = _listComments.toSet().toList();
                setState(() {});
              }
            },
          ),
          BlocListener<ImportantInfoCommentAddBloc,
              ImportantInfoCommentAddState>(
            bloc: _commentAddBloc,
            listener: (context, state) {
              if (state is ImportantInfoCommentAddLoading) {
                _isLoading = true;
                blockCircleLoading(context: context);
              }

              if (state is ImportantInfoCommentAdded) {
                if (_isLoading) {
                  _isLoading = false;
                  Navigator.of(context, rootNavigator: true).pop();
                }

                _itemCount = _itemCount + 1;
                _listComments.insert(0, state.record);
                _formKey.currentState.reset();
                _textController.clear();

                setState(() {});

                _scaffoldKey.currentState.showSnackBar(
                  SnackBar(
                    content: Container(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: Text(
                          Dictionary.successAddComment,
                          style: TextStyle(fontSize: 14.0),
                        )),
                    backgroundColor: clr.Colors.blue,
                    duration: Duration(seconds: 2),
                  ),
                );

                AnalyticsHelper.setLogEvent(
                    Analytics.EVENT_COMMENT_IMPORTANT_INFO, <String, dynamic>{
                  'id': widget.id != null ? widget.id : state.record.id,
                  'text': _textController.text.trim()
                });
              } else if (state is ImportantInfoCommentAddFailure) {
                if (_isLoading) {
                  _isLoading = false;
                  Navigator.of(context, rootNavigator: true).pop();
                }

                showDialog(
                    context: context,
                    builder: (BuildContext context) => DialogTextOnly(
                          description: state.error,
                          buttonText: "OK",
                          onOkPressed: () {
                            Navigator.of(context).pop(); // To close the dialog
                          },
                        ));
              }
            },
          )
        ],
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(Dictionary.comment,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: FontsFamily.productSans,
                      fontWeight: FontWeight.w600,
                      color: Colors.grey[800])),
              const SizedBox(height: 10.0),
              widget.userInfoModel.isVerified == 1
                  ? Column(
                      children: [
                        /**INPUT FORM COMMENT BEGIN**/
                        Form(
                          key: _formKey,
                          child: TextFormField(
                              controller: _textController,
                              validator: Validations.commentValidation,
                              autofocus: false,
                              maxLines: 6,
                              minLines: 6,
                              maxLength: 255,
                              style: TextStyle(
                                  color: Colors.black, fontSize: 15.0),
                              decoration: InputDecoration(
                                counterText: "",
                                border: OutlineInputBorder(),
                                contentPadding: const EdgeInsets.all(5.0),
                              )),
                        ),
                        Align(
                          alignment: Alignment.topRight,
                          child: ButtonTheme(
                            height: 34.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            child: RaisedButton(
                              highlightElevation: 5,
                              child: Text(Dictionary.send,
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      fontFamily: FontsFamily.productSans,
                                      color: Colors.white)),
                              onPressed: _onSubmitButtonPressed,
                            ),
                          ),
                        ),
                        /**INPUT FORM COMMENT END**/
                      ],
                    )
                  : Container(),

              /**LIST COMMENTS BEGIN**/
              BlocBuilder<ImportantInfoCommentListBloc,
                      ImportantInfoCommentListState>(
                  bloc: _commentListBloc,
                  builder: (context, state) => state
                          is ImportantInfoCommentListLoading
                      ? _buildLoading()
                      : state is ImportantInfoCommentListLoaded
                          ? _buildListComments(state)
                          : state is ImportantInfoCommentListFailure
                              ? Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 50.0,
                                  margin: const EdgeInsets.only(top: 30.0),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                      borderRadius: BorderRadius.circular(5.0)),
                                  child: Text(Dictionary.errorListComment),
                                )
                              : Container())
              /**LIST COMMENTS END**/
            ],
          ),
        ),
      ),
    );
  }

  ///BUILD LOADING
  ListView _buildLoading() {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: 2,
      itemBuilder: (context, index) {
        return Skeleton(
          child: Container(
            padding: const EdgeInsets.fromLTRB(
                Dimens.padding, 10.0, Dimens.padding, 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ListTile(
                  contentPadding: const EdgeInsets.all(0.0),
                  leading: Container(
                    width: 40.0,
                    height: 40.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(25.0),
                      child: Container(
                          width: 40.0, height: 40.0, color: Colors.grey[300]),
                    ),
                  ),
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          height: 20.0,
                          color: Colors.grey[300]),
                      const SizedBox(height: 2.0),
                      Container(
                          width: 50.0, height: 15.0, color: Colors.grey[300]),
                    ],
                  ),
                ),
                Container(
                    margin: const EdgeInsets.only(left: 56.0),
                    width: MediaQuery.of(context).size.width,
                    height: 20.0,
                    color: Colors.grey[300]),
                Container(
                    margin: const EdgeInsets.only(left: 56.0, top: 2.0),
                    width: MediaQuery.of(context).size.width,
                    height: 20.0,
                    color: Colors.grey[300]),
                Container(
                    margin: const EdgeInsets.only(left: 56.0, top: 2.0),
                    width: MediaQuery.of(context).size.width / 1.5,
                    height: 20.0,
                    color: Colors.grey[300]),
              ],
            ),
          ),
        );
      },
    );
  }

  ///BUILD LIST COMMENTS
  _buildListComments(ImportantInfoCommentListLoaded state) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 10.0),
        Text('$_itemCount ${Dictionary.comment}',
            style: TextStyle(
                fontSize: 14.0,
                fontFamily: FontsFamily.productSans,
                fontWeight: FontWeight.w600,
                color: Colors.grey[800])),
        _listComments.isEmpty
            ? Container(
                width: MediaQuery.of(context).size.width,
                height: 50.0,
                margin: const EdgeInsets.only(top: 10.0),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.circular(5.0)),
                child: Text(Dictionary.emptyComments),
              )
            : ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: _itemCount == _listComments.length
                    ? _listComments.length
                    : _listComments.length + 1,
                itemBuilder: (context, index) => index >= _listComments.length
                    ? Padding(
                        padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                        child: Column(
                          children: <Widget>[
                            CupertinoActivityIndicator(),
                            const SizedBox(
                              height: 5.0,
                            ),
                            Text(Dictionary.loadingData),
                          ],
                        ),
                      )
                    : _buildItem(index))
      ],
    );
  }

  ///BUILD ITEM FOR LIST COMMENTS
  Container _buildItem(int index) {
    return Container(
      padding:
          const EdgeInsets.fromLTRB(Dimens.padding, 10.0, Dimens.padding, 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListTile(
            contentPadding: const EdgeInsets.all(0.0),
            leading: Container(
              width: 40.0,
              height: 40.0,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(25.0),
                child: CachedNetworkImage(
                  imageUrl: _listComments[index].user.photoUrlFull,
                  fit: BoxFit.fill,
                  placeholder: (context, url) => Container(
                      color: Colors.grey[200],
                      child: Center(child: CupertinoActivityIndicator())),
                  errorWidget: (context, url, error) => Container(
                      color: Colors.grey[200],
                      child: Image.asset('${Environment.imageAssets}user.png',
                          fit: BoxFit.cover)),
                ),
              ),
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Wrap(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(
                              _listComments[index].user.name,
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: FontsFamily.productSans),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                            if (_listComments[index].user.roleLabel ==
                                'pimpinan')
                              Icon(
                                Icons.check_circle,
                                color: clr.Colors.blue,
                                size: 18.0,
                              )
                          ],
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Flexible(
                          child: Text(
                              unixTimeStampToTimeAgo(
                                  _listComments[index].createdAt),
                              style: TextStyle(
                                  fontSize: 14.0, color: Colors.grey[600]),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1),
                        )
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 2.0),
                _listComments[index].user.roleLabel == 'pimpinan' ||
                        _listComments[index].user.roleLabel == 'staffProv' ||
                        _listComments[index].user.roleLabel == 'admin'
                    ? Text(
                        _listComments[index].user.roleLabel,
                        style:
                            TextStyle(fontSize: 14.0, color: Colors.grey[600]),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      )
                    : Text(
                        'RW ' +
                            _listComments[index].user.rw +
                            ', ' +
                            _listComments[index].user.kelurahan.toLowerCase() +
                            ', ' +
                            _listComments[index].user.kecamatan.toLowerCase() +
                            ', ' +
                            _listComments[index].user.kabkota.toLowerCase(),
                        style:
                            TextStyle(fontSize: 14.0, color: Colors.grey[600]),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      )
              ],
            ),
          ),
          Container(
              margin: const EdgeInsets.only(left: 56.0),
              child: Text(
                _listComments[index].text,
                style: TextStyle(fontSize: 15.0),
              )),
        ],
      ),
    );
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (_currentPage != _pageCount + 1) {
      if (maxScroll - currentScroll <= _scrollThreshold) {
        if (_debounce?.isActive ?? false) _debounce.cancel();
        _debounce = Timer(const Duration(milliseconds: 200), () {
          _commentListBloc
              .add(ImportantInfoCommentListLoad(widget.id, _currentPage));
        });
      }
    }
  }

  void _onSubmitButtonPressed() {
    if (_formKey.currentState.validate()) {
      FocusScope.of(context).unfocus();
      _commentAddBloc.add(ImportantInfoCommentAdd(
          id: widget.id, text: _textController.text.trim()));
    }
  }

  @override
  void dispose() {
    _textController.dispose();
    _commentAddBloc.close();
    super.dispose();
  }
}
