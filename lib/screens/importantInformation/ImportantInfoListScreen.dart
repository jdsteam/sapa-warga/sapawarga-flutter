import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/important_info/important_info_categories/Bloc.dart';
import 'package:sapawarga/blocs/important_info/important_info_list/Bloc.dart';
import 'package:sapawarga/components/BaseShowCase.dart';
import 'package:sapawarga/components/BubbleCustom.dart';
import 'package:sapawarga/components/CustomAppBar.dart';
import 'package:sapawarga/components/CustomRadioItem.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';
import 'package:sapawarga/models/RadioModel.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/ImportantInfoRepository.dart';
import 'package:sapawarga/repositories/MasterCategoryRepository.dart';
import 'package:sapawarga/screens/importantInformation/ImportantInfoDetailScreen.dart';
import 'package:sapawarga/screens/importantInformation/ImportantInfoSearchScreen.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/CustomPageRoute.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:showcaseview/showcase_widget.dart';

class ImportantInfoListScreen extends StatefulWidget {
  final UserInfoModel userInfoModel;

  ImportantInfoListScreen({Key key, this.userInfoModel}) : super(key: key);

  @override
  _ImportantInfoListScreenState createState() =>
      _ImportantInfoListScreenState();
}

class _ImportantInfoListScreenState extends State<ImportantInfoListScreen> {
  final RefreshController _mainRefreshController = RefreshController();
  final GlobalKey _showcaseOne = GlobalKey();
  final GlobalKey _showcaseTwo = GlobalKey();

  BuildContext showcaseContext;

  ImportantInfoCategoriesBloc _importantInfoCategoriesBloc;
  ImportantInfoListTopBloc _importantInfoListBloc;
  ImportantInfoListOthersBloc _importantInfoListOthersBloc;
  bool isRefreshView = false;
  int indexCategory = 0;
  List<RadioModel> categoryList = List<RadioModel>();

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.IMPORTANT_INFO);
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_LIST_IMPORTANT_INFO);
    super.initState();
  }

  _initializeShowcase() async {
    bool hasShown = await Preferences.hasShowcaseImportantInfo();

    if (hasShown == null || hasShown == false) {
      Future.delayed(
          Duration(milliseconds: 200),
          () => ShowCaseWidget.of(showcaseContext)
              .startShowCase([_showcaseOne, _showcaseTwo]));
    }
  }

  @override
  Widget build(BuildContext context) {
    if (isRefreshView) {
      _importantInfoListBloc
          .add(ImportantInfoListTopLoadRefreshViewer(indexCategory));
    }
    return ShowCaseWidget(
      onFinish: () async {
        await Preferences.setShowcaseImportantInfo(true);
      },
      builder: Builder(builder: (context) {
        showcaseContext = context;
        return Scaffold(
            appBar: CustomAppBar()
                .DefaultAppBar(title: Dictionary.importantInfo, actions: [
              BaseShowCase.showcaseWidget(
                key: _showcaseTwo,
                context: context,
                nipLocation: NipLocation.TOP_RIGHT,
                nipPaddingTopRight: 5.0,
                margin: const EdgeInsets.only(top: 10.0, left: 25.0),
                widgets: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        margin: const EdgeInsets.only(bottom: 5),
                        alignment: Alignment.topLeft,
                        child: Text(
                          Dictionary.search2,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Text(
                        Dictionary.showcaseImportantInfo2,
                        style: TextStyle(color: Colors.grey[800]),
                      ),
                    ],
                  )
                ],
                onOkTap: () {
                  ShowCaseWidget.of(showcaseContext).completed(_showcaseTwo);
                },
                child: IconButton(
                    icon: Icon(Icons.search),
                    tooltip: Dictionary.search,
                    onPressed: () {
                      Navigator.of(context).push(PageTransition(
                          child: ImportantInfoSearchScreen(),
                          type: PageTransitionType.fade));
                    }),
              )
            ]),
            body: Container(
              padding: const EdgeInsets.only(top: 1.0),
              child: _buildBloc(),
            ));
      }),
    );
  }

  MultiBlocProvider _buildBloc() {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ImportantInfoCategoriesBloc>(
          create: (context) =>
              _importantInfoCategoriesBloc = ImportantInfoCategoriesBloc(
                  masterCategoryRepository: MasterCategoryRepository())
                ..add(ImportantInfoCategoriesLoad()),
        ),
        BlocProvider<ImportantInfoListOthersBloc>(
          create: (context) => _importantInfoListOthersBloc =
              ImportantInfoListOthersBloc(
                  importantInfoRepository: ImportantInfoRepository()),
        ),
        BlocProvider<ImportantInfoListTopBloc>(
            create: (context) =>
                _importantInfoListBloc = ImportantInfoListTopBloc(
                    importantInfoRepository: ImportantInfoRepository(),
                    importantInfoListOthersBloc: _importantInfoListOthersBloc)
                  ..add(ImportantInfoListTopLoad(0))),
      ],
      child: BlocListener(
          bloc: _importantInfoCategoriesBloc,
          listener: (context, state) {
            if (state is ImportantInfoCategoriesLoading) {
              categoryList.clear();
            }

            if (state is ImportantInfoCategoriesLoaded) {
              categoryList.add(RadioModel(true, Dictionary.all, 0));

              for (final MasterCategoryModel data in state.records) {
                categoryList.add(RadioModel(false, data.name, data.id));
              }
              setState(() {});

              _initializeShowcase();
            }
          },
          child: _buildContent()),
    );
  }

  _buildLoading() {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                margin: const EdgeInsets.symmetric(
                    horizontal: Dimens.padding, vertical: 10.0),
                child: Skeleton(width: 100.0, height: 20.0)),
            Container(
              margin: const EdgeInsets.only(bottom: Dimens.padding),
              height: 140.0,
              child: ListView.builder(
                  padding: const EdgeInsets.only(left: Dimens.padding),
                  scrollDirection: Axis.horizontal,
                  itemCount: 2,
                  itemBuilder: (context, index) {
                    return Skeleton(
                      child: Container(
                          margin: const EdgeInsets.only(right: Dimens.padding),
                          width: 281.0,
                          alignment: Alignment.bottomLeft,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0),
                              color: Colors.grey)),
                    );
                  }),
            ),
            _buildLoadingOthers()
          ]),
    );
  }

  _buildContent() {
    return SmartRefresher(
      controller: _mainRefreshController,
      enablePullDown: true,
      header: WaterDropMaterialHeader(),
      onRefresh: () async {
        _importantInfoCategoriesBloc.add(ImportantInfoCategoriesLoad());
        _importantInfoListBloc.add(ImportantInfoListTopLoad(0));
        _mainRefreshController.refreshCompleted();
      },
      child: ListView(children: <Widget>[
        BlocBuilder<ImportantInfoCategoriesBloc, ImportantInfoCategoriesState>(
          builder: (context, state) => state is ImportantInfoCategoriesLoading
              ? Container()
              : state is ImportantInfoCategoriesLoaded
                  ? _buildCategories()
                  : Container(),
        ),
        BlocBuilder<ImportantInfoListTopBloc, ImportantInfoListTopState>(
            bloc: _importantInfoListBloc,
            builder: (context, state) => state is ImportantInfoListTopLoading
                ? _buildLoading()
                : state is ImportantInfoListTopLoaded
                    ? state.records.isNotEmpty
                        ? _buildMain(state)
                        : Container(
                            height: MediaQuery.of(context).size.height / 1.5,
                            child: EmptyData(
                              message: Dictionary.emptyDataImportantInfo,
                            ),
                          )
                    : state is ImportantInfoListTopFailure
                        ? ErrorContent(error: state.error)
                        : _buildLoading()),
      ]),
    );
  }

  Column _buildMain(ImportantInfoListTopLoaded state) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.symmetric(
              horizontal: Dimens.padding, vertical: 10.0),
          child: Text(Dictionary.latestInfo,
              style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: FontsFamily.productSans,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey[800])),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: Dimens.padding),
          height: 140.0,
          child: ListView.builder(
              padding: const EdgeInsets.only(left: Dimens.padding),
              scrollDirection: Axis.horizontal,
              itemCount: state.records.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  child: Container(
                      margin: const EdgeInsets.only(right: Dimens.padding),
                      width: 281.0,
                      alignment: Alignment.bottomLeft,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5.0)),
                      child: Stack(
                        alignment: Alignment.bottomRight,
                        children: <Widget>[
                          state.records[index].imagePathUrl != null
                              ? Container(
                                  width: 281.0,
                                  decoration:
                                      BoxDecoration(shape: BoxShape.circle),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(5.0),
                                    child: CachedNetworkImage(
                                      imageUrl:
                                          state.records[index].imagePathUrl,
                                      fit: BoxFit.fitWidth,
                                      placeholder: (context, url) => Center(
                                          heightFactor: 9,
                                          child: CupertinoActivityIndicator()),
                                      errorWidget: (context, url, error) {
                                        return Container(
                                            width: 281.0,
                                            height: 140.0,
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle),
                                            child: Image.asset(
                                                '${Environment.imageAssets}placeholder.png',
                                                fit: BoxFit.fitWidth));
                                      },
                                    ),
                                  ),
                                )
                              : Container(
                                  width: 281.0,
                                  height: 140.0,
                                  decoration:
                                      BoxDecoration(shape: BoxShape.circle),
                                  child: Image.asset(
                                      '${Environment.imageAssets}placeholder.png',
                                      fit: BoxFit.fitWidth)),
                          Container(
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                      colors: clr.Colors.gradientBlackOpacity),
                                  borderRadius: BorderRadius.circular(5.0))),
                          Container(
                            height: 65.0,
                            alignment: Alignment.topLeft,
                            padding: const EdgeInsets.fromLTRB(
                                10.0, 10.0, 120.0, 10.0),
                            child: Text(
                              state.records[index].title,
                              maxLines: 3,
                              overflow: TextOverflow.clip,
                              style: TextStyle(
                                  fontSize: 12.0,
                                  fontFamily: FontsFamily.productSans,
                                  color: Colors.white),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(
                                right: 10.0, bottom: 10.0),
                            child: Text(
                              state.records[index].category.name,
                              maxLines: 1,
                              overflow: TextOverflow.clip,
                              style: TextStyle(
                                  fontSize: 12.0,
                                  fontFamily: FontsFamily.productSans,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                          ),
                          Positioned(
                            top: 10.0,
                            left: 10.0,
                            child: Container(
                              height: 20,
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5.0),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15.0),
                                  color: Colors.black.withOpacity(0.7)),
                              child: Row(
                                children: <Widget>[
                                  Image.asset(
                                    '${Environment.iconAssets}eye.png',
                                    height: 12.0,
                                  ),
                                  Container(
                                      height: 18.0,
                                      margin: const EdgeInsets.only(left: 3.0),
                                      child: Text(
                                          '${state.records[index].totalViewers}',
                                          style: TextStyle(
                                              fontSize: 13.0,
                                              color: Colors.white)))
                                ],
                              ),
                            ),
                          )
                        ],
                      )),
                  onTap: () {
                    _openDetail(state.records[index]);
                  },
                );
              }),
        ),
        Container(
          margin: const EdgeInsets.only(top: 5.0),
          child: BlocBuilder<ImportantInfoListOthersBloc,
              ImportantInfoListOthersState>(
            bloc: _importantInfoListOthersBloc,
            builder: (context, state) => state is ImportantInfoListOthersLoading
                ? _buildLoadingOthers()
                : state is ImportantInfoListOthersLoaded
                    ? state.records.isNotEmpty
                        ? _buildContentOthers(state)
                        : Container()
                    : Container(),
          ),
        )
      ],
    );
  }

  _buildCategories() {
    return Container(
      margin: const EdgeInsets.only(top: Dimens.padding),
      child: BaseShowCase.showcaseWidget(
        key: _showcaseOne,
        context: context,
        nipLocation: NipLocation.TOP,
        widgets: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(bottom: 5),
                alignment: Alignment.topLeft,
                child: Text(
                  Dictionary.category,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                Dictionary.showcaseImportantInfo1,
                style: TextStyle(color: Colors.grey[800]),
              ),
            ],
          )
        ],
        onOkTap: () {
          ShowCaseWidget.of(showcaseContext).completed(_showcaseOne);
        },
        child: Container(
          height: 35,
          child: ListView.builder(
            padding: const EdgeInsets.only(left: 15.0),
            scrollDirection: Axis.horizontal,
            itemCount: categoryList.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                margin: const EdgeInsets.only(right: 10.0),
                child: InkWell(
                  //highlightColor: Colors.red,
                  splashColor: Colors.blueAccent,
                  onTap: () {
                    setState(() {
                      indexCategory = categoryList[index].value;
                      isRefreshView = false;
                      categoryList
                          .forEach((element) => element.isSelected = false);
                      categoryList[index].isSelected = true;
                      _importantInfoListBloc.add(
                          ImportantInfoListTopLoad(categoryList[index].value));

                      AnalyticsHelper.setLogEvent(
                          Analytics.EVENT_FILTER_IMPORTANT_INFO,
                          <String, dynamic>{
                            'categories': '${categoryList[index].buttonText}',
                          });
                    });
                  },
                  child: RadioItem(
                    item: categoryList[index],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  _buildLoadingOthers() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Skeleton(
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: Dimens.padding),
            height: 20.0,
            width: 150.0,
            color: Colors.grey[300],
          ),
        ),
        Container(
          child: ListView.separated(
              padding: const EdgeInsets.symmetric(horizontal: Dimens.padding),
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              separatorBuilder: (context, index) => Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                  color: Colors.grey[300]),
              itemCount: 4,
              itemBuilder: (context, index) {
                return Container(
                  margin: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                  child: Skeleton(
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: 70,
                          height: 70,
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.grey[100], width: 1.0),
                              borderRadius: BorderRadius.circular(5.0),
                              color: Colors.grey[300]),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width - 120,
                          height: 70.0,
                          padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                height: 20.0,
                                margin: const EdgeInsets.symmetric(
                                    horizontal: Dimens.padding),
                                color: Colors.grey[300],
                              ),
                              Container(
                                width: 150.0,
                                height: 20.0,
                                margin: const EdgeInsets.symmetric(
                                    horizontal: Dimens.padding),
                                color: Colors.grey[300],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }),
        ),
      ],
    );
  }

  _buildContentOthers(ImportantInfoListOthersLoaded state) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.symmetric(horizontal: Dimens.padding),
          child: Text(Dictionary.importantInfoOther,
              style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: FontsFamily.productSans,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey[800])),
        ),
        Container(
          child: ListView.separated(
              padding: const EdgeInsets.symmetric(horizontal: Dimens.padding),
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              separatorBuilder: (context, index) => Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                  color: Colors.grey[300]),
              itemCount: state.records.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                  child: FlatButton(
                    padding: const EdgeInsets.all(0.0),
                    onPressed: () {
                      _openDetail(state.records[index]);
                    },
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: 70,
                          height: 70,
                          decoration: BoxDecoration(
                            border:
                                Border.all(color: Colors.grey[100], width: 1.0),
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: state.records[index].imagePathUrl != null
                                ? CachedNetworkImage(
                                    imageUrl: state.records[index].imagePathUrl,
                                    fit: BoxFit.cover,
                                    placeholder: (context, url) => Center(
                                        heightFactor: 4.2,
                                        child: CupertinoActivityIndicator()),
                                    errorWidget: (context, url, error) => Container(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                3.3,
                                        color: Colors.grey[200],
                                        child: Image.asset(
                                            '${Environment.imageAssets}placeholder_square.png',
                                            fit: BoxFit.fitWidth)),
                                  )
                                : Image.asset(
                                    '${Environment.imageAssets}placeholder.png',
                                    fit: BoxFit.fitWidth),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width - 120,
                          height: 70.0,
                          margin: const EdgeInsets.only(left: Dimens.padding),
                          padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(state.records[index].title,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      fontFamily: FontsFamily.productSans,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey[800])),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Text(state.records[index].category.name,
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          fontFamily: FontsFamily.productSans,
                                          color: Colors.grey[600])),
                                  Container(
                                    height: 16,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                        color: Colors.black.withOpacity(0.7)),
                                    child: Row(
                                      children: <Widget>[
                                        Image.asset(
                                          '${Environment.iconAssets}eye.png',
                                          height: 10.0,
                                        ),
                                        Container(
                                            height: 15.0,
                                            margin: const EdgeInsets.only(
                                                left: 3.0),
                                            child: Text(
                                                '${state.records[index].totalViewers}',
                                                style: TextStyle(
                                                    fontSize: 11.0,
                                                    color: Colors.white)))
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }),
        ),
      ],
    );
  }

  _openDetail(ImportantInfoModel record) async {
    isRefreshView = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => ImportantInfoDetailScreen(id: record.id)));
  }

  @override
  void dispose() {
    _importantInfoListOthersBloc.close();
    _importantInfoListBloc.close();
    _importantInfoCategoriesBloc.close();
    super.dispose();
  }
}
