import 'dart:convert';

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/forgot_password/Bloc.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/DialogWidgetContent.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/components/TextButton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/Validations.dart';
import 'package:sapawarga/utilities/WhatsAppHelper.dart';

class ForgotPasswordScreen extends StatelessWidget {
  ForgotPasswordScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ForgotPasswordBloc>(
      create: (context) => ForgotPasswordBloc(authRepository: AuthRepository()),
      child: ForgotPassword(),
    );
  }
}

class ForgotPassword extends StatefulWidget {
  ForgotPassword({Key key}) : super(key: key);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  ForgotPasswordBloc _forgotPasswordBloc;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();

  List<dynamic> csPhoneNumbers;
  dynamic csInfoText;

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.FORGOT_PASSWORD);
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_FORGOT_PASSWORD);
    _forgotPasswordBloc = BlocProvider.of<ForgotPasswordBloc>(context);
    csPhoneNumbers = json.decode(FirebaseConfig.callCenterNumbersValue);
    csInfoText = Dictionary.helpAdminWA;
    super.initState();
  }

  Future<RemoteConfig> _initializeRemoteConfig() async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    await remoteConfig.setDefaults(<String, dynamic>{
      FirebaseConfig.callCenterNumbersKey: FirebaseConfig.callCenterNumbersValue
    });

    try {
      await remoteConfig.fetch(expiration: Duration(minutes: 30));
      await remoteConfig.activateFetched();
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }

    return remoteConfig;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            margin: const EdgeInsets.fromLTRB(20.0, 60.0, 20.0, 20.0),
            width: MediaQuery.of(context).size.width,
            child: BlocListener<ForgotPasswordBloc, ForgotPasswordState>(
              bloc: _forgotPasswordBloc,
              listener: (context, state) {
                if (state is ForgotPasswordFailure) {
                  AnalyticsHelper.setLogEvent(
                      Analytics.EVENT_FAILED_FORGOT_PASSWORD, <String, dynamic>{
                    'email': '${_emailController.text}',
                    'message': '${state.error.toString()}'
                  });
                  showDialog(
                      context: context,
                      builder: (BuildContext context) => DialogTextOnly(
                            description: state.error.toString(),
                            buttonText: Dictionary.ok.toUpperCase(),
                            onOkPressed: () {
                              Navigator.of(context)
                                  .pop(); // To close the dialog
                            },
                          ));
                  Scaffold.of(context).hideCurrentSnackBar();
                } else if (state is ForgotPasswordLoading) {
                  Scaffold.of(context).showSnackBar(
                    SnackBar(
                      content: Row(
                        children: <Widget>[
                          CircularProgressIndicator(),
                          Container(
                            margin: const EdgeInsets.only(left: 15.0),
                            child: Text(Dictionary.loading),
                          )
                        ],
                      ),
                    ),
                  );
                } else if (state is ForgotPasswordRequested) {
                  AnalyticsHelper.setLogEvent(
                      Analytics.EVENT_SUCCESS_FORGOT_PASSWORD,
                      <String, dynamic>{
                        'email': '${_emailController.text}',
                      });

                  showDialog(
                      context: context,
                      builder: (BuildContext context) => DialogTextOnly(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            buttonAlignment: Alignment.bottomRight,
                            title: Dictionary.congratulation,
                            titleColor: Colors.grey[700],
                            description: Dictionary.successRequestResetPassword,
                            descriptionColor: Colors.grey[700],
                            buttonText: Dictionary.ok.toUpperCase(),
                            onOkPressed: () {
                              Navigator.of(context).popUntil((route) =>
                                  route.isFirst); // To close the dialog
                            },
                          ));
                  Scaffold.of(context).hideCurrentSnackBar();
                } else if (state is ValidationError) {
                  AnalyticsHelper.setLogEvent(
                      Analytics.EVENT_FAILED_FORGOT_PASSWORD, <String, dynamic>{
                    'email': '${_emailController.text}',
                    'message': Dictionary.errorUserNotFound
                  });

                  showDialog(
                      context: context,
                      builder: (BuildContext context) => DialogTextOnly(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            buttonAlignment: Alignment.bottomRight,
                            description: Dictionary.errorUserNotFound,
                            descriptionColor: Colors.grey[700],
                            buttonText: Dictionary.ok.toUpperCase(),
                            onOkPressed: () {
                              Navigator.of(context)
                                  .pop(); // To close the dialog
                            },
                          ));
                  Scaffold.of(context).hideCurrentSnackBar();
                } else {
                  Scaffold.of(context).hideCurrentSnackBar();
                }
              },
              child: BlocBuilder<ForgotPasswordBloc, ForgotPasswordState>(
                  bloc: _forgotPasswordBloc,
                  builder: (context, state) => _buildForm(context, state)),
            )));
  }

  Form _buildForm(BuildContext context, ForgotPasswordState state) {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              '${Environment.logoAssets}logo.png',
              width: 200.0,
            ),
            const SizedBox(height: 30.0),
            Text(Dictionary.restoreAccount,
                style: Theme.of(context).textTheme.headline6),
            const SizedBox(height: 5.0),
            Text(Dictionary.putEmailSapawarga,
                style: Theme.of(context).textTheme.subtitle1,
                textAlign: TextAlign.center),
            const SizedBox(height: 50.0),
            TextFormField(
              controller: _emailController,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  contentPadding:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 14),
                  labelText: Dictionary.labelEmail,
                  border: OutlineInputBorder()),
              validator: (val) => Validations.emailValidation(val),
            ),
            const SizedBox(height: 30.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                TextButtonOnly(
                    title: Dictionary.back,
                    textStyle: Theme.of(context).textTheme.subtitle1,
                    onTap: () {
                      Navigator.pop(context);
                    }),
                RoundedButton(
                    minWidth: 150,
                    title: Dictionary.next,
                    borderRadius: BorderRadius.circular(5.0),
                    color: clr.Colors.blue,
                    textStyle: Theme.of(context).textTheme.subtitle1.copyWith(
                        color: Colors.white, fontWeight: FontWeight.bold),
                    onPressed: state is! ForgotPasswordLoading
                        ? _onNextButtonPressed
                        : null),
              ],
            ),
            const SizedBox(height: 30.0),
            FutureBuilder<RemoteConfig>(
              future: _initializeRemoteConfig(),
              builder:
                  (BuildContext context, AsyncSnapshot<RemoteConfig> snapshot) {
                if (snapshot.hasData) {
                  final dataPhoneNumbers = snapshot.data
                      .getString(FirebaseConfig.callCenterNumbersKey);
                  if (dataPhoneNumbers != null) {
                    csPhoneNumbers = json.decode(dataPhoneNumbers);
                  }

                  try {
                    final dataInfoText = snapshot.data
                        .getString(FirebaseConfig.callCenterTextInfoKey);
                    if (dataInfoText != null) {
                      csInfoText = json.decode(dataInfoText);
                    }
                  } catch (e) {
                    print(e.toString());
                  }
                }

                return RoundedButton(
                  title: Dictionary.forgotEmail,
                  borderRadius: BorderRadius.circular(5.0),
                  color: Colors.white,
                  textStyle: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                  onPressed: () {
                    csPhoneNumbers.length > 1
                        ? _openDialogContact()
                        : WhatsAppHelper.checkNumber(
                            csPhoneNumbers[0], csInfoText);
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  _onNextButtonPressed() {
    AnalyticsHelper.setLogEvent(
        Analytics.EVENT_TAPPED_BUTTON_NEXT_FORGOT_PASSWORD);

    if (_formKey.currentState.validate()) {
      _forgotPasswordBloc
          .add(RequestForgotPassword(email: _emailController.text));
    } else {
      print("Validate Error");
    }
  }

  _openDialogContact() {
    AnalyticsHelper.setLogEvent(
        Analytics.EVENT_TAPPED_FORGOT_EMAIL_FORGOT_PASSWORD);

    showDialog(
        context: context,
        builder: (BuildContext context) => DialogWidgetContent(
              title: Dictionary.callCenterList,
              child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount:
                    csPhoneNumbers.length > 5 ? 5 : csPhoneNumbers.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    child: Container(
                      margin:
                          index != 0 ? const EdgeInsets.only(top: 10.0) : null,
                      padding: const EdgeInsets.fromLTRB(
                          Dimens.padding, 10.0, Dimens.padding, 10.0),
                      decoration: BoxDecoration(
                        color: Color(0xffebf8ff),
                        shape: BoxShape.rectangle,
                        border: Border.all(color: clr.Colors.blue),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Wrap(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: Image.asset(
                                '${Environment.iconAssets}whatsapp.png',
                                width: 20,
                                height: 20,
                                color: clr.Colors.green),
                          ),
                          Text(csPhoneNumbers[index],
                              style: TextStyle(
                                  fontFamily: FontsFamily.sourceSansPro,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w600,
                                  color: clr.Colors.green))
                        ],
                      ),
                    ),
                    onTap: () {
                      WhatsAppHelper.checkNumber(
                          csPhoneNumbers[index], csInfoText);
                      Navigator.of(context).pop();
                    },
                  );
                },
              ),
              buttonText: Dictionary.close.toUpperCase(),
              onOkPressed: () {
                Navigator.of(context).pop(); // To close the dialog
              },
            ));
  }

  @override
  void dispose() {
    _emailController.dispose();
    _forgotPasswordBloc.close();
    super.dispose();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(IterableProperty<dynamic>('csPhoneNumbers', csPhoneNumbers));
    properties.add(DiagnosticsProperty('csInfoText', csInfoText));
  }
}
