import 'dart:convert';

import 'package:device_apps/device_apps.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/send_otp_change_password/Bloc.dart';
import 'package:sapawarga/components/BlockCircleLoading.dart';
import 'package:sapawarga/components/CustomBottomSheet.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/DialogWidgetContent.dart';
import 'package:sapawarga/components/TextFieldComponent.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/Validations.dart';
import 'package:url_launcher/url_launcher.dart';

class ForgoUsernameScreenLogin extends StatefulWidget {
  ForgoUsernameScreenLogin({Key key}) : super(key: key);

  @override
  _ForgoUsernameScreenLoginState createState() =>
      _ForgoUsernameScreenLoginState();
}

class _ForgoUsernameScreenLoginState extends State<ForgoUsernameScreenLogin> {
  final _phoneTextController = TextEditingController();
  SendOtpPasswordBloc _sendOtpPasswordBloc;
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  List<dynamic> csPhoneNumbers;
  dynamic csInfoText;

  @override
  void initState() {
    csPhoneNumbers = json.decode(FirebaseConfig.callCenterNumbersValue);
    csInfoText = Dictionary.helpAdminWA;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            '',
          ),
          backgroundColor: Colors.white,
          elevation: 1.0,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
        ),
        body: BlocProvider<SendOtpPasswordBloc>(
          create: (BuildContext context) => _sendOtpPasswordBloc =
              SendOtpPasswordBloc(
                  authProfileRepository: AuthProfileRepository()),
          child: BlocListener<SendOtpPasswordBloc, SendOtpPasswordState>(
              bloc: _sendOtpPasswordBloc,
              listener: (context, state) {
                if (state is SendOtpPasswordLoading) {
                  _isLoading = true;
                  blockCircleLoading(context: context);
                } else if (state is SendOtpPasswordSuccess) {
                  if (_isLoading) {
                    _isLoading = false;
                    Navigator.of(context, rootNavigator: true).pop();
                  }

                  _verificationPage();

                  AnalyticsHelper.setLogEvent(
                      Analytics.EVENT_SUCCESS_SEND_OTP_USERNAME);
                } else if (state is SendOtpPasswordFailure) {
                  if (_isLoading) {
                    _isLoading = false;
                    Navigator.of(context, rootNavigator: true).pop();
                  }

                  AnalyticsHelper.setLogEvent(
                      Analytics.EVENT_FAILED_SEND_OTP_USERNAME,
                      <String, dynamic>{
                        'message': '${state.error.toString()}'
                      });

                  showDialog(
                      context: context,
                      builder: (BuildContext context) => DialogTextOnly(
                            description: state.error.toString().isNotEmpty
                                ? state.error.toString()
                                : Dictionary.failedSendOtp,
                            buttonText: Dictionary.ok,
                            onOkPressed: () {
                              Navigator.of(context, rootNavigator: true)
                                  .pop(); // To close the dialog
                            },
                          ));
                } else if (state is ValidationSendPasswordOtpError) {
                  if (_isLoading) {
                    _isLoading = false;
                    Navigator.of(context, rootNavigator: true).pop();
                  }

                  if (state.errors.toString().contains('generic') &&
                      state.errors['data']['generic'][0].toString() ==
                          Dictionary.infoOtpSend) {
                    _verificationPage();
                  } else if (state.errors
                      .toString()
                      .contains(Dictionary.userNotFound)) {
                    showCustomBottomSheet(
                        context: context,
                        image: '${Environment.imageAssets}phone_not_found.png',
                        title: Dictionary.phoneNumberNotFound,
                        message: Dictionary.phoneNumberNotFoundDesc,
                        onPressed: () async {
                          csPhoneNumbers.length > 1
                              ? _openDialogContact()
                              : _launchWhatsApp(csPhoneNumbers[0], csInfoText);

                          await AnalyticsHelper.setLogEvent(
                              Analytics.EVENT_CALL_HOTLINE);
                          Navigator.of(context).pop(true);
                        },
                        buttonText: Dictionary.contactAdmin);
                  } else {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) => DialogTextOnly(
                              description: state.errors
                                      .toString()
                                      .contains('username')
                                  ? state.errors['data']['username'][0]
                                      .toString()
                                  : state.errors.toString().contains('phone')
                                      ? state.errors['data']['phone'][0]
                                          .toString()
                                      : state.errors
                                              .toString()
                                              .contains('generic')
                                          ? state.errors['data']['generic'][0]
                                          : state.errors
                                                  .toString()
                                                  .contains('data')
                                              ? state.errors['data']
                                              : state.errors.toString(),
                              buttonText: Dictionary.ok,
                              onOkPressed: () {
                                Navigator.of(context, rootNavigator: true)
                                    .pop(); // To close the dialog
                              },
                            ));
                  }
                } else {
                  if (_isLoading) {
                    _isLoading = false;
                    Navigator.of(context, rootNavigator: true).pop();
                  }
                }
              },
              child: BlocBuilder<SendOtpPasswordBloc, SendOtpPasswordState>(
                  bloc: _sendOtpPasswordBloc,
                  builder: (context, state) => Form(
                        key: _formKey,
                        child: Container(
                          margin: const EdgeInsets.all(20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                Dictionary.changeUsername,
                                style: TextStyle(
                                    fontSize: 20.0,
                                    color: clr.Colors.veryDarkGrey,
                                    fontFamily: FontsFamily.roboto,
                                    fontWeight: FontWeight.bold),
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              Text(
                                Dictionary.changeUsernameDesc,
                                style: TextStyle(
                                    fontSize: 14.0,
                                    wordSpacing: 1,
                                    height: 1.5,
                                    fontFamily: FontsFamily.roboto,
                                    color: clr.Colors.darkGrey),
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              TextFieldComponent(
                                  title: Dictionary.noTelp,
                                  hintText: Dictionary.descNoTelp,
                                  controller: _phoneTextController,
                                  validation: Validations.phoneValidation,
                                  textInputType: TextInputType.number,
                                  isEdit: true),
                              const SizedBox(
                                height: 20,
                              ),
                              FutureBuilder<RemoteConfig>(
                                future: _initializeRemoteConfig(),
                                builder: (BuildContext context,
                                    AsyncSnapshot<RemoteConfig> snapshot) {
                                  if (snapshot.hasData) {
                                    final dataPhoneNumbers = snapshot.data
                                        .getString(FirebaseConfig
                                            .callCenterNumbersKey);
                                    if (dataPhoneNumbers != null) {
                                      csPhoneNumbers =
                                          json.decode(dataPhoneNumbers);
                                    }
                                    try {
                                      final dataInfoText = snapshot.data
                                          .getString(FirebaseConfig
                                              .callCenterTextInfoKey);
                                      if (dataInfoText != null) {
                                        csInfoText = json.decode(dataInfoText);
                                      }
                                    } catch (e) {
                                      print(e.toString());
                                    }
                                  }
                                  return Container();
                                },
                              ),
                              RaisedButton(
                                color: clr.Colors.darkblue,
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                child: Container(
                                  alignment: Alignment.center,
                                  width: MediaQuery.of(context).size.width,
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 13),
                                  child: Text(
                                    Dictionary.send,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: FontsFamily.roboto,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                                onPressed: () {
                                  if (_formKey.currentState.validate()) {
                                    _sendOtpPasswordBloc.add(
                                        SendOtpChangedPassword(
                                            phone: _phoneTextController.text));
                                  }
                                },
                              ),
                            ],
                          ),
                        ),
                      ))),
        ));
  }

  _openDialogContact() {
    showDialog(
        context: context,
        builder: (BuildContext context) => DialogWidgetContent(
              title: Dictionary.callCenterList,
              child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount:
                    csPhoneNumbers.length > 5 ? 5 : csPhoneNumbers.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    child: Container(
                      margin:
                          index != 0 ? const EdgeInsets.only(top: 10.0) : null,
                      padding: const EdgeInsets.fromLTRB(
                          Dimens.padding, 10.0, Dimens.padding, 10.0),
                      decoration: BoxDecoration(
                        color: Color(0xffebf8ff),
                        shape: BoxShape.rectangle,
                        border: Border.all(color: clr.Colors.blue),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Wrap(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: Image.asset(
                                '${Environment.iconAssets}whatsapp.png',
                                width: 20,
                                height: 20,
                                color: clr.Colors.green),
                          ),
                          Text(csPhoneNumbers[index],
                              style: TextStyle(
                                  fontFamily: FontsFamily.sourceSansPro,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w600,
                                  color: clr.Colors.green))
                        ],
                      ),
                    ),
                    onTap: () {
                      _launchWhatsApp(csPhoneNumbers[index], csInfoText);
                      Navigator.of(context).pop();
                    },
                  );
                },
              ),
              buttonText: Dictionary.close.toUpperCase(),
              onOkPressed: () {
                Navigator.of(context).pop(); // To close the dialog
              },
            ));
  }

  _launchWhatsApp(String csPhoneNumber, String csInfoText) async {
    String phoneNumber = '';
    try {
      bool isInstalled = await DeviceApps.isAppInstalled('com.whatsapp');
      phoneNumber = csPhoneNumber ?? Environment.csPhone;

      if (phoneNumber[0] == '0') {
        phoneNumber = phoneNumber.replaceFirst('0', '+62');
      }

      if (isInstalled) {
        String urlWhatsApp =
            'https://wa.me/${phoneNumber}?text=${Uri.encodeFull(csInfoText)}%0A%0A';
        if (await canLaunch(urlWhatsApp)) {
          await launch(urlWhatsApp);

          await AnalyticsHelper.setLogEvent(
              Analytics.EVENT_OPEN_WA_ADMIN_LOGIN);
        } else {
          await launch('tel://${phoneNumber}');
          await AnalyticsHelper.setLogEvent(
              Analytics.EVENT_OPEN_TELP_ADMIN_LOGIN);
        }
      } else {
        await launch('tel://${phoneNumber}');
        await AnalyticsHelper.setLogEvent(
            Analytics.EVENT_OPEN_TELP_ADMIN_LOGIN);
      }
    } catch (_) {
      await launch('tel://${phoneNumber}');
      await AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_TELP_ADMIN_LOGIN);
    }
  }

  Future<RemoteConfig> _initializeRemoteConfig() async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    await remoteConfig.setDefaults(<String, dynamic>{
      FirebaseConfig.callCenterNumbersKey: FirebaseConfig.callCenterNumbersValue
    });

    try {
      await remoteConfig.fetch(expiration: Duration(minutes: 30));
      await remoteConfig.activateFetched();
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }

    return remoteConfig;
  }

  _verificationPage() async {
    await Navigator.of(context)
        .pushNamed(NavigationConstrants.VerificationUsername, arguments: [
      _phoneTextController.text,
    ]);
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(IterableProperty<dynamic>('csPhoneNumbers', csPhoneNumbers));
    properties.add(DiagnosticsProperty('csInfoText', csInfoText));
  }
}
