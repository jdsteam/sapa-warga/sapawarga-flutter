import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/update_password/Bloc.dart';
import 'package:sapawarga/components/BlockCircleLoading.dart';
import 'package:sapawarga/components/DialogConfirmation.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/TextFieldComponent.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/screens/login/LoginScreen.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/utilities/Validations.dart';

class ChangePasswordLoginScreen extends StatefulWidget {
  final String phone;
  final String verifyToken;

  ChangePasswordLoginScreen({Key key, this.phone, this.verifyToken})
      : super(key: key);

  @override
  _ChangePasswordLoginScreenState createState() =>
      _ChangePasswordLoginScreenState();
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('phone', phone));
    properties.add(StringProperty('verifyToken', verifyToken));
  }
}

class _ChangePasswordLoginScreenState extends State<ChangePasswordLoginScreen> {
  final _passwordTextController = TextEditingController();
  final _confirmPasswordTextController = TextEditingController();
  UpdatePasswordBloc _updatePasswordBloc;
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  bool _isSuccessChangePass = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              '',
            ),
            backgroundColor: Colors.white,
            elevation: 1.0,
            iconTheme: IconThemeData(
              color: Colors.black, //change your color here
            ),
          ),
          body: BlocProvider<UpdatePasswordBloc>(
            create: (BuildContext context) => _updatePasswordBloc =
                UpdatePasswordBloc(
                    authProfileRepository: AuthProfileRepository()),
            child: BlocListener<UpdatePasswordBloc, UpdatePasswordState>(
              bloc: _updatePasswordBloc,
              listener: (context, state) {
                if (state is UpdatePasswordLoading) {
                  _isLoading = true;
                  blockCircleLoading(context: context);
                } else if (state is UpdatePasswordSuccess) {
                  if (_isLoading) {
                    _isLoading = false;
                    Navigator.of(context, rootNavigator: true).pop();
                  }

                  setState(() {
                    _isSuccessChangePass = true;
                  });

                  showBottomForgotAccount(
                      context: context, isDismissible: false);

                  AnalyticsHelper.setLogEvent(
                      Analytics.EVENT_SUCCESS_CHANGE_PASSWORD_OTP);
                } else if (state is UpdatePasswordFailure) {
                  if (_isLoading) {
                    _isLoading = false;
                    Navigator.of(context, rootNavigator: true).pop();
                  }

                  AnalyticsHelper.setLogEvent(
                      Analytics.EVENT_FAILED_CHANGE_PASSWORD_OTP,
                      <String, dynamic>{
                        'message': '${state.error.toString()}'
                      });

                  Scaffold.of(context).showSnackBar(
                    SnackBar(
                      content: Text(state.error.toString()),
                    ),
                  );
                } else if (state is ValidationUpdatePasswordError) {
                  if (_isLoading) {
                    _isLoading = false;
                    Navigator.of(context, rootNavigator: true).pop();
                  }
                  showDialog(
                      context: context,
                      builder: (BuildContext context) => DialogTextOnly(
                            description:
                                state.errors.toString().contains('username')
                                    ? state.errors['username'][0].toString()
                                    : state.errors.toString().contains('phone')
                                        ? state.errors['phone'][0].toString()
                                        : state.errors
                                                .toString()
                                                .contains('generic')
                                            ? state.errors['generic'][0]
                                            : state.errors
                                                    .toString()
                                                    .contains('otp')
                                                ? state.errors['otp'][0]
                                                : state.errors.toString(),
                            buttonText: Dictionary.ok,
                            onOkPressed: () {
                              Navigator.of(context, rootNavigator: true)
                                  .pop(); // To close the dialog
                            },
                          ));
                } else {
                  if (_isLoading) {
                    _isLoading = false;
                    Navigator.of(context, rootNavigator: true).pop();
                  }
                }
              },
              child: BlocBuilder<UpdatePasswordBloc, UpdatePasswordState>(
                bloc: _updatePasswordBloc,
                builder: (context, state) => Form(
                  key: _formKey,
                  child: ListView(
                    children: [
                      Container(
                        margin: EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              Dictionary.resetPassword,
                              style: TextStyle(
                                  fontSize: 20.0,
                                  color: clr.Colors.veryDarkGrey,
                                  fontFamily: FontsFamily.roboto,
                                  fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            Text(
                              Dictionary.descInputPassword,
                              style: TextStyle(
                                  fontSize: 14.0,
                                  wordSpacing: 1,
                                  height: 1.5,
                                  fontFamily: FontsFamily.roboto,
                                  color: clr.Colors.darkGrey),
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            TextFieldComponent(
                                title: Dictionary.hintPasswordNew,
                                hintText: Dictionary.descHintPasswordNew,
                                controller: _passwordTextController,
                                validation: (val) =>
                                    Validations.passwordValidation(val, false),
                                isPassword: true,
                                isEdit: true),
                            const SizedBox(
                              height: 20,
                            ),
                            TextFieldComponent(
                                title: Dictionary.repeatPasswordRegister,
                                hintText: Dictionary.descRepeatPasswordRegister,
                                controller: _confirmPasswordTextController,
                                isPassword: true,
                                validation: (val) =>
                                    Validations.repeatPasswordValidation(
                                        _passwordTextController.text, val),
                                isEdit: true),
                            const SizedBox(
                              height: 20,
                            ),
                            RaisedButton(
                              color: clr.Colors.darkblue,
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width,
                                padding:
                                    const EdgeInsets.symmetric(vertical: 13),
                                child: Text(
                                  Dictionary.send,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: FontsFamily.roboto,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              onPressed: () {
                                if (_formKey.currentState.validate()) {
                                  _updatePasswordBloc.add(UpdatePassword(
                                      phone: widget.phone,
                                      verifyToken: widget.verifyToken,
                                      password: _passwordTextController.text,
                                      retypePassword:
                                          _confirmPasswordTextController.text));
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        onWillPop: _onWillPop);
  }

  void showBottomForgotAccount(
      {@required BuildContext context, bool isDismissible = true}) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: const Radius.circular(8.0),
            topRight: const Radius.circular(8.0),
          ),
        ),
        isDismissible: isDismissible,
        builder: (context) {
          return WillPopScope(
              child: Container(
                margin: const EdgeInsets.all(Dimens.padding),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10.0),
                      child: Text(
                        Dictionary.changePasswordSuccess,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: FontsFamily.roboto,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10.0),
                      child: Text(
                        Dictionary.descChangePasswordSuccess,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: FontsFamily.roboto,
                          fontSize: 16.0,
                          color: clr.Colors.greyText,
                        ),
                      ),
                    ),
                    RaisedButton(
                      color: clr.Colors.darkblue,
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.symmetric(vertical: 13),
                        child: Text(
                          Dictionary.suceesLoginNow,
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: FontsFamily.roboto,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                      onPressed: () {
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) =>
                                LoginScreen(authRepository: AuthRepository()),
                          ),
                          (route) => false,
                        );
                      },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
              onWillPop: () async => false);
        });
  }

  Future<bool> _onWillPop() async {
    if (!_isSuccessChangePass) {
      await showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return DialogConfirmation(
            title: Dictionary.updatePassword,
            body: Dictionary.updatePasswordDesc,
            buttonOkText: Dictionary.yes,
            onOkPressed: () async {
              Navigator.pop(context, false);
              Navigator.pop(context, false);
            },
            buttonCancelText: Dictionary.no,
          );
        },
      );
    }

    return false;
  }
}
