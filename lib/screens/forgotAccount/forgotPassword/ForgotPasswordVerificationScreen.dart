import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:sapawarga/blocs/send_otp_change_password/Bloc.dart';
import 'package:sapawarga/blocs/verify_otp_change_password/Bloc.dart';
import 'package:sapawarga/components/BlockCircleLoading.dart';
import 'package:sapawarga/components/CustomAppBar.dart';
import 'package:sapawarga/components/DialogConfirmation.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';

class ForgotPasswordVerificationScreen extends StatefulWidget {
  final String phone;

  ForgotPasswordVerificationScreen({Key key, this.phone}) : super(key: key);

  @override
  _ForgotPasswordVerificationScreenState createState() =>
      _ForgotPasswordVerificationScreenState();
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('phone', phone));
  }
}

class _ForgotPasswordVerificationScreenState
    extends State<ForgotPasswordVerificationScreen> {
  final FocusNode _pinPutFocusNode = FocusNode();
  String _verificationCode;
  SendOtpPasswordBloc _sendOtpPasswordBloc;
  VerifyOtpPasswordBloc _verifyOtpPasswordBloc;
  bool _isLoading = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  Timer timer;
  bool isResendOtp = true;
  int countResendPassword = 0;
  DateTime dateTimeSave;
  final now = DateTime.now();

  @override
  void initState() {
    getDataCountResendOtp();
    super.initState();
  }

  Future<void> getDataCountResendOtp() async {
    final today = DateTime(now.year, now.month, now.day);
    countResendPassword = await Preferences.getCountResendOtpPassword();
    dateTimeSave =
        DateTime.fromMillisecondsSinceEpoch(await Preferences.getSaveDate());
    dateTimeSave =
        DateTime(dateTimeSave.year, dateTimeSave.month, dateTimeSave.day);
    if (today != dateTimeSave) {
      await Preferences.setCountResendOtpPassword(0);
      countResendPassword = 0;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final BoxDecoration pinPutDecoration = BoxDecoration(
      border: Border.all(color: clr.Colors.darkblue, width: 1.5),
      borderRadius: BorderRadius.circular(10.0),
    );

    return WillPopScope(
        child: Scaffold(
            key: _scaffoldKey,
            appBar: CustomAppBar().DefaultAppBar(title: Dictionary.appName),
            backgroundColor: Colors.white,
            body: MultiBlocProvider(
                providers: [
                  BlocProvider<VerifyOtpPasswordBloc>(
                      create: (BuildContext context) => _verifyOtpPasswordBloc =
                          VerifyOtpPasswordBloc(
                              authProfileRepository: AuthProfileRepository())),
                  BlocProvider<SendOtpPasswordBloc>(
                      create: (BuildContext context) => _sendOtpPasswordBloc =
                          SendOtpPasswordBloc(
                              authProfileRepository: AuthProfileRepository())),
                ],
                child: MultiBlocListener(
                  listeners: [
                    BlocListener<VerifyOtpPasswordBloc, VerifyOtpPasswordState>(
                        bloc: _verifyOtpPasswordBloc,
                        listener: (context, state) async {
                          if (state is VerifyOtpPasswordLoading) {
                            _isLoading = true;
                            await blockCircleLoading(context: context);
                          } else if (state is VerifyOtpPasswordSuccess) {
                            if (_isLoading) {
                              _isLoading = false;
                              Navigator.of(context, rootNavigator: true).pop();
                            }

                            await Navigator.of(context).pushNamed(
                                NavigationConstrants.ChangePasswordLogin,
                                arguments: [
                                  widget.phone,
                                  state.verifyToken,
                                ]);

                            await AnalyticsHelper.setLogEvent(
                                Analytics.EVENT_SUCCESS_CHANGE_PASSWORD_OTP);

                            Navigator.pop(context, true);
                          } else if (state is VerifyOtpPasswordFailure) {
                            if (_isLoading) {
                              _isLoading = false;
                              Navigator.of(context, rootNavigator: true).pop();
                            }

                            await AnalyticsHelper.setLogEvent(
                                Analytics.EVENT_FAILED_CHANGE_PASSWORD_OTP,
                                <String, dynamic>{
                                  'message': '${state.error.toString()}'
                                });

                            Scaffold.of(context).showSnackBar(
                              SnackBar(
                                content: Text(state.error.toString()),
                              ),
                            );
                          } else if (state
                              is ValidationVerifyOtpPasswordError) {
                            if (_isLoading) {
                              _isLoading = false;
                              Navigator.of(context, rootNavigator: true).pop();
                            }

                            await showDialog(
                                context: context,
                                builder: (BuildContext context) =>
                                    DialogTextOnly(
                                      description: state.errors
                                              .toString()
                                              .contains('username')
                                          ? state.errors['username'][0]
                                              .toString()
                                          : state.errors
                                                  .toString()
                                                  .contains('phone')
                                              ? state.errors['phone'][0]
                                                  .toString()
                                              : state.errors
                                                      .toString()
                                                      .contains('generic')
                                                  ? state.errors['generic'][0]
                                                  : state.errors.toString(),
                                      buttonText: Dictionary.ok,
                                      onOkPressed: () {
                                        Navigator.of(context,
                                                rootNavigator: true)
                                            .pop(); // To close the dialog
                                      },
                                    ));
                            // }
                          } else {
                            if (_isLoading) {
                              _isLoading = false;
                              Navigator.of(context, rootNavigator: true).pop();
                            }
                          }
                        }),
                    BlocListener<SendOtpPasswordBloc, SendOtpPasswordState>(
                        bloc: _sendOtpPasswordBloc,
                        listener: (context, state) {
                          if (state is SendOtpPasswordLoading) {
                            _isLoading = true;
                            blockCircleLoading(context: context);
                          } else if (state is SendOtpPasswordSuccess) {
                            if (_isLoading) {
                              _isLoading = false;
                              Navigator.of(context, rootNavigator: true).pop();
                            }

                            setState(() {
                              isResendOtp = false;
                            });
                            timer =
                                Timer.periodic(Duration(minutes: 7), (Timer t) {
                              setState(() {
                                isResendOtp = true;
                              });
                            });

                            AnalyticsHelper.setLogEvent(Analytics
                                .EVENT_SUCCESS_SEND_OTP_CHANGE_USERNAME);

                            Scaffold.of(context).showSnackBar(
                              SnackBar(
                                content: Text(Dictionary.sendVerificationCode),
                              ),
                            );
                          } else if (state is SendOtpPasswordFailure) {
                            if (_isLoading) {
                              _isLoading = false;
                              Navigator.of(context, rootNavigator: true).pop();
                            }

                            AnalyticsHelper.setLogEvent(
                                Analytics.EVENT_FAILED_SEND_OTP_CHANGE_USERNAME,
                                <String, dynamic>{
                                  'message': '${state.error.toString()}'
                                });

                            Scaffold.of(context).showSnackBar(
                              SnackBar(
                                content: Text(state.error.toString().isNotEmpty
                                    ? state.error.toString()
                                    : Dictionary.failedSendOtp),
                              ),
                            );
                          } else if (state is ValidationSendPasswordOtpError) {
                            if (_isLoading) {
                              _isLoading = false;
                              Navigator.of(context, rootNavigator: true).pop();
                            }

                            FocusScope.of(context).unfocus();
                            Scaffold.of(context).showSnackBar(
                              SnackBar(
                                content: Text(
                                    state.errors.toString().contains('generic')
                                        ? state.errors['data']['generic'][0]
                                        : Dictionary.failedSendOtp),
                              ),
                            );
                          } else {
                            if (_isLoading) {
                              _isLoading = false;
                              Navigator.of(context, rootNavigator: true).pop();
                            }
                          }
                        }),
                  ],
                  child: Container(
                    child: ListView(
                      padding: const EdgeInsets.all(20),
                      children: [
                        const SizedBox(height: 10),
                        Image.asset(
                          '${Environment.imageAssets}verification_password.png',
                          height: 80,
                        ),
                        const SizedBox(height: 20),
                        Text(
                          Dictionary.inputVerificationCode,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 22.0,
                              fontWeight: FontWeight.w600,
                              fontFamily: FontsFamily.roboto),
                        ),
                        const SizedBox(height: 20),
                        Text(
                          Dictionary.inputVerificationCodeDesc +
                              ' ' +
                              widget.phone +
                              Dictionary.inputVerificationCodeDesc2,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16.0,
                              fontFamily: FontsFamily.roboto,
                              color: clr.Colors.darkGrey),
                        ),
                        const SizedBox(
                          height: 35,
                        ),
                        PinPut(
                            textStyle: TextStyle(
                                fontSize: 20, fontFamily: FontsFamily.roboto),
                            preFilledWidget: Text(
                              '',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                            submittedFieldDecoration: pinPutDecoration,
                            selectedFieldDecoration: pinPutDecoration.copyWith(
                              border: Border.all(
                                width: 2,
                                color: clr.Colors.blue,
                              ),
                            ),
                            inputDecoration: InputDecoration(
                                contentPadding: EdgeInsets.zero,
                                border: InputBorder.none,
                                counterText: '',
                                hintText: '',
                                hintStyle: TextStyle(
                                    color: Colors.black,
                                    fontSize: 12,
                                    fontFamily: FontsFamily.roboto)),
                            followingFieldDecoration: pinPutDecoration,
                            fieldsCount: 6,
                            focusNode: _pinPutFocusNode,
                            eachFieldWidth: 50.0,
                            eachFieldHeight: 55.0,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            textInputAction: TextInputAction.go,
                            onSubmit: (String pin) {
                              setState(() {
                                _verificationCode = pin;
                              });
                            }),
                        const SizedBox(
                          height: 35,
                        ),
                        RaisedButton(
                            color: clr.Colors.darkblue,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            child: Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width,
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              margin: const EdgeInsets.symmetric(vertical: 5),
                              child: Text(
                                Dictionary.verification,
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.white,
                                    fontFamily: FontsFamily.roboto,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            onPressed: () {
                              FocusScope.of(context).unfocus();
                              if (_verificationCode != null) {
                                _verifyOtpPasswordBloc.add(
                                    VerifyOtpChangedPassword(
                                        phone: widget.phone,
                                        otp: _verificationCode));
                              } else {
                                final snackBar = SnackBar(
                                  content:
                                      Text(Dictionary.verificationCodeCheck),
                                );
                                _scaffoldKey.currentState
                                    .showSnackBar(snackBar);
                              }
                            }),
                        const SizedBox(
                          height: 15,
                        ),
                        countResendPassword < 7
                            ? isResendOtp
                                ? RichText(
                                    text: TextSpan(children: [
                                    TextSpan(
                                        text: Dictionary.notRecievedCode,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: FontsFamily.roboto,
                                            color: Colors.black)),
                                    TextSpan(
                                        text: ' ' + Dictionary.resending,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: FontsFamily.roboto,
                                            fontWeight: FontWeight.bold,
                                            color: clr.Colors.darkblue),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () async {
                                            setState(() {
                                              countResendPassword++;
                                            });

                                            await Preferences
                                                .setCountResendOtpPassword(
                                                    countResendPassword);
                                            if (countResendPassword == 6) {
                                              await Preferences.setSaveDate(
                                                  DateTime.now()
                                                      .millisecondsSinceEpoch);
                                            }
                                            await AnalyticsHelper.setLogEvent(
                                                Analytics
                                                    .EVENT_SUCCESS_SEND_OTP_CHANGE_PASSWORD);
                                            _sendOtpPasswordBloc.add(
                                                SendOtpChangedPassword(
                                                    phone: widget.phone));
                                          })
                                  ]))
                                : RichText(
                                    text: TextSpan(children: [
                                    TextSpan(
                                        text: Dictionary.resendOTPInfo,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: FontsFamily.roboto,
                                            color: Colors.black)),
                                  ]))
                            : RichText(
                                text: TextSpan(children: [
                                TextSpan(
                                    text: Dictionary.resendOtpMax,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: FontsFamily.roboto,
                                        color: Colors.black)),
                              ]))
                      ],
                    ),
                  ),
                ))),
        onWillPop: _onWillPop);
  }

  Future<bool> _onWillPop() async {
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return DialogConfirmation(
          title: Dictionary.otpVerification,
          body: Dictionary.otpVerificationDesc,
          buttonOkText: Dictionary.yes,
          onOkPressed: () async {
            Navigator.pop(context, false);
            Navigator.pop(context, false);
          },
          buttonCancelText: Dictionary.no,
        );
      },
    );
    return false;
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<Timer>('timer', timer));
    properties.add(DiagnosticsProperty<bool>('isResendOtp', isResendOtp));
    properties.add(IntProperty('countResendPassword', countResendPassword));
    properties.add(DiagnosticsProperty<DateTime>('dateTimeSave', dateTimeSave));
    properties.add(DiagnosticsProperty<DateTime>('now', now));
  }
}
