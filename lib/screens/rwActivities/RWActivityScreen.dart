import 'dart:async';
import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/authentication/AuthenticationBloc.dart';
import 'package:sapawarga/blocs/authentication/AuthenticationEvent.dart';
import 'package:sapawarga/blocs/rw_activities/kab_kota_category_list/KabKotaCategoryListBloc.dart';
import 'package:sapawarga/blocs/rw_activities/kab_kota_category_list/KabKotaCategoryListEvent.dart';
import 'package:sapawarga/blocs/rw_activities/kab_kota_category_list/KabKotaCategoryListState.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/CheckConnection.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/AreaModelCategory.dart';
import 'package:sapawarga/models/HelpQnaModel.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/RWActivityRepository.dart';
import 'package:sapawarga/screens/rwActivities/RWActivityListScreen.dart';
import 'package:sapawarga/screens/rwActivities/RWActivityListScreenMe.dart';
import 'package:sapawarga/utilities/BasicUtils.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';

class RWActivityScreen extends StatefulWidget {
  final UserInfoModel userInfoModel;

  RWActivityScreen({Key key, this.userInfoModel}) : super(key: key);

  @override
  RWActivityScreenState createState() => RWActivityScreenState();
}

class RWActivityScreenState extends State<RWActivityScreen>
    with TickerProviderStateMixin {
  final searchController = TextEditingController();
  final searchControllerKabkota = TextEditingController();
  AuthenticationBloc _authenticationBloc;
  AnimationController _animationController;
  KabKotaCategoryListBloc _listBloc;
  bool isBackRefreshPage = false;
  bool helpScreen = false;
  bool toolTipLoad = false;
  bool statConnect;
  bool isSearch = false;
  bool hasChange = false;
  bool isGeneral = true;
  bool isFilterKab = false;
  List<Item> _rwCategoryKabList = List<Item>();
  Item itemKabKotaCategoryModel;
  var containerWidth = 40.0;
  Timer _debounce;

  int current = 0;
  List<HelpQnaModel> qnaHelpModel = [];
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  void initState() {
    _initialize();

    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    searchControllerKabkota.addListener((() {
      _onSearchChanged();
    }));

    _animationController = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    );

    qnaHelpModel.add(HelpQnaModel(
        Dictionary.titleHelpRwActivities1,
        '${Environment.imageAssets}helprw1.png',
        Dictionary.deschelpRwActivities1));
    qnaHelpModel.add(HelpQnaModel(
        Dictionary.titleHelpRwActivities2,
        '${Environment.imageAssets}helprw2.png',
        Dictionary.deschelpRwActivities2));
    qnaHelpModel.add(HelpQnaModel(
        Dictionary.titleHelpRwActivities3,
        '${Environment.imageAssets}helprw3.png',
        Dictionary.deschelpRwActivities3));
    super.initState();
  }

  _initialize() async {
    helpScreen = await Preferences.hasShowHelpRwActivity();

    await CheckConnection.checkConnection().then((isConnect) {
      if (isConnect != null && isConnect) {
        statConnect = true;
      } else {
        statConnect = false;
      }
    });

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return statConnect == null || statConnect
        ? DefaultTabController(
            length: 2,
            child: Scaffold(
              key: scaffoldState,
              appBar: AppBar(
                title: _titleBar(),
                titleSpacing: 0.0,
                actions: <Widget>[
                  RotationTransition(
                    turns: Tween(begin: 0.0, end: 1.0)
                        .animate(_animationController),
                    child: IconButton(
                      icon: isSearch ? Icon(Icons.close) : Icon(Icons.search),
                      onPressed: () {
                        _searchPressed();
                      },
                    ),
                  ),
                  IconButton(
                      icon: Icon(Icons.tune),
                      onPressed: () async {
                        isGeneral ? _searchFilterKabPressed() : '';
                      }),
                ],
                bottom: TabBar(
                  tabs: <Widget>[
                    Tab(
                        child: Text(Dictionary.general,
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: FontsFamily.intro),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis)),
                    Tab(
                        child: Text(Dictionary.myActivities,
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: FontsFamily.intro),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis)),
                  ],
                ),
              ),
              body: Stack(
                children: <Widget>[
                  TabBarView(
                    children: <Widget>[
                      RWActivityListScreen(
                          tooltipLoad: toolTipLoad,
                          rwActivityScreenState: this,
                          userInfoModel: widget.userInfoModel),
                      RWActivityListScreenMe(
                          tooltipLoad: toolTipLoad,
                          rwActivityScreenState: this,
                          userInfoModel: widget.userInfoModel)
                    ],
                  ),
                  !helpScreen ? helpRwActivity() : Container()
                ],
              ),
            ),
          )
        : Scaffold(
            appBar: AppBar(
              title: Text(Dictionary.activities,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: FontsFamily.intro),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis),
              actions: <Widget>[
                IconButton(
                    icon: Icon(Icons.info),
                    onPressed: () async {
                      if (helpScreen) {
                        setState(() {
                          toolTipLoad = true;
                        });
                      }
                    })
              ],
            ),
            body: ErrorContent(error: Dictionary.errorConnection),
          );
  }

  Widget _titleBar() {
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.centerRight,
          child: AnimatedOpacity(
            opacity: isSearch ? 1.0 : 0.0,
            duration: Duration(milliseconds: 500),
            child: AnimatedContainer(
              duration: Duration(milliseconds: 300),
              width: containerWidth,
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: TextField(
                controller: searchController,
                autofocus: false,
                textInputAction: TextInputAction.go,
                style: TextStyle(color: Colors.black, fontSize: 16.0),
                decoration: InputDecoration(
                    hintStyle: TextStyle(fontSize: 13.0),
                    hintText: Dictionary.searchHintRwActivities,
                    border: InputBorder.none,
                    contentPadding: const EdgeInsets.symmetric(
                        horizontal: 15.0, vertical: 10.0)),
              ),
            ),
          ),
        ),
        !isSearch
            ? Container(
                height: MediaQuery.of(context).size.height,
                alignment: Alignment.centerLeft,
                child: Visibility(
                  visible: !isSearch,
                  child: Text(Dictionary.rwActivities,
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: FontsFamily.intro),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis),
                ))
            : Container(),
      ],
    );
  }

  void _searchPressed() {
    return setState(() {
      isSearch = !isSearch;
      _animationController.forward(from: 0.0);
      itemKabKotaCategoryModel = null;

      searchControllerKabkota.text = '';
      _showSearch();
    });
  }

  void _searchFilterKabPressed() {
    return setState(() {
      searchControllerKabkota.text = '';
      itemKabKotaCategoryModel = null;
      isFilterKab = false;
      _rwActivitiesBottomSheet(context);
    });
  }

  void _showSearch() {
    if (!isSearch) {
      containerWidth = 50.0;
      FocusScope.of(context).unfocus();
    } else {
      containerWidth = MediaQuery.of(context).size.width;
    }
    searchController.clear();
  }

  Widget helpRwActivity() {
    CarouselSlider basicSlider;
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
      child: Container(
          alignment: Alignment.bottomCenter,
          color: Colors.black12.withOpacity(0.5),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Card(
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.white70, width: 1),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Stack(
                children: <Widget>[
                  basicSlider = CarouselSlider(
                      initialPage: 0,
                      height: 450,
                      enableInfiniteScroll: false,
                      viewportFraction: 1.0,
                      onPageChanged: (record) {
                        setState(() {
                          current = record;
                        });
                      },
                      items: qnaHelpModel.map((record) {
                        return Builder(
                          builder: (BuildContext context) {
                            return Container(
                                margin: const EdgeInsets.all(10),
                                child: Stack(
                                  children: <Widget>[
                                    Positioned(
                                      top: 0,
                                      left: 0,
                                      right: 0,
                                      child: Container(
                                        margin: const EdgeInsets.all(10),
                                        alignment: Alignment.center,
                                        child: Text(
                                          record.title,
                                          style: TextStyle(
                                              fontSize: 22.0,
                                              fontWeight: FontWeight.bold),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          margin:
                                              const EdgeInsets.only(bottom: 10),
                                          child: Image.asset(
                                            record.assetImage,
                                            fit: BoxFit.cover,
                                            width: 170,
                                          ),
                                        ),
                                        Container(
                                          margin:
                                              const EdgeInsets.only(bottom: 10),
                                          child: Text(
                                            record.desc,
                                            style: TextStyle(fontSize: 16.0),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ));
                          },
                        );
                      }).toList()),
                  Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: map<Widget>(qnaHelpModel, (index, record) {
                            return Container(
                              width: 8.0,
                              height: 8.0,
                              margin: const EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 2.0),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: current == index
                                      ? clr.Colors.blue
                                      : Colors.grey),
                            );
                          }),
                        ),
                        Container(
                          margin: const EdgeInsets.all(10),
                          child: RoundedButton(
                            title: qnaHelpModel.length - 1 == current
                                ? Dictionary.buttonStartRwActivities
                                : Dictionary.buttonNext,
                            borderRadius: BorderRadius.circular(5.0),
                            color: clr.Colors.blue,
                            textStyle: Theme.of(context)
                                .textTheme
                                .button
                                .copyWith(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16.0),
                            onPressed: () async {
                              if (qnaHelpModel.length - 1 == current) {
                                await Preferences.setShowHelpRwActivity(true);
                                setState(() {
                                  toolTipLoad = true;
                                  helpScreen = true;
                                });
                              } else {
                                await basicSlider.nextPage(
                                    duration: Duration(milliseconds: 300),
                                    curve: Curves.linear);
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ))),
    );
  }

  Container showBottomSheet() {
    return Container();
  }

  void _rwActivitiesBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10.0),
            topRight: Radius.circular(10.0),
          ),
        ),
        elevation: 10.0,
        builder: (BuildContext context) {
          return Container(
              child: BlocProvider<KabKotaCategoryListBloc>(
            create: (context) {
              return _listBloc = KabKotaCategoryListBloc(RWActivityRepository())
                ..add(KabKotaCategoryListLoad());
            },
            child: _buildBlocListener(),
          ));
        });
  }

  _buildBlocListener() {
    return BlocListener<KabKotaCategoryListBloc, KabKotaCategoryListState>(
      bloc: _listBloc,
      listener: (context, state) {
        if (state is KabKotaCategoryListLoaded) {
          _rwCategoryKabList.addAll(state.records.data.items);
          _rwCategoryKabList = _rwCategoryKabList.toSet().toList();
          setState(() {});
        } else if (state is KabKotaCategoryListFailure) {
          if (state.error.contains(Dictionary.errorUnauthorized)) {
            _authenticationBloc.add(LoggedOut());
            Navigator.of(context).pop();
          }
        }
      },
      child: BlocBuilder<KabKotaCategoryListBloc, KabKotaCategoryListState>(
        bloc: _listBloc,
        builder: (context, state) => Wrap(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(top: 14.0),
                  color: Colors.grey[300],
                  height: 1.5,
                  width: 40.0,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(top: 2),
                  color: Colors.grey[300],
                  height: 1.5,
                  width: 40.0,
                ),
              ],
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(left: Dimens.padding, top: 15.0),
              child: Text(
                Dictionary.chooseKabKota,
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600),
              ),
            ),
            Container(
              padding:
                  const EdgeInsets.only(top: 5, bottom: 5, left: 2, right: 2),
              margin: const EdgeInsets.only(
                  top: 10, left: Dimens.padding, right: Dimens.padding),
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(5.0)),
              child: Wrap(children: [
                Container(
                  width: MediaQuery.of(context).size.width - 70,
                  height: 30.0,
                  child: TextField(
                      controller: searchControllerKabkota,
                      autofocus: false,
                      maxLines: 1,
                      minLines: 1,
                      maxLength: 255,
                      style: TextStyle(color: Colors.black, fontSize: 17.0),
                      decoration: InputDecoration(
                          hintText: Dictionary.searchKabKota,
                          counterText: "",
                          border: InputBorder.none,
                          contentPadding: const EdgeInsets.all(5.0))),
                ),
                Container(
                    width: 25.0,
                    height: 30.0,
                    child: Icon(
                      Icons.search,
                      color: Colors.grey,
                      size: 20.0,
                    )),
              ]),
            ),
            Container(
                child: state is KabKotaCategoryListLoading
                    ? _buildLoading()
                    : state is KabKotaCategoryListLoaded
                        ? state.records.data.items.isNotEmpty
                            ? _buildContent(state)
                            : EmptyData(message: Dictionary.emptyDataRWActivity)
                        : state is KabKotaCategoryListFailure
                            ? ErrorContent(error: state.error)
                            : _buildLoading())
          ],
        ),
      ),
    );
  }

  Widget _buildContent(KabKotaCategoryListLoaded state) {
    return Column(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(
              left: Dimens.padding, top: 2.0, right: Dimens.padding),
          width: MediaQuery.of(context).size.width,
          alignment: Alignment.topLeft,
          height: 500,
          child: ListView(
            children: <Widget>[
              ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: state.records.data.items.length,
                  itemBuilder: (context, index) => GestureDetector(
                      onTap: () {
                        setState(() {
                          itemKabKotaCategoryModel = _rwCategoryKabList[index];
                          isFilterKab = true;
                        });
                        Navigator.pop(context);
                      },
                      child: Container(
                        color: Colors.grey[50],
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(top: 15, bottom: 5),
                              child: Text(
                                StringUtils.capitalizeWord(
                                    _rwCategoryKabList[index].name),
                                style: TextStyle(
                                    fontSize: 17.0,
                                    fontWeight: FontWeight.w300),
                              ),
                            ),
                            Container(
                              margin:
                                  const EdgeInsets.only(top: 5.0, bottom: 5),
                              color: Colors.grey[300],
                              height: 1.5,
                              width: MediaQuery.of(context).size.width,
                            ),
                          ],
                        ),
                      ))),
              const SizedBox(height: 250)
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildLoading() {
    return Container(
      margin: const EdgeInsets.only(
          left: Dimens.padding, top: 12.0, right: Dimens.padding),
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.topLeft,
      height: 500,
      child: ListView.builder(
        itemCount: 10,
        itemBuilder: (context, index) => Skeleton(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(top: 15, bottom: 5),
                    width: MediaQuery.of(context).size.width,
                    height: 20.0,
                    color: Colors.grey[300]),
                const SizedBox(height: 2.0),
                Container(
                  margin: const EdgeInsets.only(top: 5.0, bottom: 5),
                  color: Colors.grey[300],
                  height: 1.5,
                  width: MediaQuery.of(context).size.width,
                ),
                const SizedBox(height: 2.0),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _onSearchChanged() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      if (searchControllerKabkota.text.trim().isNotEmpty) {
        if (isGeneral) {
          _rwCategoryKabList.clear();
          _listBloc
              .add(KabKotaCategorySearchLoad(searchControllerKabkota.text));
        }
      } else {
        _rwCategoryKabList.clear();
        _listBloc.add(KabKotaCategoryListLoad());
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    searchController.dispose();
    _animationController.dispose();
    searchControllerKabkota.dispose();
    if (_listBloc != null) {
      _listBloc.close();
    }
    super.dispose();
  }
}
