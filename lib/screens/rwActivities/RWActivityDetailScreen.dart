import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/account_profile/AccountProfileBloc.dart';
import 'package:sapawarga/blocs/account_profile/Bloc.dart';
import 'package:sapawarga/blocs/rw_activities/Bloc.dart';
import 'package:sapawarga/blocs/rw_activities/rw_activity_comment/Bloc.dart';
import 'package:sapawarga/blocs/rw_activities/rw_activity_comment/rw_activity_comment_add/RWActivityCommentAddBloc.dart';
import 'package:sapawarga/components/CustomAppBar.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/HeroImagePreviewScreen.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/RWActivityCommentModel.dart';
import 'package:sapawarga/models/RWActivityModel.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/RWActivityRepository.dart';
import 'package:sapawarga/screens/rwActivities/RWActivityDetailComments.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/BasicUtils.dart';
import 'package:sapawarga/utilities/EmptyStringCheck.dart';
import 'package:sapawarga/utilities/FormatDate.dart';
import 'package:http/http.dart' as http;

class RWActivityDetailScreen extends StatefulWidget {
  final ItemRWActivity record;
  final int id;
  final String title;
  final UserInfoModel userInfoModel;

  RWActivityDetailScreen(
      {Key key, this.record, this.id, this.title, this.userInfoModel})
      : super(key: key);

  @override
  _RWActivityDetailScreenState createState() => _RWActivityDetailScreenState();
}

class _RWActivityDetailScreenState extends State<RWActivityDetailScreen> {
  final _textMessage = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final ScrollController _scrollController = ScrollController();
  final RefreshController _mainRefreshController = RefreshController();
  List<ItemRwActivityComment> _listComments = List<ItemRwActivityComment>();

  ItemRWActivity record;
  RWActivityCommentAddBloc _addCommentBloc;
  RWActivityCommentListBloc _listCommentBloc;
  RWActivityDetailBloc _rwActivityDetailBloc;
  int currentIndexPage = 0;
  AccountProfileBloc _accountProfileBloc;

  bool _isLoaded = false;
  int _pageCount = 0;
  int _currentPage = 1;
  int _itemCount = 0;
  final _scrollThreshold = 200.0;
  String _likeState = '${Environment.imageAssets}like-empty.png';

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  void initState() {
    record = widget.record;
    AnalyticsHelper.setLogEvent(
        Analytics.EVENT_VIEW_DETAIL_KEGIATAN_RW, <String, dynamic>{
      'id': widget.id != null ? widget.id : record.id,
      'title': widget.title != null ? widget.title : record.text
    });

    _scrollController.addListener(() {
      _onScroll();
    });

    _textMessage.addListener((() {
      setState(() {});
    }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar().DefaultAppBar(title: Dictionary.activities),
      body: Container(
        child: MultiBlocProvider(
            providers: [
              BlocProvider<RWActivityDetailBloc>(
                create: (context) => _rwActivityDetailBloc =
                    RWActivityDetailBloc(RWActivityRepository())
                      ..add(RWActivityDetailLoad(widget.id, record)),
              ),
              BlocProvider<RWActivityCommentListBloc>(
                  create: (context) => _listCommentBloc =
                      RWActivityCommentListBloc(RWActivityRepository())
//                      ..add(RWActivityCommentListLoad(record.id, _currentPage)),
                  ),
              BlocProvider<RWActivityCommentAddBloc>(
                create: (context) => _addCommentBloc =
                    RWActivityCommentAddBloc(RWActivityRepository()),
              ),
              BlocProvider<AccountProfileBloc>(
                create: (BuildContext context) =>
                    _accountProfileBloc = AccountProfileBloc.profile(
                  authProfileRepository: AuthProfileRepository(),
                )..add(
                        AccountProfileLoad(),
                      ),
              ),
            ],
            child: MultiBlocListener(
              listeners: [
                BlocListener<RWActivityCommentListBloc,
                    RWActivityCommentListState>(
                  bloc: _listCommentBloc,
                  listener: (context, state) {
                    if (state is RWActivityCommentListLoaded) {
                      _pageCount = state.records.meta.pageCount;
                      _currentPage = state.records.meta.currentPage + 1;
                      _itemCount = state.records.meta.totalCount;
                      _listComments.addAll(state.records.items);
                      _listComments = _listComments.toSet().toList();
                      _isLoaded = true;
                      setState(() {});
                    }
                  },
                ),
                BlocListener<RWActivityDetailBloc, RWActivityDetailState>(
                  bloc: _rwActivityDetailBloc,
                  listener: (context, state) {
                    if (state is RWActivityDetailLoaded) {
                      record = state.records;
                      _listCommentBloc.add(RWActivityCommentListLoad(
                          widget.id != null ? widget.id : record.id,
                          _currentPage));
                      setState(() {});
                    }
                  },
                ),
                BlocListener<RWActivityCommentAddBloc,
                    RWActivityCommentAddState>(
                  bloc: _addCommentBloc,
                  listener: (context, state) {
                    if (state is RWActivityCommentAdded) {
                      _itemCount = _itemCount + 1;
                      _listComments.insert(0, state.record);

                      setState(() {});

                      _scaffoldKey.currentState.showSnackBar(
                        SnackBar(
                          content: Container(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 10.0),
                              child: Text(
                                Dictionary.successAddComment,
                                style: TextStyle(fontSize: 14.0),
                              )),
                          backgroundColor: clr.Colors.blue,
                          duration: Duration(seconds: 2),
                        ),
                      );
                    } else if (state is RWActivityCommentAddFailure) {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => DialogTextOnly(
                                description: state.error,
                                buttonText: "OK",
                                onOkPressed: () {
                                  Navigator.of(context)
                                      .pop(); // To close the dialog
                                },
                              ));
                    }
                  },
                ),
              ],
              child: widget.userInfoModel != null
                  ? _loadRWDetail(widget.userInfoModel)
                  : BlocBuilder<AccountProfileBloc, AccountProfileState>(
                      bloc: _accountProfileBloc,
                      builder: (context, state) {
                        return state is AccountProfileLoading
                            ? _buildLoading()
                            : state is AccountProfileLoaded
                                ? _loadRWDetail(state.record)
                                : Container();
                      }),
            )),
      ),
    );
  }

  _loadRWDetail(UserInfoModel userInfoModel) {
    return BlocBuilder<RWActivityDetailBloc, RWActivityDetailState>(
        builder: (context, state) => state is RWActivityDetailLoading
            ? _buildLoading()
            : state is RWActivityDetailLoaded
                ? _buildContent(userInfoModel)
                : state is RWActivityDetailFailure
                    ? ErrorContent(error: state.error)
                    : Container());
  }

  Widget _buildLoading() {
    return Skeleton(
      child: Container(
        padding: const EdgeInsets.fromLTRB(
            Dimens.padding, 10.0, Dimens.padding, 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ListTile(
              contentPadding: const EdgeInsets.all(0.0),
              leading: Container(
                width: 40.0,
                height: 40.0,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25.0),
                  child: Container(
                      width: 40.0, height: 40.0, color: Colors.grey[300]),
                ),
              ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      width: MediaQuery.of(context).size.width / 2,
                      height: 20.0,
                      color: Colors.grey[300]),
                  const SizedBox(height: 2.0),
                  Container(width: 50.0, height: 15.0, color: Colors.grey[300]),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 5.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(25.0),
                child: Container(
                    height: MediaQuery.of(context).size.height / 3.5,
                    color: Colors.grey[300]),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 20,
                          height: 20,
                          color: Colors.grey[300],
                        ),
                        const SizedBox(width: 5.0),
                        Container(
                            width: MediaQuery.of(context).size.width / 4,
                            height: 20.0,
                            color: Colors.grey[300]),
                      ],
                    ),
                  ),
                  Container(
                    child: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 20,
                          height: 20,
                          color: Colors.grey[300],
                        ),
                        const SizedBox(width: 5.0),
                        Container(
                            width: MediaQuery.of(context).size.width / 4,
                            height: 20.0,
                            color: Colors.grey[300]),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container _buildContent(UserInfoModel userInfoModel) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Flexible(
            child: SmartRefresher(
              controller: _mainRefreshController,
              enablePullDown: true,
              header: WaterDropMaterialHeader(),
              onRefresh: () async {
                _isLoaded = false;
                setState(() {});
                _currentPage = 1;
                _listComments.clear();
                _listCommentBloc.add(RWActivityCommentListLoad(
                    widget.id != null ? widget.id : record.id, _currentPage));
                _mainRefreshController.refreshCompleted();
              },
              child: ListView(
                controller: _scrollController,
                children: <Widget>[
                  /** Detail description post **/
                  _buildPost(),
                  Divider(thickness: 2.0),

                  /** List of comments **/
                  RWActivityDetailComments(
                      commentListBloc: _listCommentBloc,
                      listComments: _listComments,
                      itemCount: _itemCount)
                ],
              ),
            ),
          ),

          /** Input field & send button **/
          userInfoModel.isVerified == 1
              ? _isLoaded
                  ? Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border:
                              Border(top: BorderSide(color: Colors.grey[300]))),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(right: 50),
                            child: TextField(
                                controller: _textMessage,
                                autofocus: false,
                                maxLines: 5,
                                minLines: 1,
                                maxLength: 255,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16.0),
                                decoration: InputDecoration(
                                    hintText: Dictionary.hintComment,
                                    counterText: "",
                                    border: InputBorder.none,
                                    contentPadding: const EdgeInsets.only(
                                        left: Dimens.contentPadding,
                                        top: Dimens.contentPadding,
                                        bottom: Dimens.contentPadding))),
                          ),
                          Positioned(
                              right: 15.0,
                              bottom: 15.0,
                              child: _textMessage.text.trim().isNotEmpty
                                  ? GestureDetector(
                                      child: Icon(Icons.send,
                                          color: clr.Colors.blue),
                                      onTap: () {
                                        setState(() {
                                          _addCommentBloc.add(
                                            RWActivityCommentAdd(
                                              id: widget.id != null
                                                  ? widget.id
                                                  : record.id,
                                              text: _textMessage.text.trim(),
                                            ),
                                          );

                                          AnalyticsHelper.setLogEvent(
                                              Analytics
                                                  .EVENT_COMMENT_KEGIATAN_RW,
                                              <String, dynamic>{
                                                'id': widget.id != null
                                                    ? widget.id
                                                    : record.id,
                                                'text': _textMessage.text.trim()
                                              });

                                          _textMessage.clear();
                                          FocusScope.of(context)
                                              .requestFocus(FocusNode());
                                        });
                                      })
                                  : Icon(Icons.send, color: clr.Colors.grey))
                        ],
                      ))
                  : Container()
              : Container(),
        ],
      ),
    );
  }

  _buildPost() {
    bool isLiked = record.isLiked;

    if (isLiked) {
      _likeState = '${Environment.imageAssets}liked.png';
    } else {
      _likeState = '${Environment.imageAssets}like-empty.png';
    }

    return Container(
      padding: const EdgeInsets.symmetric(
          vertical: 10.0, horizontal: Dimens.padding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListTile(
            contentPadding: const EdgeInsets.all(0.0),
            leading: Container(
              width: 40.0,
              height: 40.0,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(25.0),
                child: CachedNetworkImage(
                  imageUrl: record.user.photoUrlFull,
                  fit: BoxFit.fill,
                  placeholder: (context, url) => Container(
                      color: Colors.grey[200],
                      child: Center(child: CupertinoActivityIndicator())),
                  errorWidget: (context, url, error) => Container(
                      color: Colors.grey[200],
                      child: Image.asset('${Environment.imageAssets}user.png',
                          fit: BoxFit.cover)),
                ),
              ),
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  record.user.name,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: FontsFamily.productSans),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  '${Dictionary.RW} ' +
                      StringUtils.capitalizeWord(
                        emptyStringCheck(record.user.rw) +
                            ', ' +
                            emptyStringCheck(record.user.kelurahan)
                                .toLowerCase() +
                            ', ' +
                            emptyStringCheck(record.user.kecamatan)
                                .toLowerCase() +
                            ', ' +
                            emptyStringCheck(record.user.kabkota).toLowerCase(),
                      ),
                  style: TextStyle(fontSize: 14.0, color: Colors.grey[600]),
                )
              ],
            ),
          ),
          CarouselSlider(
              initialPage: 0,
              enableInfiniteScroll: false,
              viewportFraction: 1.0,
              onPageChanged: (record) {
                setState(() {
                  currentIndexPage = record;
                });
              },
              items: record.images.map((records) {
                return Container(
                  width: MediaQuery.of(context).size.width,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: GestureDetector(
                      child: Hero(
                        tag: Dictionary.heroImageTag,
                        child: CachedNetworkImage(
                          imageUrl: records.url,
                          fit: BoxFit.fitWidth,
                          placeholder: (context, url) => Center(
                              heightFactor: 8.2,
                              child: CupertinoActivityIndicator()),
                          errorWidget: (context, url, error) => Container(
                              height: MediaQuery.of(context).size.height / 3.3,
                              color: Colors.grey[200],
                              child: Image.asset(
                                  '${Environment.imageAssets}placeholder.png',
                                  fit: BoxFit.fitWidth)),
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => HeroImagePreview(
                                      Dictionary.heroImageTag,
                                      galleryItems: record,
                                      initialIndex: currentIndexPage,
                                    )));
                      },
                    ),
                  ),
                );
              }).toList()),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: map<Widget>(record.images, (indexImage, record) {
              return Container(
                width: 8.0,
                height: 8.0,
                margin:
                    const EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: currentIndexPage == indexImage
                        ? clr.Colors.blue
                        : Colors.grey),
              );
            }),
          ),
          Container(
            margin: const EdgeInsets.only(top: 10.0),
            child: Container(
              margin: const EdgeInsets.only(left: 2.0),
              child: Row(
                children: <Widget>[
                  GestureDetector(
                    child: Container(
                      width: 24,
                      height: 24,
                      child: Image.asset(
                        _likeState,
                      ),
                    ),
                    onTap: () {
                      List<ItemRWActivity> listRecords = [];
                      listRecords.add(record);
                      if (isLiked) {
                        _updateLike(listRecords, record.id, true);
                      } else {
                        _updateLike(listRecords, record.id, false);
                      }
                    },
                  ),
                  const SizedBox(width: 5.0),
                  Text(
                    record.likesCount > 0
                        ? '${record.likesCount} ' + Dictionary.likes
                        : Dictionary.likes,
                    style: TextStyle(fontSize: 15.0),
                  ),
                  const SizedBox(width: 10.0),
                  Container(
                    margin: const EdgeInsets.only(right: 2.0),
                    child: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          '${Environment.iconAssets}comments.png',
                          width: 24.0,
                          height: 24.0,
                        ),
                        record.commentsCount > 0
                            ? const SizedBox(width: 5.0)
                            : Container(),
                        Text(
                          record.commentsCount > 0
                              ? '${record.commentsCount} ' + Dictionary.comment
                              : ' ' + Dictionary.comment,
                          style: TextStyle(fontSize: 15.0),
                        )
                      ],
                    ),
                  ),
                  const SizedBox(width: 10.0),
                  GestureDetector(
                    child: Container(
                        child: Row(
                      children: <Widget>[
                        Image.asset('${Environment.iconAssets}share.png',
                            height: 24.0),
                        const SizedBox(width: 5.0),
                        Text(
                          Dictionary.share,
                          style: TextStyle(fontSize: 15.0),
                        ),
                      ],
                    )),
                    onTap: () {
                      _shareImage(record.id, record.imagePathFull, record.text);
                    },
                  ),
                ],
              ),
            ),
          ),
          record.status == 0
              ? Container(
                  margin: const EdgeInsets.only(top: 15.0),
                  child: Row(
                    children: <Widget>[
                      Image.asset(
                        '${Environment.iconAssets}warning_rw.png',
                        width: 18.0,
                        height: 18.0,
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 5),
                        width: MediaQuery.of(context).size.width - 120,
                        child: Text(
                          Dictionary.contentNotPermittedRWActivities,
                          style: TextStyle(fontSize: 15.0, color: Colors.red),
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  ))
              : const SizedBox(height: 15.0),
          Container(
            margin: const EdgeInsets.only(top: 5.0),
            child: Text(
              record.text,
              style: TextStyle(fontSize: 15.0),
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            unixTimeStampToTimeAgo(record.createdAt),
            style: TextStyle(fontSize: 14.0, color: Colors.grey[600]),
          )
        ],
      ),
    );
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (_currentPage != _pageCount + 1) {
      if (maxScroll - currentScroll <= _scrollThreshold) {
        _listCommentBloc.add(RWActivityCommentListLoad(
            widget.id != null ? widget.id : record.id, _currentPage));
      }
    }
  }

  _updateLike(List<ItemRWActivity> records, int id, bool isLike) {
    for (int i = 0; i < records.length; i++) {
      if (records[i].id == id) {
        if (isLike) {
          if (records[i].isLiked) {
            setState(() {
              records[i].isLiked = false;
              records[i].likesCount = records[i].likesCount - 1;
            });
          }
        } else {
          setState(() {
            records[i].likesCount = records[i].likesCount + 1;
            records[i].isLiked = true;
          });
        }
      }
    }
  }

  Future<void> _shareImage(int id, String url, String text) async {
    try {
      final response = await http.get(url);
      await Share.file(Dictionary.rwActivities, 'kegiatan_rw.jpg',
          response.bodyBytes, 'image/jpg',
          text: '$text ${Dictionary.sharedFrom}');

      await AnalyticsHelper.setLogEvent(Analytics.EVENT_SHARE_KEGIATAN_RW,
          <String, dynamic>{'id': id, 'image': url});
    } catch (e) {
      print('error: $e');
    }
  }

  @override
  void dispose() {
    _textMessage.dispose();
    _addCommentBloc.close();
    _listCommentBloc.close();
    _accountProfileBloc.close();
    _rwActivityDetailBloc.close();
    super.dispose();
  }
}
