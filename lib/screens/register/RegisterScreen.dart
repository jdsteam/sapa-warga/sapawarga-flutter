import 'dart:convert';

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/remote_home/Bloc.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/repositories/FirebaseRepository.dart';

// ignore: must_be_immutable
class RegisterScreen extends StatelessWidget {
  RegisterScreen({Key key}) : super(key: key);

  final int roleCodeRW = 50;
  final int roleCodePublic = 10;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocProvider<RemoteHomeBloc>(
      create: (BuildContext context) =>
          RemoteHomeBloc(firebaseRepository: FirebaseRepository())
            ..add(RemoteHomeLoad()),
      child: BlocBuilder<RemoteHomeBloc, RemoteHomeState>(
        builder: (context, state) {
          return state is RemoteHomeLoading
              ? _buildLoading()
              : state is RemoteHomeLoaded
                  ? _buildContent(state.remoteConfig, context)
                  : Container();
        },
      ),
    ));
  }

  _buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  _buildContent(RemoteConfig remoteConfig, BuildContext context) {
    Map<String, dynamic> menuConfig =
        json.decode(remoteConfig.getString(FirebaseConfig.menuConfig));

    return Container(
      padding: const EdgeInsets.all(20),
      child: ListView(
        children: [
          const SizedBox(
            height: 20,
          ),
          Text(
            Dictionary.titleLogin,
            style: TextStyle(
                fontSize: 32,
                fontFamily: FontsFamily.roboto,
                fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            Dictionary.descRegister +
                ' ' +
                (menuConfig['RegisterAccount']['RegisterRW'] &&
                        menuConfig['RegisterAccount']['RegisterPublic']
                    ? '${Dictionary.rwDesc} atau ${Dictionary.public}'
                    : menuConfig['RegisterAccount']['RegisterPublic']
                        ? Dictionary.public
                        : menuConfig['RegisterAccount']['RegisterRW']
                            ? Dictionary.rwDesc
                            : ''),
            style: TextStyle(
                fontSize: 14,
                wordSpacing: 1,
                height: 1.5,
                fontFamily: FontsFamily.roboto,
                color: clr.Colors.darkGrey),
          ),
          const SizedBox(
            height: 20,
          ),
          menuConfig['RegisterAccount']['RegisterRW']
              ? _buildButtonRegister(
                  onPressed: () {
                    Navigator.of(context).pushNamed(
                        NavigationConstrants.RegisterForm,
                        arguments: roleCodeRW);
                  },
                  title: Dictionary.titleRegisterButtonRw,
                  desc: Dictionary.descRegisterButtonRw,
                  imageIcon: '${Environment.iconAssets}rwIcon.png')
              : Container(),
          const SizedBox(
            height: 20,
          ),
          menuConfig['RegisterAccount']['RegisterPublic']
              ? _buildButtonRegister(
                  onPressed: () {
                    Navigator.of(context).pushNamed(
                        NavigationConstrants.RegisterForm,
                        arguments: roleCodePublic);
                  },
                  title: Dictionary.titleRegisterButtonPublic,
                  desc: Dictionary.descRegisterButtonPublic,
                  imageIcon: '${Environment.iconAssets}wargaIcon.png')
              : Container(),
          const SizedBox(
            height: 20,
          ),
          RichText(
              text: TextSpan(children: [
            TextSpan(
                text: Dictionary.confirmHaveAccount,
                style: TextStyle(
                    fontSize: 14,
                    fontFamily: FontsFamily.roboto,
                    color: clr.Colors.netralGrey)),
            TextSpan(
                text: Dictionary.loginNow,
                style: TextStyle(
                    fontSize: 14,
                    fontFamily: FontsFamily.roboto,
                    decoration: TextDecoration.underline,
                    color: clr.Colors.darkblue),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    Navigator.of(context, rootNavigator: true).pop();
                  })
          ]))
        ],
      ),
    );
  }

  _buildButtonRegister(
      {GestureTapCallback onPressed,
      String title,
      String desc,
      String imageIcon}) {
    return RaisedButton(
      onPressed: onPressed,
      elevation: 0,
      padding: const EdgeInsets.all(0.0),
      color: Colors.grey[200],
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: BorderSide(width: 1, color: Colors.grey[300])),
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Image.asset(imageIcon, height: 60),
                const SizedBox(
                  width: 20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: TextStyle(
                          fontSize: 20,
                          fontFamily: FontsFamily.roboto,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                      desc,
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: FontsFamily.roboto,
                          color: clr.Colors.darkGrey),
                    ),
                  ],
                ),
              ],
            ),
            Image.asset(
              '${Environment.iconAssets}direct_page_black.png',
              fit: BoxFit.fitWidth,
              width: 8,
            )
          ],
        ),
      ),
    );
  }
}
