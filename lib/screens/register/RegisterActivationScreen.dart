import 'package:device_apps/device_apps.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/UrlThirdParty.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:url_launcher/url_launcher.dart';
import 'package:pedantic/pedantic.dart';

class RegisterActivationScreen extends StatefulWidget {
  RegisterActivationScreen({Key key}) : super(key: key);

  @override
  _RegisterActivationScreenState createState() =>
      _RegisterActivationScreenState();
}

class _RegisterActivationScreenState extends State<RegisterActivationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
          margin: EdgeInsets.all(20),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              const SizedBox(
                height: 60,
              ),
              Image.asset('${Environment.imageAssets}image_verification.png',
                  height: 160, width: 160),
              const SizedBox(
                height: 20,
              ),
              Text(
                Dictionary.verifyAccount,
                style: TextStyle(
                    fontSize: 20,
                    fontFamily: FontsFamily.roboto,
                    fontWeight: FontWeight.w600),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                Dictionary.verifyAccountDesc,
                style: TextStyle(
                    fontSize: 14,
                    height: 1.5,
                    fontFamily: FontsFamily.roboto,
                    color: clr.Colors.darkGrey),
                textAlign: TextAlign.center,
              ),
              Expanded(
                  child: Align(
                alignment: FractionalOffset.bottomCenter,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    RaisedButton(
                      color: clr.Colors.darkblue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      child: Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.symmetric(vertical: 13),
                        child: Text(
                          Dictionary.seeWhatsapp,
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: FontsFamily.roboto,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                      onPressed: () async {
                        _launchWhatsApp();
                      },
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                  ],
                ),
              ))
            ],
          )),
    ));
  }

  _launchWhatsApp() async {
    bool isInstalled = await DeviceApps.isAppInstalled(UrlThirdParty.whatsapp);

    if (isInstalled) {
      unawaited(DeviceApps.openApp(UrlThirdParty.whatsapp));
    } else {
      if (await canLaunch(UrlThirdParty.urlWhatsapp)) {
        await launch(UrlThirdParty.urlWhatsapp);
      } else {
        throw 'Could not launch ${UrlThirdParty.urlWhatsapp}';
      }
    }
  }
}
