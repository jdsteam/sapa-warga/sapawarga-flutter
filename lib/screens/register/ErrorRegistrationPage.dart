import 'package:flutter/material.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';

class ErrorRegistrationPage extends StatelessWidget {
  final String errorInfo;
  final String pathImage;

  ErrorRegistrationPage({Key key, this.errorInfo, this.pathImage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(pathImage, width: 150.0, height: 150.0),
              Image.asset(
                  '${Environment.imageAssets}loading_error_registration.png',
                  width: 20.0,
                  height: 20.0),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Text(
                  errorInfo,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 14,
                    fontFamily: FontsFamily.roboto,
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
