import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/area_category_public/KabKotaCategoryListPublicBloc.dart';
import 'package:sapawarga/blocs/area_category_public/KabKotaCategoryListPublicEvent.dart';
import 'package:sapawarga/blocs/area_category_public/KabKotaCategoryListPublicState.dart';
import 'package:sapawarga/blocs/area_category_public/kecamatan_area_public/KecamatanAreaListPublicBloc.dart';
import 'package:sapawarga/blocs/area_category_public/kecamatan_area_public/KecamatanAreaListPublicEvent.dart';
import 'package:sapawarga/blocs/area_category_public/kecamatan_area_public/KecamatanAreaListPublicState.dart';
import 'package:sapawarga/blocs/area_category_public/kelurahan_area_public/KelurahanAreaListPublicBloc.dart';
import 'package:sapawarga/blocs/area_category_public/kelurahan_area_public/KelurahanAreaListPublicEvent.dart';
import 'package:sapawarga/blocs/area_category_public/kelurahan_area_public/KelurahanAreaListPublicState.dart';
import 'package:sapawarga/blocs/register/Bloc.dart';
import 'package:sapawarga/components/BlockCircleLoading.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/DropDownComponent.dart';
import 'package:sapawarga/components/TextFieldComponent.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/AreaModelCategory.dart';
import 'package:sapawarga/repositories/AreaRepository.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/repositories/RegisterRepository.dart';
import 'package:sapawarga/screens/login/LoginScreen.dart';
import 'package:sapawarga/screens/register/RegisterScreen.dart';
import 'package:sapawarga/utilities/Validations.dart';

class RegisterFormScreen extends StatefulWidget {
  final int roleId;

  RegisterFormScreen({Key key, @required this.roleId}) : super(key: key);

  @override
  _RegisterFormScreenState createState() => _RegisterFormScreenState();
}

class _RegisterFormScreenState extends State<RegisterFormScreen> {
  final _nameTextController = TextEditingController();
  final _phoneTextController = TextEditingController();
  final _passwordTextController = TextEditingController();
  final _confirmPasswordTextController = TextEditingController();
  final _kabupatenTextController = TextEditingController();
  final _kecamatanTextController = TextEditingController();
  final _kelurahanTextController = TextEditingController();
  final _rwTextController = TextEditingController();
  final PageController _pageController = PageController();
  final _formKey = GlobalKey<FormState>();
  double currentPage = 0;
  final authRepository = AuthRepository();
  RegisterBloc _registerBloc;
  KabKotaCategoryListPublicBloc _kabKotaCategoryListPublicBloc;
  KecamatanAreaListPublicBloc _kecamatanAreaListPublicBloc;
  KelurahanAreaListPublicBloc _kelurahanAreaListPublicBloc;

  bool _isLoading = false;
  bool _isKabkotaFieldEmpty = false;
  bool _isKecamatanFieldEmpty = false;
  bool _isKelurahanFieldEmpty = false;
  bool _isRwFieldEmpty = false;
  final List<Item> _listKecamatanArea = [];
  final List<Item> _listKabupatenKotaArea = [];
  final List<Item> _listKelurahanArea = [];
  final List<Item> _listRW = [];

  @override
  void initState() {
    _pageController.addListener(() {
      setState(() {
        currentPage = _pageController.page;
      });
    });
    super.initState();
  }

  addDataRW() {
    _listRW.clear();
    _rwTextController.text = '';
    for (int i = 0; i < 50; i++) {
      _listRW.add(Item(
          id: i + 1,
          name: (i + 1).toString().length > 1
              ? '0' + (i + 1).toString()
              : '00' + (i + 1).toString()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Form(
          key: _formKey,
          child: Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.white,
                title: Container(
                  padding: const EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
                  margin: const EdgeInsets.only(right: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List<Widget>.generate(
                      2,
                      (index) {
                        return currentPage == index
                            ? Container(
                                width: 65.0,
                                height: 8.0,
                                margin: const EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 7.0),
                                decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(30.0),
                                    color: clr.Colors.darkblue),
                              )
                            : Container(
                                width: 65.0,
                                height: 8.0,
                                margin: const EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 7.0),
                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  borderRadius: BorderRadius.circular(30.0),
                                  color: const Color.fromRGBO(0, 0, 0, 0.4),
                                ),
                              );
                      },
                    ),
                  ),
                ),
                elevation: 1.0,
                iconTheme: IconThemeData(
                  color: Colors.black, //change your color here
                ),
              ),
              body: MultiBlocProvider(
                  providers: [
                    BlocProvider<RegisterBloc>(
                      create: (BuildContext context) =>
                          _registerBloc = RegisterBloc(
                        registerRepository: RegisterRepository(),
                      ),
                    ),
                    BlocProvider<KabKotaCategoryListPublicBloc>(
                      create: (BuildContext context) =>
                          _kabKotaCategoryListPublicBloc =
                              KabKotaCategoryListPublicBloc(
                        AreaRepository(),
                      )..add(
                                  KabKotaCategoryListPublicLoad(),
                                ),
                    ),
                    BlocProvider<KecamatanAreaListPublicBloc>(
                      create: (BuildContext context) =>
                          _kecamatanAreaListPublicBloc =
                              KecamatanAreaListPublicBloc(
                        AreaRepository(),
                      ),
                    ),
                    BlocProvider<KelurahanAreaListPublicBloc>(
                      create: (BuildContext context) =>
                          _kelurahanAreaListPublicBloc =
                              KelurahanAreaListPublicBloc(
                        AreaRepository(),
                      ),
                    ),
                  ],
                  child: MultiBlocListener(
                    listeners: [
                      BlocListener<RegisterBloc, RegisterState>(
                        bloc: _registerBloc,
                        listener: (context, state) async {
                          if (state is RegisterLoading) {
                            _isLoading = true;
                            await blockCircleLoading(context: context);
                          } else if (state is RegisterSuccess) {
                            if (_isLoading) {
                              _isLoading = false;
                              Navigator.of(context, rootNavigator: true).pop();
                            }
                            await Navigator.of(context).pushNamed(
                                NavigationConstrants.RegisterActivation);
                            // _pageController.jumpToPage(1);
                          } else if (state is RegisterFailure) {
                            if (_isLoading) {
                              _isLoading = false;
                              Navigator.of(context, rootNavigator: true).pop();
                            }

                            await showDialog(
                                context: context,
                                builder: (BuildContext context) =>
                                    DialogTextOnly(
                                      description: state.error,
                                      buttonText: Dictionary.ok,
                                      onOkPressed: () {
                                        Navigator.of(context,
                                                rootNavigator: true)
                                            .pop(); // To close the dialog
                                      },
                                    ));
                          } else if (state is ValidationError) {
                            if (_isLoading) {
                              _isLoading = false;
                              Navigator.of(context, rootNavigator: true).pop();
                            }
                            if (state.errors.toString().contains('phone')) {
                              await showDialog(
                                context: context,
                                builder: (BuildContext context) =>
                                    DialogTextOnly(
                                  description: state.errors
                                          .toString()
                                          .contains('username')
                                      ? state.errors['username'][0].toString()
                                      : state.errors
                                              .toString()
                                              .contains('phone')
                                          ? state.errors['phone'][0].toString()
                                          : state.errors.toString(),
                                  buttonText: Dictionary.ok,
                                  onOkPressed: () {
                                    Navigator.of(context, rootNavigator: true)
                                        .pop(); // To close the dialog
                                  },
                                ),
                              );
                            } else if (state.errors
                                .toString()
                                .contains('password')) {
                              await showDialog(
                                context: context,
                                builder: (BuildContext context) =>
                                    DialogTextOnly(
                                  description:
                                      state.errors['password'][0].toString(),
                                  buttonText: Dictionary.ok,
                                  onOkPressed: () {
                                    Navigator.of(context, rootNavigator: true)
                                        .pop(); // To close the dialog
                                  },
                                ),
                              );
                            } else if (_pageController.page == 0.0) {
                              _pageController.jumpToPage(1);
                            } else if (state.errors
                                .toString()
                                .contains('message')) {
                              await Navigator.of(context).pushNamed(
                                NavigationConstrants.RegisterErrorPage,
                                arguments: [
                                  state.errors['message'].toString(),
                                  '${Environment.imageAssets}image_error_registration_info.png',
                                ],
                              );
                            } else {
                              await showDialog(
                                  context: context,
                                  builder: (BuildContext context) =>
                                      DialogTextOnly(
                                        description: state.errors
                                                .toString()
                                                .contains('kabkota_id')
                                            ? state.errors['kabkota_id'][0]
                                                .toString()
                                            : state.errors
                                                    .toString()
                                                    .contains('kec_id')
                                                ? state.errors['kec_id'][0]
                                                    .toString()
                                                : state.errors
                                                        .toString()
                                                        .contains('kel_id')
                                                    ? state.errors['kel_id'][0]
                                                        .toString()
                                                    : state.errors
                                                            .toString()
                                                            .contains('rw')
                                                        ? state.errors['rw'][0]
                                                            .toString()
                                                        : state.errors
                                                            .toString(),
                                        buttonText: Dictionary.ok,
                                        onOkPressed: () {
                                          Navigator.of(context,
                                                  rootNavigator: true)
                                              .pop(); // To close the dialog
                                        },
                                      ));
                            }
                          } else {
                            if (_isLoading) {
                              _isLoading = false;
                              Navigator.of(context, rootNavigator: true).pop();
                            }
                          }
                        },
                      ),
                      BlocListener<KabKotaCategoryListPublicBloc,
                          KabKotaCategoryListPublicState>(
                        bloc: _kabKotaCategoryListPublicBloc,
                        listener: (context, state) {
                          if (state is KabKotaCategoryListPublicLoaded) {
                            setState(() {
                              _listKabupatenKotaArea
                                  .addAll(state.records.data.items);
                            });
                          } else if (state
                              is KabKotaCategoryListPublicLoading) {
                            setState(() {
                              _isKabkotaFieldEmpty = false;
                              _isKecamatanFieldEmpty = false;
                              _isKelurahanFieldEmpty = false;
                              _isRwFieldEmpty = false;
                              _listKabupatenKotaArea.clear();
                              _listKecamatanArea.clear();
                              _listKelurahanArea.clear();
                              _listRW.clear();
                              _kecamatanTextController.text = '';
                              _kelurahanTextController.text = '';
                              _rwTextController.text = '';
                            });
                          }
                        },
                      ),
                      BlocListener<KecamatanAreaListPublicBloc,
                          KecamatanAreaListPublicState>(
                        bloc: _kecamatanAreaListPublicBloc,
                        listener: (context, state) {
                          if (state is KecamatanAreaListPublicLoading) {
                            setState(() {
                              _isKecamatanFieldEmpty = false;
                              _isKelurahanFieldEmpty = false;
                              _isRwFieldEmpty = false;
                              _listKecamatanArea.clear();
                              _listKelurahanArea.clear();
                              _listRW.clear();
                              _kecamatanTextController.text = '';
                              _kelurahanTextController.text = '';
                              _rwTextController.text = '';
                            });
                          } else if (state is KecamatanAreaListPublicLoaded) {
                            setState(() {
                              _listKecamatanArea
                                  .addAll(state.records.data.items);
                            });
                          }
                        },
                      ),
                      BlocListener<KelurahanAreaListPublicBloc,
                          KelurahanAreaListPublicState>(
                        bloc: _kelurahanAreaListPublicBloc,
                        listener: (context, state) {
                          if (state is KelurahanAreaListPublicLoading) {
                            setState(() {
                              _isKelurahanFieldEmpty = false;
                              _isRwFieldEmpty = false;
                              _listKelurahanArea.clear();
                              _listRW.clear();
                              _kelurahanTextController.text = '';
                              _rwTextController.text = '';
                            });
                          } else if (state is KelurahanAreaListPublicLoaded) {
                            setState(() {
                              _listKelurahanArea
                                  .addAll(state.records.data.items);
                            });
                          }
                        },
                      ),
                    ],
                    child: BlocBuilder<RegisterBloc, RegisterState>(
                      bloc: _registerBloc,
                      builder: (context, state) => PageView(
                        controller: _pageController,
                        physics: const NeverScrollableScrollPhysics(),
                        children: [
                          Container(
                            child: ListView(
                              padding: const EdgeInsets.all(20),
                              children: [
                                Text(
                                  widget.roleId == RegisterScreen().roleCodeRW
                                      ? Dictionary.registerAccount
                                      : Dictionary.registePublic,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: FontsFamily.roboto),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                TextFieldComponent(
                                    title: Dictionary.fullName,
                                    hintText: Dictionary.addName,
                                    controller: _nameTextController,
                                    validation: Validations.nameValidation,
                                    isEdit: true),
                                const SizedBox(
                                  height: 20,
                                ),
                                TextFieldComponent(
                                    textInputType: TextInputType.number,
                                    title: Dictionary.phoneRegister,
                                    hintText: Dictionary.hintPhoneRegister,
                                    controller: _phoneTextController,
                                    validation: Validations.handPhoneValidation,
                                    isEdit: true),
                                const SizedBox(
                                  height: 20,
                                ),
                                TextFieldComponent(
                                    title: Dictionary.labelPassword,
                                    hintText: Dictionary.inputPasswordRegister,
                                    controller: _passwordTextController,
                                    validation: (val) =>
                                        Validations.passwordValidation(
                                            val, false),
                                    limitCharacter: 16,
                                    isPassword: true,
                                    isEdit: true),
                                const SizedBox(
                                  height: 10,
                                ),
                                TextFieldComponent(
                                    title: Dictionary.repeatPasswordRegister,
                                    hintText:
                                        Dictionary.repeatPasswordRegisterHint,
                                    controller: _confirmPasswordTextController,
                                    isPassword: true,
                                    limitCharacter: 16,
                                    validation: (val) =>
                                        Validations.repeatPasswordValidation(
                                            _passwordTextController.text, val),
                                    isEdit: true),
                                const SizedBox(
                                  height: 10,
                                ),
                                RaisedButton(
                                    color: clr.Colors.darkblue,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    child: Container(
                                      alignment: Alignment.center,
                                      width: MediaQuery.of(context).size.width,
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 13),
                                      child: Text(
                                        Dictionary.saveRegister,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: FontsFamily.roboto),
                                      ),
                                    ),
                                    onPressed: () {
                                      if (_formKey.currentState.validate()) {
                                        FocusScope.of(context).unfocus();
                                        _registerBloc.add(
                                          SendRegister(
                                              name: _nameTextController.text,
                                              password:
                                                  _passwordTextController.text,
                                              email: '',
                                              phone: _phoneTextController.text,
                                              kabkotaId: '',
                                              kecId: '',
                                              kelId: '',
                                              rw: '',
                                              role: ''),
                                        );
                                      }
                                    }),
                                const SizedBox(
                                  height: 15,
                                ),
                                RichText(
                                    text: TextSpan(children: [
                                  TextSpan(
                                    text: Dictionary.confirmHaveAccount,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: FontsFamily.roboto,
                                        color: clr.Colors.netralGrey),
                                  ),
                                  TextSpan(
                                      text: Dictionary.loginNow,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: FontsFamily.roboto,
                                          decoration: TextDecoration.underline,
                                          color: clr.Colors.darkblue),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          Navigator.pushAndRemoveUntil(
                                            context,
                                            MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  LoginScreen(
                                                      authRepository:
                                                          AuthRepository()),
                                            ),
                                            (route) => false,
                                          );
                                        })
                                ]))
                              ],
                            ),
                          ),
                          Container(
                            child: ListView(
                              padding: const EdgeInsets.all(20),
                              children: [
                                Text(
                                  widget.roleId == RegisterScreen().roleCodeRW
                                      ? Dictionary.registerAccount
                                      : Dictionary.registePublic,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: FontsFamily.roboto),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                _listKabupatenKotaArea.isNotEmpty
                                    ? DropDownComponent(
                                        title: Dictionary.kabkota,
                                        hintText: Dictionary.chooseKabkota,
                                        items: _listKabupatenKotaArea,
                                        controller: _kabupatenTextController,
                                        isEmpty: _isKabkotaFieldEmpty,
                                        onChanged: () {
                                          _kecamatanAreaListPublicBloc.add(
                                            KecamatanAreaListPublicLoad(
                                                id: _kabupatenTextController
                                                    .text),
                                          );
                                        },
                                      )
                                    : DropDownComponent(
                                        title: Dictionary.kabkota,
                                        hintText: Dictionary.chooseKabkota,
                                        items: _listKabupatenKotaArea,
                                        controller: _kabupatenTextController,
                                        isEmpty: false,
                                      ),
                                const SizedBox(
                                  height: 20,
                                ),
                                _listKecamatanArea.isNotEmpty
                                    ? DropDownComponent(
                                        title: Dictionary.subDistrict,
                                        hintText: Dictionary.chooseKecamatan,
                                        items: _listKecamatanArea,
                                        controller: _kecamatanTextController,
                                        isEmpty: _isKecamatanFieldEmpty,
                                        onChanged: () {
                                          _kelurahanAreaListPublicBloc.add(
                                            KelurahanAreaListPublicLoad(
                                                id: _kecamatanTextController
                                                    .text),
                                          );
                                        },
                                      )
                                    : DropDownComponent(
                                        title: Dictionary.subDistrict,
                                        hintText: Dictionary.chooseKecamatan,
                                        items: _listKecamatanArea,
                                        controller: _kecamatanTextController,
                                        isEmpty: false,
                                      ),
                                const SizedBox(
                                  height: 20,
                                ),
                                _listKelurahanArea.isNotEmpty
                                    ? DropDownComponent(
                                        title: Dictionary.kelurahan,
                                        hintText: Dictionary.chooseKelurahan,
                                        items: _listKelurahanArea,
                                        controller: _kelurahanTextController,
                                        isEmpty: _isKelurahanFieldEmpty,
                                        onChanged: () {
                                          setState(() {
                                            addDataRW();
                                          });
                                        },
                                      )
                                    : DropDownComponent(
                                        title: Dictionary.kelurahan,
                                        hintText: Dictionary.chooseKelurahan,
                                        items: _listKelurahanArea,
                                        controller: _kelurahanTextController,
                                        isEmpty: false,
                                      ),
                                const SizedBox(
                                  height: 20,
                                ),
                                _listRW.isNotEmpty
                                    ? DropDownComponent(
                                        title: Dictionary.rw,
                                        hintText: Dictionary.chooseRw,
                                        items: _listRW,
                                        controller: _rwTextController,
                                        isEmpty: _isRwFieldEmpty,
                                      )
                                    : DropDownComponent(
                                        title: Dictionary.rw,
                                        hintText: Dictionary.chooseRw,
                                        controller: _rwTextController,
                                        isEmpty: false,
                                      ),
                                const SizedBox(
                                  height: 20,
                                ),
                                RaisedButton(
                                  color: clr.Colors.darkblue,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  child: Container(
                                    alignment: Alignment.center,
                                    width: MediaQuery.of(context).size.width,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 13),
                                    child: Text(
                                      Dictionary.save,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: FontsFamily.roboto),
                                    ),
                                  ),
                                  onPressed: () async {
                                    checkEmptyField();
                                    if (_formKey.currentState.validate()) {
                                      if (!_isKabkotaFieldEmpty &&
                                          !_isKecamatanFieldEmpty &&
                                          !_isKelurahanFieldEmpty &&
                                          !_isRwFieldEmpty) {
                                        FocusScope.of(context).unfocus();
                                        _registerBloc.add(
                                          SendRegister(
                                            name: _nameTextController.text,
                                            password:
                                                _passwordTextController.text,
                                            email: '',
                                            phone: _phoneTextController.text,
                                            kabkotaId:
                                                _kabupatenTextController.text,
                                            kecId:
                                                _kecamatanTextController.text,
                                            kelId:
                                                _kelurahanTextController.text,
                                            rw: _rwTextController.text,
                                            role: widget.roleId.toString(),
                                          ),
                                        );
                                      }
                                    }
                                  },
                                ),
                                const SizedBox(
                                  height: 15,
                                ),
                                RichText(
                                    text: TextSpan(children: [
                                  TextSpan(
                                    text: Dictionary.confirmHaveAccount,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: FontsFamily.roboto,
                                        color: clr.Colors.netralGrey),
                                  ),
                                  TextSpan(
                                      text: Dictionary.loginNow,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: FontsFamily.roboto,
                                          decoration: TextDecoration.underline,
                                          color: clr.Colors.darkblue),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          Navigator.pushAndRemoveUntil(
                                            context,
                                            MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  LoginScreen(
                                                authRepository:
                                                    AuthRepository(),
                                              ),
                                            ),
                                            (route) => false,
                                          );
                                          // Navigator.of(context, rootNavigator: true).pop();
                                        })
                                ]))
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ))),
        ),
        onWillPop: _onWillPop);
  }

  Future<bool> _onWillPop() async {
    if (currentPage == 1.0) {
      _pageController.jumpToPage(0);
    } else {
      Navigator.pop(context, false);
    }

    return false;
  }

  checkEmptyField() {
    // Check empty field
    if (_kabupatenTextController.text == '') {
      setState(() {
        _isKabkotaFieldEmpty = true;
      });
    } else {
      setState(() {
        _isKabkotaFieldEmpty = false;
      });
    }
    if (_kecamatanTextController.text == '') {
      setState(() {
        _isKecamatanFieldEmpty = true;
      });
    } else {
      setState(() {
        _isKecamatanFieldEmpty = false;
      });
    }
    if (_kelurahanTextController.text == '') {
      setState(() {
        _isKelurahanFieldEmpty = true;
      });
    } else {
      setState(() {
        _isKelurahanFieldEmpty = false;
      });
    }
    if (_rwTextController.text == '') {
      setState(() {
        _isRwFieldEmpty = true;
      });
    } else {
      setState(() {
        _isRwFieldEmpty = false;
      });
    }
  }
}
