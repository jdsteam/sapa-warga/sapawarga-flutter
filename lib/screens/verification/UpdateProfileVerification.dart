import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/account_profile/Bloc.dart';
import 'package:sapawarga/blocs/educations_list/Bloc.dart';
import 'package:sapawarga/blocs/jobs_list/Bloc.dart';
import 'package:sapawarga/components/BlockCircleLoading.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/DialogWithImage.dart';
import 'package:sapawarga/components/DropDownComponent.dart';
import 'package:sapawarga/components/TextFieldComponent.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/EducationRepository.dart';
import 'package:sapawarga/repositories/JobRepository.dart';
import 'package:sapawarga/utilities/ImagePickerHelper.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:sapawarga/utilities/Validations.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';

class UpdateProfileVerification extends StatefulWidget {
  final UserInfoModel userInfoModel;

  UpdateProfileVerification({Key key, this.userInfoModel}) : super(key: key);

  @override
  _UpdateProfileVerificationState createState() =>
      _UpdateProfileVerificationState();
}

class _UpdateProfileVerificationState extends State<UpdateProfileVerification> {
  final EducationRepository educationRepository = EducationRepository();
  final JobRepository jobRepository = JobRepository();

  final _fullNameTextController = TextEditingController();
  final _fullAddressTextController = TextEditingController();
  final _userNameTextController = TextEditingController();
  final _nikTextController = TextEditingController();
  final RefreshController _mainRefreshController = RefreshController();

  final String _format = 'dd-MMMM-yyyy';
  String minDate = '1900-01-01';
  final DateTimePickerLocale _locale = DateTimePickerLocale.id;

  final _educationTextController = TextEditingController();
  final _jobTextController = TextEditingController();
  final _birthDayController = TextEditingController();

  bool _isEducationFieldEmpty = false;
  bool _isJobFieldEmpty = false;
  bool _isBirthdayEmpty = false;
  bool _isSKFileEmpty = false;

  final _formKey = GlobalKey<FormState>();
  EducationsListBloc _educationsListBloc;
  JoblistBloc _joblistBloc;
  AccountProfileEditBloc _blocProfile;
  File skFile;
  bool _isLoading = false;
  ImagePickerHelper _imagePickerHelper;

  @override
  void initState() {
    _imagePickerHelper = ImagePickerHelper(context);
    _imagePickerHelper.streamController.stream.listen((event) {
      setState(() {
        _isSKFileEmpty = false;
        skFile = event;
      });
    });

    _fullNameTextController.text = widget.userInfoModel.name ?? '';
    _fullAddressTextController.text = widget.userInfoModel.address ?? '';
    _userNameTextController.text = widget.userInfoModel.username ?? '';
    _nikTextController.text = widget.userInfoModel.nik ?? '';
    _birthDayController.text = widget.userInfoModel.birthDate?.toString();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerDown: (_) {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus &&
            currentFocus.focusedChild != null) {
          currentFocus.focusedChild.unfocus();
        }
      },
      child: Form(
        key: _formKey,
        child: MultiBlocProvider(
          providers: [
            BlocProvider<EducationsListBloc>(
              create: (context) => _educationsListBloc =
                  EducationsListBloc(educationRepository: educationRepository)
                    ..add(
                      EducationsLoad(),
                    ),
            ),
            BlocProvider<JoblistBloc>(
              create: (context) =>
                  _joblistBloc = JoblistBloc(jobRepository: jobRepository)
                    ..add(
                      JobsLoad(),
                    ),
            ),
            BlocProvider<AccountProfileEditBloc>(
              create: (context) => _blocProfile = AccountProfileEditBloc(
                authProfileRepository: AuthProfileRepository(),
              ),
            ),
          ],
          child: MultiBlocListener(
            listeners: [
              BlocListener<AccountProfileEditBloc, AccountProfileEditState>(
                  bloc: _blocProfile,
                  listener: (context, state) async {
                    if (state is AccountProfileEditUpdated) {
                      if (_isLoading) {
                        _isLoading = false;
                        Navigator.of(context, rootNavigator: true).pop();
                      }

                      await Preferences.setHasFirstVerificationAccount(true);

                      _loadUpdateProfileSuccess();
                    } else if (state is AccountProfileEditFailure) {
                      if (_isLoading) {
                        _isLoading = false;
                        Navigator.of(context, rootNavigator: true).pop();
                      }
                      await showDialog(
                          context: context,
                          builder: (BuildContext context) => DialogTextOnly(
                                description: state.error,
                                buttonText: "OK",
                                onOkPressed: () {
                                  Navigator.of(context)
                                      .pop(); // To close the dialog
                                },
                              ));
                    } else if (state is AccountProfileEditValidationError) {
                      if (_isLoading) {
                        _isLoading = false;
                        Navigator.of(context, rootNavigator: true).pop();
                      }
                      await showDialog(
                          context: context,
                          builder: (BuildContext context) => DialogTextOnly(
                                description:
                                    state.errors.toString().contains('username')
                                        ? state.errors['username'][0].toString()
                                        : state.errors
                                                .toString()
                                                .contains('birth_date')
                                            ? state.errors['birth_date'][0]
                                                .toString()
                                            : state.errors.toString(),
                                buttonText: "OK",
                                onOkPressed: () {
                                  Navigator.of(context)
                                      .pop(); // To close the dialog
                                },
                              ));
                    } else if (state is AccountProfileEditLoading) {
                      _isLoading = true;
                      await blockCircleLoading(context: context);
                    } else {
                      if (_isLoading) {
                        _isLoading = false;
                        Navigator.of(context, rootNavigator: true).pop();
                      }
                    }
                  }),
              BlocListener<EducationsListBloc, EducationsListState>(
                bloc: _educationsListBloc,
                listener: (context, state) async {
                  if (state is EducationsFailure) {
                    await showDialog(
                        context: context,
                        builder: (BuildContext context) => DialogTextOnly(
                              description: state.error,
                              buttonText: "OK",
                              onOkPressed: () {
                                Navigator.of(context)
                                    .pop(); // To close the dialog
                              },
                            ));
                  }
                },
              ),
            ],
            child: BlocBuilder(
              bloc: _blocProfile,
              builder: (BuildContext context, AccountProfileEditState state) {
                return Scaffold(
                  appBar: AppBar(
                    title: Text(
                      Dictionary.profile,
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                          color: Colors.black,
                          fontFamily: FontsFamily.roboto),
                    ),
                    backgroundColor: Colors.white,
                    elevation: 1.0,
                    iconTheme: IconThemeData(
                      color: Colors.black, //change your color here
                    ),
                  ),
                  body: SmartRefresher(
                    controller: _mainRefreshController,
                    enablePullDown: true,
                    header: WaterDropMaterialHeader(),
                    onRefresh: () async {
                      _educationsListBloc.add(EducationsLoad());
                      _joblistBloc.add(JobsLoad());
                      _mainRefreshController.refreshCompleted();
                    },
                    child: SingleChildScrollView(
                      child: Container(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          children: [
                            TextFieldComponent(
                                title: Dictionary.fullName,
                                hintText: Dictionary.hintFullName,
                                controller: _fullNameTextController,
                                validation: Validations.nameValidation,
                                textInputType: TextInputType.text,
                                isEdit: true),
                            const SizedBox(
                              height: 20,
                            ),
                            TextFieldComponent(
                                title: Dictionary.address,
                                hintText: Dictionary.placeHolderAddress,
                                controller: _fullAddressTextController,
                                validation: Validations.addressValidation,
                                textInputType: TextInputType.text,
                                isEdit: true),
                            const SizedBox(
                              height: 20,
                            ),
                            TextFieldComponent(
                              title: Dictionary.nik,
                              hintText: Dictionary.hintNik,
                              controller: _nikTextController,
                              validation: Validations.nikValidation,
                              textInputType: TextInputType.number,
                              isEdit: true,
                              limitCharacter: 16,
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            buildDateField(
                                title: Dictionary.birthDate,
                                placeholder: _birthDayController.text == ''
                                    ? Dictionary.birthdayPlaceholder
                                    : DateFormat.yMMMMd('id').format(
                                        DateTime.parse(_birthDayController.text
                                            .substring(0, 10))),
                                isEmpty: _isBirthdayEmpty),
                            const SizedBox(
                              height: 20,
                            ),
                            BlocBuilder<EducationsListBloc,
                                EducationsListState>(
                              bloc: _educationsListBloc,
                              builder: (context, state) {
                                return state is EducationsLoaded
                                    ? DropDownComponent(
                                        title: Dictionary.education,
                                        hintText: Dictionary.lastEducation,
                                        items: state.record,
                                        controller: _educationTextController,
                                        isEmpty: _isEducationFieldEmpty,
                                        onChanged: () {
                                          setState(() {
                                            _isEducationFieldEmpty = false;
                                          });
                                        },
                                      )
                                    : DropDownComponent(
                                        title: Dictionary.education,
                                        hintText: Dictionary.lastEducation,
                                        controller: _educationTextController,
                                        isEmpty: false,
                                      );
                              },
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            BlocBuilder<JoblistBloc, JoblistState>(
                              bloc: _joblistBloc,
                              builder: (context, state) {
                                return state is JobsLoaded
                                    ? DropDownComponent(
                                        title: Dictionary.job,
                                        hintText: Dictionary.currentJob,
                                        items: state.record,
                                        controller: _jobTextController,
                                        isEmpty: _isJobFieldEmpty,
                                        onChanged: () {
                                          setState(() {
                                            _isJobFieldEmpty = false;
                                          });
                                        },
                                      )
                                    : DropDownComponent(
                                        title: Dictionary.job,
                                        hintText: Dictionary.currentJob,
                                        controller: _jobTextController,
                                        isEmpty: false,
                                      );
                              },
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            TextFieldComponent(
                                title: Dictionary.username,
                                hintText: Dictionary.hintUsername,
                                controller: _userNameTextController,
                                validation: Validations.usernameValidation,
                                isEdit: true),
                            const SizedBox(
                              height: 20,
                            ),
                            widget.userInfoModel.roleLabel == 'RW'
                                ? Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        Dictionary.infoSK,
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            color: clr.Colors.veryDarkGrey,
                                            fontFamily: FontsFamily.roboto,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            Dictionary.uploadSK,
                                            style: TextStyle(
                                                fontSize: 12.0,
                                                color: clr.Colors.veryDarkGrey,
                                                fontFamily: FontsFamily.roboto,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            Dictionary.requiredForm,
                                            style: TextStyle(
                                                fontSize: 10.0,
                                                color: clr.Colors.darkblue,
                                                fontFamily: FontsFamily.roboto,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      InkWell(
                                        child: skFile == null
                                            ? Row(
                                                children: [
                                                  Expanded(
                                                    child: Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 18,
                                                              bottom: 18,
                                                              left: 10,
                                                              right: 10),
                                                      decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        shape:
                                                            BoxShape.rectangle,
                                                        border: Border.all(
                                                            color: _isSKFileEmpty
                                                                ? Colors.red
                                                                : Colors
                                                                    .grey[400],
                                                            width: 1.5),
                                                        borderRadius:
                                                            BorderRadius.only(
                                                          topLeft:
                                                              Radius.circular(
                                                                  8.0),
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  8.0),
                                                        ),
                                                      ),
                                                      child: Text(
                                                        Dictionary.chooseFoto,
                                                        style: TextStyle(
                                                            color: clr.Colors
                                                                .netralGrey,
                                                            fontFamily:
                                                                FontsFamily
                                                                    .roboto,
                                                            fontSize: 12),
                                                      ),
                                                    ),
                                                  ),
                                                  Container(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 18,
                                                            bottom: 18,
                                                            left: 15,
                                                            right: 15),
                                                    decoration: BoxDecoration(
                                                      color: Colors.grey[100],
                                                      shape: BoxShape.rectangle,
                                                      border: Border.all(
                                                          color: _isSKFileEmpty
                                                              ? Colors.red
                                                              : Colors
                                                                  .grey[400],
                                                          width: 1.5),
                                                      borderRadius:
                                                          BorderRadius.only(
                                                        topRight:
                                                            Radius.circular(
                                                                8.0),
                                                        bottomRight:
                                                            Radius.circular(
                                                                8.0),
                                                      ),
                                                    ),
                                                    child: Text(
                                                      Dictionary.search,
                                                      style: TextStyle(
                                                          color: clr.Colors
                                                              .netralGrey,
                                                          fontFamily:
                                                              FontsFamily
                                                                  .roboto,
                                                          fontSize: 12),
                                                    ),
                                                  ),
                                                ],
                                              )
                                            : Container(
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                padding: const EdgeInsets.only(
                                                    top: 18,
                                                    bottom: 18,
                                                    left: 10,
                                                    right: 10),
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  shape: BoxShape.rectangle,
                                                  border: Border.all(
                                                      color: _isSKFileEmpty
                                                          ? Colors.red
                                                          : Colors.grey[400],
                                                      width: 1.5),
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                ),
                                                child: Row(
                                                  children: [
                                                    Container(
                                                      child: Image.file(
                                                        skFile,
                                                        width: 120,
                                                        height: 120,
                                                      ),
                                                    ),
                                                    const SizedBox(width: 10),
                                                    Expanded(
                                                      child: Text(
                                                        skFile.path
                                                            .split('/')
                                                            .last
                                                            .toString(),
                                                        style: TextStyle(
                                                            fontSize: 14.0,
                                                            color: clr.Colors
                                                                .veryDarkGrey,
                                                            fontFamily:
                                                                FontsFamily
                                                                    .roboto,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                    )
                                                  ],
                                                )),
                                        onTap: () {
                                          FocusScope.of(context).unfocus();
                                          _imagePickerHelper.openDialog();
                                        },
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      _isSKFileEmpty
                                          ? Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 15, bottom: 10),
                                              child: Text(
                                                Dictionary.foto +
                                                    Dictionary
                                                        .pleaseCompleteAllField,
                                                style: TextStyle(
                                                    color: Colors.red,
                                                    fontSize: 12),
                                              ),
                                            )
                                          : Container(),
                                      Text(
                                        Dictionary.infoSizeFile,
                                        style: TextStyle(
                                            fontSize: 14.0,
                                            color: clr.Colors.netralGrey,
                                            fontFamily: FontsFamily.roboto),
                                      ),
                                      const SizedBox(
                                        height: 20,
                                      ),
                                    ],
                                  )
                                : Container(),
                            RaisedButton(
                              color: clr.Colors.darkblue,
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width,
                                padding:
                                    const EdgeInsets.symmetric(vertical: 13),
                                child: Text(
                                  Dictionary.save,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: FontsFamily.roboto,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              onPressed: () {
                                checkEmptyField();
                                if (_formKey.currentState.validate()) {
                                  if (!_isJobFieldEmpty &&
                                      !_isEducationFieldEmpty &&
                                      !_isBirthdayEmpty &&
                                      !_isSKFileEmpty) {
                                    FocusScope.of(context).unfocus();
                                    _blocProfile.add(
                                      AccountProfileEditSubmit(
                                        userInfoModel: UserInfoModel(
                                          name: _fullNameTextController.text,
                                          username:
                                              _userNameTextController.text,
                                          educationLevelId:
                                              _educationTextController.text !=
                                                      'null'
                                                  ? int.parse(
                                                      _educationTextController
                                                          .text)
                                                  : null,
                                          jobTypeId:
                                              _jobTextController.text != 'null'
                                                  ? int.parse(
                                                      _jobTextController.text)
                                                  : null,
                                          email: null,
                                          address:
                                              _fullAddressTextController.text,
                                          lat: null,
                                          lon: null,
                                          phone: widget.userInfoModel.phone,
                                          rt: null,
                                          facebook: null,
                                          instagram: null,
                                          twitter: null,
                                          nik: _nikTextController.text,
                                          birthDate: DateTime.parse(
                                              _birthDayController.text),
                                        ),
                                        skFIle: skFile,
                                      ),
                                    );
                                  }
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  // Function to build Date Picker
  void _showDatePickerBirthday() {
    DatePicker.showDatePicker(
      context,
      pickerTheme: DateTimePickerTheme(
        showTitle: true,
        confirm: Text(Dictionary.save, style: TextStyle(color: Colors.red)),
        cancel: Text(Dictionary.cancel, style: TextStyle(color: Colors.cyan)),
      ),
      minDateTime: DateTime.parse(minDate),
      maxDateTime: DateTime.now(),
      initialDateTime: _birthDayController.text == ''
          ? DateTime.now()
          : DateTime.parse(_birthDayController.text),
      dateFormat: _format,
      locale: _locale,
      onClose: () {
        setState(() {
          _birthDayController.text = _birthDayController.text;
        });
      },
      onCancel: () {
        setState(() {
          _birthDayController.text = _birthDayController.text;
        });
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          _isBirthdayEmpty = false;
          _birthDayController.text = dateTime.toString();
        });
      },
    );
  }

  // Funtion to build date field
  Widget buildDateField({String title, placeholder, bool isEmpty}) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: [
              Text(
                title,
                style: TextStyle(
                    fontSize: 12.0,
                    color: clr.Colors.veryDarkGrey,
                    fontFamily: FontsFamily.roboto,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                Dictionary.requiredForm,
                style: TextStyle(
                    fontSize: 10.0,
                    color: clr.Colors.darkblue,
                    fontFamily: FontsFamily.roboto,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          InkWell(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
              _showDatePickerBirthday();
            },
            child: Container(
              height: 60,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  color: Colors.grey[100],
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(
                      color: isEmpty ? Colors.red : Colors.grey[400],
                      width: 1.5)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      placeholder,
                      style: TextStyle(
                          fontSize: 12,
                          fontFamily: FontsFamily.roboto,
                          color: placeholder == Dictionary.birthdayPlaceholder
                              ? clr.Colors.netralGrey
                              : Colors.black),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Container(
                        height: 15,
                        child: Image.asset(
                            '${Environment.iconAssets}calendar.png')),
                  )
                ],
              ),
            ),
          ),
          isEmpty
              ? const SizedBox(
                  height: 10,
                )
              : Container(),
          isEmpty
              ? Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Text(
                    title + Dictionary.pleaseCompleteAllField,
                    style: TextStyle(color: Colors.red, fontSize: 12),
                  ),
                )
              : Container()
        ],
      ),
    );
  }

  checkEmptyField() {
    // Check empty field
    if (_birthDayController.text == '') {
      setState(() {
        _isBirthdayEmpty = true;
      });
    } else {
      setState(() {
        _isBirthdayEmpty = false;
      });
    }
    if (_educationTextController.text == '') {
      setState(() {
        _isEducationFieldEmpty = true;
      });
    } else {
      setState(() {
        _isEducationFieldEmpty = false;
      });
    }
    if (_jobTextController.text == '') {
      setState(() {
        _isJobFieldEmpty = true;
      });
    } else {
      setState(() {
        _isJobFieldEmpty = false;
      });
    }
    if (skFile == null) {
      setState(() {
        _isSKFileEmpty = true;
      });
    } else {
      setState(() {
        _isSKFileEmpty = false;
      });
    }
  }

  _loadUpdateProfileSuccess() async {
    await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => DialogWithImage(
        title: Dictionary.profileSuccessUpdated,
        description: '',
        locationImage: '${Environment.imageAssets}activation_image.png',
        heightImage: 150,
        buttonName: Dictionary.ok,
        onNextPressed: () async {
          Navigator.of(context).pop();
          Navigator.pop(context, true);
          // Navigator.of(context).popUntil((route) => route.isFirst);
        },
      ),
    );
  }

  @override
  void dispose() {
    _fullNameTextController.dispose();
    _fullAddressTextController.dispose();
    _userNameTextController.dispose();
    _nikTextController.dispose();
    _educationTextController.dispose();
    _jobTextController.dispose();
    _birthDayController.dispose();
    _imagePickerHelper.streamController.close();
    _educationsListBloc.close();
    _joblistBloc.close();
    _blocProfile.close();

    super.dispose();
  }
}
