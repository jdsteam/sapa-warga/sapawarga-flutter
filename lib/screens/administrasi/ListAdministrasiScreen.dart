import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/remote_home/Bloc.dart';
import 'package:sapawarga/blocs/remote_home/RemoteHomeBloc.dart';
import 'package:sapawarga/components/BrowserScreen.dart';
import 'package:sapawarga/components/CustomAppBar.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/OpenChromeSapariBrowser.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/AdministrasiModel.dart';
import 'package:sapawarga/repositories/FirebaseRepository.dart';
import 'package:sapawarga/screens/administrasi/DetailAdministrasiScreen.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:url_launcher/url_launcher.dart';

class ListAdministrasiScreen extends StatefulWidget {
  ListAdministrasiScreen({Key key}) : super(key: key);

  @override
  _ListAdministrasiScreenState createState() => _ListAdministrasiScreenState();
}

class _ListAdministrasiScreenState extends State<ListAdministrasiScreen> {
  bool statConnect;
  final TextEditingController _searchController = TextEditingController();
  Timer _debounce;
  String searchQuery;
  RemoteHomeBloc _remoteHomeBloc;
  final RefreshController _mainRefreshController = RefreshController();

  @override
  void initState() {
    AnalyticsHelper.setLogEvent(Analytics.ADMINISTRATION);

    _searchController.addListener((() {
      _onSearchChanged();
    }));

    checkConnection().then((isConnect) {
      if (isConnect != null && isConnect) {
        setState(() {
          statConnect = true;
        });
      } else {
        setState(() {
          statConnect = false;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(Dictionary.administration,
              style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: FontsFamily.intro),
              maxLines: 1,
              overflow: TextOverflow.ellipsis),
        ),
        body: BlocProvider<RemoteHomeBloc>(
          create: (BuildContext context) => _remoteHomeBloc =
              RemoteHomeBloc(firebaseRepository: FirebaseRepository())
                ..add(RemoteHomeLoad()),
          child: BlocBuilder<RemoteHomeBloc, RemoteHomeState>(
            builder: (context, state) => SmartRefresher(
                controller: _mainRefreshController,
                enablePullDown: true,
                header: WaterDropMaterialHeader(),
                onRefresh: refresh,
                child: state is RemoteHomeLoading
                    ? _buildLoading()
                    : state is RemoteHomeLoaded
                        ? _buildContent(state.remoteConfig)
                        : Container()),
          ),
        ));
  }

  Future<void> refresh() async {
    _remoteHomeBloc.add(RemoteHomeLoad());
    _clearSearchQuery();
    _mainRefreshController.refreshCompleted();
  }

  _buildLoading() {
    return ListView(
      padding: EdgeInsets.all(10),
      children: [
        Container(
          margin: EdgeInsets.only(top: 15),
          child: Text(
            Dictionary.serviceInformation,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w600,
                fontFamily: FontsFamily.roboto,
                fontSize: 16.0),
          ),
        ),
        Container(
          height: 225,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  width: 150,
                  padding: EdgeInsets.only(right: 10, top: 10),
                  child: Column(
                    children: <Widget>[
                      Skeleton(
                        height: 140,
                        width: 150,
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Skeleton(
                        width: 150,
                        height: 30,
                      )
                    ],
                  ),
                );
              },
              itemCount: 5),
        ),
        CustomAppBar.buildSearchField(_searchController,
            Dictionary.searchAdministration, updateSearchQuery,
            margin: EdgeInsets.only(bottom: 10)),
        const SizedBox(height: 15),
        Text(
          Dictionary.residenceService,
          style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w600,
              fontFamily: FontsFamily.roboto,
              fontSize: 16.0),
        ),
        const SizedBox(height: 15),
        GridView.count(
          physics: NeverScrollableScrollPhysics(),
          childAspectRatio: (2 / 0.8),
          shrinkWrap: true,
          crossAxisCount: 2,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          children: List.generate(10, (index) {
            return Card(
                elevation: 6.0,
                shadowColor: Colors.black.withOpacity(0.4),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                    side: BorderSide(width: 2, color: Colors.grey[300])),
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    children: <Widget>[
                      Skeleton(
                        width: 40,
                        height: 40,
                      ),
                      const SizedBox(width: Dimens.sizedBoxHeight),
                      Expanded(
                          child: Skeleton(
                        height: 20,
                      ))
                    ],
                  ),
                ));
          }),
        )
      ],
    );
  }

  List<AdministrasiModel> getDataAdministrasi() {
    List<AdministrasiModel> dataList = [];

    dataList.add(AdministrasiModel(
        id: 1,
        title: 'Syarat Pembuatan Kartu Keluarga',
        detail:
            '<p> Peraturan Presiden (Perpres) Nomor 96 Tahun 2018 tentang Persyaratan dan Tata Cara Pendaftaran Penduduk dan Pencatatan Sipil.</p><ol><li>Menurut Perpres, penerbitan Kartu Keluarga bagi Penduduk WNI atau Penduduk Orang Asing terdiri atas:&nbsp; <ul><li> penerbitan Kartu Keluarga baru;</li><br /> <li> penerbitan Kartu Keluarga karena perubahan data;</li> <li> dan penerbitan Kartu Keluarga karena hilang atau rusak.<br /></li></ul></li><li>Penerbitan Kartu Keluarga baru untuk penduduk WNI harus memenuhi persyaratan:&nbsp;<ul><li> buku nikah/kutipan akta perkawinan atau kutipan akta perceraian;</li><br /><li> surat keterangan pindah/surat keterangan pindah datang bagi penduduk yang pindah dalam wilayah NKRI;</li><br /><li> surat keterangan pindah luar negeri yang diterbitkan oleh Disdukcapil Kabupaten/Kota bagi WNI yang datang dari wilayah luar NKRI karena pindah;</li><br /><li> surat keterangan pengganti tanda identitas bagi penduduk rentan administrasi kependudukan; dan</li><br /><li> Petikan Keputusan Presiden tentang pewarganegaraan dan berita acara pengucapan sumpah atau pernyataan janji setia bagi penduduk WNI yang semula berkewarganegaraan asing.<br /></li></ul></li> <li>Adapun penerbitan Kartu Keluarga baru bagi Penduduk Orang Asing, harus memenuhi persyaratan:&nbsp;<ul><li> izin tinggal tetap;</li><br /><li> buku nikah/kutipan akta perkawinan atau kutipan akta perceraian atau yang disebut dengan nama lain; dan</li><br /><li> surat keterangan pindah bagi Penduduk yang pindah dalam wilayah NKRI. <br /></li></ul></li><li>Untuk penerbitan Kartu Keluarga karena perubahan data, harus memenuhi persyaratan:<ul><li> Kartu Keluarga lama; </li><br /><li> dan surat keterangan/bukti perubahan Peristiwa Kependudukan dan Peristiwa Penting.<br /></li></ul></li><li>Sedangkan penerbitan Kartu Keluarga karena hilang atau rusak bagi Penduduk WNI harus memenuhi persyaratan:&nbsp;<ul><li> surat keterangan hilang dari Kepolisian atau KK yang rusak;<li>dan KTP-el.<br /></li></ul></li><li>Untuk penerbitan Kartu Keluarga yang hilang atau rusak bagi Penduduk Orang Asing, menurut PP ini, harus memenuhi persyaratan:&nbsp;<ul><li> surat keterangan hilang dari Kepolisian atau Kartu Keluarga yang rusak;</li><br /><li> kartu izin tinggal tetap; dan</li><br /><li> KTP-el.<br /></li></ul></li><li>Syarat lain membuat Kartu Keluarga terbaru 2019 karena pengurangan anggota keluarga : <ul><li> Kartu keluarga lama</li><br /> <li> Surat kematian (bagi anggota yang meninggal)</li><br /> <li> Surat keterangan pindah (bagi yang pindah)</li></ul></li></ol>',
        name: 'kependudukan',
        image:
            '${Environment.imageAssets}administration/syarat-pembuatan-kk.png'));
    dataList.add(AdministrasiModel(
        id: 2,
        title: 'Syarat Pembuatan KTP-el',
        detail:
            '<p>Persyaratan untuk pembuatan KTP-el adalah sebagai berikut:</p><ol><li>Berusia minimal 17 tahun.</li><li>Surat pengantar dari pihak Rukun Tetangga (RT) dan Rukun Warga (RW).</li><li>Fotokopi Kartu Keluarga (KK).</li><li>Surat keterangan pindah dari kota asal, jika Anda bukan asli warga setempat.</li><li>Surat keterangan pindah dari luar negeri, dan surat ini harus diterbitkan oleh Instansi Pelaksana bagi Warga Negara Indonesia (WNI) yang datang dari luar negeri karena pindah.</li><li>Anda dapat langsung mendatangi kantor Kelurahan untuk difoto oleh petugas dan melakukan perekaman sidik jari.<br /></li></ol>',
        name: 'kependudukan',
        image:
            '${Environment.imageAssets}administration/syarat-pembuatan-ktp-el.png'));
    dataList.add(AdministrasiModel(
        id: 3,
        title: 'Syarat Pembuatan Akta Kelahiran',
        detail:
            '<p>Syarat umum yang harus dilampirkan untuk kepengurusan pembuatan akta kelahiran: </p><ol><li>Surat keterangan kelahiran dari Dokter / Bidan / Penolong kelahiran.</li><li>Foto kopi kartu keluarga (KK) dan KTP orang tua bayi.</li><li>Foto kopi akta perkawinan / akta nikah orang tua yang dilegalisir.</li><li>Foto kopi KTP 2 orang saksi yang mengetahui kelahiran.</li><li>Surat pengantar dari Rukun Tetangga (RT) dan Rukun Warga (RW).</li></ol><p>Cara dan alur/langkah membuat akta kelahiran: </p> <ol><li> Pemohon datang ke kantor Desa / Kelurahan setempat untuk pengurusan pembuatan akta kelahiran dengan membawa syarat yang sudah lengkap.</li><br /><li> Form/ lampiran dari Desa disetujui, kemudian ke kantor Kecamatan dengan membawa berkas lengkap dari Desa dan data dari 2 (dua) saksi.</li><br /><li> Pada kantor Kecamatan proses data akan dilakukan dari pemeriksaan berkas, dan lainnya, jika sudah selesai akan diterbitkan Surat Kartu Keluarga baru dengan penambahan angota keluarga baru (NIK baru).</li><br /><li> Kemudian bawa semua berkas dari Kecamatan ke kantor Dinas Kependudukan dan Pencatatan Sipil (Dispendukcapil) Kota / Kabupaten setempat. Pemohon datang ke bagian loket pendaftaran kemudian mengisi formulir dan bermaterai yang sudah disediakan.</li><br /> <li>Proses ini selesai sampai pada pengambilan Surat Akta Kelahiran, biasanya maksimal sampai 12 hari kerja.</li><br /><li> Untuk biaya pembuatan Akta Kelahiran Gratis.</li></ol><p><strong>Pembuatan Akta Kelahiran Umum</strong></p><p>Membuat Akta Kelahiran Umum<br />Didalam lingkup pembuatan Akta Kelahiran Umum ini meliputi:</p><br /><ol><li>Anggota keluarga baru atau bayi yang baru lahir, sehingga harus dibuatkan Surat Akta Kelahiran tidak lebih dari 60 hari dari tanggal kelahiran.</li><br /><li> Membuat Akta Kelahiran untuk anak yang berumur kurang dari 1 tahun, tetapi lewat waktu pembuatan kurang dari 60 hari dari tanggal kelahiran, sehingga harus mengurus Akta keterlambatan.</li></ol><p>Untuk syarat, cara dan langkah-langkahnya ikuti panduan diatas, hanya saja untuk pengurusan Akta Kelahiran yang kurang dari 1 tahun bisa dikenakan denda, besarnya denda sesuai ketentuan kantor Dispendukcapil Kota / kabupaten setempat.</p><p>Tambahan: Jika ada yang ingin mengurus Akta Kelahiran anak tetapi Kartu Keluarga orang tua masih terpisah atau belum gabung, dapat diikutsertakan pada Kartu Keluarga Kakek/Nenek atau orang tau dari ibundanya, dengan melampirkan Kartu Keluarga dan KTP dari Kakek/Nenek serta dilengkapi syarat umumnya.</p><p><strong>Pembuatan Akta Kelahiran Pengadilan</strong></p><p>Didalam lingkup pembuatan Akta Kelahiran Pengadilan ini meliputi:<br />Seseorang yang sudah berumur lebih dari 1 tahun tetapi belum mempunyai Akta Kelahiran (Telat urus/ sudah dewasa)</p><p>Cara dan alur pembuatan:</p> <br /><ol><li> Pemohon datang ke Pangadilan Negeri setempat untuk mendapatkan surat penetepan dari Pengadilan Negeri dengan membawa syarat umum yang sudah lengkap.</li><br /><li> Mendaftar dan mengisi formulir yang disediakan.</li><br /><li> Proses di Pengadilan Negeri bisa memakan waktu 1 minggu.</li><br /><li> Bawa lampiran penetapan dari Pengadilan Negeri dan syarat umum lainnya untuk mengurus ke kantor Dispendukcapil.</li><br /><li> Proses urus selanjutnya sama dengan pembuatan Akta Kelahiran Umum maupun Dispensasi.</li><p><strong>Pembuatan Akta Kelahiran yang Hilang</strong></p><p>Jika terjadi kehilangan Surat Akta Kelahiran, kamu tidak perlu takut atau panik karena surat tersebut masih dapat dicetak ulang atau diduplikasi dengan membawa persyaratan yang lengkap ke Dispendukcapil.</p><p>Syarat yang harus dilengkapi ketika mengurus Akta Kelahiran yang hilang:</p><ol><li> Membuat surat pernyataan kehilangan / rusak dari yang bersangkutan</li><br /><li> Membuat surat keterangan kehilangan dari kepolisian setempat</li><br /><li> Foto kopi Akta Kelahiran yang hilang</li><br /><li> Foto kopi KK dan KTP</li><p>Mengurus Akta Kelahiran yang hilang bisa dilakukan oleh yang bersangkutan atau orang tua dari si kehilangan. Bawa syarat lengkapnya ke kantor Dinas Kependudukan dan Pencatatan Sipil sesuai tempat penerbitan Akta Kelahiran yang lama.</p><p>Tambahan: Untuk anak yang lahir diluar nikah bisa membuat Surat Akta Kelahiran dengan catatan membuat surat pernyataan bertandatangan diatas materai, dengan menjelaskan bahwa anak tersebut lahir diluar nikah.</p>',
        name: 'kependudukan',
        image:
            '${Environment.imageAssets}administration/syarat-pembuatan-akta-kelahiran.png'));
    dataList.add(AdministrasiModel(
        id: 4,
        title: 'Syarat Pembuatan SKCK',
        detail:
            '<p><strong>Mempersiapkan Surat Pengantar</strong></p> <p>Untuk mendapatkan surat pengantar dari Kelurahan, terlebih dahulu Anda dapat berkunjung ke ketua RT setempat agar dibuatkan surat pengantar untuk disampaikan ke ketua RW. Setelah dari RW, Anda akan mendapatkan surat pengantar ke Kelurahan/ Desa dengan keperluan tersebut.</p> <p>Untuk pengurusan surat pengantar sampai ke tingkat Kelurahan atau Desa, biasanya ada biaya administrasi untuk kas kelompok masyarakat atau juga bisa tanpa biaya apa pun (tergantung kebijakan Desa setempat). Di Kelurahan atau Balai Desa, silakan menemui petugas untuk menyerahkan surat dari Kepala Dusun/Ketua RW kepadanya. Selanjutnya, Anda akan diminta untuk mengisi dan melengkapi formulir yang dibutuhkan.</p> <p>Perlu diperhatikan, mempersiapkan surat pengantar dari Kelurahan tidaklah menjadi syarat mutlak. Di beberapa wilayah/ daerah di Indonesia, Polsek atau Polres telah meniadakan syarat membawa surat pengantar Kelurahan. Hal ini dilakukan demi kemudahan dan kenyamanan masyarakat yang tidak ingin waktunya habis hanya untuk mengurus SKCK.</p> <p><strong>Persyaratan yang Diperlukan untuk Proses Pengurusan SKCK</strong></p> <p>Setelah Anda mendapatkan surat pengantar dari Kelurahan dan rekomendasi ke Kantor Kecamatan, sebelum mengurus SKCK ke Polsek atau Polres, silakan lengkapi persyaratan ini:</p> <p>Bagi yang terdaftar sebagai Warga Negara Indonesia (WNI)</p> <ol> <li>Fotokopi Kartu Tanda Penduduk (KTP) atau Surat Izin Mengemudi (SIM).</li> <li>Fotokopi Kartu Keluarga (KK).</li> <li>Fotokopi Akta Kelahiran/Surat Kenal Lahir/Ijazah Terakhir.</li> <li>Pas foto 4x6 berlatar/background merah sebanyak 6 lembar.</li> </ol> <p>Bagi yang terdaftar sebagai Warga Negara Asing (WNA)</p> <ol> <li>Fotokopi Paspor.</li> <li>Surat Sponsor dari Perusahaan (Asli).</li> <li>Fotokopi Surat Nikah.</li> <li>Fotokopi Kartu Izin Tinggal Terbatas (Kitas) atau Kartu Izin Tinggal Tetap (Kitap).</li> <li>Pas foto 4x6 berlatar/background kuning sebanyak 6 lembar.</li></ol> <p><strong>Pengurusan SKCK di Polsek atau Polres</strong></p> <p>Setelah kelengkapan yang perlu dipersiapkan sudah terpenuhi, pengurusan SKCK dilanjutkan di Polsek (tingkat Kecamatan) atau ke Polres (tingkat Kabupaten/ Kota). </p> <p>Untuk keperluan melamar pekerjaan, kelengkapan administrasi PNS/CPNS, dan pembuatan visa atau keperluan lain yang bersifat antarnegara, Anda bisa langsung datang ke Polres (tingkat Kabupaten/Kota), dan bukan membuat SKCK di Polsek.</p> <p>Pastikan Anda datang ke Polsek atau Polres pada jam operasional pelayanan, yaitu hari kerja Senin-Jumat pukul 08.00-15.00 atau Sabtu pukul 08.00-11.00. Silakan Anda langsung menuju loket bagian SKCK untuk mendaftarkan/memasukkan berkas yang telah Anda siapkan. Nanti Anda akan diminta untuk mengisi formulir.</p> <p>Pihak Polsek akan meminta kelengkapan syarat-syarat seperti yang telah dijelaskan di atas sebagai kelengkapan rekomendasi. Karena itu, agar memudahkan dan tidak bolak-balik ke memfotokopi atau mencetak foto, lebih baik persiapkan persyaratan-persyaratan yang telah dijelaskan dalam jumlah banyak.</p> <p>Bagi anda yang mengurus SKCK baru dan belum punya rumus sidik jari, anda bisa buat dulu di Polres di bagian rekam rumus sidik jari. Untuk pengambilan sidik jari ini, biasanya ditemukan adanya pengenaan biaya sebesar Rp5.000 atau lebih (tergantung kebijakan Polsek atau Polres setempat) meskipun hal tersebut tidak tercantum dalam PP No. 60 Tahun 2016 tentang Tarif dan Jenis Penerimaan Negara Bukan Pajak (PNBP).</p> <p>Kalau mengurus SKCK di Polres, prosesnya dapat lebih cepat untuk mendapatkan sidik jari sebagai salah satu syarat dalam penerbitan SKCK. Ketika mengurus penerbitan SKCK di Polsek, pihak Polsek akan memberikan surat rekomendasi untuk pembuatan rumus sidik jari di Polres.</p> <p>Setelah proses sidik jari selesai, anda dapat mengumpulkan berkas-berkas yang telah disiapkan dan membayar uang penerbitan SKCK di loket. Anda tinggla menunggu antrian dan SKCK akan segera selesai.</p> <p><strong>Persyaratan Pembuatan SKCK Online</strong></p> <p>Pada dasarnya persyaratan yang diperlukan untuk membuat SKCK secara online adalah sama, perbedaannya hanyalah semua harus dalam bentuk file digital berupa hasil scan dokumen-dokumen seperti yang disyaratkan dalam pembuatan SKCK secara konvensional (manual).</p> <p>Meskipun demikian, dokumen-dokumen dalam bentuk hardcopy (fotokopi) nantinya juga diperlukan untuk verifikasi dalam pengambilan SKCK di Polres atau Polda setempat. Berikut adalah berkas-berkas yang diperlukan:</p> <ol> <li>Scan KTP asli/tanda pengenal lainnya.</li> <li>Scan Kartu Keluarga asli.</li> <li>Scan Akta Kelahiran atau Surat Kenal Lahir.</li> <li>Scan foto diri 4x6 dengan background merah (6 lembar/siapkan lebih untuk antisipasi).</li> <li>Scan paspor bagi warga Negara Indonesia yang akan keluar negeri dalam rangka kunjungan, sekolah, atau untuk keperluan penerbitan Visa.</li> </ol> <p>Selain hal itu ada juga ketentuan pengambilan SKCK melalui registrasi online. Jika registrasi online dilakukan sebelum pukul 08.00 waktu setempat, SKCK dapat diambil di loket pelayanan sampai dengan pukul 14.00 pada hari yang sama dengan menunjukkan ID/Kode Registrasi serta dokumen-dokumen yang telah disyaratkan.</p> <p>Satu hal lagi, untuk pendaftar melalui registrasi online akan diberikan waktu untuk pengambilan SKCK selambat-lambatnya 3 (tiga) hari. Jika melebihi waktu yang ditentukan maka pemohon harus melakukan registrasi ulang karena input akan secara otomatis dihapus setelah melewati batas waktu tersebut.</p> <p>Secara praktis, berikut tahapan cara membuat SKCK online:</p> <ol> <li>Akses website&nbsp;<a href="http://skck.polri.go.id/" target="_blank" rel="noreferrer nofollow">skck.polri.go.id</a>&nbsp;Pilih menu formulir online SKCK, klik Next Step.</li> <li>Anda akan diarahkan ke menu pengisian Data Pribadi Nama, NIK, wilayah Polres, hingga bagian paling akhir perintah upload foto.</li> <li>Selanjutnya, Anda akan mengisikan nama ayah dan ibu kandung, alamat tempat tinggal, pekerjaan, sampai nama saudara kandung.</li> <li>Tahap isian berikutnya adalah Data Pendidikan mencakup nama sekolah, kota, serta tahun lulus sekolah.</li> <li>Di tahapan ini, akan ada kotak dialog pertanyaan seperti apakah kamu pernah terlibat tindak pidana atau tidak. Isi terlebih dahulu sebelum klik Next/Selanjutnya.</li> <li>Jangan lupa untuk mengisi dengan jujur apa tujuan Anda membuat SKCK: keperluan pendidikan, pekerjaan, atau yang lainnya. Pastikan sudah diisi sebelum Klik Next.</li> <li>Tahapan akhir, setelah semua tahapan pengisian selesai. Klik Kirim Data untuk proses pembuatan SKCK oleh sistem.</li> <li>Selanjutnya, Anda akan mendapatkan kode atau nomor registrasi untuk dibawa ke loket Polsek/Polres guna ditukarkan dengan SKCK aslinya. Pastikan Anda mencatat dengan baik nomor ini.</li> </ol> <p>Catatan : saat akan melakukan pengambilan SKCK asli, Anda akan ditanyai rumus sidik jari. Untuk ini, Anda bisa mengurusnya di Polres dengan surat rekomendasi dari Polsek setempat.</p> <p>Dokumen syarat perekaman rumus sidik jari:</p> <ol> <li>Fotokopi KTP sebanyak 1 lembar.</li> <li>Foto depan ukuran 4&times;6 background merah 2 lembar.</li> <li>Foto samping kiri ukuran 4&times;6 background merah 1 lembar.</li> <li>Foto samping kanan ukuran 4&times;6 background merah 1 lembar.</li> <li>Melengkapi form sidik jari (nama, bentuk wajah, rambut, dan ciri fisik lainnya).</li> <li>Selanjutnya, jika sudah selesai semuanya, Anda tinggal membayar biaya penerbitan SKCK di loket pembayaran sebesar Rp30.000.</li> </ol> <p>Terbitnya Peraturan Pemerintah (PP) Nomor 60 Tahun 2016 tentang Tarif dan Jenis Penerimaan Negara Bukan Pajak (PNBP) mengubah sejumlah tarif/biaya yang dimasukkan ke kas Negara, termasuk biaya pembuatan SKCK. Biaya SKCK di seluruh wilayah di Indonesia yang semula Rp10.000, sejak 6 Januari 2017 naik menjadi Rp30.000.</p>',
        name: 'polres',
        image:
            '${Environment.imageAssets}administration/syarat-pembuatan-skck.png'));
    dataList.add(AdministrasiModel(
        id: 5,
        title: 'Syarat Pembuatan Surat Kematian',
        detail:
            '<p>Syarat yang harus dilengkapi ketika hendak mengurus surat keterangan kematian :</p><br /><ol><li> Surat pengantar RT / RW</li><br /><li> Surat pengantar dari Desa / Kelurahan</li><br /><li> KTP dan KK yang bersangkutan (almarhum)</li><br /><li> Foto kopi KTP 2 orang saksi</li><br /><li> Surat keterangan kematian dari Dokter, Rumah Sakit, Puskesmas.</li></ol><p>Catatan: untuk surat keterangan kematian, jika saat meninggal di Rumah Sakit maka surat keterangan kematian bisa kamu urus pada dokter / rumah sakit tersebut. Jika meninggal di rumah kamu bisa meminta surat keterangan kematian pada Puskesmas setempat untuk surat pengganti dari Dokter.</p><p>Prosedur Cara Membuat Surat Kematian :</p><br /><ol><li> Lengkapi semua syarat yang diperlukan diatas</li><br /><li> Meminta surat pengantar dari RT dan RW</li><br /><li> Surat pengantar dibawa ke Desa / Kelurahan berikut syarat lengkapnya, di Desa diminta mengisi Formulir pembuatan Surat Kematian kemudian diterbitkan Surat Kematian tersebut</li><br /><li> Kemudian berkas dari Kelurahan tersebut dibawa untuk diproses pada Kantor Kecamatan</li><br /><li> Berkas yang sudah ditanda tangani oleh pihak Kecamatan kemudian dibawa ke Kantor Dinas Kependudukan dan Catatan Sipil (Disdukcapil) Kabupaten / Kota setempat untuk diproses terkait Akta / Surat Kematian yang diterbitkan.</li></ol><p>Sampai disini proses pembuatan dan mengurus Surat Kematian telah selesai dan bisa kamu gunakan sebagai mana mestinya.</p> <p>Tambahan :</p><p><ol><li> Setiap kematian wajib dicatatkan oleh (orang tua/ Suami/ Istri/ Anak atau kuasanya/Ketua RT) kepada Dinas selambat &ndash; lambatnya 30 (tiga puluh) hari sejak tanggal kematian.</li></p><p><li> Begitu juga warga Negara Indonesia yang meninggal di luar Negeri, waktu untuk melaporkan adalah 30 (tiga puluh) hari setelah kembali ke Indonesia, dengan melampirkan syarat tambahan sebagai berikut: </li> <br /><ul><li> Sertifikat kematian dari negara dimana kematian terjadi,</li><br /> <li> Paspor almarhum/ almarhumah</li></ul></ol></p>',
        name: 'kependudukan',
        image:
            '${Environment.imageAssets}administration/syarat-pembuatan-surat-kematian.png'));

    return dataList;
  }

  _buildContent(RemoteConfig remoteConfig) {
    Map<String, dynamic> data = json.decode(
        remoteConfig.getString(FirebaseConfig.residenceServicesMenuKey));

    List<dynamic> items = data['items'];

    for (int i = 0; i < items.length; i++) {
      if (!items[i]['enabled']) {
        items.removeAt(i);
      }
    }

    if (searchQuery != null) {
      items = items
          .where((test) =>
              test['name'].toLowerCase().contains(searchQuery.toLowerCase()))
          .toList();
    }

    return statConnect == null || statConnect
        ? ListView(
            padding: EdgeInsets.all(10),
            children: [
              Container(
                margin: EdgeInsets.only(top: 15),
                child: Text(
                  Dictionary.serviceInformation,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      fontFamily: FontsFamily.roboto,
                      fontSize: 16.0),
                ),
              ),
              Container(
                height: 225,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) {
                      var dataAdministrasi = getDataAdministrasi()[index];
                      return GestureDetector(
                        onTap: () {
                          AnalyticsHelper.setLogEvent(
                              Analytics.EVENT_INFORMATION_SERVICES,
                              <String, dynamic>{
                                'title': dataAdministrasi.title,
                                'category': dataAdministrasi.name
                              });

                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      DetailAdiministrasiScreen(
                                          administrasiModel:
                                              dataAdministrasi)));
                        },
                        child: Container(
                          width: 150,
                          padding: EdgeInsets.only(right: 10, top: 10),
                          child: Column(
                            children: <Widget>[
                              Image.asset(dataAdministrasi.image,
                                  height: 140, width: 150),
                              const SizedBox(
                                height: 5,
                              ),
                              Container(
                                alignment: Alignment.center,
                                margin: EdgeInsets.only(top: 10),
                                child: Text(
                                  dataAdministrasi.title,
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontFamily: FontsFamily.roboto,
                                      fontWeight: FontWeight.w600),
                                  textAlign: TextAlign.left,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                    itemCount: getDataAdministrasi().length),
              ),
              CustomAppBar.buildSearchField(_searchController,
                  Dictionary.searchAdministration, updateSearchQuery,
                  margin: EdgeInsets.only(bottom: 10)),
              const SizedBox(height: 15),
              Text(
                Dictionary.residenceService,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontFamily: FontsFamily.roboto,
                    fontSize: 16.0),
              ),
              const SizedBox(height: 15),
              items.isNotEmpty
                  ? GridView.count(
                      physics: NeverScrollableScrollPhysics(),
                      childAspectRatio: (2 / 0.9),
                      shrinkWrap: true,
                      crossAxisCount: 2,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      children: List.generate(items.length, (index) {
                        return GestureDetector(
                            onTap: () async {
                              await AnalyticsHelper.setLogEvent(
                                  Analytics.EVENT_RESIDENCE_SERVICES,
                                  <String, dynamic>{
                                    'city': items[index]['name'],
                                    'url': items[index]['url']
                                  });

                              if (items[index]['url'].toString().isEmpty) {
                                await showDialog(
                                    context: context,
                                    builder: (BuildContext context) =>
                                        DialogTextOnly(
                                          description: Dictionary
                                              .urlAdministrationIsEmpty,
                                          buttonText: "OK",
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          onOkPressed: () {
                                            Navigator.of(context)
                                                .pop(); // To close the dialog
                                          },
                                        ));
                              } else {
                                switch (items[index]['mode']) {
                                  case 'webview':
                                    await Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) => BrowserScreen(
                                                url: items[index]['url'],
                                                useInAppWebView: false)));
                                    break;
                                  case 'inappwebview':
                                    await Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) => BrowserScreen(
                                                url: items[index]['url'],
                                                useInAppWebView: true)));
                                    break;
                                  case 'chrome':
                                    openChromeSafariBrowser(
                                        url: items[index]['url']);
                                    break;
                                  case 'redirect':
                                    _launchURL(items[index]['url']);
                                    break;
                                  case 'playstore':
                                    if (await canLaunch(items[index]['url'])) {
                                      await launch(items[index]['url']);
                                    } else {
                                      throw 'Could not launch ${items[index]['url']}';
                                    }
                                    break;
                                  default:
                                    openChromeSafariBrowser(
                                        url: items[index]['url']);
                                }
                              }
                            },
                            child: Card(
                                elevation: 6.0,
                                shadowColor: Colors.black.withOpacity(0.4),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    side: BorderSide(
                                        width: 2, color: Colors.grey[300])),
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: Row(
                                    children: <Widget>[
                                      CachedNetworkImage(
                                        width: 40,
                                        height: 40,
                                        imageUrl:
                                            items[index]['cover-image'] != null
                                                ? items[index]['cover-image']
                                                : "",
                                        fit: BoxFit.fill,
                                        placeholder: (context, url) => Center(
                                            heightFactor: 8.2,
                                            child:
                                                CupertinoActivityIndicator()),
                                        errorWidget: (context, url, error) =>
                                            Container(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height /
                                                    3.3,
                                                color: Colors.grey[200],
                                                child: Image.asset(
                                                    '${Environment.imageAssets}placeholder.png',
                                                    fit: BoxFit.fitWidth)),
                                      ),
                                      const SizedBox(
                                          width: Dimens.sizedBoxHeight),
                                      Flexible(
                                        child: Text(
                                          items[index]['name'],
                                          maxLines: 3,
                                          textAlign: TextAlign.left,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontFamily: FontsFamily.roboto,
                                              color: Colors.grey[600]),
                                        ),
                                      )
                                    ],
                                  ),
                                )));
                      }),
                    )
                  : EmptyData(
                      message: Dictionary.emptyDataAdministration,
                    )
            ],
          )
        : ErrorContent(error: Dictionary.errorConnection);
  }

  void _onSearchChanged() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      if (_searchController.text.trim().isNotEmpty) {
        setState(() {
          searchQuery = _searchController.text;
          AnalyticsHelper.setLogEvent(
              Analytics.EVENT_SEARCH_RESIDENCE_SERVICES, <String, dynamic>{
            'search': searchQuery,
          });
        });
      } else {
        _clearSearchQuery();
      }
    });
  }

  void _clearSearchQuery() {
    setState(() {
      _searchController.clear();
      updateSearchQuery(null);
    });
  }

  void updateSearchQuery(String newQuery) {
    setState(() {
      searchQuery = newQuery;
    });
  }

  // ignore: missing_return
  Future<bool> checkConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
      return false;
    }
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<bool>('statConnect', statConnect));
    properties.add(StringProperty('searchQuery', searchQuery));
  }
}
