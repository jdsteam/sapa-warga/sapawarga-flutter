import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:sapawarga/blocs/administrasi/Bloc.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/models/AdministrasiModel.dart';
import 'package:html/dom.dart' as dom;
import 'package:sapawarga/repositories/PhoneBookRepository.dart';
import 'package:sapawarga/screens/phonebook/PhoneBookDetailScreen.dart';

class DetailAdiministrasiScreen extends StatelessWidget {
  final AdministrasiModel administrasiModel;
  final PhoneBookRepository phoneBookRepository = PhoneBookRepository();

  DetailAdiministrasiScreen({Key key, @required this.administrasiModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AdministrationDetailBloc>(
      create: (context) =>
          AdministrationDetailBloc(phoneBookRepository: phoneBookRepository),
      child: DetailAdiministrasi(administrasiModel: administrasiModel),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<AdministrasiModel>(
        'administrasiModel', administrasiModel));
    properties.add(DiagnosticsProperty<PhoneBookRepository>(
        'phoneBookRepository', phoneBookRepository));
  }
}

class DetailAdiministrasi extends StatefulWidget {
  final AdministrasiModel administrasiModel;

  DetailAdiministrasi({Key key, @required this.administrasiModel})
      : super(key: key);

  @override
  _DetailAdiministrasiState createState() => _DetailAdiministrasiState();
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<AdministrasiModel>(
        'administrasiModel', administrasiModel));
  }
}

class _DetailAdiministrasiState extends State<DetailAdiministrasi> {
  AdministrationDetailBloc administrationDetailBloc;

  @override
  void initState() {
    administrationDetailBloc =
        BlocProvider.of<AdministrationDetailBloc>(context);
    administrationDetailBloc
        .add(AdministrationDetailLoad(instansi: widget.administrasiModel.name));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AdministrationDetailBloc, AdministrationDetailState>(
        bloc: administrationDetailBloc,
        builder: (context, state) {
          return Scaffold(
              appBar: AppBar(
                  title: Text(Dictionary.titleAdministration,
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: FontsFamily.intro),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis)),
              body: Container(
                  child: state is AdministrationDetailLoading
                      ? _buildLoading()
                      : state is AdministrationDetailLoaded
                          ? _buildContent(state)
                          : state is AdministrationDetailFailure
                              ? ErrorContent(error: state.error)
                              : Container()));
        });
  }

  _buildLoading() {
    return Card(
      child: Container(
        child: Column(
          children: <Widget>[
            Skeleton(
                margin: 10,
                height: 35,
                width: MediaQuery.of(context).size.width),
            Skeleton(
                margin: 10,
                height: 300,
                width: MediaQuery.of(context).size.width),
            Skeleton(
                margin: 10,
                height: 50,
                width: MediaQuery.of(context).size.width)
          ],
        ),
      ),
    );
  }

  _buildContent(AdministrationDetailLoaded state) {
    return Builder(
      builder: (context) => SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          child: Card(
            child: Container(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  Container(
                      margin: const EdgeInsets.only(bottom: 10),
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        widget.administrasiModel.title,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold),
                      )),
                  Html(
                      data: widget.administrasiModel.detail,
                      defaultTextStyle:
                          TextStyle(color: Colors.black, fontSize: 15.0),
                      customTextAlign: (dom.Node node) {
                        return TextAlign.justify;
                      }),
                  Container(
                    margin: const EdgeInsets.only(top: 10),
                    width: MediaQuery.of(context).size.width,
                    child: RaisedButton(
                      onPressed: () {
                        if (state.phoneBookModel != null) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PhoneBookDetailScreen(
                                      record: state.phoneBookModel)));
                        } else {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(
                              content:
                                  Text(Dictionary.noDataFoundAdministration),
                            ),
                          );
                        }
                      },
                      color: Colors.green,
                      child: Text(
                        Dictionary.toAdministration +
                            " " +
                            widget.administrasiModel.name,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    administrationDetailBloc.close();
    super.dispose();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<AdministrationDetailBloc>(
        'administrationDetailBloc', administrationDetailBloc));
  }
}
