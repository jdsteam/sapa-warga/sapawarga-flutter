import 'package:device_apps/device_apps.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pedantic/pedantic.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as prefix0;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/UrlThirdParty.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/EsamsatModel.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:url_launcher/url_launcher.dart';

class EsamsatListScreen extends StatefulWidget {
  EsamsatListScreen({Key key}) : super(key: key);

  @override
  _EsamsatListScreenState createState() => _EsamsatListScreenState();
}

class _EsamsatListScreenState extends State<EsamsatListScreen> {
  final List<EsamsatModel> items = [
    EsamsatModel(
        Dictionary.payViaSambara,
        '${Environment.logoAssets}sambara.png',
        UrlThirdParty.pathPlaystore + UrlThirdParty.sambara,
        UrlThirdParty.sambara),
    EsamsatModel(
        Dictionary.payViaBukalapak,
        '${Environment.logoAssets}bukalapak.png',
        UrlThirdParty.bukalapak,
        UrlThirdParty.bukalapak),
    EsamsatModel(
        Dictionary.payViaTokopedia,
        '${Environment.logoAssets}tokopedia.png',
        UrlThirdParty.tokopedia,
        UrlThirdParty.tokopedia),
  ];

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.ESAMSAT,
        screenClassOverride: 'ESamsatScreen');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Dictionary.eSamsat),
      ),
      body: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 10, top: 10),
            alignment: Alignment.centerLeft,
            width: MediaQuery.of(context).size.width,
            child: Text(
              Dictionary.payEsamsatVia,
              style: TextStyle(
                  color: Colors.grey[600],
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
          ),
          ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.all(10.0),
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(5),
                child: SizedBox(
                  height: 70,
                  child: RaisedButton(
                    padding: EdgeInsets.all(10.0),
                    onPressed: () {
                      _checkedAppsInstalled(
                          items[0].packageNameApps, items[0].url);
                    },
                    color: Colors.white,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: <Widget>[
                          Image.asset(
                            items[0].assetImage,
                            fit: BoxFit.fitHeight,
                            width: 90.0,
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: Text(
                                items[0].name,
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                                maxLines: 2,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(5),
                child: SizedBox(
                  height: 70,
                  child: RaisedButton(
                    padding: EdgeInsets.all(10.0),
                    onPressed: () {
                      _checkedAppsInstalled(
                          items[1].packageNameApps, items[1].url);
                    },
                    color: prefix0.Colors.pink,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: <Widget>[
                          Image.asset(
                            items[1].assetImage,
                            fit: BoxFit.fitHeight,
                            width: 90.0,
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: Text(
                                items[1].name,
                                style: TextStyle(
                                    fontSize: 16, color: Colors.white),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(5),
                child: SizedBox(
                  height: 70,
                  child: RaisedButton(
                    padding: EdgeInsets.all(10.0),
                    onPressed: () {
                      _checkedAppsInstalled(
                          items[2].packageNameApps, items[2].url);
                    },
                    color: Colors.white,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: <Widget>[
                          Image.asset(
                            items[2].assetImage,
                            fit: BoxFit.fitHeight,
                            width: 90.0,
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Text(
                                items[2].name,
                                style: TextStyle(
                                    fontSize: 16, color: Colors.green),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Future<void> _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw Dictionary.errorUrl + url;
    }
  }

  Future<void> _checkedAppsInstalled(String package, String url) async {
    bool isInstalled = await DeviceApps.isAppInstalled(package);

    if (isInstalled) {
      unawaited(DeviceApps.openApp(package));
    } else {
      await _launchUrl(url);
    }
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(IterableProperty<EsamsatModel>('items', items));
  }
}
