import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pedantic/pedantic.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/polling_list/Bloc.dart';
import 'package:sapawarga/components/BlockCircleLoading.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/PollingModel.dart';
import 'package:sapawarga/repositories/PollingRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:shimmer/shimmer.dart';

class PollingListScreen extends StatefulWidget {
  PollingListScreen({Key key}) : super(key: key);

  @override
  _PollingListScreenState createState() => _PollingListScreenState();
}

class _PollingListScreenState extends State<PollingListScreen> {
  final RefreshController _mainRefreshController = RefreshController();
  final ScrollController _scrollController = ScrollController();
  int maxDatalength;
  int _page = 1;
  List<PollingModel> dataListModel = [];

  PollingListBloc _pollingListBloc;
  AuthenticationBloc _authenticationBloc;
  PollingRepository _pollingRepository;

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.POLLING,
        screenClassOverride: 'PollingScreen');
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_LIST_POLLING);

    _initialize();
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _pollingRepository = PollingRepository();
    _scrollController.addListener(() {
      _scrollListener();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PollingListBloc>(
      create: (BuildContext context) => _pollingListBloc =
          PollingListBloc(pollingRepository: PollingRepository())
            ..add(PollingListLoad(page: _page)),
      child: BlocListener<PollingListBloc, PollingListState>(
        listener: (context, state) {
          if (state is PollingListFailure) {
            if (state.error.contains(Dictionary.errorUnauthorized)) {
              _authenticationBloc.add(LoggedOut());
              Navigator.of(context).pop();
            }
          }
        },
        child: BlocBuilder<PollingListBloc, PollingListState>(
            bloc: _pollingListBloc,
            builder: (context, state) {
              return Scaffold(
                  appBar: AppBar(
                    title: Text(Dictionary.polling,
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: FontsFamily.intro),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis),
                  ),
                  body: SmartRefresher(
                      controller: _mainRefreshController,
                      enablePullDown: true,
                      header: WaterDropMaterialHeader(),
                      onRefresh: () async {
                        _page = 1;
                        dataListModel.clear();
                        _pollingListBloc.add(PollingListLoad(page: _page));
                        _mainRefreshController.refreshCompleted();
                      },
                      child: state is PollingListLoading
                          ? _buildLoading()
                          : state is PollingListLoaded
                              ? state.records.isNotEmpty
                                  ? _buildContent(state)
                                  : EmptyData(
                                      message: Dictionary.emptyDataPolling)
                              : state is PollingListFailure
                                  ? ErrorContent(error: state.error)
                                  : _buildLoading()));
            }),
      ),
    );
  }

  _buildLoading() {
    return ListView.builder(
      itemCount: 5,
      itemBuilder: (context, index) => Card(
        margin: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
        child: Container(
            margin: const EdgeInsets.all(15.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width - 175,
                          height: 20.0,
                          color: Colors.grey[300]),
                      const SizedBox(height: 5.0),
                      Container(
                          width: MediaQuery.of(context).size.width - 175,
                          height: 20.0,
                          color: Colors.grey[300]),
                      const SizedBox(height: 5.0),
                      Container(
                          width: 100.0, height: 20.0, color: Colors.grey[300]),
                    ],
                  ),
                ),
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.white,
                  child: Container(
                    width: 100.0,
                    height: 40.0,
                    color: Colors.grey[300],
                  ),
                )
              ],
            )),
      ),
    );
  }

  _buildContent(PollingListLoaded state) {
    if (state.isLoad) {
      _page++;
      maxDatalength = state.maxData;
      dataListModel.addAll(state.records);
      state.isLoad = false;
    }

    return ListView.builder(
        itemCount: dataListModel.length + 1,
        controller: _scrollController,
        itemBuilder: (context, index) {
          return index >= dataListModel.length
              ? maxDatalength != dataListModel.length
                  ? Padding(
                      padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                      child: Column(
                        children: <Widget>[
                          CupertinoActivityIndicator(),
                          const SizedBox(
                            height: 5.0,
                          ),
                          Text(Dictionary.loadingData),
                        ],
                      ),
                    )
                  : Container()
              : Card(
                  margin:
                      const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          margin: const EdgeInsets.only(
                              left: 15.0, top: 15.0, right: 15.0),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width - 175,
                                child: Text(state.records[index].name,
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize: 16.0, color: Colors.black)),
                              ),
                              Container(
                                width: 110.0,
                                child: RaisedButton(
                                  color: Colors.green,
                                  textColor: Colors.white,
                                  child: Text(Dictionary.vote.toUpperCase()),
                                  onPressed: () {
                                    _openPollingDetail(context, state, index);
                                  },
                                ),
                              )
                            ],
                          )),
                      Container(
                          margin:
                              const EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 15.0),
                          child:
                              Text('${state.records[index].votesCount} Votes')),
                    ],
                  ),
                );
        });
  }

  Future<void> _openPollingDetail(
      BuildContext context, PollingListLoaded state, int index) async {
    unawaited(blockCircleLoading(context: context));

    await Future.delayed(Duration(milliseconds: 100));

    try {
      bool isVoted = await _pollingRepository.getVoteStatus(
          pollingId: state.records[index].id);

      if (!isVoted) {
        Navigator.pop(context);
        unawaited(Navigator.pushNamed(
            context, NavigationConstrants.PollingDetail,
            arguments: state.records[index]));
      } else {
        Navigator.pop(context);
        unawaited(Fluttertoast.showToast(
            msg: Dictionary.pollingHasVotedMessage,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            backgroundColor: Colors.blue,
            textColor: Colors.white));
      }
    } catch (e) {
      Navigator.pop(context);
      unawaited(Fluttertoast.showToast(
          msg: CustomException.onConnectionException(e.toString()),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.TOP,
          backgroundColor: Colors.red,
          textColor: Colors.white));
    }
  }

  Future<void> _initialize() async {
    maxDatalength = await Preferences.getTotalCount();
  }

  void _scrollListener() {
    if (dataListModel.length != maxDatalength) {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _pollingListBloc.add(PollingListLoad(page: _page));
      }
    }
  }

  @override
  void dispose() {
    _pollingListBloc.close();
    _scrollController.dispose();
    _mainRefreshController.dispose();
    super.dispose();
  }
}
