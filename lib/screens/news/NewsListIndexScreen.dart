import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/news_list_featured/Bloc.dart';
import 'package:sapawarga/blocs/news_list_featured/NewsListlFeaturedEvent.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/NewsModel.dart';
import 'package:sapawarga/repositories/NewsRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'NewsDetailScreen.dart';
import 'NewsIndexScreen.dart';

class NewsListIndexScreen extends StatefulWidget {
  final bool isIdKota;

  NewsListIndexScreen({Key key, @required this.isIdKota}) : super(key: key);

  @override
  _State createState() => _State();
}

class _State extends State<NewsListIndexScreen> {
  final RefreshController _mainRefreshController = RefreshController();
  final ScrollController _scrollController = ScrollController();

  NewsListFeaturedBloc _newsListFeaturedBloc;

  bool get _isIdKota => widget.isIdKota;

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.NEWS);

    AnalyticsHelper.setLogEvent(widget.isIdKota
        ? Analytics.EVENT_VIEW_KABKOTA_NEWS
        : Analytics.EVENT_VIEW_LIST_JABAR_NEWS);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NewsListFeaturedBloc>(
      create: (context) => _newsListFeaturedBloc =
          NewsListFeaturedBloc(newsRepository: NewsRepository())
            ..add(NewsListFeaturedLoad(isIdKota: _isIdKota)),
      child: BlocBuilder<NewsListFeaturedBloc, NewsListFeaturedState>(
          bloc: _newsListFeaturedBloc,
          builder: (context, state) {
            return Scaffold(
              appBar: AppBar(
                  title: Text(Dictionary.newsIndex,
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: FontsFamily.intro),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis)),
              body: Container(
                child: SmartRefresher(
                    controller: _mainRefreshController,
                    enablePullDown: true,
                    header: WaterDropMaterialHeader(),
                    onRefresh: () async {
                      _newsListFeaturedBloc
                          .add(NewsListFeaturedLoad(isIdKota: _isIdKota));
                      _mainRefreshController.refreshCompleted();
                    },
                    child: state is NewsListFeaturedLoading
                        ? _buildFeaturedLoading()
                        : state is NewsListFeatuedLoaded
                            ? state.listtNews.isNotEmpty
                                ? _buildFeaturedContent(state)
                                : EmptyData(message: Dictionary.empty)
                            : state is NewsListFeaturedFailure
                                ? _buildFeaturedFailure(state)
                                : Container()),
              ),
            );
          }),
    );
  }

  _buildFeaturedLoading() {
    return Container(
      margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
      child: ListView(children: <Widget>[
        GestureDetector(
            onTap: () {},
            child: Container(
              child: Card(
                child: Skeleton(
                  height: 200.0,
                  width: MediaQuery.of(context).size.width,
                  padding: 10.0,
                ),
              ),
            )),
        GridView.count(
          shrinkWrap: true,
          crossAxisCount: 2,
          children: List.generate(4, (index) {
            return GestureDetector(
              onTap: () {},
              child: Card(
                child: Column(
                  children: <Widget>[
                    Skeleton(
                      height: 80.0,
                      width: MediaQuery.of(context).size.width,
                      padding: 8.0,
                    ),
                    Skeleton(
                      height: 15.0,
                      width: MediaQuery.of(context).size.width,
                      margin: 8.0,
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Row(
                        children: <Widget>[
                          Skeleton(
                            height: 25.0,
                            width: 25.0,
                          ),
                          Skeleton(
                            height: 20.0,
                            width: 100.0,
                            margin: 5.0,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            );
          }),
        ),
      ]),
    );
  }

  _buildFeaturedContent(NewsListFeatuedLoaded state) {
    List<NewsModel> listDataGrid = List<NewsModel>();
    listDataGrid.addAll(state.listtNews);
    NewsModel newsModel = state.listtNews[0];

    listDataGrid.removeWhere((item) => item.id == newsModel.id);

    return ListView(
        controller: _scrollController,
        padding: const EdgeInsets.all(10.0),
        children: <Widget>[
          GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => NewsDetailScreen(
                            newsId: newsModel.id, isIdKota: _isIdKota)));
              },
              child: Container(
                child: Card(
                  child: Stack(
                    children: <Widget>[
                      CachedNetworkImage(
                        imageUrl: newsModel.coverPathUrl,
                        fit: BoxFit.fill,
                        placeholder: (context, url) => Center(
                            heightFactor: 8.2,
                            child: CupertinoActivityIndicator()),
                        errorWidget: (context, url, error) => Container(
                            height: MediaQuery.of(context).size.height / 3.3,
                            color: Colors.grey[200],
                            child: Image.asset(
                                '${Environment.imageAssets}placeholder.png',
                                fit: BoxFit.fitWidth)),
                      ),
                      Positioned(
                        bottom: 0.0,
                        right: 0.0,
                        left: 0.0,
                        child: Container(
                          margin: const EdgeInsets.all(10.0),
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width,
                                child: Text(newsModel.title,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis),
                              ),
                              Row(
                                children: <Widget>[
                                  Image.network(
                                    newsModel.channel.iconUrl,
                                    width: 25.0,
                                    height: 25.0,
                                  ),
                                  Text(
                                    newsModel.channel.name,
                                    style: TextStyle(
                                        fontSize: 12.0, color: Colors.white),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )),
          GridView.count(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            crossAxisCount: 2,
            children: List.generate(listDataGrid.length, (index) {
              return GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => NewsDetailScreen(
                              newsId: listDataGrid[index].id,
                              isIdKota: _isIdKota)));
                },
                child: Card(
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 90,
                        child: CachedNetworkImage(
                          imageUrl: listDataGrid[index].coverPathUrl,
                          fit: BoxFit.fill,
                          placeholder: (context, url) => Center(
                              heightFactor: 4.2,
                              child: CupertinoActivityIndicator()),
                          errorWidget: (context, url, error) => Container(
                              height: MediaQuery.of(context).size.height / 7.5,
                              color: Colors.grey[200],
                              child: Image.asset(
                                  '${Environment.imageAssets}placeholder.png',
                                  fit: BoxFit.fitWidth)),
                        ),
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Container(
                            padding: const EdgeInsets.only(
                                top: 5.0, right: 5.0, left: 5.0),
                            child: Text(listDataGrid[index].title,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.bold),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis),
                          ),
                          Container(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Row(
                              children: <Widget>[
                                Image.network(
                                  listDataGrid[index].channel.iconUrl,
                                  width: 25.0,
                                  height: 25.0,
                                ),
                                Text(
                                  listDataGrid[index].channel.name,
                                  style: TextStyle(
                                      fontSize: 12.0, color: Colors.grey),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              );
            }),
          ),
          NewsIndexScreen(
              isIdKota: widget.isIdKota, scrollController: _scrollController)
        ]);
  }

  _buildFeaturedFailure(NewsListFeaturedFailure state) {
    return ErrorContent(error: state.error);
  }

  @override
  void dispose() {
    _mainRefreshController.dispose();
    _scrollController.dispose();
    _newsListFeaturedBloc.close();
    super.dispose();
  }
}
