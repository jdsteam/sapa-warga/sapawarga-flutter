import 'dart:math' as math;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/news_detail/Bloc.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/HeroImagePreviewScreen.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/NewsModel.dart';
import 'package:sapawarga/repositories/NewsRepository.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/TimeAgo.dart' as time_ago;
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

import 'NewsListIndexScreen.dart';

class NewsDetailScreen extends StatelessWidget {
  final int newsId;
  final bool isIdKota;
  final NewsRepository newsRepository = NewsRepository();

  NewsDetailScreen({Key key, @required this.newsId, @required this.isIdKota})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NewsDetailBloc>(
      create: (context) => NewsDetailBloc(newsRepository: newsRepository),
      child: _NewsDetail(newsId: newsId, isIdKota: isIdKota),
    );
  }
}

class _NewsDetail extends StatefulWidget {
  final int newsId;
  final bool isIdKota;

  _NewsDetail({@required this.newsId, @required this.isIdKota});

  @override
  _NewsDetailState createState() => _NewsDetailState();
}

class _NewsDetailState extends State<_NewsDetail> {
  final RefreshController _mainRefreshController = RefreshController();
  NewsDetailBloc _newsDetailBloc;

  int get _newsId => widget.newsId;

  bool get _isIdKota => widget.isIdKota;

  Image _likeState =
      Image.asset('${Environment.iconAssets}thumb_up_outline.png');

  @override
  void initState() {
    AnalyticsHelper.setCurrentScreen(Analytics.NEWS);
    _newsDetailBloc = BlocProvider.of<NewsDetailBloc>(context);
    _newsDetailBloc.add(NewsDetailLoad(newsId: _newsId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewsDetailBloc, NewsDetailState>(
        bloc: _newsDetailBloc,
        builder: (context, state) {
          return Scaffold(
              appBar: AppBar(
                  title: Text(Dictionary.news,
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: FontsFamily.intro),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis),
                  actions: <Widget>[
                    state is NewsDetailLoaded
                        ? Container(
                            margin: const EdgeInsets.only(right: 10.0),
                            child: IconButton(
                              icon: Icon(Icons.share),
                              onPressed: () {
                                Share.share(
                                    '${state.newsDetail.title}\nBaca Selengkapnya: ${state.newsDetail.sourceUrl}');

                                AnalyticsHelper.setLogEvent(
                                    Analytics.EVENT_SHARE_NEWS,
                                    <String, dynamic>{
                                      'id': state.newsDetail.id,
                                      'title': state.newsDetail.title
                                    });
                              },
                            ))
                        : Container()
                  ]),
              body: Container(
                child: SmartRefresher(
                    controller: _mainRefreshController,
                    enablePullDown: true,
                    header: WaterDropMaterialHeader(),
                    onRefresh: () async {
                      _newsDetailBloc.add(NewsDetailLoad(newsId: _newsId));
                      _mainRefreshController.refreshCompleted();
                    },
                    child: state is NewsDetailLoading
                        ? _buildLoading()
                        : state is NewsDetailLoaded
                            ? _buildContent(state)
                            : state is NewsDetailFailure
                                ? ErrorContent(error: state.error)
                                : Container()),
              ));
        });
  }

  _buildLoading() {
    return Skeleton(
      child: Column(
        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: 230,
              color: Colors.grey[300]),
          Padding(
            padding: const EdgeInsets.only(
                left: 15.0, top: 15.0, right: 15.0, bottom: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                    width: MediaQuery.of(context).size.width,
                    height: 20.0,
                    color: Colors.grey[300]),
                const SizedBox(height: 5.0),
                Container(
                    width: MediaQuery.of(context).size.width,
                    height: 20.0,
                    color: Colors.grey[300]),
                const SizedBox(height: 10.0),
                Row(
                  children: <Widget>[
                    Container(
                        width: 35.0, height: 35.0, color: Colors.grey[300]),
                    Container(
                      margin: const EdgeInsets.only(left: 5.0),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                                width: 80.0,
                                height: 15.0,
                                color: Colors.grey[300]),
                            const SizedBox(height: 5.0),
                            Container(
                                width: 100.0,
                                height: 15.0,
                                color: Colors.grey[300]),
                          ]),
                    )
                  ],
                ),
                const SizedBox(height: 30.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: _listTextShimmer(),
                ),
                const SizedBox(height: 10.0),
                Column(
                  children: _listTextShimmer(),
                )
              ],
            ),
          )
        ],
      ),
      height: null,
      width: null,
    );
  }

  List<Widget> _listTextShimmer() {
    List<Widget> list = List();
    for (int i = 0; i < 4; i++) {
      list.add(Container(
          width: i == 3 ? 100.0 : MediaQuery.of(context).size.width,
          height: 20.0,
          color: Colors.grey[300]));
      list.add(const SizedBox(height: 5.0));
    }
    return list;
  }

  _buildContent(NewsDetailLoaded state) {
    AnalyticsHelper.setLogEvent(
        Analytics.EVENT_VIEW_DETAIL_NEWS, <String, dynamic>{
      'id': state.newsDetail.id,
      'title': state.newsDetail.title
    });

    bool isLiked =
        state.newsDetail.isLiked != null ? state.newsDetail.isLiked : false;

    if (isLiked) {
      _likeState = Image.asset(
        '${Environment.iconAssets}thumb_up.png',
        color: clr.Colors.green,
      );
    } else {
      _likeState = Image.asset('${Environment.iconAssets}thumb_up_outline.png');
    }

    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        margin: const EdgeInsets.only(bottom: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            GestureDetector(
              child: Hero(
                tag: Dictionary.heroImageTag,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey,
                  child: CachedNetworkImage(
                    imageUrl: state.newsDetail.coverPathUrl,
                    placeholder: (context, url) => Center(
                        heightFactor: 10.2,
                        child: CupertinoActivityIndicator()),
                    errorWidget: (context, url, error) => Container(
                        height: MediaQuery.of(context).size.height / 3.3,
                        color: Colors.grey[200],
                        child: Image.asset(
                            '${Environment.imageAssets}placeholder.png',
                            fit: BoxFit.fitWidth)),
                  ),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (_) => HeroImagePreview(
                              Dictionary.heroImageTag,
                              imageUrl: state.newsDetail.coverPathUrl,
                            )));
              },
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, top: 15.0, right: 15.0, bottom: 15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    state.newsDetail.title,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 10.0),
                  Row(
                    children: <Widget>[
                      Image.network(
                        state.newsDetail.channel.iconUrl,
                        width: 25.0,
                        height: 25.0,
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 5.0),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                state.newsDetail.channel.name,
                                style: TextStyle(fontSize: 12.0),
                              ),
                              Text(
                                  time_ago.fromTimeMillis(
                                      state.newsDetail.createdAt),
                                  style: TextStyle(fontSize: 12.0))
                            ]),
                      )
                    ],
                  ),
                  const SizedBox(height: 10.0),
                  Html(
                      data: state.newsDetail.content,
                      defaultTextStyle:
                          TextStyle(color: Colors.black, fontSize: 15.0),
                      customTextAlign: (dom.Node node) {
                        return TextAlign.justify;
                      },
                      onLinkTap: (url) {
                        _launchURL(url);
                      })
                ],
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(left: 20.0, top: 15.0, right: 20.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Container(
                                width: 24.0,
                                height: 24.0,
                                child: Transform(
                                  alignment: Alignment.center,
                                  transform: Matrix4.rotationY(math.pi),
                                  child: _likeState,
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 5.0, top: 8.0),
                                child: Text(
                                  Dictionary.likeNews,
                                  style: TextStyle(
                                    fontFamily: FontsFamily.productSans,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        onTap: () {
                          _newsDetailBloc
                              .add(NewsDetailLike(newsId: state.newsDetail.id));
                          if (isLiked) {
                            _updateLike(state.newsDetail, true);
                            AnalyticsHelper.setLogEvent(
                                Analytics.EVENT_LIKE_NEWS, <String, dynamic>{
                              'id': state.newsDetail.id,
                              'title': state.newsDetail.title
                            });
                          } else {
                            _updateLike(state.newsDetail, false);
                            AnalyticsHelper.setLogEvent(
                                Analytics.EVENT_UNLIKE_NEWS, <String, dynamic>{
                              'id': state.newsDetail.id,
                              'title': state.newsDetail.title
                            });
                          }
                        },
                      ),
                      GestureDetector(
                        child: Container(
                          height: 24.0,
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Text(Dictionary.originalArticle,
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: FontsFamily.sourceSansPro,
                                  color: clr.Colors.green)),
                        ),
                        onTap: () {
                          _launchURL(state.newsDetail.sourceUrl);

                          AnalyticsHelper.setLogEvent(
                              Analytics.EVENT_VIEW_ORIGINAL_ARTICLE_NEWS,
                              <String, dynamic>{
                                'id': state.newsDetail.id,
                                'title': state.newsDetail.title,
                                'url': state.newsDetail.sourceUrl
                              });
                        },
                      ),
                    ],
                  ),
                  const SizedBox(height: 25.0),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(top: 5.0, bottom: 20.0),
                    child: OutlineButton(
                      borderSide: BorderSide(color: Colors.grey[600]),
                      child: Text(Dictionary.otherNews,
                          style: TextStyle(
                              fontSize: 16.0,
                              fontFamily: FontsFamily.sourceSansPro,
                              color: Colors.grey[700])),
                      padding: const EdgeInsets.all(15.0),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => _isIdKota
                                    ? NewsListIndexScreen(isIdKota: true)
                                    : NewsListIndexScreen(isIdKota: false)));
                      },
                    ),
                  ),
                  _latestNews(state),
                  const SizedBox(height: 10.0)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _latestNews(NewsDetailLoaded state) {
    return Column(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.only(bottom: 20.0),
          child: Text(Dictionary.otherNews,
              style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: FontsFamily.sourceSansPro,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey[800])),
        ),
        ListView.builder(
            shrinkWrap: true,
            itemCount: state.latestNews.length,
            itemBuilder: (context, position) {
              return GestureDetector(
                child: Card(
                  elevation: 3.0,
                  margin: const EdgeInsets.only(bottom: 15.0),
                  color: Colors.white,
                  child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: 80.0,
                              height: 80.0,
                              child: CachedNetworkImage(
                                imageUrl:
                                    state.latestNews[position].coverPathUrl,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5.0),
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                placeholder: (context, url) =>
                                    CupertinoActivityIndicator(),
                                errorWidget: (context, url, error) => Container(
                                    height: MediaQuery.of(context).size.height /
                                        3.3,
                                    color: Colors.grey[200],
                                    child: Image.asset(
                                        '${Environment.imageAssets}placeholder.png',
                                        fit: BoxFit.fitWidth)),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width - 180.0,
                              height: 80.0,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(state.latestNews[position].title,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontFamily: FontsFamily.sourceSansPro,
                                          fontWeight: FontWeight.w600)),
                                  Row(
                                    children: <Widget>[
                                      Image.network(
                                        state.latestNews[position].channel
                                            .iconUrl,
                                        width: 20.0,
                                        height: 20.0,
                                      ),
                                      const SizedBox(
                                        width: 5.0,
                                        height: 1.0,
                                      ),
                                      Text(
                                          state.latestNews[position].channel
                                              .name,
                                          style: TextStyle(fontSize: 12.0))
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )),
                ),
                onTap: () {
                  _newsDetailBloc.add(
                      NewsDetailLoad(newsId: state.latestNews[position].id));
                },
              );
            })
      ],
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _updateLike(NewsModel record, bool isLike) {
    if (isLike) {
      if (record.isLiked) {
        setState(() {
          record.isLiked = false;
          record.likesCount = record.likesCount - 1;
        });
      }
    } else {
      setState(() {
        record.isLiked = true;
        record.likesCount = record.likesCount + 1;
      });
    }
  }

  @override
  void dispose() {
    _newsDetailBloc.close();
    super.dispose();
  }
}
