/*
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/components/Bubble.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/models/QnAModel.dart';

class QnAGovernorChatScreen extends StatefulWidget {
  @override
  _QnAGovernorChatScreenState createState() => _QnAGovernorChatScreenState();
}

class _QnAGovernorChatScreenState extends State<QnAGovernorChatScreen> {

  final _textMessage = TextEditingController();

  List<QnAModel> list = [];

  String initialJson = "[{\"date\":\"2019/11/27\",\"messages\":[{\"id\":13,\"user\":\"staffrw\",\"text\":\"Pak tolong di tindak lanjuti, karena kami sebagai warga merasa dirugikan, dan yang bikin heran kenapa oknum tersebut masih aktif bekerja di balai desa... Mohon bantuan pak\",\"time\":\"12:40:05\"},{\"id\":12,\"user\":\"gubernur\",\"text\":\"Tentunya kami teruskan yang kurang terutama peningkatan PAD perbaikan rasionalitas dari sisi keuangan, meminta masukan dari KPK ada, Kemendagri ada\",\"time\":\"10:30:05\"},{\"id\":11,\"user\":\"staffrw\",\"text\":\"maaf pak mau tanya tentang bansos, gini bulan september kemarin keluarga saya tidak dapat bansos(beras + telor) karena kehabisan stoknya, logika saya kalau bulan kemarin tidak dapat, berarti untuk bulan ini dikalkulasikan paket bansosnya?\",\"time\":\"09:13:05\"}]},{\"date\":\"2019/11/25\",\"messages\":[{\"id\":3,\"user\":\"staffrw\",\"text\":\"Apakah benar program PTSL di kenakan tarif hingga 700 rb pak??\",\"time\":\"15:40:05\"},{\"id\":2,\"user\":\"gubernur\",\"text\":\"Waalaikumsalam\",\"time\":\"15:30:05\"},{\"id\":1,\"user\":\"staffrw\",\"text\":\"Assalamualaikum pak Gubernur\",\"time\":\"15:11:05\"}]}]";

  @override
  void initState() {

    list = listQnAFromJson(initialJson);

    _textMessage.addListener((() {
      setState(() {

      });
    }));

    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Stack(
            alignment: AlignmentDirectional.centerStart,
            children: <Widget>[
              CircleAvatar(
                backgroundColor: Colors.brown.shade800,
                child: Text('RH'),
              ),
              Container(
                  margin: EdgeInsets.only(left: 50.0),
                  child: Text(
                    'Gubernur Jawa Barat',
                    style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, fontFamily: FontsFamily.intro),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ))
            ],
          ),
          titleSpacing: 0.0),
      body: Container(
          color: Colors.grey[200],
          child: Column(
            children: <Widget>[

              */
/** Messages Container **//*

              Flexible(
                child: ListView.builder(
                  padding: EdgeInsets.symmetric(vertical: Dimens.padding),
                  reverse: true,
                  itemCount: list.length,
                  itemBuilder: (context, iList) {

                    */
/** Message List **//*

                    return Column(
                        children: <Widget>[

                          */
/** Separator Date **//*

                          Container(
                            margin: EdgeInsets.only(top: 20.0),
                            alignment: Alignment.center,
                            child: Container(
                              padding: EdgeInsets.all(5.0),
                              decoration: BoxDecoration(
                                  color: Color(0xFFebebeb),
                                  borderRadius: BorderRadius.circular(5.0),
                                  border: Border.all(color: Color(0xFFe6e6e6),)
                              ),
                              child: Text(list[iList].date,
                                  style: TextStyle(color: Colors.grey)),
                            ),
                          ),

                          */
/** Message Bubbles **//*

                          ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            reverse: true,
                            itemCount: list.isNotEmpty ? list[iList].messages.length : 0,
                            itemBuilder: (context, iMessage) {
                              if (list[iList].messages[iMessage].user == 'staffrw') {
                                return _buildBubbleMe(context, list[iList].messages[iMessage], iMessage+1 != list[iList].messages.length ? list[iList].messages[iMessage].user == list[iList].messages[iMessage+1].user ? 8.0 : Dimens.padding : Dimens.padding);
                              } else {
                                return _buildBubbleOther(context, list[iList].messages[iMessage], Dimens.padding);
                              }
                            },
                          )
                        ],
                      );
                  }
                ),
              ),

              */
/** Input field & send button **//*

              Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(top: BorderSide(color: Colors.grey[300]))),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(right: 50),
                        child: TextField(
                            controller: _textMessage,
                            autofocus: false,
                            maxLines: 5,
                            minLines: 1,
                            maxLength: 255,
                            style:
                                TextStyle(color: Colors.black, fontSize: 16.0),
                            decoration: InputDecoration(
                                hintText: 'Tulis pesan ...',
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(
                                    left: Dimens.contentPadding,
                                    top: Dimens.contentPadding,
                                    bottom: Dimens.contentPadding))),
                      ),
                      Positioned(
                          right: 15.0,
                          bottom: 15.0,
                          child: _textMessage.text.trim().isNotEmpty ? GestureDetector(
                              child: Icon(Icons.send, color: clr.Colors.blue),
                              onTap: () {
                                setState(() {
                                  list[0].messages.insert(0, Message(id: 112, user: 'staffrw', text: _textMessage.text.trim(), time: '15:10:00'));
                                  _textMessage.clear();
                                });
                              }) : Icon(Icons.send, color: clr.Colors.grey))
                    ],
                  )),
            ],
          )),
    );
  }

  _buildBubbleMe(BuildContext context, Message message, double margin) {
    return Container(
      margin: EdgeInsets.only(right: Dimens.padding, top: margin),
      alignment: Alignment.bottomRight,
      child: SizedBox(
        width: MediaQuery.of(context).size.width / 1.2,
        child: Container(
          alignment: Alignment.bottomRight,
          child: Bubble(
            color: clr.Colors.bubbleChatBlue,
            borderRadius: 7.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(message.text),
                Text(message.time,
                    style: TextStyle(fontSize: 12.0, color: Colors.grey)),
              ],
            ),
            nipHeight: 0.0,
          ),
        ),
      ),
    );
  }

  _buildBubbleOther(BuildContext context, Message message, double margin) {
    return Container(
      margin: EdgeInsets.only(left: Dimens.padding, top: margin),
      alignment: Alignment.bottomLeft,
      child: SizedBox(
        width: MediaQuery.of(context).size.width / 1.2,
        child: Container(
          alignment: Alignment.bottomLeft,
          child: Bubble(
            color: Colors.white,
            borderRadius: 7.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(message.text),
                Text(message.time,
                    style: TextStyle(fontSize: 12.0, color: Colors.grey)),
              ],
            ),
            nipHeight: 0.0,
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _textMessage.dispose();
    super.dispose();
  }
}
*/
