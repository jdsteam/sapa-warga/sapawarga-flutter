import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/qna_governor/Bloc.dart';
import 'package:sapawarga/components/BlockCircleLoading.dart';
import 'package:sapawarga/components/CustomAppBar.dart';
import 'package:sapawarga/components/DialogTextOnly.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/repositories/QnAGovernorRepository.dart';

class QnAGovernorAddScreen extends StatefulWidget {
  QnAGovernorAddScreen({Key key}) : super(key: key);

  @override
  _QnAGovernorAddScreenState createState() => _QnAGovernorAddScreenState();
}

class _QnAGovernorAddScreenState extends State<QnAGovernorAddScreen> {
  final _textMessage = TextEditingController();
  BuildContext showcaseContext;
  QnAGovernorAddBloc _addBloc;
  AuthenticationBloc _authenticationBloc;
  bool _isLoading = false;
  bool _validate = false;

  @override
  void initState() {
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _textMessage.addListener((() {
      setState(() {});
    }));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar().DefaultAppBar(title: Dictionary.qnaGovernorAdd),
      body: buildContent(context),
    );
  }

  Widget buildContent(BuildContext context) {
    return BlocProvider<QnAGovernorAddBloc>(
      create: (context) =>
          _addBloc = QnAGovernorAddBloc(QnAGovernorRepository()),
      child: BlocListener<QnAGovernorAddBloc, QnAGovernorAddState>(
          bloc: _addBloc,
          listener: (context, state) {
            if (state is QnAGovernorAddLoading) {
              _isLoading = true;
              blockCircleLoading(context: context);
            } else if (state is QnAGovernorAdded) {
              if (_isLoading) {
                _isLoading = false;
                Navigator.of(context, rootNavigator: true).pop();
              }

              Navigator.pop(context, state.record);
            } else if (state is QnAGovernorAddFailure) {
              if (_isLoading) {
                _isLoading = false;
                Navigator.of(context, rootNavigator: true).pop();
              }
              if (state.error.contains(Dictionary.errorUnauthorized)) {
                _authenticationBloc.add(LoggedOut());
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              } else {
                showDialog(
                    context: context,
                    builder: (BuildContext context) => DialogTextOnly(
                          description: state.error.toString(),
                          buttonText: "OK",
                          onOkPressed: () {
                            Navigator.of(context, rootNavigator: true)
                                .pop(); // To close the dialog
                          },
                        ));
              }
            }
          },
          child: _buildForm(context)),
    );
  }

  _buildForm(BuildContext context) {
    return Container(
      padding:
          const EdgeInsets.fromLTRB(Dimens.padding, 25.0, Dimens.padding, 0.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Deskripsi',
              style: TextStyle(
                  fontSize: 14.0, fontFamily: FontsFamily.productSans)),
          const SizedBox(height: 8.0),
          TextField(
            controller: _textMessage,
            autofocus: true,
            minLines: 6,
            maxLines: 6,
            maxLength: 255,
            style:
                TextStyle(fontSize: 14.0, fontFamily: FontsFamily.productSans),
            decoration: InputDecoration(
              hintText: Dictionary.writeQuestion,
              contentPadding: const EdgeInsets.all(10.0),
              border: OutlineInputBorder(),
              errorText: _validate ? Dictionary.errorMinimumQuestion : null,
            ),
          ),
          const SizedBox(height: Dimens.padding),
          RoundedButton(
            title: Dictionary.send,
            borderRadius: BorderRadius.circular(5.0),
            color: clr.Colors.blue,
            textStyle:
                TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            onPressed: _textMessage.text.trim().isNotEmpty
                ? () {
                    if (_textMessage.text.trim().length >= 10) {
                      setState(() {
                        _validate = false;
                      });
                      _addBloc.add(
                          QnAGovernorAdd(question: _textMessage.text.trim()));
                    } else {
                      setState(() {
                        _validate = true;
                      });
                    }
                  }
                : null,
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _textMessage.dispose();
    _addBloc.close();
    super.dispose();
  }
}
