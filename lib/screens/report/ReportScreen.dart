import 'package:flutter/material.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:sapawarga/components/Bubble.dart';
import 'package:sapawarga/components/TextButton.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:sapawarga/constants/UrlThirdParty.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:showcaseview/showcaseview.dart';

class ReportScreen extends StatefulWidget {
  ReportScreen({Key key}) : super(key: key);

  @override
  _ReportScreenState createState() => _ReportScreenState();
}

class _ReportScreenState extends State<ReportScreen> {
  final GlobalKey _showcaseOne = GlobalKey();
  final GlobalKey _showcaseTwo = GlobalKey();
  final GlobalKey _showcaseThree = GlobalKey();
  BuildContext showcaseContext;

  @override
  void initState() {
    super.initState();
    _initialize();
  }

  @override
  Widget build(BuildContext context) {
    return ShowCaseWidget(
      onFinish: () async {
        await Preferences.setShowcaseJabar(true);
      },
      builder: Builder(
        builder: (context) {
          showcaseContext = context;
          return Scaffold(
            appBar: AppBar(title: Text(Dictionary.report)),
            body: _buildContent(context),
          );
        },
      ),
    );
  }

  _initialize() async {
    bool hasShown = await Preferences.hasShowcaseJabar();

    if (hasShown == null || hasShown == false) {
      Future.delayed(
          Duration(milliseconds: 200),
          () => ShowCaseWidget.of(showcaseContext)
              .startShowCase([_showcaseOne, _showcaseTwo, _showcaseThree]));
    }
  }

  Widget _buildContent(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(Dimens.contentPadding),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _buildLapor(context),
          const SizedBox(
            height: Dimens.cardMargin,
          ),
          _buildJQR(context)
        ],
      ),
    );
  }

  _buildLapor(BuildContext context) {
    return _buildShowcase(
      key: _showcaseOne,
      context: context,
      widgets: <Widget>[
        Text(Dictionary.showcaseDescription1),
      ],
      onOkTap: () {
        ShowCaseWidget.of(context).completed(_showcaseOne);
      },
      child: _buildShowcase(
        key: _showcaseTwo,
        context: context,
        widgets: <Widget>[
          Text(Dictionary.showcaseDescription2),
        ],
        onOkTap: () {
          ShowCaseWidget.of(context).completed(_showcaseTwo);
        },
        child: _buildShowcase(
            key: _showcaseThree,
            context: context,
            widgets: <Widget>[
              Text(Dictionary.showcaseDescription3),
            ],
            onOkTap: () {
              ShowCaseWidget.of(context).completed(_showcaseThree);
            },
            child: GestureDetector(
              child: _cardLapor(context),
              onTap: () {
                _launchSms();
              },
            )),
      ),
    );
  }

  _buildJQR(BuildContext context) {
    return GestureDetector(
      child: _cardJQR(context),
      onTap: () {
        Navigator.pushNamed(context, NavigationConstrants.Browser,
            arguments: UrlThirdParty.urlJQR);
      },
    );
  }

  Showcase _buildShowcase(
      {GlobalKey key,
      BuildContext context,
      List<Widget> widgets,
      Widget child,
      GestureTapCallback onOkTap}) {
    return Showcase.withWidget(
      width: MediaQuery.of(context).size.width,
      height: 100.0,
      key: key,
      container: Container(
        margin: const EdgeInsets.only(top: 10.0),
        child: Bubble(
          nipLocation: NipLocation.TOP,
          color: Colors.white,
          child: Container(
            width: MediaQuery.of(context).size.width - 50,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Column(
                  children: widgets,
                ),
                Container(
                    alignment: Alignment.topRight,
                    margin: const EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                    child: TextButtonOnly(
                        title: Dictionary.ok.toUpperCase(),
                        textStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: clr.Colors.green),
                        onTap: onOkTap))
              ],
            ),
          ),
        ),
      ),
      child: child,
    );
  }

  Card _cardLapor(BuildContext context) {
    return Card(
      elevation: 5.0,
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: const EdgeInsets.all(Dimens.cardContentMargin),
        child: Column(
          children: <Widget>[
            Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    width: 80.0,
                    height: 80.0,
                    child: Image.asset(
                      '${Environment.logoAssets}lapor.jpg',
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 95.0),
                    width: MediaQuery.of(context).size.width,
                    height: 80.0,
                    alignment: Alignment.centerLeft,
                    decoration: BoxDecoration(
                        border: Border(
                            bottom:
                                BorderSide(color: Colors.blue, width: 2.0))),
                    child: Text(
                      Dictionary.titleReport,
                      style: TextStyle(fontSize: Dimens.textTitleSize),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  )
                ],
              ),
            ),
            Container(
                margin: const EdgeInsets.only(top: 15.0),
                child: Text(Dictionary.reportDescription,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        fontSize: Dimens.textBodySize,
                        color: Colors.grey[700])))
          ],
        ),
      ),
    );
  }

  Card _cardJQR(BuildContext context) {
    return Card(
      elevation: 5.0,
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: const EdgeInsets.all(Dimens.cardContentMargin),
        child: Column(
          children: <Widget>[
            Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    width: 80.0,
                    height: 80.0,
                    child: Image.asset(
                      '${Environment.logoAssets}jqr.png',
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 95.0),
                    width: MediaQuery.of(context).size.width,
                    height: 80.0,
                    alignment: Alignment.centerLeft,
                    decoration: BoxDecoration(
                        border: Border(
                            bottom:
                                BorderSide(color: Colors.blue, width: 2.0))),
                    child: Text(
                      Dictionary.titleReportJQR,
                      style: TextStyle(fontSize: Dimens.textTitleSize),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  )
                ],
              ),
            ),
            Container(
                margin: const EdgeInsets.only(top: 15.0),
                child: Text(Dictionary.reportJQRDescription,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        fontSize: Dimens.textBodySize,
                        color: Colors.grey[700])))
          ],
        ),
      ),
    );
  }

  _launchSms() async {
    UserInfoModel userInfo = await AuthProfileRepository().getUserInfo();
    List<String> recipients = ["${Environment.laporPhone}"];
    String message =
        'LAPORJABAR SW_${_formatString(userInfo.name)} ${_formatString2(userInfo.kabkota.name)} ${_formatString2(userInfo.kecamatan.name)} ${Dictionary.putReportMessages}';

    await FlutterSms.sendSMS(message: message, recipients: recipients)
        .catchError((onError) {
      debugPrint(onError);
    });
  }

  String _formatString(String text) {
    return text.trim().replaceAll(RegExp(' +'), '_').toUpperCase();
  }

  String _formatString2(String text) {
    return text.trim().replaceAll(RegExp('[^a-zA-Z]'), '').toUpperCase();
  }
}
