import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/screens/mission/HistoryMissionScreen.dart';
import 'package:sapawarga/screens/mission/NewMissionScreen.dart';
import 'package:sapawarga/screens/mission/OnProgressMissionScreen.dart';

class MissionScreen extends StatefulWidget {
  MissionScreen({Key key}) : super(key: key);

  @override
  _MissionScreenState createState() => _MissionScreenState();
}

class _MissionScreenState extends State<MissionScreen> {
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        key: scaffoldState,
        appBar: AppBar(
          title: Text(Dictionary.mission,
              style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: FontsFamily.intro),
              maxLines: 1,
              overflow: TextOverflow.ellipsis),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(
                  child: Text(Dictionary.newMission,
                      style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: FontsFamily.intro),
                      textAlign: TextAlign.center)),
              Tab(
                  child: Text(Dictionary.onProgressMission,
                      style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: FontsFamily.intro),
                      textAlign: TextAlign.center)),
              Tab(
                  child: Text(
                Dictionary.historyMission,
                style: TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: FontsFamily.intro),
                textAlign: TextAlign.center,
              )),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            NewMissionScreen(),
            OnProgressMissionScreen(),
            HistoryMissionScreen(),
          ],
        ),
      ),
    );
  }
}
