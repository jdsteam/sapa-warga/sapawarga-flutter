import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/authentication/AuthenticationBloc.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/gamifications/gamification_list/Bloc.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/Dimens.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/GamificationsModel.dart';
import 'package:sapawarga/repositories/GamificationsRepository.dart';
import 'package:sapawarga/screens/mission/DetailNewMissionScreen.dart';
import 'package:sapawarga/utilities/FormatDate.dart';

class NewMissionScreen extends StatefulWidget {
  NewMissionScreen({Key key}) : super(key: key);

  @override
  _NewMissionScreenState createState() => _NewMissionScreenState();
}

class _NewMissionScreenState extends State<NewMissionScreen> {
  GamificationsListBloc gamificationsListBloc;
  List<ItemGamification> _gamificationList = List<ItemGamification>();
  final RefreshController _mainRefreshController = RefreshController();
  AuthenticationBloc _authenticationBloc;
  final ScrollController _scrollController = ScrollController();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final _scrollThreshold = 200.0;
  int _pageCount = 0;
  int _currentPage = 1;

  @override
  void initState() {
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _scrollController.addListener(() {
      _onScroll();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: Container(
        child: BlocProvider<GamificationsListBloc>(
          create: (context) => gamificationsListBloc = GamificationsListBloc(
              gamificationsRepository: GamificationsRepository())
            ..add(GamificationsNewMissionListLoad(page: _currentPage)),
          child: BlocListener<GamificationsListBloc, GamificationsListState>(
            bloc: gamificationsListBloc,
            listener: (context, state) {
              if (state is GamificationsListLoaded) {
                _pageCount = state.records.meta.pageCount;
                _currentPage = state.records.meta.currentPage + 1;
                _gamificationList.addAll(state.records.items);
                _gamificationList = _gamificationList.toSet().toList();
                setState(() {});
              } else if (state is GamificationsListFailure) {
                if (state.error.contains(Dictionary.errorUnauthorized)) {
                  _authenticationBloc.add(LoggedOut());
                  Navigator.of(context).pop();
                }
              }
            },
            child: BlocBuilder<GamificationsListBloc, GamificationsListState>(
              bloc: gamificationsListBloc,
              builder: (context, state) => SmartRefresher(
                  controller: _mainRefreshController,
                  enablePullDown: true,
                  header: WaterDropMaterialHeader(),
                  onRefresh: () async {
                    _currentPage = 1;
                    _gamificationList.clear();
                    gamificationsListBloc.add(
                        GamificationsNewMissionListLoad(page: _currentPage));
                    _mainRefreshController.refreshCompleted();
                  },
                  child: state is GamificationsListLoading
                      ? _buildLoading()
                      : state is GamificationsListLoaded
                          ? state.records.items.isNotEmpty
                              ? _buildContent(state)
                              : EmptyData(
                                  message: Dictionary.emptyDataNewMission)
                          : state is GamificationsListFailure
                              ? ErrorContent(error: state.error)
                              : _buildLoading()),
            ),
          ),
        ),
      ),
    );
  }

  _buildContent(GamificationsListLoaded state) {
    return ListView(
      controller: _scrollController,
      children: <Widget>[
        Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.topLeft,
                child: Container(
                  margin: const EdgeInsets.all(5),
                  child: Text(Dictionary.newMission,
                      style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey[600],
                          fontFamily: FontsFamily.intro),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis),
                ),
              ),
              ListView.builder(
                  itemCount:
                      state.records.meta.totalCount == _gamificationList.length
                          ? _gamificationList.length
                          : _gamificationList.length + 1,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      child: Container(
                          height: 143.0,
                          margin: const EdgeInsets.only(top: 10.0),
                          padding: const EdgeInsets.all(20),
                          alignment: Alignment.topCenter,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: clr.Colors.gradientBlueToPurple),
                              borderRadius: BorderRadius.circular(5.0)),
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    child: Text(
                                      _gamificationList[index].title,
                                      maxLines: 3,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: 19.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    ),
                                  ),
                                  Container(
                                    margin:
                                        const EdgeInsets.only(left: 5, top: 5),
                                    child: Image.asset(
                                        '${Environment.iconAssets}icon_new_mission.png',
                                        height: 25,
                                        fit: BoxFit.fitWidth),
                                  ),
                                ],
                              ),
                              Spacer(),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    padding: const EdgeInsets.only(right: 5),
                                    child: Image.asset(
                                        '${Environment.iconAssets}icon_time.png',
                                        height: 20,
                                        fit: BoxFit.fitWidth),
                                  ),
                                  Text(
                                    'Berakhir : ' +
                                        unixTimeStampToDateWithoutMultiplication(
                                            _gamificationList[index]
                                                .endDate
                                                .millisecondsSinceEpoch),
                                    maxLines: 3,
                                    overflow: TextOverflow.clip,
                                    style: TextStyle(
                                        fontSize: 17.0, color: Colors.white),
                                  )
                                ],
                              ),
                            ],
                          )),
                      onTap: () async {
                        bool isTakeMission = await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DetailNewMissionScreen(
                                      id: _gamificationList[index].id,
                                    )));

                        if (isTakeMission != null && isTakeMission) {
                          scaffoldKey.currentState.showSnackBar(
                            SnackBar(
                              content: Container(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0),
                                  child: Text(
                                    Dictionary.takeMission,
                                    style: TextStyle(fontSize: 14.0),
                                  )),
                              backgroundColor: clr.Colors.blue,
                              duration: Duration(seconds: 2),
                            ),
                          );
                          _gamificationList.clear();
                          gamificationsListBloc
                              .add(GamificationsNewMissionListLoad(page: 1));
                        }
                      },
                    );
                  })
            ],
          ),
        ),
      ],
    );
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (_currentPage != _pageCount + 1) {
      if (maxScroll - currentScroll <= _scrollThreshold) {
        gamificationsListBloc
            .add(GamificationsNewMissionListLoad(page: _currentPage));
      }
    }
  }

  _buildLoading() {
    return ListView.builder(
      padding: const EdgeInsets.only(bottom: 80.0, top: 10),
      itemCount: 2,
      itemBuilder: (context, index) => Skeleton(
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.fromLTRB(
              Dimens.padding, 5.0, Dimens.padding, 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(width: 70.0, height: 25.0, color: Colors.grey[300]),
              Container(
                margin: const EdgeInsets.only(top: 10.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5.0),
                  child: Container(
                      height: MediaQuery.of(context).size.height / 4.8,
                      color: Colors.grey[300]),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
