import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sapawarga/blocs/authentication/AuthenticationBloc.dart';
import 'package:sapawarga/blocs/authentication/Bloc.dart';
import 'package:sapawarga/blocs/gamifications/gamification_detail_mission/Bloc.dart';
import 'package:sapawarga/blocs/gamifications/gamification_detail_mission/GamificationsDetailMissionBloc.dart';
import 'package:sapawarga/components/CustomAppBar.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/MySeparator.dart';
import 'package:sapawarga/components/TimelineComponent.dart';
import 'package:sapawarga/constants/Colors.dart' as clr;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/GamificationDetailOnProgressModel.dart';
import 'package:sapawarga/models/GamificationOnprogressModel.dart';
import 'package:sapawarga/repositories/GamificationsRepository.dart';
import 'package:sapawarga/utilities/FormatDate.dart';
import 'package:timeline_list/timeline.dart';
import 'package:timeline_list/timeline_model.dart';

class DetailMissionScreen extends StatefulWidget {
  final ItemOnProgressModel itemOnProgressModel;
  final String title;

  DetailMissionScreen({Key key, this.itemOnProgressModel, this.title})
      : super(key: key);

  @override
  _DetailMissionScreenState createState() => _DetailMissionScreenState();
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<ItemOnProgressModel>(
        'itemOnProgressModel', itemOnProgressModel));
    properties.add(StringProperty('title', title));
  }
}

class _DetailMissionScreenState extends State<DetailMissionScreen> {
  GamificationsDetailMissionBloc _gamificationsDetailMissionBloc;
  List<ItemGamificationDetailModel> _gamificationListDetail =
      List<ItemGamificationDetailModel>();
  final RefreshController _mainRefreshController = RefreshController();
  AuthenticationBloc _authenticationBloc;
  List<TimelineModel> timeLineitems = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar().DefaultAppBar(title: widget.title),
      body: Container(
        child: BlocProvider<GamificationsDetailMissionBloc>(
          create: (context) =>
              _gamificationsDetailMissionBloc = GamificationsDetailMissionBloc(
                  gamificationsRepository: GamificationsRepository())
                ..add(GamificationsDetailMissionLoad(
                    id: widget.itemOnProgressModel.gamificationId)),
          child: BlocListener<GamificationsDetailMissionBloc,
              GamificationsDetailMissionState>(
            bloc: _gamificationsDetailMissionBloc,
            listener: (context, state) {
              if (state is GamificationsDetailMissionLoaded) {
                _gamificationListDetail.clear();
                _gamificationListDetail.addAll(state.records.items);
                _gamificationListDetail =
                    _gamificationListDetail.toSet().toList();
                setState(() {});
              } else if (state is GamificationsDetailMissionFailure) {
                if (state.error.contains(Dictionary.errorUnauthorized)) {
                  _authenticationBloc.add(LoggedOut());
                  Navigator.of(context).pop();
                }
              }
            },
            child: BlocBuilder<GamificationsDetailMissionBloc,
                GamificationsDetailMissionState>(
              bloc: _gamificationsDetailMissionBloc,
              builder: (context, state) => SmartRefresher(
                  controller: _mainRefreshController,
                  enablePullDown: true,
                  header: WaterDropMaterialHeader(),
                  onRefresh: () async {
                    timeLineitems.clear();
                    _gamificationListDetail.clear();
                    _gamificationsDetailMissionBloc.add(
                        GamificationsDetailMissionLoad(
                            id: widget.itemOnProgressModel.gamificationId));
                    _mainRefreshController.refreshCompleted();
                  },
                  child: state is GamificationsDetailMissionLoading
                      ? _buildLoading()
                      : state is GamificationsDetailMissionLoaded
//                          ? state.records.items.isNotEmpty
                          ? _buildContent(state)
//                              : EmptyData(
//                                  message: Dictionary.emptyDataRWActivity)
                          : state is GamificationsDetailMissionFailure
                              ? ErrorContent(error: state.error)
                              : _buildLoading()),
            ),
          ),
        ),
      ),
    );
  }

  void setTimlineData(GamificationsDetailMissionLoaded state) {
    if (state.records.items.isNotEmpty) {
      for (int i = 0; i < state.records.items[0].totalHit; i++) {
        if (i < state.records.items.length) {
          if (i + 1 == state.records.items.length &&
              i + 1 == state.records.items[0].totalHit) {
            timeLineitems.add(timelineConmponent(
                getNameSubmission(state.records.items[i].objectEvent) +
                    (i + 1).toString(),
                unixTimeStampToDate(state.records.items[i].createdAt),
                FontAwesomeIcons.award,
                Colors.green));
          } else {
            timeLineitems.add(timelineConmponent(
                getNameSubmission(state.records.items[i].objectEvent) +
                    (i + 1).toString(),
                unixTimeStampToDate(state.records.items[i].createdAt),
                Icons.check,
                Colors.green));
          }
        } else if (i + 1 == state.records.items[0].totalHit) {
          timeLineitems.add(timelineConmponent(
              getNameSubmission(state.records.items[0].objectEvent) +
                  (i + 1).toString(),
              '',
              FontAwesomeIcons.award,
              Colors.grey));
        } else {
          timeLineitems.add(timelineConmponent(
              getNameSubmission(state.records.items[0].objectEvent) +
                  (i + 1).toString(),
              '',
              Icons.check,
              Colors.grey));
        }
      }
    } else {
      for (int i = 0;
          i < widget.itemOnProgressModel.gamification.totalHit;
          i++) {
        if (i + 1 == widget.itemOnProgressModel.gamification.totalHit) {
          timeLineitems.add(timelineConmponent(
              getNameSubmission(
                      widget.itemOnProgressModel.gamification.objectEvent) +
                  (i + 1).toString(),
              '',
              FontAwesomeIcons.award,
              Colors.grey));
        } else {
          timeLineitems.add(timelineConmponent(
              getNameSubmission(
                      widget.itemOnProgressModel.gamification.objectEvent) +
                  (i + 1).toString(),
              '',
              Icons.check,
              Colors.grey));
        }
      }
    }
  }

  _buildContent(GamificationsDetailMissionLoaded state) {
    setTimlineData(state);

    return SingleChildScrollView(
        child: Container(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildButtonColumn('icon_kegiatan_rw_white.png'),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 15, bottom: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(widget.itemOnProgressModel.gamification.title,
                          style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                          textAlign: TextAlign.left),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(widget.itemOnProgressModel.gamification.description,
                          style: TextStyle(
                              fontSize: 14.0, color: Colors.grey[600]),
                          textAlign: TextAlign.left),
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        width: MediaQuery.of(context).size.width,
                        child: MySeparator(
                          color: Colors.grey[600],
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Timeline(
              children: timeLineitems,
              position: TimelinePosition.Left,
              lineColor: Colors.green,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics()),
        ],
      ),
    ));
  }

  _buildButtonColumn(String iconPath) {
    return Column(
      children: [
        Container(
          width: 55,
          height: 55,
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              blurRadius: 6.0,
              color: Colors.black.withOpacity(.2),
              offset: Offset(3.0, 5.0),
            ),
          ], borderRadius: BorderRadius.circular(12.0), color: clr.Colors.blue),
          child: IconButton(
            color: Theme.of(context).textTheme.bodyText1.color,
            icon: Image.asset('${Environment.iconAssets}' + iconPath,
                height: 25, fit: BoxFit.fitWidth, width: 30),
            onPressed: () {},
          ),
        ),
      ],
    );
  }

  String getNameSubmission(String labelName) {
    if (labelName == 'news_view_detail') {
      return Dictionary.readNewsMission;
    } else if (labelName == 'news_important_view_detail') {
      return Dictionary.readImportantNewsMission;
    } else {
      return Dictionary.postRwActivitiesMission;
    }
  }

  _buildLoading() {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 5.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(5.0),
              child: Container(
                  height: MediaQuery.of(context).size.height / 1.5,
                  color: Colors.grey[300]),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(IterableProperty<TimelineModel>('timeLineitems', timeLineitems));
  }
}
