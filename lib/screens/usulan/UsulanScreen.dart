import 'dart:ui' as prefix0;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/RoundedButton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/CheckConnection.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/screens/usulan/AddUsulanScreen.dart';
import 'package:sapawarga/screens/usulan/MyUsulanScreen.dart';
import 'package:sapawarga/screens/usulan/GeneralUsulanScreen.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';

class UsulanScreen extends StatefulWidget {
  UsulanScreen({Key key}) : super(key: key);

  @override
  UsulanScreenState createState() => UsulanScreenState();
}

class UsulanScreenState extends State<UsulanScreen> {
  bool isBackRefreshPage = false;
  bool isCloseUsulan = false;
  bool statConnect;
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    Future.delayed(
        Duration(milliseconds: 100),
        () => CheckConnection.checkConnection().then((isConnect) {
              if (isConnect != null && isConnect) {
                setState(() {
                  statConnect = true;
                });
              } else {
                setState(() {
                  statConnect = false;
                });
              }
            }));
    AnalyticsHelper.setCurrentScreen(Analytics.PROPOSAL,
        screenClassOverride: 'UsulanScreen');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
          key: scaffoldState,
          appBar: isCloseUsulan
              ? AppBar(
                  automaticallyImplyLeading: true,
                  title: Text(Dictionary.usulan,
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: FontsFamily.intro),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis),
                  leading: IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context, false);
                    },
                  ),
                  bottom: TabBar(
                    tabs: <Widget>[
                      Tab(
                        child: Text(Dictionary.general,
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: FontsFamily.intro),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis),
                      ),
                      Tab(
                        child: Text(Dictionary.myUsulan,
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: FontsFamily.intro),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis),
                      ),
                    ],
                  ),
                )
              : AppBar(
                  automaticallyImplyLeading: true,
                  title: Text(Dictionary.usulan,
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: FontsFamily.intro),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis),
                ),
          body: statConnect == null || statConnect
              ? Stack(
                  children: <Widget>[
                    TabBarView(
                      children: <Widget>[
                        GeneralUsulanScreen(),
                        MyUsulanScreen(
                            isBackRefreshPage: isBackRefreshPage,
                            usulanScreenState: this)
                      ],
                    ),
                    !isCloseUsulan ? closeUsulan() : Container()
                  ],
                )
              : ErrorContent(error: Dictionary.errorConnection)
//        floatingActionButton: FloatingActionButton.extended(
//          backgroundColor: prefix0.Colors.blue,
//          onPressed: () {
//            _navigateResult(context);
//          },
//          icon: Icon(Icons.add),
//          label: Text(Dictionary.addUsulan),
//        ),
//        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
          ),
    );
  }

  _navigateResult(BuildContext context) async {
    final result = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => AddUsulanScreen()));
    if (result == Dictionary.successSendUsulan) {
      Future.delayed(const Duration(milliseconds: 100), () {
        scaffoldState.currentState.showSnackBar(SnackBar(
          content: Text(Dictionary.successSendUsulan),
        ));
      });
      setState(() {
        isBackRefreshPage = true;
      });
    } else if (result == Dictionary.successSaveDraft) {
      Future.delayed(const Duration(milliseconds: 100), () {
        scaffoldState.currentState.showSnackBar(
          SnackBar(
            content: Text(Dictionary.successSaveDraft),
          ),
        );
      });
      setState(() {
        isBackRefreshPage = true;
      });
    } else {
      setState(() {
        isBackRefreshPage = false;
      });
    }
  }

  Widget closeUsulan() {
    return BackdropFilter(
      filter: prefix0.ImageFilter.blur(sigmaX: 3, sigmaY: 3),
      child: Container(
          color: Colors.black12.withOpacity(0.8),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Container(
              margin: const EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  Text(
                    'Terima kasih telah memberikan USULAN untuk membangun Jawa Barat! ',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    '\n\n Demi meningkatkan kualitas layanan, untuk sementara opsi menambahkan usulan kami TUTUP. '
                    '\n\n Kami akan kembali membuka opsi menambahkan usulan saat sistem fitur USULAN telah diperbarui. '
                    '\n\nJangan khawatir, Anda tetap dapat melihat usulan-usulan yang telah dibuat.'
                    '\n\n Usulan-usulan yang telah masuk sedang kami upayakan untuk diproses secara kolektif agar menjadi pertimbangan pemerintah dalam membuat kebijakan pada tahun 2020.',
                    style: TextStyle(color: Colors.white, fontSize: 18.0),
                    textAlign: TextAlign.left,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.centerRight,
                    margin: const EdgeInsets.only(top: 30),
                    child: RoundedButton(
                      minWidth: 10,
                      title: Dictionary.next2,
                      borderRadius: BorderRadius.circular(5.0),
                      color: Colors.blue,
                      textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 16),
                      onPressed: () {
                        setState(() {
                          isCloseUsulan = true;
                        });
                      },
                    ),
                  )
                ],
              ),
            ),
          )),
    );
  }
}
