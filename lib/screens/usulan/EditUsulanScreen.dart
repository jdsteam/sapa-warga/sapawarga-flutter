import 'package:flutter/material.dart';
import 'package:sapawarga/models/AddPhotoModel.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';
import 'package:sapawarga/screens/usulan/CreateUsulanScreen.dart';

// ignore: must_be_immutable
class EditUsulanScreen extends StatefulWidget {
  final MasterCategoryModel categorySelected;
  final List<MasterCategoryModel> listCategoryUsulanModel;
  final String titleUsulan;
  final String descUsulan;
  List<AddPhotoModel> photoUsulan;
  final int id;

  EditUsulanScreen(
      {Key key,
      this.categorySelected,
      this.listCategoryUsulanModel,
      this.titleUsulan,
      this.descUsulan,
      this.photoUsulan,
      this.id})
      : super(key: key);

  @override
  _EditUsulanScreenState createState() => _EditUsulanScreenState();
}

class _EditUsulanScreenState extends State<EditUsulanScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Edit Usulan"),
          backgroundColor: Colors.blue,
        ),
        body: CreateUsulanScreen(
          categorySelected: widget.categorySelected,
          listCategoryUsulanModel: widget.listCategoryUsulanModel,
          titleUsulan: widget.titleUsulan,
          descUsulan: widget.descUsulan,
          photoUsulan: widget.photoUsulan,
          id: widget.id,
        ));
  }
}
