import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/usulan/usulanumum/Bloc.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/com'
    'ponents/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/CategoryModel.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/models/UsulanModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/UsulanRepository.dart';
import 'package:sapawarga/screens/usulan/DetailUsulanScreen.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/FormatDate.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:sapawarga/utilities/UsulanHelper.dart';

class GeneralUsulanScreen extends StatelessWidget {
  GeneralUsulanScreen({Key key}) : super(key: key);

  final UsulanRepository usulanRepository = UsulanRepository();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UsulanListBloc>(
      create: (context) => UsulanListBloc(usulanRepository: usulanRepository),
      child: UsulanUmum(),
    );
  }
}

class UsulanUmum extends StatefulWidget {
  UsulanUmum({Key key}) : super(key: key);

  @override
  _UsulanUmumState createState() => _UsulanUmumState();
}

class _UsulanUmumState extends State<UsulanUmum> {
  final ScrollController _scrollController = ScrollController();

  int maxDatalength;
  UsulanListBloc _usulanListBloc;
  int _page = 1;
  UserInfoModel userInfo;
  String likeStat;
  List<UsulanModel> dataListModel = [];
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_LIST_GENERAL_USULAN);

    _initialize();
    getProfile();
    _usulanListBloc = BlocProvider.of<UsulanListBloc>(context);
    _usulanListBloc.add(UsulanListLoad(page: _page));
    _scrollController.addListener(() {
      _scrollListener();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UsulanListBloc, UsulanListState>(
        bloc: _usulanListBloc,
        builder: (context, state) {
          return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: _refresh,
            child: state is UsulanListLoading
                ? _buildListLoading()
                : state is UsulanListLoaded
                    ? state.records.isNotEmpty
                        ? _buildContentUmum(state)
                        : EmptyData(message: Dictionary.empty)
                    : state is UsulanListFailure
                        ? _buildListFailure(state)
                        : Container(),
          );
        });
  }

  _buildContentUmum(UsulanListLoaded state) {
    if (state.isLoad) {
      _page++;
      maxDatalength = state.maxLengthData;
      dataListModel.addAll(state.records);
      state.isLoad = false;
    }

    if (dataListModel.isNotEmpty) {
      return ListView.separated(
        shrinkWrap: true,
        controller: _scrollController,
        itemCount: dataListModel.length + 1,
        itemBuilder: (BuildContext context, int index) {
          return index >= dataListModel.length
              ? maxDatalength != dataListModel.length
                  ? Padding(
                      padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                      child: Column(
                        children: <Widget>[
                          CupertinoActivityIndicator(),
                          const SizedBox(
                            height: 5.0,
                          ),
                          Text(Dictionary.loadingData),
                        ],
                      ),
                    )
                  : Container()
              : _loadCard(dataListModel[index]);
        },
        separatorBuilder: (BuildContext context, int dex) => Divider(),
      );
    } else {
      return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        alignment: Alignment.center,
        child: Text(
          Dictionary.empty,
          style: TextStyle(
              color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ),
      );
    }
  }

  Widget _loadCard(UsulanModel records) {
    if (UsulanHelper.isLikeUsulan(records.likesUsers, userInfo.name)) {
      likeStat = '${Environment.imageAssets}liked.png';
    } else {
      likeStat = '${Environment.imageAssets}like-empty.png';
    }

    return GestureDetector(
      onTap: () {
        _navigateResult(context, records);
      },
      child: Container(
        margin: const EdgeInsets.all(16.0),
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  width: 30,
                  height: 30,
                  child: CircleAvatar(
                    minRadius: 90,
                    maxRadius: 150,
                    backgroundImage: records.author.photoUrlFull != null
                        ? NetworkImage(records.author.photoUrlFull)
                        : ExactAssetImage('assets/images/user.png'),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 1.25,
                  margin: const EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        records.author.name != null
                            ? records.author.name
                            : Dictionary.anonim,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14.0,
                            fontFamily: FontsFamily.productSans,
                            fontWeight: FontWeight.bold),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4),
                        child: Text(
                          unixTimeStampToDateTime(records.updatedAt),
                          style: TextStyle(
                              color: Colors.grey[600], fontSize: 12.0),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Stack(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(top: 10),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 3.2,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: CachedNetworkImage(
                      imageUrl: records.attachments != null
                          ? records.attachments[0].url
                          : "",
                      fit: BoxFit.fill,
                      placeholder: (context, url) => Center(
                          heightFactor: 8.2,
                          child: CupertinoActivityIndicator()),
                      errorWidget: (context, url, error) => Container(
                          height: MediaQuery.of(context).size.height / 3.3,
                          color: Colors.grey[200],
                          child: Image.asset(
                              '${Environment.imageAssets}placeholder.png',
                              fit: BoxFit.fitWidth)),
                    ),
                  ),
                ),
                Positioned(
                  right: 10,
                  top: 20,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(6.0),
                    child: Container(
                      color: UsulanHelper.getColor(records.statusLabel),
                      padding: const EdgeInsets.all(8),
                      child: Text(
                        records.category.name,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: FontsFamily.productSans),
                      ),
                    ),
                  ),
                )
              ],
            ),
            Container(
              margin: const EdgeInsets.only(right: 10, top: 10, bottom: 10),
              child: Row(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      _usulanListBloc.add(UsulanListLike(id: records.id));
                      if (UsulanHelper.isLikeUsulan(
                          records.likesUsers, userInfo.name)) {
                        updateLike(records.id, true);

                        AnalyticsHelper.setLogEvent(
                            Analytics.EVENT_UNLIKE_USULAN, <String, dynamic>{
                          'id': records.id,
                          'title': records.title
                        });
                      } else {
                        updateLike(records.id, false);

                        AnalyticsHelper.setLogEvent(
                            Analytics.EVENT_LIKE_USULAN, <String, dynamic>{
                          'id': records.id,
                          'title': records.title
                        });
                      }
                    },
                    child: Image.asset(
                      likeStat,
                      width: 20.0,
                      height: 20.0,
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 5),
                    child: Text(
                      records.likesCount.toString(),
                      style: TextStyle(fontSize: 12.0, color: Colors.black),
                    ),
                  )
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Text(
                records.title,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.left,
                softWrap: true,
                maxLines: 2,
                style: TextStyle(fontSize: 12.0, color: Colors.black),
              ),
            )
          ],
        ),
      ),
    );
  }

  _buildListFailure(UsulanListFailure state) {
    return ErrorContent(error: state.error);
  }

  _buildListLoading() {
    return ListView.builder(
      itemCount: 5,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          margin: const EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Skeleton(
                    width: 30,
                    height: 30,
                    child: CircleAvatar(
                      minRadius: 20,
                      maxRadius: 20,
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Skeleton(height: 15, width: 150, margin: 5),
                        Skeleton(height: 15, width: 150, margin: 5),
                      ],
                    ),
                  )
                ],
              ),
              Stack(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(top: 10),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 3.2,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Skeleton(
                          height: 25,
                          width: MediaQuery.of(context).size.width,
                          margin: 5),
                    ),
                  ),
                ],
              ),
              Container(
                margin: const EdgeInsets.only(right: 10, top: 10, bottom: 10),
                child: Row(
                  children: <Widget>[
                    Skeleton(height: 25, width: 25, margin: 5),
                    Skeleton(height: 25, width: 25, margin: 5)
                  ],
                ),
              ),
              Skeleton(
                  height: 25,
                  width: MediaQuery.of(context).size.width,
                  margin: 5)
            ],
          ),
        );
      },
    );
  }

  Future<void> _initialize() async {
    maxDatalength = await Preferences.getTotalCount();
  }

  Future<void> _refresh() async {
    _page = 1;
    dataListModel.clear();
    _usulanListBloc.add(UsulanListRefresh());
  }

  void _scrollListener() {
    if (dataListModel.length != maxDatalength) {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _usulanListBloc.add(UsulanListLoad(page: _page));
      }
    }
  }

  Future<void> getProfile() async {
    userInfo = await AuthProfileRepository().getUserInfo();
  }

  void updateLike(int id, bool isLike) {
    for (int i = 0; i < dataListModel.length; i++) {
      if (dataListModel[i].id == id) {
        if (isLike) {
          for (int j = 0; j < dataListModel[i].likesUsers.length; j++) {
            if (dataListModel[i].likesUsers[j].name == userInfo.name) {
              setState(() {
                dataListModel[i].likesUsers.removeAt(j);
                dataListModel[i].likesCount = dataListModel[i].likesCount - 1;
              });
            }
          }
        } else {
          Category category = Category(id: 99, name: userInfo.name);
          setState(() {
            dataListModel[i].likesCount = dataListModel[i].likesCount + 1;
            dataListModel[i].likesUsers.add(category);
          });
        }
      }
    }
  }

  @override
  void dispose() {
    _usulanListBloc.close();
    _scrollController.dispose();
    super.dispose();
  }

  _navigateResult(BuildContext context, UsulanModel records) async {
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => DetailUsulanScreen(
                  idUsulan: records.id,
                  isUsulanSaya: false,
                )));
    UsulanModel usulanModel = result;
    if (usulanModel != null) {
      for (int i = 0; i < dataListModel.length; i++) {
        if (dataListModel[i].id == usulanModel.id) {
          setState(() {
            dataListModel[i] = usulanModel;
          });
        }
      }
    }
  }
}
