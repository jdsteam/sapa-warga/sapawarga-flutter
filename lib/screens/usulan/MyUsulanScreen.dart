import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sapawarga/blocs/usulan/myusulan/Bloc.dart';
import 'package:sapawarga/components/EmptyData.dart';
import 'package:sapawarga/components/ErrorContent.dart';
import 'package:sapawarga/components/Skeleton.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/FontsFamily.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/UsulanModel.dart';
import 'package:sapawarga/repositories/UsulanRepository.dart';
import 'package:sapawarga/screens/usulan/DetailUsulanScreen.dart';
import 'package:sapawarga/screens/usulan/UsulanScreen.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:sapawarga/utilities/FormatDate.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:sapawarga/utilities/UsulanHelper.dart';

// ignore: must_be_immutable
class MyUsulanScreen extends StatefulWidget {
  final UsulanRepository usulanRepository = UsulanRepository();
  bool isBackRefreshPage;
  UsulanScreenState usulanScreenState;

  MyUsulanScreen({Key key, this.isBackRefreshPage, this.usulanScreenState})
      : super(key: key);

  @override
  _MyUsulanScreenState createState() => _MyUsulanScreenState();
}

class _MyUsulanScreenState extends State<MyUsulanScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<MyUsulanListBloc>(
      create: (context) =>
          MyUsulanListBloc(usulanRepository: widget.usulanRepository),
      child: MyUsulan(
          isBackRefreshPage: widget.isBackRefreshPage,
          usulanScreenState: widget.usulanScreenState),
    );
  }
}

// ignore: must_be_immutable
class MyUsulan extends StatefulWidget {
  bool isBackRefreshPage;
  UsulanScreenState usulanScreenState;

  MyUsulan({Key key, this.isBackRefreshPage, this.usulanScreenState})
      : super(key: key);

  @override
  _MyUsulanState createState() => _MyUsulanState();
}

class _MyUsulanState extends State<MyUsulan> {
  final ScrollController _scrollController = ScrollController();
  int countDatalength, maxDatalength;
  MyUsulanListBloc _usulanListBloc;
  List<UsulanModel> dataListModel = [];
  int _page = 1;
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    AnalyticsHelper.setLogEvent(Analytics.EVENT_VIEW_LIST_MY_USULAN);

    _usulanListBloc = BlocProvider.of<MyUsulanListBloc>(context);
    _usulanListBloc.add(MyUsulanListLoad(page: _page));
    _initialize();
    _scrollController.addListener(() {
      _scrollListener();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isBackRefreshPage) {
      setState(() {
        dataListModel.clear();
        _page = 1;
        _usulanListBloc.add(MyUsulanListRefresh());
      });
      widget.usulanScreenState.setState(() {
        widget.usulanScreenState.isBackRefreshPage = false;
      });
    }

    return BlocBuilder<MyUsulanListBloc, MyUsulanListState>(
        bloc: _usulanListBloc,
        builder: (context, state) {
          return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: _refresh,
            child: state is MyUsulanListLoading
                ? _buildListLoading()
                : state is MyUsulanListLoaded
                    ? state.records.isNotEmpty
                        ? _myBuildContentUsulan(state)
                        : EmptyData(message: Dictionary.empty)
                    : state is MyUsulanListFailure
                        ? _buildListFailure(state)
                        : Container(),
          );
        });
  }

  //Build widget when request success
  _myBuildContentUsulan(MyUsulanListLoaded state) {
    if (state.isLoadData) {
      _page++;
      maxDatalength = state.maxData;
      dataListModel.addAll(state.records);
      state.isLoadData = false;
    }

    if (dataListModel.isNotEmpty) {
      return Container(
        padding: const EdgeInsets.all(16),
        child: ListView.builder(
          controller: _scrollController,
          itemCount: dataListModel.length + 1,
          itemBuilder: (BuildContext context, int index) {
            return index >= dataListModel.length
                ? maxDatalength != dataListModel.length
                    ? Padding(
                        padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                        child: Column(
                          children: <Widget>[
                            CupertinoActivityIndicator(),
                            const SizedBox(
                              height: 5.0,
                            ),
                            Text(Dictionary.loadingData),
                          ],
                        ),
                      )
                    : Container()
                : _loadCard(dataListModel[index]);
          },
        ),
      );
    } else {
      return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        alignment: Alignment.center,
        child: Text(
          Dictionary.empty,
          style: TextStyle(
              color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ),
      );
    }
  }

  Widget _loadCard(UsulanModel records) {
    return GestureDetector(
        onTap: () {
          _navigateResult(context, records);
        },
        child: Container(
          child: Card(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width / 1.2,
                    alignment: Alignment.topRight,
                    child: Image.asset(
                        '${Environment.imageAssets}' +
                            UsulanHelper.getStringLabel(records.statusLabel),
                        height: 14,
                        width: 25),
                  ),
                  Container(
                    margin:
                        const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width / 5,
                          height: MediaQuery.of(context).size.height / 10,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: CachedNetworkImage(
                              imageUrl: records.attachments != null
                                  ? records.attachments[0].url
                                  : "",
                              fit: BoxFit.fill,
                              placeholder: (context, url) => Center(
                                  heightFactor: 8.2,
                                  child: CupertinoActivityIndicator()),
                              errorWidget: (context, url, error) => Container(
                                  height:
                                      MediaQuery.of(context).size.height / 3.3,
                                  color: Colors.grey[200],
                                  child: Image.asset(
                                      '${Environment.imageAssets}placeholder.png',
                                      fit: BoxFit.fitWidth)),
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.6,
                          child: Column(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width,
                                padding: const EdgeInsets.only(left: 12),
                                child: Text(
                                  records.title,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.left,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              Container(
                                  width: MediaQuery.of(context).size.width,
                                  padding:
                                      const EdgeInsets.only(left: 12, top: 8),
                                  child: Text(
                                    records.category.name,
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 14.0),
                                    textAlign: TextAlign.left,
                                  )),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                padding:
                                    const EdgeInsets.only(left: 12, top: 8),
                                child: Text(
                                  records.statusLabel,
                                  style: TextStyle(
                                      color: UsulanHelper.getColor(
                                          records.statusLabel),
                                      fontSize: 12.0,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: FontsFamily.productSans),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              UsulanHelper.getDatePublish(
                                          records.submittedAt,
                                          records.approvedAt,
                                          records.statusLabel) !=
                                      0
                                  ? Container(
                                      width: MediaQuery.of(context).size.width,
                                      padding: const EdgeInsets.only(
                                          left: 12, top: 10),
                                      child: Text(
                                        unixTimeStampToDateTime(
                                            UsulanHelper.getDatePublish(
                                                records.submittedAt,
                                                records.approvedAt,
                                                records.statusLabel)),
                                        style: TextStyle(
                                            color: Colors.grey[600],
                                            fontSize: 11.0),
                                        textAlign: TextAlign.left,
                                      ),
                                    )
                                  : Container()
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }

  //Set widget failure if data error
  _buildListFailure(MyUsulanListFailure state) {
    return ErrorContent(error: state.error);
  }

  //Loading List My Usulan
  _buildListLoading() {
    return ListView.separated(
      itemCount: 5,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          margin: const EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Skeleton(
                    margin: 10,
                    width: MediaQuery.of(context).size.width / 5,
                    height: MediaQuery.of(context).size.height / 8,
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width / 1.5,
                        child: Skeleton(height: 12, margin: 4),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 1.5,
                        child: Skeleton(height: 12, margin: 4),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 1.5,
                        child: Skeleton(height: 10, margin: 4),
                      )
                    ],
                  )
                ],
              ),
            ],
          ),
        );
      },
      separatorBuilder: (BuildContext context, int dex) => Divider(),
    );
  }

  Future<void> _initialize() async {
    maxDatalength = await Preferences.getUsulanTotalCountMe();
  }

  Future<void> _refresh() async {
    dataListModel.clear();
    _usulanListBloc.add(MyUsulanListRefresh());
    _page = 1;
  }

  void _scrollListener() {
    if (dataListModel.length != maxDatalength) {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _usulanListBloc.add(MyUsulanListLoad(page: _page));
      }
    }
  }

  _navigateResult(BuildContext context, UsulanModel records) async {
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => DetailUsulanScreen(
                  idUsulan: records.id,
                  isUsulanSaya: true,
                )));
    var isMyStatLoad = await Preferences.getMyUsulanStat();
    if (result == Dictionary.successDeleteUsulan) {
      _page = 1;
      dataListModel.clear();
      _usulanListBloc.add(MyUsulanListRefresh());
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text(Dictionary.successDeleteUsulan),
        ),
      );
    } else if (result == Dictionary.successSendUsulan) {
      _page = 1;
      dataListModel.clear();
      _usulanListBloc.add(MyUsulanListRefresh());
    } else if (isMyStatLoad) {
      _page = 1;
      dataListModel.clear();
      await Preferences.setMyUsulanStat(false);
      _usulanListBloc.add(MyUsulanListRefresh());
    }
  }

  @override
  void dispose() {
    _usulanListBloc.close();
    _scrollController.dispose();
    super.dispose();
  }
}
