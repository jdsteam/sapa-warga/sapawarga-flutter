import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/enums/AttachmentType.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/models/AddPhotoModel.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';

class AttachmentRepository {
  Future<AddPhotoModel> addPhoto(File image, AttachmentType type) async {
    String stringType;

    switch (type) {
      case AttachmentType.phonebook_photo:
        stringType = 'phonebook_photo';
        break;

      case AttachmentType.aspirasi_photo:
        stringType = 'aspirasi_photo';
        break;

      case AttachmentType.news_photo:
        stringType = 'news_photo';
        break;

      case AttachmentType.user_post_photo:
        stringType = 'user_post_photo';
        break;

      case AttachmentType.banner_photo:
        stringType = 'banner_photo';
        break;

      case AttachmentType.popup_photo:
        stringType = 'popup_photo';
        break;

      case AttachmentType.user_photo:
        stringType = 'user_photo';
        break;

      case AttachmentType.news_important:
        stringType = 'news_important';
        break;

      case AttachmentType.news_important_attachment:
        stringType = 'news_important_attachment';
        break;

      default:
        stringType = 'user_post_photo';
    }

    String token = await AuthRepository().getToken();

    Uri uri = Uri.parse('${EndPointPath.attachment}');
    http.MultipartRequest request = http.MultipartRequest('POST', uri);

    request.headers['Authorization'] = 'Bearer $token';

    request.files.add(await http.MultipartFile.fromPath('file', image.path));

    request.fields['type'] = stringType;

    final response = await request.send().timeout(Environment.requestTimeout);

    String responseBody = await response.stream.transform(utf8.decoder).join();

    if (response.statusCode == 200) {
      return addPhotoModelFromJson(json.encode(json.decode(responseBody)));
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else if (response.statusCode == 422) {
      Map<String, dynamic> responseData = json.decode(responseBody);
      throw ValidationException(responseData['data']);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
}
