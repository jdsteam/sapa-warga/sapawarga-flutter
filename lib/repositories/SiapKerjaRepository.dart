import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/constants/UrlThirdParty.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';

class SiapKerjaRepository {
  Future<String> generateTokenTotp(
      {@required Map<String, dynamic> envSiapKerja}) async {
    await Future.delayed(Duration(seconds: 1));

    Map requestData = {
      'clientName': envSiapKerja['clientName'],
      'clientSecret': envSiapKerja['clientSecret'],
    };

    var requestBody = json.encode(requestData);

    var response = await http
        .post(
          '${UrlThirdParty.siapKerja}/orgs/${envSiapKerja['orgsId']}/projects/${envSiapKerja['projectsId']}/auth/token',
          headers: await HttpHeaders.headers(),
          body: requestBody,
        )
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      Map<String, dynamic> responseData = json.decode(response.body);

      return responseData['token'];
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else if (response.statusCode == 422) {
      Map<String, dynamic> responseData = json.decode(response.body);
      throw ValidationException(responseData['data']);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  Future<String> loginOrSignUp(
      {@required UserInfoModel userInfoModel,
      @required String tOtpToken,
      @required Map<String, dynamic> envSiapKerja}) async {
    await Future.delayed(Duration(seconds: 1));

    Map requestData = {
      'email': userInfoModel.email ?? '',
      'name': userInfoModel.name ?? '',
      'roleId': envSiapKerja['roleId'] ?? '',
    };

    var requestBody = json.encode(requestData);

    var response = await http
        .post(
          '${UrlThirdParty.siapKerja}/orgs/${envSiapKerja['orgsId']}/projects/${envSiapKerja['projectsId']}/auth/user',
          headers: await HttpHeaders.headers(totpToken: tOtpToken),
          body: requestBody,
        )
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      Map<String, dynamic> responseData = json.decode(response.body);

      return responseData['token'];
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else if (response.statusCode == 422) {
      Map<String, dynamic> responseData = json.decode(response.body);
      throw ValidationException(responseData['data']);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  Future<bool> sendProfile(
      {@required UserInfoModel userInfoModel,
      @required Map<String, dynamic> envSiapKerja}) async {
    await Future.delayed(Duration(seconds: 1));
    String accessToken = await Preferences.getAccessTokenSiapKerja();

    Map requestData = {
      'token': accessToken ?? '',
      'nik': userInfoModel.nik ?? '',
      'firstName': userInfoModel.name ?? '',
      'lastName': '',
      'address': userInfoModel.address ?? '',
      'email': userInfoModel.email ?? '',
      'lastGraduate': userInfoModel.educationLevel.title ?? '',
      'gender': '',
      'birthDate':
          DateFormat('dd-MM-yyyy', 'id').format(userInfoModel.birthDate) ?? '',
      'labourPicture': '',
    };

    var requestBody = json.encode(requestData);

    var response = await http
        .post(
          '${UrlThirdParty.siapKerja}/orgs/${envSiapKerja['orgsId']}/projects/${envSiapKerja['projectsId']}/functions/updateUserProfile/execute',
          headers: await HttpHeaders.headers(token: accessToken),
          body: requestBody,
        )
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      Map<String, dynamic> responseData = json.decode(response.body);

      return responseData['result']['ok'];
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else if (response.statusCode == 422) {
      Map<String, dynamic> responseData = json.decode(response.body);
      throw ValidationException(responseData['data']);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
}
