import 'dart:convert';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:http/http.dart' as http;
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/GamificationDetailOnProgressModel.dart';
import 'package:sapawarga/models/GamificationOnprogressModel.dart';
import 'package:sapawarga/models/GamificationsModel.dart';
import 'AuthRepository.dart';

class GamificationsRepository {

  //get request list new mission
  Future<GamificationModel> getNewMission({int page, int status}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.gamifications}?page=$page&limit=20&status$status',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = gamificationFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get request list mission on progress
  Future<GamificationOnprogressModel> getMissionOnProgress({int page}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.gamificationsMe}?page=$page&limit=20',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = gamificationOnProgressFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get request list mission history
  Future<GamificationOnprogressModel> getMissionHistory({int page}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.gamificationsMe}?old=true&page=$page&limit=20',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = gamificationOnProgressFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get request  detail mission on progress & history
  Future<GamificationDetailOnProgressModel> getDetailMission({int id}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.gamificationsDetail}/$id',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);


    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = gamificationDetailFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get request detail new mission
  Future<ItemGamification> getNewMissionDetail({int id}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.gamifications}/$id',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = itemGamificationFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //request for take mission
  Future<bool> takeMission({int id}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .post('${EndPointPath.takeMission}/$id',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);


    if (response.statusCode == 200) {
      return true;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get my badge mission
  Future<GamificationOnprogressModel> getMyBadge({int page}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.gamificationsMyBadge}?page=$page&limit=20',
        headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = gamificationOnProgressFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

}
