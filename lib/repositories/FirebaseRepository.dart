import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';

class FirebaseRepository {
  Future<RemoteConfig> connectRemoteConfig() async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    await remoteConfig.setDefaults(<String, dynamic>{
      FirebaseConfig.highlightMenuKey: FirebaseConfig.highlightMenuValue,
      FirebaseConfig.announcementKey: FirebaseConfig.announcementValue,
      FirebaseConfig.secretEnvSiapKerja: FirebaseConfig.secretEnvSiapKerjaValue,
      FirebaseConfig.descTermCondition: FirebaseConfig.descTermConditionValue,
      FirebaseConfig.package: FirebaseConfig.packageValue,
      FirebaseConfig.code: FirebaseConfig.codeValue,
      FirebaseConfig.ramadhanWidget: FirebaseConfig.ramadhanWidgetValue
    });

    try {
      await remoteConfig.fetch(expiration: Duration(minutes: 15));
      await remoteConfig.activateFetched();
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }

    return remoteConfig;
  }
}
