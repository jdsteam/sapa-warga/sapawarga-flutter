import 'dart:convert';

import 'package:sapawarga/models/HelpModel.dart';

class HelpRepository {
  List<HelpModel> fetchRecords(String helpData) {
    List<dynamic> data = jsonDecode(helpData);

    final list = helpModelFromJson(jsonEncode(data));
    return list;
  }
}
