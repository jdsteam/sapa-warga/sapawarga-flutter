import 'package:flutter/material.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/AreaModelCategory.dart';
import 'package:http/http.dart' as http;

class AreaRepository {
  static int provinceCode = 1;
  static int cityCode = 2;
  static int subDistrictCode = 3;
  static int urbanVillageCode = 4;

  Future<AreaModelCategory> getCategoryListAreaPublic(
      {@required String depth, String id}) async {
    await Future.delayed(Duration(milliseconds: 500));
    String params;

    if (depth == cityCode.toString()) {
      params = 'parent_id=1&all=true&depth=$depth';
    } else {
      params = 'parent_id=$id&all=true&depth=$depth';
    }

    final response = await http
        .get('${EndPointPath.categoryAreaListPublic}?$params',
            headers: await HttpHeaders.headers())
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      var data = areaModelCategoryFromJson(response.body);
      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
}
