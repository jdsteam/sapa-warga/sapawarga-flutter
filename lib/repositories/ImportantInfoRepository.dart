import 'dart:convert';

import 'package:meta/meta.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/ImportantInfoCommentModel.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:http/http.dart' as http;
import 'package:sapawarga/utilities/SharedPreferences.dart';

class ImportantInfoRepository {
  Future<List<ImportantInfoModel>> fetchRecords(int categoryId, {int limit}) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(seconds: 1));

    final response = await http
        .get('${EndPointPath.importantInfo}${categoryId > 0 ? '?category_id=$categoryId${limit != null ? '&limit=$limit' : ''}' : '${limit != null ? '?limit=$limit' : ''}'}',
        headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];

      final list = listImportantInfoModelFromJson(jsonEncode(data));

      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  Future<List<ImportantInfoModel>> searchRecords({@required String keyword, @required int page}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.importantInfo}?search=$keyword&page=$page',
        headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];
      int totalCount =  jsonDecode(response.body)['data']['_meta']['totalCount'];

      await Preferences.setTotalCount(totalCount);

      final list = listImportantInfoModelFromJson(jsonEncode(data));

      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  Future<List<ImportantInfoModel>> getTopFiveRecords(int categoryId) async {
    List<ImportantInfoModel> list = await fetchRecords(categoryId, limit: 5);
    return list;
  }

  Future<List<ImportantInfoModel>> getOtherRecords(int categoryId) async {
    List<ImportantInfoModel> records = await fetchRecords(categoryId, limit: 45);

    if (records.length > 5) {
      records.removeRange(0, 5);
      return records;
    } else {
      return [];
    }


  }

  Future<ImportantInfoModel> getDetail(int id) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(seconds: 1));

    final response = await http
        .get('${EndPointPath.importantInfo}/$id',
        headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
        dynamic data = jsonDecode(response.body)['data'];

        final record = importantInfoModelFromJson(jsonEncode(data));

        return record;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 404) {
      throw Exception(ErrorException.notFound);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  Future<ImportantInfoCommentModel> fetchCommentRecords({@required int id, int page}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.importantInfo}/$id/comments?page=$page&limit=5',
        headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = importantInfoCommentModelFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  Future<ItemImportantInfoComment> postComment({@required int id, @required String text}) async {
    String token = await AuthRepository().getToken();

    Map requestData = {
      'news_important_id': id,
      'text': text,
      'status': 10,
    };

    var requestBody = json.encode(requestData);

    final response = await http
        .post('${EndPointPath.importantInfo}/$id/comments',
        headers: await HttpHeaders.headers(token: token), body: requestBody)
        .timeout(Environment.requestTimeout);

    print(response.body);

    if (response.statusCode == 201) {
      dynamic resData = jsonDecode(response.body)['data'];

      var data = itemImportantInfoCommentFromJson(jsonEncode(resData));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  Future<bool> sendLike({@required int id}) async {
    String token = await AuthRepository().getToken();

    var response = await http
        .post('${EndPointPath.importantInfo}/likes/$id',
        headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception(response.statusCode);
    }
  }
}