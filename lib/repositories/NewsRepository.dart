import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/models/NewsModel.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'AuthProfileRepository.dart';

class NewsRepository {
  //for get latest news data
  Future<List<NewsModel>> getLatestNews({@required int newsId}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.newsRelated}?id=$newsId&limit=2',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];

      List<NewsModel> list = List<NewsModel>();

      try {
        for (final news in data) {
          if (news['id'] != newsId) {
            list.add(newsModelFromJson(jsonEncode(news)));
          }
        }
      } catch (e) {
        print(e);
      }

      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get data detail news
  Future<NewsModel> getNewsDetail({@required int newsId}) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(seconds: 1));

    final response = await http
        .get('${EndPointPath.news}/$newsId',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      return newsModelFromJson(json.encode(json.decode(response.body)['data']));
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 404) {
      throw Exception(ErrorException.notFound);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get data related news
  Future<NewsModel> getRelatedNews({@required int newsId}) async {
    String token = await AuthRepository().getToken();

    await Future.delayed(Duration(seconds: 1));

    final response = await http
        .get('${EndPointPath.newsRelated}/$newsId',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      return newsModelFromJson(json.encode(json.decode(response.body)['data']));
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //for get city name
  Future<String> getCityName() async {
    UserInfoModel userInfo = await AuthProfileRepository().getUserInfo();
    return userInfo.kabkota.name;
  }

  //get data news
  Future<List<NewsModel>> getNews(
      {@required bool isKotaId, @required int page}) async {
    String token = await AuthRepository().getToken();
    UserInfoModel userInfo = await AuthProfileRepository().getUserInfo();
    String queryParameter;
    int pageNews = page ?? 1;

    queryParameter = "?page=$pageNews&limit=20";

    if (isKotaId) {
      queryParameter = queryParameter + '&kabkota_id=' + userInfo.kabkotaId;
    }

    final response = await http
        .get('${EndPointPath.news}$queryParameter',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];
      List<NewsModel> list = List<NewsModel>();

      var totalCount =
          jsonDecode(response.body)['data']['_meta']['totalCount'].toString();

      await Preferences.setTotalCount(int.parse(totalCount));

      try {
        for (final news in data) {
          list.add(newsModelFromJson(jsonEncode(news)));
        }
      } catch (e) {
        print(e);
      }

      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //get data featured from news
  Future<List<NewsModel>> getFeaturedNews({@required bool isKotaId}) async {
    String token = await AuthRepository().getToken();
    UserInfoModel userInfo = await AuthProfileRepository().getUserInfo();
    String kabkotaId;

    if (isKotaId) {
      kabkotaId = '?kabkota_id=' + userInfo.kabkotaId;
    } else {
      kabkotaId = "";
    }

    final response = await http
        .get('${EndPointPath.newsFeatured}$kabkotaId',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data'];
      List<NewsModel> list = List<NewsModel>();
      try {
        for (final news in data) {
          if (news != null) {
            list.add(newsModelFromJson(jsonEncode(news)));
          }
        }
      } catch (e) {
        print(e.toString());
      }
      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  //for send status like on news detail
  Future<bool> sendLike({@required int id}) async {
    String token = await AuthRepository().getToken();

    var response = await http
        .post('${EndPointPath.news}/likes/$id',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception(response.statusCode);
    }
  }
}
