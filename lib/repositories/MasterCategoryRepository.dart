import 'dart:convert';

import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/enums/CategoryTypes.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';

import 'AuthRepository.dart';
import 'package:http/http.dart' as http;

class MasterCategoryRepository {

  Future<List<MasterCategoryModel>> getMasterCategories(CategoryType type) async {

    String token = await AuthRepository().getToken();
    final response = await http
        .get('${EndPointPath.masterCategory}?type=${CategoryTypes.getString(type)}',
        headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];

      var list = listMasterCategoryFromJson(jsonEncode(data));
      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

}