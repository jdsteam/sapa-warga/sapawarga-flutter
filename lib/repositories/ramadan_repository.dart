import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:http/http.dart' as http;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/ramadan_model.dart';

class RamadanRepository {
  Future<Item> getRamadan(kabkota, String url) async {
    final response = await http.get(url).timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      final respon = jsonDecode(response.body);

      final data = ramadanModelFromJson(jsonEncode(respon));

      List<Item> dataRamadan;
      Item dataRamadanToday;

      // get day
      int dayNow = DateTime.now().day;

      // filter kabkota
      data.forEach((element) {
        if (element.kabkot == kabkota) {
          dataRamadan = element.items;
        }
      });

      if (dataRamadan == null) return null;

      // filter date
      dataRamadan.forEach((element) {
        Timestamp stamp = Timestamp.fromMillisecondsSinceEpoch(element.tanggal);
        DateTime date = stamp.toDate();

        // get day
        int dayDate = date.day;
        if (dayNow == dayDate) {
          dataRamadanToday = element;
        }
      });

      return dataRamadanToday;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
}
