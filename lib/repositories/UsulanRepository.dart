import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http_parser/http_parser.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/configs/DBProvider.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/models/AddPhotoModel.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/models/UsulanModel.dart';
import 'package:http/http.dart' as http;
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:sqflite/sqflite.dart';
import 'AuthRepository.dart';

class UsulanRepository {
  //for get data usulan
  Future<List<UsulanModel>> getUsulan({int page}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.usulan}?page=$page&limit=20',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];

      var totalUsulan =
          jsonDecode(response.body)['data']['_meta']['totalCount'].toString();

      await Preferences.setTotalCount(int.parse(totalUsulan));

      var list = usulanModelFromJson(jsonEncode(data));

      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
  //request for delete usulan
  Future<bool> deleteUsulan({@required int idUsulan}) async {
    String token = await AuthRepository().getToken();

    await http
        .delete('${EndPointPath.usulan}/$idUsulan',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    return true;
  }
  //request for like usulan
  Future<bool> likeUsulan({@required int id}) async{
    String token = await AuthRepository().getToken();

   var response = await http
        .post('${EndPointPath.likeUsulan}/$id',
        headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);
    if (response.statusCode == 200) {
      return true;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else if (response.statusCode == 422) {
      Map<String, dynamic> responseData = json.decode(response.body);

      throw ValidationException(responseData['data']);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }

  }
  //request for send usulan
  Future<bool> sendUsulan(
      {@required String title,
      @required String description,
      @required String status,
      @required String categoryId,
      @required List<AddPhotoModel> attachment, int id}) async {
    String token = await AuthRepository().getToken();
    AuthProfileRepository authProfileRepository = AuthProfileRepository();
    UserInfoModel userInfoModel =
        await authProfileRepository.readLocalUserInfo();
    await Future.delayed(Duration(seconds: 1));

    List attachJson;
    List<Map> dataAttach = [];

    if (attachment.isNotEmpty) {
      for (int i = 0; i < attachment.length; i++) {
        Map requestDataPhoto = {
          'type': 'photo',
          'path': attachment[i].data.path
        };

        dataAttach.add(requestDataPhoto);
      }
      attachJson = dataAttach;
    }
    Map requestData = {
      'title': title,
      'description': description,
      'kabkota_id': userInfoModel.kabkotaId,
      'kec_id': userInfoModel.kecId,
      'kel_id': userInfoModel.kelId,
      'status': status,
      'category_id': categoryId,
      'attachments': attachJson,
    };
    var requestBody = json.encode(requestData);
    var response;
    if (id != null) {
      response = await http
          .put('${EndPointPath.usulan}/$id',
              headers: await HttpHeaders.headers(token: token),
              body: requestBody)
          .timeout(Environment.requestTimeout);
    } else {
      response = await http
          .post('${EndPointPath.usulan}',
              headers: await HttpHeaders.headers(token: token),
              body: requestBody)
          .timeout(Environment.requestTimeout);
    }

    if (response.statusCode == 200 || response.statusCode == 201) {
      return true;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else if (response.statusCode == 422) {
      Map<String, dynamic> responseData = json.decode(response.body);

      throw ValidationException(responseData['data']);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
  //request for get list my usulan
  Future<List<UsulanModel>> getMyUsulan({int page}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.usulan}/me?page=$page&limit=20',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];

      var totalUsulan =
          jsonDecode(response.body)['data']['_meta']['totalCount'].toString();

      await Preferences.setUsulanTotalCountMe(int.parse(totalUsulan));

      var list = usulanModelFromJson(jsonEncode(data));

      return list;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
  //request for get detail usulan
  Future<UsulanModel> getUsulanDetail({int id}) async {
    String token = await AuthRepository().getToken();

    final response = await http
        .get('${EndPointPath.usulan}/$id',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      return usulanDetailModelFromJson(
          json.encode(json.decode(response.body)['data']));
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
  //request for get category usulan
  Future<void> getCategoryUsulan({int page}) async {
    String token = await AuthRepository().getToken();
    final response = await http
        .get('${EndPointPath.masterCategory}?type=aspirasi&page=$page&limit=20',
            headers: await HttpHeaders.headers(token: token))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['data']['items'];

      var list = listMasterCategoryFromJson(jsonEncode(data));
      list.forEach((record) {
        insertToDatabaseCategoryUsulan(record);
      });
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
  //request for add foto send usulan
  Future<AddPhotoModel> addPhoto(image) async {
    AuthRepository authRepository = AuthRepository();

    String token = await authRepository.getToken();

    Uri uri = Uri.parse('${EndPointPath.attachment}');
    http.MultipartRequest request = http.MultipartRequest('POST', uri);

    request.headers['Authorization'] = 'Bearer $token';

    request.files.add(await http.MultipartFile.fromPath('file', image.path,
        contentType: MediaType('file', 'jpeg')));
    request.fields['type'] = 'aspirasi_photo';

    http.StreamedResponse response = await request.send();

    String responseBody = await response.stream.transform(utf8.decoder).join();

    if (response.statusCode == 200) {
      return addPhotoModelFromJson(json.encode(json.decode(responseBody)));
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else if (response.statusCode == 422) {
      Map<String, dynamic> responseData = json.decode(responseBody);
      throw ValidationException(responseData['data']);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
  //get data list usulan from db
  Future<List<UsulanModel>> getRecordsLocal() async {
    final jsonData = await Preferences.getUsulanList();

    return usulanModelFromJson(jsonData);
  }

  Future<bool> hasRecordsLocal() async {
    bool hasRecords = await Preferences.hasUsulanList();
    return hasRecords;
  }
  //insert data category into database
  Future<void> insertToDatabaseCategoryUsulan(
      MasterCategoryModel record) async {
    Database db = await DBProvider.db.database;
    try {
      await db.insert(
        'CategoryUsulan',
        record.toJson(),
        conflictAlgorithm: ConflictAlgorithm.ignore,
      );
    } catch (e) {
      print(e.toString());
    }
  }

  Future<bool> hasLocalDataCategoryUsulan() async {
    Database db = await DBProvider.db.database;

    int count = Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM CategoryUsulan'));

    return count > 0;
  }
  //get data category usulan from db
  Future<List<MasterCategoryModel>> getLocalDataCategoryUsulan() async {
    Database db = await DBProvider.db.database;

    var res = await db.query('CategoryUsulan');

    List<MasterCategoryModel> list = res.isNotEmpty
        ? res.map((c) => MasterCategoryModel.fromJson(c)).toList()
        : [];

    return list;
  }

  Future<List<MasterCategoryModel>> getRecordsCategoryUsulan(
      {bool forceRefresh = false, int page = 1}) async {
    bool hasLocal = await hasLocalDataCategoryUsulan();

    if (hasLocal == false || forceRefresh == true) {
      await getCategoryUsulan(page: page);
    }

    List<MasterCategoryModel> localRecords = await getLocalDataCategoryUsulan();

    return localRecords;
  }
}
