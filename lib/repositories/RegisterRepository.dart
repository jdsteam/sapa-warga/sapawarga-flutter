import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/EndPointPath.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/constants/HttpHeaders.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';

class RegisterRepository {
  Future<bool> sendRegister({
    @required String name,
    @required String password,
    @required String email,
    @required String phone,
    @required String kabkotaId,
    @required String kecId,
    @required String kelId,
    @required String rw,
    @required String role,
  }) async {
    await Future.delayed(Duration(seconds: 1));
    String rwRegister = rw.length > 1 ? '0' + rw : '00' + rw;

    Map requestData = {
      'SignupForm': {
        'name': name,
        'password': password,
        'email': email,
        'phone': phone,
        'kabkota_id': kabkotaId,
        'kec_id': kecId,
        'kel_id': kelId,
        'rw': rwRegister,
        'role': role
      }
    };

    var requestBody = json.encode(requestData);

    var response = await http
        .post('${EndPointPath.register}',
            headers: await HttpHeaders.headers(), body: requestBody)
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      return true;
    }
    if (response.statusCode == 201) {
      return true;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else if (response.statusCode == 422) {
      Map<String, dynamic> responseData = json.decode(response.body);
      throw ValidationException(responseData['data']);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }

  Future<bool> activationAccount({
    @required String activationToken,
  }) async {
    await Future.delayed(Duration(seconds: 1));

    var response = await http
        .get('${EndPointPath.activationAccount}',
            headers:
                await HttpHeaders.headers(activationToken: activationToken))
        .timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      return true;
    }
    if (response.statusCode == 201) {
      return true;
    } else if (response.statusCode == 401) {
      Map<String, dynamic> responseData = json.decode(response.body);
      throw Exception(responseData['data']);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else if (response.statusCode == 422) {
      Map<String, dynamic> responseData = json.decode(response.body);
      throw ValidationException(responseData['data']);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
}
