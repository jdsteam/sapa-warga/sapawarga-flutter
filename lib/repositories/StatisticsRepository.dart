import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/ErrorException.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/models/statistics_pikobar_model.dart';

class StatisticsRepository {
  Future<StatisticsPikobarModel> getStatisticsPikobar(apiUrl, apiKey) async {
    final response = await http.get(apiUrl,
        headers: {'api-key': apiKey}).timeout(Environment.requestTimeout);

    if (response.statusCode == 200) {
      final respon = jsonDecode(response.body);

      final data = statisticsPikobarModelFromJson(jsonEncode(respon));

      return data;
    } else if (response.statusCode == 401) {
      throw Exception(ErrorException.unauthorizedException);
    } else if (response.statusCode == 408) {
      throw Exception(ErrorException.timeoutException);
    } else {
      throw Exception(Dictionary.somethingWrong);
    }
  }
}
