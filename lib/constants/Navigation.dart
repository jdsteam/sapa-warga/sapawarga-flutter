import 'package:flutter/material.dart';

class NavigationConstrants {
  static final navKey = GlobalKey<NavigatorState>();

  // Pages routes
  static const String Service = "/layanan";
  static const String Survey = "/survey";
  static const String Polling = "/polling";
  static const String PollingDetail = "/polling-detail";
  static const String Phonebook = "/phonebook";
  static const String importanInformation = "/important-information";
  static const String Esamsat = "/esamsat";
  static const String RWActivity = "/rw-activity";
  static const String QnAGovernor = "/qnagovernor";
  static const String Browser = "/browser";
  static const String Aspirasi = "/aspirasi";
  static const String infoPKB = "/info-pkb";
  static const String Lapor = "/lapor";
  static const String SaberHoax = "/saber-hoax";
  static const String Pikobar = "/pikobar";
  static const String SaberHoaxDetail = "/saber-hoax-detail";
  static const String ForgotPassword = "/forgot-password";
  static const String ChangePasswordLogin = "/change-password-login";
  static const String ChangePassword = "/change-password";
  static const String Mission = "/mission";
  static const String BroadcastDetail = "/broadcast-detail";
  static const String NewsIndex = "/news-index";
  static const String NewsDetail = "/news-detail";
  static const String NotificationList = "/notification-list";
  static const String ImportantInfoList = "/important-info-list";
  static const String AdministrationList = "/administration-list";
  static const String SubProfile = "/sub-profile";
  static const String SubContact = "/sub-contact";
  static const String SubAddress = "/sub-address";
  static const String UpdateUsername = "/update-username";
  static const String VerificationCode = "/verfication-code";
  static const String VerificationPasswrod = "/verfication-password";
  static const String VerificationUsername = "/verfication-username";
  static const String Register = "/register";
  static const String RegisterForm = "/registerForm";
  static const String RegisterRwRegion = "/registerRwRegion";
  static const String RegisterVerification = "/registerVerification";
  static const String RegisterActivation = "/registerActivation";
  static const String RegisterErrorPage = "/registerErrorPage";
  static const String ForgotPasswordLogin = "/forgotPasswordLogin";
  static const String ForgotUsernameLogin = "/forgotUsernameLogin";
  static const String addEmail = "/addEmail";
  static const String termConditionSiapKerja = "/termConditionSiapKerja";
}
