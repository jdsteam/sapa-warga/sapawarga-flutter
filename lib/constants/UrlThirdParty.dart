class UrlThirdParty {
  static const String perizinan =
      'https://dpmptsp.jabarprov.go.id/sicantik/main/pendaftaranbaru';
  static const String tokopedia =
      'https://www.tokopedia.com/pajak/samsat/jawa-barat';
  static const String bukalapak =
      'https://www.bukalapak.com/bukajabar/e-samsat';
  static const String sambara = 'id.go.bapenda.sambara';
  static const String pathPlaystore =
      'https://play.google.com/store/apps/details?id=';
  static const String infoLelang = 'https://lpse.jabarprov.go.id/eproc4';

  static const String privacyPolicy =
      'https://digitalservice.jabarprov.go.id/index.php/privacy-policy-2/';
  static const String termOfService =
      'https://digitalservice.jabarprov.go.id/index.php/term-of-service/';
  static const String manualBook =
      'https://drive.google.com/uc?export=download&id=1rFPMmLGHpK0X6c1NftzdSrYQAHcPIzSU';

  static const String apiHumasJabar = 'http://humas.jabarprov.go.id/api';
  static const String newsHumasJabarTerkini =
      'http://humas.jabarprov.go.id/terkini';
  static const String newsHumasJabar = apiHumasJabar + '/berita-terkini';

  static const String urlJQR = 'https://jabarqr.id';

  static String urlPlayStore = 'market://details?id=com.sapawarga.jds';
  static String urlPlayStoreAlt =
      'https://play.google.com/store/apps/details?id=com.sapawarga.jds&hl=in';
  static String whatsapp = 'com.whatsapp';
  static String urlPathFacebook = 'fb://facewebmodal/f?href=';
  static String urlPathInstagram = 'https://www.instagram.com/';
  static String urlPathTwitter = 'http://mobile.twitter.com/';
  static String urlImageErrorPlacholder =
      'https://fomantic-ui.com/images/image-16by9.png';
  static String urlCommunityGuideLine =
      'https://digitalservice.jabarprov.go.id/index.php/panduan-komunitas-sapawarga/';
  static String urlGuideRwActivities =
      'https://digitalservice.jabarprov.go.id/index.php/panduan-komunitas-pada-kegiatan-rw/';
  static String urlInfoPKB = 'https://bapenda.jabarprov.go.id/infopkb/';
  static String urlPikobar = 'https://pikobar.jabarprov.go.id/';
  static String urlWhatsapp = 'market://details?id=com.whatsapp';
  static String siapKerja = 'https://prod-qore-app.qorebase.io';
}
