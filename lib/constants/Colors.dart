import 'dart:ui' show Color;

class Colors {
  static final blue = Color(0xFF00AADE);
  static final darkblue = Color(0xFF1565C0);
  static final green = Color(0xFF009D56);
  static final lightGreen = Color(0xFFE6F6EC);
  static final darkGreen = Color(0xFF006430);
  static final grey = Color(0xFFE2E2E2);
  static final pink = Color(0xFFD71149);
  static final bubbleChatBlue = Color(0xFFD5E9F4);
  static final darkRed = Color(0xFFD23C3C);
  static final greyContainer = Color(0xFFFAFAFA);
  static final greyBorder = Color(0xffEEEEEE);
  static final darkGrey = Color(0xff828282);
  static final greyText = Color(0xFF616161);
  static final menuBorderColor = Color(0xFFE0E0E0);
  static final blueCardColor = Color(0xFFE3F2FD);

  static final gradientBlue = [Color(0xFF00AADE), Color(0xFF0669B1)];
  static final gradientBlueToPurple = [Color(0xFF005C97), Color(0xFF363795)];
  static final gradientBlueOpacity = [
    Color(0xFF00AADE),
    Color(0xFF0669B1).withOpacity(0.8)
  ];
  static final gradientBlackOpacity = [
    Color(0xFF000000).withOpacity(0.1),
    Color(0xFF000000).withOpacity(0.7)
  ];

  static final yellow = Color(0xfffdcc3b);
  static final orange = Color(0xffe8b638);

  static final disableText = Color(0xffBDBDBD);
  static final netralGrey = Color(0xFF757575);
  static final veryDarkGrey = Color(0xff333333);
  static final greyBackground = Color(0xffF5F5F5);
  static final greyText2 = Color(0xff9E9E9E);
}
