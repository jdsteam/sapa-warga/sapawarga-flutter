class Dictionary {
  static String appName = 'Sapawarga';
  static String version = '0.0.3';

  // Menu & Title
  static String home = 'Beranda';
  static String titleNews = 'JAWA BARAT';
  static String titleHumasJabar = 'Humas Jabar';
  static String importantInfo = 'Info Penting';
  static String latestInfo = 'Info Terbaru';
  static String importantInfoOther = 'Info Penting Lainnya';
  static String statistics = 'Update Terkini Covid di Jawa Barat';
  static String message = 'Pesan';
  static String notification = 'Notifikasi';
  static String help = 'Bantuan';
  static String account = 'Akun';
  static String phoneBook = 'Nomor Penting';
  static String survey = 'Survei';
  static String polling = 'Polling';
  static String aspiration = 'Usulan';
  static String report = 'Lapor';
  static String eSamsat = 'E-Samsat';
  static String saberHoax = 'Jabar Saber Hoax';
  static String pikobar = 'Info Corona';
  static String other = 'Lainnya';
  static String filter = 'Filter';
  static String nearbyLocation = 'Lokasi Terdekat';
  static String infoPKB = 'Cek Pajak\nKendaraan';
  static String otherMenus = 'Menu lainnya';
  static String login = 'Masuk';
  static String address = 'Alamat';
  static String restoreAccount = 'Pengembalian Akun';
  static String congratulation = 'Selamat';
  static String changePassword = 'Ubah Kata Sandi';
  static String completeYourProfile = 'Lengkapi Profilmu';
  static String qnaGovernor = 'Tanya Gubernur';
  static String qnaGovernorAdd = 'Buat Pertanyaan';
  static String detail = 'Detail';
  static String rwActivities = 'Kegiatan RW';
  static String myActivities = 'Kegiatan Saya';
  static String activities = 'Kegiatan';
  static String searchKabKota = 'Pilih Kabupaten / Kota';
  static String chooseKabKota = 'Cari Kabupaten / Kota';
  static String callCenterList = 'Daftar Call Center';
  static String probable = 'Probable';
  static String suspect = 'Suspek';
  static String positif = 'Terkonfirmasi';
  static String recover = 'Sembuh';
  static String die = 'Meninggal';
  static String opdDesc = 'Orang Dalam Pemantauan';
  static String pdpDesc = 'Pasien Dalam Pengawasan';

  static String heroImageTag = 'preview';
  static String unknown = 'unknown';

  static String titleLogin = 'Selamat datang di Sapawarga!';

  static String inputIdUserDesc = 'Masukkan username yang telah terdaftar';

  // menu others
  static String licensing = 'Perizinan';
  static String submitHelp = 'Ajukan Bantuan';
  static String chat = 'Chat';
  static String ppob = 'PPOB';
  static String administration = 'Administrasi';
  static String permissions = 'Perizinan';
  static String priceInfo = 'Info Harga';
  static String blancoInfo = 'Info Blanko';
  static String comment = 'Komentar';
  static String likes = 'Sukai';

  static String forgotPassword = 'Lupa password anda?';
  static String forgotPasswordLogin = 'Lupa username atau kata sandi?';
  static String empty = 'Data tidak ditemukan';
  static String offline = 'Tidak ada jaringan internet';
  static String failedSave = 'Data gagal tersimpan';
  static String successSave = 'Data berhasil tersimpan';
  static String successRequestResetPassword =
      'Tautan reset password telah dikirim ke email anda';
  static String successChangePassword = 'Kata sandi berhasil diubah';
  static String successChangeProfile = 'Profil berhasil diubah';
  static String successAddComment = 'Komentar berhasil diposting';
  static String confirmGps =
      'Untuk melanjutkan, mohon untuk mengaktifkan GPS anda';
  static String maxUploadPhoto =
      'Foto yang diupload melebihi batas maksimal file';
  static String logoutMessage =
      'Apakah Anda yakin ingin keluar dari aplikasi Sapawarga?';
  static String pollingVotedSuccessMessage = 'Polling berhasil dikirim';
  static String pollingHasVotedMessage = 'Anda sudah mengisi polling ini';
  static String somethingWrong = 'Terjadi kesalahan';
  static String copyRight = 'Hak Cipta Pemerintah Provinsi Jawa Barat';
  static String msgExitApp = 'Tekan sekali lagi untuk keluar aplikasi';
  static String termOfService = 'Ketentuan Penggunaan';
  static String privacyPolicy = 'Kebijakan Privasi';
  static String downloadGuideBook = 'Unduh Buku Panduan';
  static String titleReport = 'Laporan via Lapor';
  static String titleReportJQR = 'Lapor via Jabar Quick Response';
  static String putEmailSapawarga =
      'Masukkan email yang telah terdaftar di Sapawarga';
  static String useYourEmail = 'Gunakan alamat email anda';
  static String setYourLocation = 'Set lokasi tempat tinggal anda';
  static String putReportMessages = 'Isi laporan anda disini';
  static String city = 'Kabupaten / Kota';
  static String subDistrict = 'Kecamatan';
  static String village = 'Kelurahan';
  static String originalSource = 'Sumber Asli';
  static String originalArticle = 'Artikel Asli';
  static String likeNews = 'Sukai berita ini';
  static String news = 'Berita';
  static String newsIndex = 'Indeks Berita';
  static String videoPostJabar = 'Video Jawa Barat';
  static String videoPostKokab = 'Video Kabupaten Kota';
  static String otherNews = 'Berita Lainnya';
  static String latestNews = 'Berita Terbaru';
  static String viewAll = 'Lihat Semua';
  static String attachment = 'Lampiran';
  static String contentNotPermitted = 'Mengandung konten yang tidak diizinkan';
  static String contentNotPermittedRWActivities =
      'Postingan anda tidak sesuai dengan panduan komunitas.';
  static String readMore = ' Baca Selengkapnya';
  static String sharedFrom = '\n_dibagikan dari Sapawarga App_';
  static String moreDetail = ' Selengkapnya';
  static String people = 'Orang';

  static String later = 'Nanti';
  static String next = 'Lanjut';
  static String next2 = 'Selanjutnya';
  static String back = 'Kembali';
  static String yes = 'Ya';
  static String no = 'Tidak';
  static String cancel = 'Batal';
  static String close = 'Tutup';
  static String ok = 'Ok';
  static String fillSurvey = 'Isi Survei';
  static String vote = 'Isi Polling';
  static String send = 'Kirim';
  static String update = 'Perbaharui';
  static String save = 'Simpan';
  static String approve = 'Setuju';
  static String useGoogleAccount = 'Gunakan akun Google';
  static String setLocation = 'Set Lokasi...';
  static String addQuestion = 'Buat Pertanyaan';
  static String startQna = 'Mulai Tanya Jawab';
  static String contactUs = 'Hubungi Kami';
  static String forgotEmail = 'Lupa Email?';
  static String contactAdmin = 'Hubungi Admin';
  static String postActivity = 'Posting Kegiatan';
  static String tapHelp = 'Tap untuk dapatkan bantuan';
  static String search = 'Cari';
  static String search2 = 'Pencarian';
  static String all = 'Semua';
  static String rateIt = 'Beri Penilaian';
  static String downloadAttachment = 'Unduh Lampiran';
  static String share = 'Bagikan';

  // Form Label & Hint
  static String labelUsername = 'Username';
  static String labelPassword = 'Kata Sandi';
  static String labelHintPassword = 'Kata sandi Anda';
  static String labelName = 'Nama';
  static String labelEmail = 'Email';
  static String labelPhone = 'Nomor Telp';
  static String labelAddress = 'Alamat';
  static String labelPasswordOld = 'Masukkan Kata Sandi Lama';
  static String labelPasswordNew = 'Masukkan Kata Sandi Baru';
  static String labelPasswordNewConfirm = 'Ulangi Kata Sandi Baru';
  static String hintPasswordOld = 'Kata Sandi Lama';
  static String hintPasswordNew = 'Kata Sandi Baru';
  static String descHintPasswordNew = 'Tuliskan kata sandi baru anda';
  static String hintDescription = 'Tulis deskripsi';
  static String hintComment = 'Tulis komentar ...';
  static String hintSearch = 'Ketikkan kata kunci ...';
  static String labelInputUsernameOrPhoneNumber = 'Contoh: asep123';
  static String labelUsernameOrPhoneNumber = 'Username atau Nomor Telepon';

  // Form Validation Error
  static String errorEmptyUsername = 'Username harus diisi';
  static String errorKabupaten = 'Kabupaten harus diisi';
  static String errorKecamatan = 'Kecamatan harus diisi';
  static String errorRW = 'RW harus diisi';
  static String errorMinimumUsername = 'Username minimal 4 karakter';
  static String errorMaximumUsername =
      'Username melebihi batas maksimal karakter';
  static String errorInvalidUsername =
      'Hanya huruf kecil, angka, underscore dan titik yang diperbolehkan';
  static String errorEmptyPassword = 'Kata Sandi harus diisi';
  static String errorMinimumPassword = 'Kata sandi minimal 8 karakter';
  static String errorMaximumPassword = 'Kata Sandi maksimal 255 karakter';
  static String errorInvalidPassword =
      'Password Harus mengandung angka dan huruf ';
  static String errorEmptyEmail = 'Email harus diisi';
  static String errorInvalidEmail = 'Email tidak valid';
  static String errorEmptyConfirmPassword = 'Konfirmasi kata sandi harus diisi';
  static String errorEmptyRepeatPassword = 'Ulang kata sandi harus diisi';
  static String errorNotMatchPassword = 'Konfirmasi kata sandi tidak sesuai';
  static String errorNotMatchRepeatPassword = 'Ulang kata sandi tidak sesuai';
  static String errorEmptyName = 'Nama harus diisi';
  static String errorMinimumName =
      'Nama harus terdiri dari 4 karakter atau lebih';
  static String errorMaximumName = 'Nama harus kurang dari 255 karakter';
  static String errorInvalidName = 'Format nama tidak sesuai';
  static String errorMinimumPhone =
      'Nomor telepon harus terdiri 3 sampai 13 nomor';
  static String errorMinimumHandPhone =
      'Nomor handphone harus terdiri 3 sampai 13 nomor';
  static String errorMinimumWhatsappNumber =
      'Nomor WA harus terdiri 3 sampai 13 nomor';
  static String errorMinimumOtp = 'Kode verifikasi harus terdiri 6 digit angka';
  static String errorMaximumPhone = 'Nomor Telepon harus kurang dari 13 nomor';
  static String errorMaximumHandPhone =
      'Nomor handphone harus kurang dari 13 nomor';
  static String errorMaximumWhatsappNumber =
      'Nomor WA tidak boleh lebih dari 13 nomor';
  static String errorEmptyPhone = 'Nomor Telepon harus diisi';
  static String errorEmptyHandPhone = 'Nomor handphone harus diisi';
  static String errorWhatsappNumber = 'Nomor WA harus diisi';
  static String errorOtp = 'Kode verifikasi harus diisi';
  static String errorInvalidPhone = 'Format nomor telepon tidak sesuai';
  static String errorInvalidHandPhone = 'Format nomor handphone tidak sesuai';
  static String errorInvalidWhatsappNumber = 'Format nomor WA tidak sesuai';
  static String errorUserNotFound = 'Tidak ada user dengan email tersebut';
  static String errorMaximumAddress = 'Alamat harus kurang dari 255 karakter';
  static String errorEmptyAddress = 'Alamat harus diisi';
  static String errorMaximumRT = 'RT harus 3 angka';
  static String errorEmptyRT = 'RT harus diisi';
  static String errorInvalidRT = 'Format RT tidak sesuai';
  static String errorInvalidInstagram = 'Format instagram tidak sesuai';
  static String errorInvalidTwitter = 'Format twitter tidak sesuai';
  static String errorInvalidFacebook = 'Format facebook tidak sesuai';
  static String errorMinimumQuestion = 'Pertanyaan minimal 10 karakter';
  static String errorEmptyComment = 'Komentar tidak boleh kosong';
  static String errorMinimumComment = 'Komentar minimal 10 karakter';

  // Error messages
  static String errorConnection = 'Periksa kembali koneksi Internet';
  static String errorIOConnection = 'Koneksi ke server bermasalah';
  static String errorUrl = 'Tidak bisa mengakses halaman';
  static String errorLogin = 'Terjadi kesalahan saat login';
  static String errorTimeOut = 'Server tidak merespon';
  static String errorNotFound = 'Data tidak ditemukan';
  static String errorUnauthorized = 'Izin akses ke server ditolak';
  static String errorInternal = 'Server sedang dalam perbaikan';
  static String errorExternal =
      'Terjadi kesalahan periksa kembali koneksi internet anda';
  static String errorGetEmail = 'Gagal mendapatkan email, coba lagi';
  static String errorStatusUnActive = 'Username belum aktif';
  static String errorListComment = 'Tidak dapat menampilkan komentar';
  static String errorStatisticsNotExists =
      'Data statistik saat ini tidak tersedia, silakan coba kembali beberapa saat';

  // Dialog message
  static String permissionDownloadManualBook =
      'Untuk menyimpan buku panduan, izinkan aplikasi Sapawarga untuk mengakses penyimpanan Anda.';
  static String permissionDownloadAttachment =
      'Untuk mengunduh lampiran, izinkan aplikasi Sapawarga untuk mengakses penyimpanan Anda.';
  static String permissionGalery =
      'Untuk membuka gallery, izinkan aplikasi Sapawarga untuk mengakses penyimpanan Anda.';
  static String permissionCamera =
      'Untuk mengambil foto, izinkan aplikasi Sapawarga untuk mengakses kamera Anda.';
  static String permissionPhoneBookMap =
      'Untuk menampilkan map nomor penting di lokasi terdekat, izinkan aplikasi Sapawarga untuk mengakses lokasi Anda.';
  static String permissionLocationMap =
      'Untuk menampilkan map, izinkan aplikasi Sapawarga untuk mengakses lokasi Anda.';
  static String permissionRWActivities =
      'Untuk memposting kegiatan RW, izinkan aplikasi Sapawarga untuk mengakses penyimpanan dan kamera Anda.';
  static String updateAppAvailable =
      'Versi terbaru telah tersedia,\nsegera perbaharui aplikasi anda.';
  static String confirmExitTitle = 'Tutup Aplikasi?';
  static String confirmExit = 'Yakin akan keluar dari Sapawarga?';
  static String accountLoginInfo =
      'Untuk mendapatkan username dan password, anda dapat menghubungi nomor berikut (WhatsApp atau Telpon)\n\n';
  static String forgotEmailInfo =
      'Jika anda lupa email atau kata sandi, anda dapat menghubungi nomor berikut (WhatsApp atau Telpon)\n\n';
  static String csPhoneNumber = '081212124023';
  static String helpAdminWA =
      'Untuk mengajukan pertanyaan terkait penggunaan aplikasi sapawarga *WAJIB* mengisi data dibawah ini ya. \n\n*Nama Lengkap* : \n*RT / RW* : \n*Nomor Telepon* : \n*Kelurahan / Desa* : \n*Kecamatan* : \n*Kabupaten / Kota* : \n\n*Detail Pertanyaan* :';
  static String titleRate = 'Bantu Sapawarga Jadi Lebih Baik';
  static String descriptionRate =
      'Seberapa puas Anda dalam menggunakan aplikasi Sapawarga?';
  static String titleFailedLogin = 'Ooops!';
  static String descFailedLogin =
      'Anda telah 3 kali salah memasukan username atau nomor telepon. Hubungi hotline Sapawarga untuk mengajukan bantuan!';
  static String helpHotline = 'Hubungi bantuan';

  // Loading
  static String loadingLogin = 'Masuk...';
  static String loading = 'Mohon tunggu...';

  // Forgot Password Popup
  static String forgotPasswordInfo1 =
      'Anda dapat menghubungi Call Center Sapawarga ';
  static String forgotPasswordInfo2 = '082315192724';
  static String forgotPasswordInfo3 = ' atau menghubungi email berikut ';
  static String forgotPasswordInfo4 = 'digital.service@jabarprov.go.id';
  static String forgotPasswordLaunch1 = 'tel://082315192724';
  static String forgotPasswordLaunch2 =
      'mailto:digital.service@jabarprov.go.id?subject=Lupa%20Kata%20Sandi%20Sapawarga';

  static String loadingData = 'Sedang mengambil data ...';
  static String loadFacebookUrlFailed =
      'Link akun facebook anda belum dipasang';
  static String loadInstagramUrlFailed =
      'Link akun instagram anda belum dipasang';
  static String loadTwitterUrlFailed = 'Link akun twitter anda belum dipasang';

  static String emptyDataSaberHoax = 'Tidak ada data dalam list Saber Hoax';
  static String emptyDataPhoneBook = 'Tidak ada data dalam list Nomor Penting';
  static String emptyDataSurvey = 'Tidak ada data dalam list Survei';
  static String emptyDataPolling = 'Tidak ada data dalam list Polling';
  static String emptyDataNotifications = 'Tidak ada data dalam list Notifikasi';
  static String emptyDataMessages = 'Tidak ada data dalam list Pesan';
  static String emptyDataImportantInfo =
      'Tidak ada data dalam list Info Penting';
  static String emptyDataQnA = 'Tidak ada data dalam list Tanya Jawab';
  static String emptyDataRWActivity = 'Tidak ada data dalam list Kegiatan RW';
  static String emptyDataAdministration = 'Data yang dicari tidak ditemukan';
  static String emptyComments = 'Belum ada komentar';

  static String appVersion = 'Versi Aplikasi';
  static String changePasswordProfile = 'Ubah Password';

  //Form Edit Profile
  static String profile = 'Profil';
  static String contact = 'Kontak';
  static String name = 'Nama';
  static String birthDate = 'Tanggal Lahir';
  static String education = 'Pendidikan';
  static String job = 'Pekerjaan';
  static String username = 'Username';
  static String email = 'Email';
  static String telephone = 'Telepon';
  static String noTelp = 'Nomor Telepon';
  static String fullAddress = 'Alamat Lengkap';
  static String kabkota = 'Kabupaten / Kota';
  static String kecamatan = 'Kecamatan';
  static String kelurahan = 'Desa / Kelurahan';
  static String rw = 'RW';
  static String rt = 'RT';
  static String role = 'Role';
  static String socmed = 'Sosial Media';
  static String instagram = 'Instagram';
  static String facebook = 'Facebook';
  static String twitter = 'Twitter';
  static String saveProfile = 'Simpan';
  static String takePhoto = 'Ambil foto';
  static String takePhotoFromGallery = 'Ambil dari gallery';
  static String understand = 'Mengerti';
  static String changeProfile = 'Ganti Foto Profil';

  // place holder edit profile
  static String placeHolderName = 'Masukkan Nama';
  static String placeHolderNik = 'Masukkan NIK';
  static String placeHolderBirthday = 'Masukkan Tanggal Lahir';
  static String placeHolderEducation = 'Masukkan Pendidikan';
  static String placeHolderJob = 'Masukkan Pekerjaan';
  static String placeHolderUsername = 'Masukkan Username';
  static String placeHolderEmail = 'Masukkan Email';
  static String placeHolderNoTelp = 'Masukkan No Telepon';
  static String placeHolderAddress = 'Masukkan Alamat';
  static String placeHolderFullAddress = 'Masukkan Alamat Lengkap';
  static String placeHolderRt = 'Masukkan RT';
  static String placeHolderTwitter = 'Masukkan Twitter';
  static String placeHolderInstagram = 'Masukkan Instagram';
  static String placeHolderFacebook = 'Masukkan Facebook';

  static String successSaveProfile = 'Perubahan data berhasil disimpan.';
  static String successSendUsulan = 'Usulan berhasil dikirim.';
  static String successSaveDraft = 'Usulan berhasil disimpan.';
  static String updatePhotoTitle = 'Update Foto.';
  static String successSavePhoto = 'Foto Profile telah di rubah';
  static String successDeleteUsulan = 'Usulan berhasil dihapus';

  static String reportDescription =
      'Sarana aspirasi dan pengaduan berbasis media sosial bertujuan agar masyarakat dapat berpartisipasi untuk pengawasan program dan kinerja pemerintah dalam penyelenggaraan pembangunan dan pelayanan publik.';
  static String reportJQRDescription =
      'Layanan aduan kemanusiaan bagi masyarakat Jawa Barat yang akan diterima oleh tim Jabar Quick Response dan diseleksi berdasarkan skala prioritas masalah. Laporan yang dikirim dapat berupa aduan atau permintaan bantuan kemanusiaan.';
  static String reportSaberHoax =
      'Sampurasun, wargi! Terima kasih sudah menghubungi Jabar Saber Hoaks melalui SAPAWARGA. Silakan tulis laporan dan lampirkan tautan atau gambar dugaan hoaks di bawah ini.';

  // onBoarding
  static String finish = 'Selesai';
  static String skip = 'Lewati';

  static String titleOnboarding1 = 'Selamat datang di versi terbaru Sapawarga!';
  static String textOnboarding1 = 'Apa saja yang baru di Sapawarga?';

  static String titleOnboarding2 = importantInfo;
  static String textOnboarding2 =
      'Temukan info-info penting dari dinas-dinas di Jawa Barat, seperti jadwal SIM keliling, lowongan kerja, beasiswa, dll.';

  static String titleOnboarding3 = rwActivities;
  static String textOnboarding3 =
      'Bagikan cerita dan foto-foto kegiatan daerah Anda ke semua RW se-Jabar. Bisa berbagi jempol dan komentar juga, lho!';

  static String titleOnboarding4 = 'Yuk, login lagi ke Sapawarga!';
  static String textOnboarding4 =
      'Masih ingat ID dan PASSWORD Anda, kan? Lupa? Jangan khawatir!';

  //Usulan
  static const String dipublikasikan = 'Dipublikasikan';
  static const String terkirim = 'Terkirim';
  static const String ditolak = 'Ditolak';
  static const String draft = 'Draft';
  static String saveDraft = 'Simpan Draft';
  static String note = 'Catatan';
  static String detailUsulan = 'Detail Usulan';
  static String anonim = 'Anonim';
  static String uploadImage = 'Upload Gambar';
  static String titleUsulan = aspiration;
  static String typeUsulan = 'Tulis Usulan';
  static String seeUsulan = 'Lihat Catatan';
  static String desc = 'Deskripsi';
  static String confirmDraft =
      'Anda belum menyelesaikan posting Anda. Apakah ingin menyimpan sebagai draft?';
  static String usulan = 'Usulan';
  static String general = 'Umum';
  static String myUsulan = 'Usulan Saya';
  static String addTitleUsulan = 'Judul Usulan';
  static String editUsulan = 'Edit Usulan';
  static String deleteUsulan = 'Hapus Usulan';
  static String confirmationDeleteUsulan =
      'Apakah Anda yakin ingin menghapus usulan ini?';
  static String confirm = 'Konfirmasi';
  static String confirmSendEditUsulan =
      'Apakah anda setuju untuk memberikan usulan untuk Jawa Barat?';
  static String addUsulan = 'Buat Usulan';
  static String addMaxPhoto =
      'Anda hanya dapat menyertakan maksimal 5 foto saja';

  //Category
  static String chooseCategory = 'Pilih Kategori';
  static String category = 'Kategori';

  //Validtion Usulan
  static String errorEmptyTitleUsulan = 'Judul tidak boleh kosong';
  static String errorMinimumTitleUsulan = 'Judul minimal 10 karakter';
  static String errorMaximumTitleUsulan = 'Judul maksimal 60 karakter';
  static String errorTitleFormat = 'Judul tidak sesuai';
  static String errorEmptyDescUsulan = 'Detail Harus diisi';

  static String usulanUmum = 'Umum';
  static String usulanSaya = 'Usulan Saya';

  //Administration
  static String titleAdministration = 'Detail Administrasi';
  static String noDataFoundAdministration = 'Lokasi terdekat tidak ditemukan';
  static String toAdministration = 'Menuju';
  static String serviceInformation = 'Informasi Layanan';
  static String searchAdministration = 'Cari disini';
  static String residenceService = 'Layanan Kependudukan';
  static String urlAdministrationIsEmpty =
      'Layanan Administrasi Kependudukan belum tersedia';

  //Showcase (Wizard Guide)
  static String showcaseDescription1 =
      'Sampaikan laporan Anda melalui SMS langsung kepada instansi pemerintah Jawa Barat!';
  static String showcaseDescription2 =
      'Ketik laporan Anda dengan format:\nLAPORJABAR<spasi>SW_[nama]<spasi>[nama kab/kota]<spasi>[nama kecamatan]<spasi>[isi laporan]';
  static String showcaseDescription3 =
      'Sederhana, kan? Yuk, kirim laporan pertama Anda dengan KLIK menu ini!';
  static String showcaseHome1 =
      'Bagikan cerita dan foto kegiatan sekitar Anda dengan RW se-Jabar';
  static String showcaseHome2 =
      'Dapatkan info penting tentang beasiswa, lowongan kerja, jadwal SIM Keliling, dll.';
  static String showcaseHome3 =
      'Ikuti informasi dan perkembangan terbaru penyakit novel coronavirus (COVID-19) di fitur ini';
  static String showcaseImportantInfo1 =
      'Saring untuk tampilkan info penting sesuai dengan kategori yang Anda mau\n\nGeser ke kanan untuk melihat kategori lainnya';
  static String showcaseImportantInfo2 =
      'Cari info penting yang Anda mau tampilkan dengan ketikkan kata kunci';
  static String showcaseEditEducation =
      'Masukkan tingkat pendidikan terakhir dan pekerjaan Anda saat ini';
  static String lastEducation = 'Pendidikan terakhir';

  //Broadcast
  static String successDelete = 'Pesan berhasil dihapus';
  static String delete = 'Hapus';
  static String deleteMessage = 'Hapus Pesan';
  static String confirmDeleteMessage =
      'Apakah Anda yakin ingin menghapus pesan ini?';

  //Esamsat
  static String payEsamsatVia = 'Bayar pajak via';
  static String payViaSambara = 'Pembayaran menggunakan Sambara';
  static String payViaTokopedia = 'Pembayaran menggunakan Tokopedia';
  static String payViaBukalapak = 'Pembayaran menggunakan Bukalapak';

  // Label Name
  static String governor = 'Gubernur';
  static String governorJabar = 'Gubernur Jawa Barat';

  //Qna
  static String showCaseQna1 =
      'Bagikan foto dokumentasi kegiatan yang ada di lingkungan RW Anda!';
  static String showCaseQna2 = 'Berikan komentar dan sukai kegiatan RW lain';
  static String showCaseQna3 =
      'Kirim pertanyaan anda dengan menekan tombol ini';
  static String showCaseQna4 =
      'Disini kamu bisa memberikan LIKE untuk setiap pertanyaan yang menurut kamu menarik.';
  static String welcomeQna = 'Selamat datang ';
  static String welcomeQna2 = 'Di fitur Tanya Jawab Gubernur';
  static String descQna =
      'Tanya Gubernur merupakan fitur terbaru dari aplikasi sapawarga, anda dapat melakukan tanya jawab langsung dengan Gubernur Pertanyaan yang anda tanyakan akan direspon oleh Gubernur ketika mendapatkan jempol paling banyak Respon jawaban akan disampaikan secara langsung dan dijawab pada setiap minggunya';
  static String titleShowCaseQna1 = 'Sukai Pertanyaan';
  static String titleShowCaseQna2 = 'Buat Pertanyaan';
  static String titleHelpQna1 = 'Selamat Datang di Fitur Tanya Jawab Gubernur';
  static String titleHelpQna2 =
      'Sampaikan pertanyaan Anda langsung kepada pimpinan!';
  static String deschelpQna1 =
      'Tanya Jawab Gubernur adalah fitur terbaru di aplikasi Sapawarga. Anda dapat memberikan pertanyaan kepada Gubernur.';
  static String deschelpQna2 =
      'Pertanyaan teratas yang Anda berikan akan dijawab langsung oleh Gubernur.';
  static String writeQuestion = 'Tulis pertanyaan ...';
  static String buttonNext = 'Selanjutnya';
  static String buttonStartQna = 'Mulai Pertanyaan';
  static String buttonStartRwActivities = 'Mulai Post Kegiatan';

  //RW Activities
  static String RW = 'RW';
  static String showCaseRwActivity1 =
      'Untuk dapat menambahkan kegiatan dapat menekan tombol berikut';
  static String showCaseRwActivity2 =
      'Anda dapat melakukan likes dengan menekan tombol berikut';
  static String showCaseRwActivity3 =
      'Anda dapat memberikan komentar dengan menekan tomboll berikut';
  static String welcomeRwActivity = 'Di fitur Kegiatan RW';
  static String titleShowCaseRwActivity = 'Bagikan Kegiatan';
  static String titleShowCaseRwActivity2 = 'Komentar dan Sukai';
  static String titleShowCaseRwActivity3 = 'Komentar';
  static String showCaseRW1 =
      'Sampaikan pertanyaan Anda kepada Gubernur terkait pembangunan, program pemerintah, fasilitas umum, dsb.';
  static String showCaseRW2 =
      'Tekan ikon jempol untuk menyukai pertanyaan yang menurut Anda menarik';
  static String titleHelpRwActivities1 = 'Selamat datang di fitur Kegiatan RW!';
  static String titleHelpRwActivities2 =
      'Berikan komentar dan sukai kegiatan RW lain';
  static String titleHelpRwActivities3 = 'Jadi inspirasi';
  static String deschelpRwActivities1 =
      'Kegiatan RW adalah fitur terbaru di aplikasi Sapawarga. Bagikan foto kegiatan RW di sekitar Anda di sini!';
  static String deschelpRwActivities2 =
      'Anda dapat menyukai kegiatan dan berbagi komentar dengan pengguna lain';
  static String deschelpRwActivities3 =
      'Bagikan foto dan cerita kegiatan RW Anda dan jadilah inspirasi bagi RW se-Jawa Barat';
  static String errorLengthTextDescription =
      'Deskripsi kegiatan minimal 10 karakter';
  static String postRWActivitiesRejected = 'Post kegiatan ditolak';
  static String searchHintRwActivities = 'Cari Deskripsi, Nama RW';
  static String titleGovernor = 'Gubernur Provinsi Jawa Barat';
  static String titleLeadOfPKK = 'Ketua Tim Penggerak PKK Jawa Barat';
  static String titleStaffProv = 'Staff Provinsi Jawa Barat';
  static String nameBuAtalia = 'ataliapr';
  static String nameRidwanKamil = 'ridwankamil';
  static String nameRoleStaffProv = 'staffProv';
  static String fotoRwActivitiesIsEmpty =
      'Foto belum ditambahkan, silahkan upload foto kegiatan anda';
  static String descRwActivitiesIsEmpty =
      'Deskripsi kegiatan belum ditambahkan, silahkan tambah deskripsi kegiatan anda';
  static String tagRwActivitiesIsEmpty =
      'Tag kegiatan belum ditambahkan, silahkan tambah tag kegiatan anda';
  static String infoTag = 'Tambahkan tag untuk post kegiatan anda';
  static String maxPhotoLength =
      'Anda hanya dapat menyertakan maksimal 5 foto saja';
  static String tagTitle = 'Tag Kegiatan RW';
  static String displayTag = 'display';
  static String valueTag = 'value';

  //Gamifications
  static String mission = 'Misi';
  static String readNewsMission = 'baca berita ';
  static String readImportantNewsMission = 'baca info penting ';
  static String postRwActivitiesMission = 'posting kegiatan ';
  static String newMission = 'Misi Baru';
  static String missionDone = 'Misi Selesai';
  static String takeMission = 'Misi Telah Diambil';
  static String onProgressMission = 'On Progress';
  static String historyMission = 'Misi Sebelumnya';
  static String detailHistoryMission = 'Detail Misi Sebelumnya';
  static String detailOnProgressMission = 'Detail On Progres';
  static String overviewMission = 'Mission Overview';
  static String startMission = 'Mulai Misi';
  static String emptyDataOnProgress = 'Tidak ada data dalam List On Progress';
  static String emptyDataHistory = 'Tidak ada data dalam List Misi Sebelumnya';
  static String emptyDataNewMission = 'Tidak ada data dalam List Misi Baru';
  static String badge = 'Medali';

  //Update Username & no Wa
  static String updateUsernameTitle = 'Perbarui Username dan Nomor Telepon';
  static String updateUsernameDesc =
      'Terus dapatkan layanan terbaik dari aplikasi Sapawarga dengan memperbarui username dan nomor telepon yang tersambung ke aplikasi Whatsapp.';
  static String inputNewUsername = 'Masukkan username terbaru Anda';
  static String inputWhatsappNumber = 'Nomor Whatsapp';
  static String inputYourWhatsappNumber = 'Masukkan nomor telepon Anda';
  static String updateNow = 'Perbaharui Sekarang';
  static String needHelp = 'Perlu Bantuan';
  static String usernameHasbeenUpdatedTitle =
      'Username dan Nomor Telepon Anda Telah Berhasil Diperbarui';
  static String usernameHasbeenUpdatedDesc =
      'Terima kasih anda sudah memperbaharui username dan nomor telepon anda';
  static String okUnderstand = 'Baik, Saya Mengerti';
  static String updateNumberAndUsernameTitle =
      'Perbarui username dan Nomor Anda!';
  static String updateNumberAndUsernameDesc =
      'Ganti username Anda dengan nama yang mudah diingat dan nomor telepon yang tersambung ke aplikasi Whatsapp.';
  static String successSendOtp = 'Kode verifikasi telah dikirim ke whatsapp';
  static String failedSendOtp = 'OTP gagal di kirim, silahkan coba lagi';
  static String failedVerifyOtp = 'Verifikasi OTP gagal, silahkan coba kembali';
  static String verificationCode = 'Kode Verifikasi';
  static String inputVerificationCode = 'Masukan Kode Verifikasi';
  static String inputVerificationCodeDesc =
      'Silahkan cek kode verifikasi pada pesan whatsapp dengan nomor';
  static String inputVerificationCodeDesc2 = '. Kode ini akan berlaku 5 menit';
  static String sendVerificationCode = 'Kode Verifikasi Dikirim';
  static String sendVerificationCodeDesc =
      'Kode verifikasi telah berhasil dikirimkan ke nomer Whatsapp';
  static String verificationCodeCheck = 'Kode verifikasi tidak boleh kosong';
  static String notRecievedCode = 'Belum menerima kode Verifikasi?';
  static String resending = 'Kirim Ulang';
  static String verification = 'Verifikasi';
  static String otpVerification = 'Verifikasi OTP';
  static String otpVerificationDesc =
      'Proses verifikasi sedang berlangsung, apakah anda yakin ingin kembali?';
  static String updatePassword = 'Ubah Kata Sandi';
  static String updatePasswordDesc =
      'Proses ubah kata sandi sedang berlangsung, apakah anda yakin ingin kembali?';
  static String verificationOTP =
      'Kodde verifikasi tidak boleh kurang dari 6 digit';
  static String resendOTPInfo =
      'Kirim ulang otp bisa dilakukan kembali setelah 5 menit';

  //Register
  static String descRegister = 'Saat ini, Sapawarga dapat dipergunakan oleh';
  static String titleRegisterButtonRw = 'Sebagai RW';
  static String descRegisterButtonRw = 'Untuk ketua RW';
  static String titleRegisterButtonPublic = 'Sebagai Publik';
  static String descRegisterButtonPublic = 'Untuk warga Jawa Barat';
  static String confirmHaveAccount = 'Anda sudah punya akun? ';
  static String loginNow = 'Login Sekarang ';
  static String confirmDoesntHaveAccount = 'Anda belum punya akun?  ';
  static String register = 'Registrasi';
  static String registerAccount = 'Registrasi akun RW Anda';
  static String registePublic = 'Registrasi akun Publik Anda';
  static String addName = 'Cantumkan nama Anda';
  static String phoneRegister = 'No. Handphone';
  static String hintPhoneRegister = 'Contoh : 0878676656';
  static String inputPasswordRegister = 'Tuliskan kata sandi anda';
  static String repeatPasswordRegister = 'Ulang Kata Sandi';
  static String descRepeatPasswordRegister = 'Ulang kata sandi baru';
  static String repeatPasswordRegisterHint = 'Ulang kata sandi anda';
  static String saveRegister = 'Simpan dan lanjut';
  static String confirmExitRegister =
      'Apakah anda yakin ingin keluar dan perbarui username 1 minggu kemudian?';
  static String requiredForm = ' • Harus Diisi';
  static String pleaseCompleteAllField = ' harus diisi';
  static String chooseKabkota = 'Pilih Kabupaten/Kota Domisili Anda';
  static String chooseKecamatan = 'Pilih Kecamatan Domisili Anda';
  static String chooseKelurahan = 'Pilih Kelurahan Domisili Anda';
  static String chooseRw = 'Pilih RW Domisili Anda';
  static String verifyAccount = 'Aktivasi Akun Anda';
  static String verifyAccountDesc =
      'Kami telah mengirimkan pesan yang berisi link aktivasi akun ke nomor Whatsapp anda. Lihat Whatsapp untuk mengakses pesan.';
  static String seeWhatsapp = 'Lihat Whatsapp';

  static String birthdayPlaceholder = 'Pilih Tanggal';

  static String errorEmptyNIK = 'NIK harus diisi';
  static String errorMinimumNIK = 'NIK harus terdiri dari 16 karakter';
  static String errorMaximumNIK = 'NIK harus terdiri dari 16 karakter';

  static String nik = 'NIK';
  static String hintNik = 'Tuliskan NIK anda';

  // Toast
  static String downloadingFile = 'Mengunduh file ...';

  static String fullName = 'Nama Lengkap';
  static String hintFullName = 'Cantumkan nama anda';

  static String currentJob = 'Pekerjaan saat ini';

  static String hintUsername = 'Tuliskan username anda';

  static String infoSK =
      'Upload SK penunjukkan RW untuk memastikan bahwa anda adalah ketua RW';
  static String uploadSK = 'Upload SK';
  static String chooseFoto = 'Pilih Foto';
  static String foto = 'Foto';
  static String infoSizeFile = 'JPG atau PNG (2MB)';
  static String profileSuccessUpdated = 'Profile Berhasil Diupdate';

  static String infoAccountActive = 'Selamat, Akun Anda telah Aktif';
  static String descAccountActive =
      'Untuk memastikan bahwa anda adalah ketua RW. Mohon isi data profile terlebih dahulu';
  static String descAccountActivePublic =
      'Silahkan isi profil untuk melengkapi data anda';
  static String updateProfileNow = 'Isi Profile Sekarang';
  static String updateProfileFailed =
      'Terjadi kesalahan, silahkan coba kembali';
  static String infoUpdateAccount =
      'Upload SK penunjukan RW untuk mengakses fitur-fitur ketua RW. Klik disini untuk upload SK penunjukan RW';
  static String infoRegister =
      'Silahkan login menggunakan akun yang sudah anda registrasi sebelumnya';
  static String infoAfterSubmitVerification =
      'Terima kasih telah melengkapi data profil dan SK Penunjukkan RW. Proses verifikasi membutuhkan waktu maksimal 2 x 24 jam.';
  static String change = 'Ubah';
  static String fileSk = 'File SK';
  static String verificationSuccess = 'Verifikasi Berhasil';
  static String verificationSuccessDesc =
      'Akun anda telah diverifikasi sebagai Ketua RW ';
  static String verificationSuccessDesc2 =
      '. Terima kasih telah melakukan update data Ketua RW.';
  static String villageDesc = 'Desa';
  static String infoOtpSend =
      'Anda sudah pernah meminta kode OTP. Silahkan cek di pesan Whatsapp Anda.';
  static String infoMaxResendOtp =
      'Kirim ulang otp hanya bisa dilakukan 7 kali, bisa dilakukan kembali 1 minggu kemudian';

  //forgot password
  static String resetPassword = 'Atur Ulang Kata Sandi';
  static String descResetPassword =
      'Masukkan nomor telepon yang terdaftar untuk melakukan atur ulang kata sandi Anda.';
  static String descNoTelp = 'Contoh: 08786766565';
  static String descEmail = 'Contoh: asep@sapawarga.com';
  static String descInputPassword =
      'Masukkan kata sandi baru untuk atur ulang. Setelah anda melakukan atur ulang kata sandi, kata sandi lama anda akan terhapus.';
  static String changePasswordSuccess = 'Atur ulang kata sandi berhasil';
  static String sendUsernameSuccess = 'Username Anda Berhasil Dikirim';
  static String descChangePasswordSuccess =
      'Kata sandi anda telah berhasil di perbaharui, silahkan lanjutkan login untuk mengakses sapawarga';
  static String descChangeUsernameSuccess =
      'Username berhasil dikirim, silahkan untuk cek akun whatsapp anda';
  static String suceesLoginNow = 'Ok, Login Sekarang';
  static String resendOtpMax =
      'Kirim ulang otp maksimal hanya 7 kali dalam sehari, silahkan coba kembali besok';
  static String userNotFound = 'User tidak ditemukan';
  static String phoneNumberNotFound = 'Nomor Belum Terdaftar';
  static String phoneNumberNotFoundDesc =
      'Nomor Anda belum terdaftar dan silahkan menghubungi admin desa anda atau hotline Sapawarga terkait kendala ini.';
  static String changeUsernameDesc =
      'Masukkan nomor telepon yang terdaftar untuk melakukan atur ulang Username Anda.';
  static String changeUsername = 'Atur Ulang Username';
  static String addEmail = 'Silahkan isi Email Anda';
  static String addEmailDesc =
      'Email anda belum terdaftar pada aplikasi Sapawarga, isi email pada kolom berikut';
  static String waiting =
      'Anda akan segera terhubung\ndengan aplikasi Siap Kerja';
  static String exit = 'EXIT';
  static String titleTermCondition = 'Sapawarga X Siap Kerja';
  static String descTermCondition =
      'Dengan menyetujui ketentuan pengguna dibawah ini, Akun sapawarga anda akan terhubung dengan aplikasi Siap Kerja. Dengan aplikasi ini anda dapat melakukan pencarian dan melamar pekerjaan yang telah disediakan oleh DISNAKER.';
  static String agree = 'Setujui';
  static String forgotAccountDesc = 'Manakah yang anda akan atur ulang?';
  static String rwDesc = 'Ketua RW';
  static String public = 'Publik';
  static String cookingOilDistribution = 'Distribusi Minyak Goreng';
}
