import 'package:package_info/package_info.dart';

import 'Dictionary.dart';

class HttpHeaders {
  static Future<Map<String, String>> headers(
      {String token,
      String activationToken,
      String verifyToken,
      String totpToken}) async {
    String version = Dictionary.version;
    String packageName;

    await PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      version = packageInfo.version;
      packageName = packageInfo.packageName;
    });

    return {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'X-Requested-With': packageName,
      'X-Requested-With-Version': version,
      'Authorization':
          'Bearer ${token != null && token.isNotEmpty ? token : ''}',
      'activation-token':
          'Bearer ${activationToken != null && activationToken.isNotEmpty ? activationToken : ''}',
      'verify-token':
          'Bearer ${verifyToken != null && verifyToken.isNotEmpty ? verifyToken : ''}',
      'x-totp-token':
          '${totpToken != null && totpToken.isNotEmpty ? totpToken : ''}',
    };
  }
}
