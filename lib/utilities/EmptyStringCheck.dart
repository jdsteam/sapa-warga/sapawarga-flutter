String emptyStringCheck(String value) {
  if (value != null && value.isNotEmpty) {
    return value;
  } else {
    return '';
  }
}