import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sapawarga/components/DialogRequestPermission.dart';
import 'package:sapawarga/constants/Dictionary.dart';

///How to implement this component :
///listen [streamController] inside initState
///call [openDialog] for pick source image
///close [streamController] inside Dispose

class ImagePickerHelper {
  final BuildContext context;

  ImagePickerHelper(this.context);

  StreamController streamController = StreamController();
  ImagePicker imagePicker = ImagePicker();

  openDialog() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  leading: Icon(Icons.camera),
                  title: Text(Dictionary.takePhoto),
                  onTap: () async {
                    await _permissionCamera();
                  },
                ),
                ListTile(
                  leading: Icon(Icons.image),
                  title: Text(Dictionary.takePhotoFromGallery),
                  onTap: () async {
                    await _permissionGallery();
                  },
                ),
              ],
            ),
          );
        });
  }

  Future<void> _permissionCamera() async {
    PermissionStatus permission =
        await PermissionHandler().checkPermissionStatus(PermissionGroup.camera);

    if (permission != PermissionStatus.granted) {
      await showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/photo-camera.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionCamera,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler().requestPermissions(
                      [PermissionGroup.camera]).then(_onStatusRequestedCamera);
                },
              ));
    } else {
      await _openCamera();
    }
  }

  Future<void> _permissionGallery() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);

    if (permission != PermissionStatus.granted) {
      await showDialog(
          context: context,
          builder: (BuildContext context) => DialogRequestPermission(
                image: Image.asset(
                  'assets/icons/folder.png',
                  fit: BoxFit.contain,
                  color: Colors.white,
                ),
                description: Dictionary.permissionGalery,
                onOkPressed: () {
                  Navigator.of(context).pop();
                  PermissionHandler()
                      .requestPermissions([PermissionGroup.storage]).then(
                          _onStatusRequestedStorage);
                },
              ));
    } else {
      await _openGallery();
    }
  }

  void _onStatusRequestedStorage(
      Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.storage];
    if (status == PermissionStatus.granted) {
      _permissionGallery();
    } else {
      Navigator.of(context).pop();
    }
  }

  void _onStatusRequestedCamera(
      Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.camera];
    if (status == PermissionStatus.granted) {
      _permissionCamera();
    } else {
      Navigator.of(context).pop();
    }
  }

  Future<void> _openCamera() async {
    PickedFile file = await imagePicker.getImage(
        source: ImageSource.camera, maxHeight: 640, maxWidth: 640);

    if (file != null && file.path != null) {
      streamController.sink
          .add(await FlutterExifRotation.rotateImage(path: file.path));
      Navigator.of(context).pop();
    } else {
      streamController.sink.add(null);
      Navigator.of(context).pop();
    }
  }

  Future<void> _openGallery() async {
    final PickedFile file = await imagePicker.getImage(
        source: ImageSource.gallery, maxHeight: 640, maxWidth: 640);

    if (file != null && file.path != null) {
      streamController.sink
          .add(await FlutterExifRotation.rotateImage(path: file.path));
      Navigator.of(context).pop();
    } else {
      streamController.sink.add(null);
      Navigator.of(context).pop();
    }
  }
}
