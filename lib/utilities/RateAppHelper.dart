import 'package:flutter/material.dart';
import 'package:sapawarga/components/DialogRateApp.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/constants/UrlThirdParty.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'package:url_launcher/url_launcher.dart';

class RateAppHelper {
  static showRateApp(BuildContext context) async {
    int oldTime = await Preferences.getTimeRateLater();
    int countRateLater = await Preferences.getCountRateLater();
    bool hasRated = await Preferences.hasRatedApp();

    if (oldTime == null) {
      oldTime = DateTime.now().millisecondsSinceEpoch;
    }

    int days = DateTime.now()
        .difference(DateTime.fromMillisecondsSinceEpoch(oldTime))
        .inDays;

    if (!hasRated) {
      if (countRateLater == 0) {
        await showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) => DialogRateApp(
                  title: Dictionary.titleRate,
                  description: Dictionary.descriptionRate,
                  onOkPressed: () async {
                    await Preferences.setHasRatedApp(true);
                    await Preferences.setTimeRateLater(0);
                    await Preferences.setCountRateLater(0);

                    if (await canLaunch(UrlThirdParty.urlPlayStore)) {
                      await launch(UrlThirdParty.urlPlayStore);
                    } else {
                      if (await canLaunch(UrlThirdParty.urlPlayStoreAlt)) {
                        await launch(UrlThirdParty.urlPlayStoreAlt);
                      } else {
                        throw 'Could not launch ${UrlThirdParty.urlPlayStoreAlt}';
                      }
                    }

                    Navigator.of(context).pop();
                  },
                  onCancelPressed: () async {
                    countRateLater = countRateLater + 1;

                    await Preferences.setTimeRateLater(
                        DateTime.now().millisecondsSinceEpoch);
                    await Preferences.setCountRateLater(countRateLater);

                    Navigator.of(context).pop();
                  },
                ));
      } else {
        if (days >= 3) {
          await showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) => DialogRateApp(
                    title: Dictionary.titleRate,
                    description: Dictionary.descriptionRate,
                    onOkPressed: () async {
                      await Preferences.setHasRatedApp(true);
                      await Preferences.setTimeRateLater(0);
                      await Preferences.setCountRateLater(0);

                      if (await canLaunch(UrlThirdParty.urlPlayStore)) {
                        await launch(UrlThirdParty.urlPlayStore);
                      } else {
                        if (await canLaunch(UrlThirdParty.urlPlayStoreAlt)) {
                          await launch(UrlThirdParty.urlPlayStoreAlt);
                        } else {
                          throw 'Could not launch ${UrlThirdParty.urlPlayStoreAlt}';
                        }
                      }

                      Navigator.of(context).pop();
                    },
                    onCancelPressed: () async {
                      countRateLater = countRateLater + 1;

                      await Preferences.setTimeRateLater(
                          DateTime.now().millisecondsSinceEpoch);
                      await Preferences.setCountRateLater(countRateLater);

                      Navigator.of(context).pop();
                    },
                  ));
        }
      }
    }
  }
}
