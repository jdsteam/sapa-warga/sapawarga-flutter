import 'package:flutter/cupertino.dart';
import 'package:sapawarga/constants/Navigation.dart';
import 'package:url_launcher/url_launcher.dart';

class OpenInAppBrowser {
  ///Function for check url redirect to playstore or open url to browser
  ///[externalUrl] variable for accomodate link url
  ///[context] variable for accomodate BuildContext from widget
  ///[callback] variable for set custom function if url does'nt redirect to playstore

  static checkedUrlLaunch(
      {@required String externalUrl,
      @required BuildContext context,
      final void Function() callback}) async {
    if (externalUrl != null) {
      if (externalUrl.contains('market') ||
          externalUrl.contains('play.google')) {
        if (await canLaunch(externalUrl)) {
          await launch(externalUrl);
        } else {
          throw 'Could not launch $externalUrl';
        }
      } else {
        callback != null
            ? callback()
            : await Navigator.pushNamed(context, NavigationConstrants.Browser,
                arguments: externalUrl);
      }
    }
  }
}
