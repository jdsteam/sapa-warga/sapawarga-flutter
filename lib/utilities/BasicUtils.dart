import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';

class StringUtils {
  static String capitalizeWord(String str) {
    try {
      List<String> words = str.toLowerCase().split(RegExp("\\s"));
      String capitalizeWord = "";
      for (final String w in words) {
        String first = w.substring(0, 1);
        String afterFirst = w.substring(1);
        capitalizeWord += first.toUpperCase() + afterFirst + " ";
      }
      return capitalizeWord.trim();
    } catch (e) {
      print(e.toString());
      return str;
    }
  }

  static Future<String> userDataUrlAppend(String url) async {
    String token = await AuthRepository().getToken();
    UserInfoModel user = await AuthProfileRepository().getUserInfo();

    if (url == null) {
      return url;
    } else {
      String link = '';
      Map<String, String> usrMap = {'_userToken_': '', '_userID_': ''};

      if (user != null) {
        usrMap = {'_userToken_': token, '_userID_': user.id.toString()};
      }

      usrMap.forEach((key, value) {
        link = url.replaceAll(key, value);
      });

      return link;
    }
  }
}
