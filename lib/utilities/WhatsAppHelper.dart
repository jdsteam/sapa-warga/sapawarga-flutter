import 'package:device_apps/device_apps.dart';
import 'package:sapawarga/constants/Analytics.dart';
import 'package:sapawarga/environment/Environment.dart';
import 'package:sapawarga/utilities/AnalyticsHelper.dart';
import 'package:url_launcher/url_launcher.dart';

class WhatsAppHelper {
  static checkNumber(String csPhoneNumber, String csInfoText) async {
    String phoneNumber = '';
    try {
      bool isInstalled = await DeviceApps.isAppInstalled('com.whatsapp');
      phoneNumber = csPhoneNumber ?? Environment.csPhone;

      if (phoneNumber[0] == '0') {
        phoneNumber = phoneNumber.replaceFirst('0', '+62');
      }

      if (isInstalled) {
        String urlWhatsApp =
            'https://wa.me/${phoneNumber}?text=${Uri.encodeFull(csInfoText)}%0A%0A';
        if (await canLaunch(urlWhatsApp)) {
          await launch(urlWhatsApp);

          await AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_WA_ADMIN_HELP);
        } else {
          await launch('tel://${phoneNumber}');
        }
      } else {
        await launch('tel://${phoneNumber}');

        await AnalyticsHelper.setLogEvent(Analytics.EVENT_OPEN_TELP_ADMIN_HELP);
      }
    } catch (_) {
      await launch('tel://${phoneNumber}');
    }
  }
}
