import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  /// ----------------------------------------------------------
  /// Method that returns the api phone book page
  /// ----------------------------------------------------------
  static Future<int> getPhoneBookPage() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getInt('phoneBookPage');
  }

  /// ----------------------------------------------------------
  /// Method that saves the api phone  page
  /// ----------------------------------------------------------
  static Future<bool> setPhoneBookPage(int value) async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.setInt('phoneBookPage', value);
  }

  /// ----------------------------------------------------------
  /// Method that returns the refresh list myUsulan
  /// ----------------------------------------------------------
  static Future<bool> getMyUsulanStat() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getBool('MyUsulanStat') != null
        ? prefs.getBool('MyUsulanStat')
        : false;
  }

  /// ----------------------------------------------------------
  /// Method that saves stat myUsulan
  /// ----------------------------------------------------------
  static Future<bool> setMyUsulanStat(bool value) async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.setBool('MyUsulanStat', value);
  }

  /// ----------------------------------------------------------
  /// Method that returns total counts data
  /// ----------------------------------------------------------

  static Future<int> getTotalCount() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getInt('TotalCount');
  }

  /// ----------------------------------------------------------
  /// Method that saves total counts data
  /// ----------------------------------------------------------
  static Future<bool> setTotalCount(int value) async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.setInt('TotalCount', value);
  }

  /// ----------------------------------------------------------
  /// Method that returns usulan total counts
  /// ----------------------------------------------------------

  static Future<int> getUsulanTotalCountMe() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getInt('UsulanTotalCountMe');
  }

  /// ----------------------------------------------------------
  /// Method that saves usulan total count
  /// ----------------------------------------------------------
  static Future<bool> setUsulanTotalCountMe(int value) async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.setInt('UsulanTotalCountMe', value);
  }

  /// ----------------------------------------------------------
  /// Method that return the video list jabar
  /// ----------------------------------------------------------
  static Future<String> getVideoListJabar() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getString('videoListJabar');
  }

  /// ----------------------------------------------------------
  /// Method that saves the usulan list
  /// ----------------------------------------------------------
  static Future<void> setUsulanist(String jsonUsulanList) async {
    await Future.delayed(Duration(seconds: 1));

    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setString('UsulanList', jsonUsulanList);
  }

  /// ----------------------------------------------------------
  /// Method that saves the video list jabar
  /// ----------------------------------------------------------
  static Future<void> setVideoListJabar(String jsonVideoList) async {
    await Future.delayed(Duration(seconds: 1));

    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setString('videoListJabar', jsonVideoList);
  }

  /// ----------------------------------------------------------
  /// Method that check has video list jabar
  /// ----------------------------------------------------------
  static Future<bool> hasVideoListJabar() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getString('videoListJabar') == null ? false : true;
  }

  /// ----------------------------------------------------------
  /// Method that removes the video list jabar
  /// ----------------------------------------------------------
  static Future<void> deleteVideoListJabar() async {
    final prefs = await SharedPreferences.getInstance();

    await prefs.remove('videoListJabar');
  }

  /// ----------------------------------------------------------
  /// Method that return the video list kokab
  /// ----------------------------------------------------------
  static Future<String> getVideoListKokab() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getString('videoListKokab');
  }

  /// ----------------------------------------------------------
  /// Method that saves the video list kokab
  /// ----------------------------------------------------------
  static Future<void> setVideoListKokab(String jsonVideoList) async {
    await Future.delayed(Duration(seconds: 1));

    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setString('videoListKokab', jsonVideoList);
  }

  /// ----------------------------------------------------------
  /// Method that check has video list kokab
  /// ----------------------------------------------------------
  static Future<bool> hasVideoListKokab() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getString('videoListKokab') == null ? false : true;
  }

  /// ----------------------------------------------------------
  /// Method that removes the video list kokab
  /// ----------------------------------------------------------
  static Future<void> deleteVideoListKokab() async {
    final prefs = await SharedPreferences.getInstance();

    await prefs.remove('videoListKokab');
  }

  /// ----------------------------------------------------------
  /// Method that return the usulan list
  /// ----------------------------------------------------------
  static Future<String> getUsulanList() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getString('UsulanList');
  }

  /// ----------------------------------------------------------
  /// Method that check has usulan list
  /// ----------------------------------------------------------
  static Future<bool> hasUsulanList() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getString('UsulanList') == null ? false : true;
  }

  /// ----------------------------------------------------------
  /// Method that saves the state showcase report JABAR
  /// ----------------------------------------------------------
  static Future<void> setShowcaseJabar(bool isShown) async {
    await Future.delayed(Duration(seconds: 1));

    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setBool('showcaseJabar', isShown);
  }

  /// ----------------------------------------------------------
  /// Method that check showcase report JABAR has shown
  /// ----------------------------------------------------------
  static Future<bool> hasShowcaseJabar() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getBool('showcaseJabar') != null
        ? prefs.getBool('showcaseJabar')
        : false;
  }

  /// ----------------------------------------------------------
  /// Method that saves the state showcase Qna
  /// ----------------------------------------------------------
  static Future<void> setShowcaseQna(bool isShown) async {
    await Future.delayed(Duration(seconds: 1));

    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setBool('showcaseQna', isShown);
  }

  /// ----------------------------------------------------------
  /// Method that check showcase Qna has shown
  /// ----------------------------------------------------------
  static Future<bool> hasShowcaseQna() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getBool('showcaseQna') != null
        ? prefs.getBool('showcaseQna')
        : false;
  }

  /// ----------------------------------------------------------
  /// Method that saves the state showcase Qna
  /// ----------------------------------------------------------
  static Future<void> setShowcaseQna2(bool isShown) async {
    await Future.delayed(Duration(seconds: 1));

    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setBool('showcaseQna2', isShown);
  }

  /// ----------------------------------------------------------
  /// Method that check showcase Qna has shown
  /// ----------------------------------------------------------
  static Future<bool> hasShowcaseQna2() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getBool('showcaseQna2') != null
        ? prefs.getBool('showcaseQna2')
        : false;
  }

  /// ----------------------------------------------------------
  /// Method that saves records total count
  /// ----------------------------------------------------------
  static Future<bool> setRecordsTotalCount(int value) async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.setInt('RecordsTotalCount', value);
  }

  /// ----------------------------------------------------------
  /// Method that returns records total counts
  /// ----------------------------------------------------------

  static Future<int> getRecordTotalCount() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getInt('RecordsTotalCount');
  }

  /// ----------------------------------------------------------
  /// Method that saves QnA total count
  /// ----------------------------------------------------------
  static Future<bool> setQnATotalCount(int value) async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.setInt('QnATotalCount', value);
  }

  /// ----------------------------------------------------------
  /// Method that returns QnA total counts
  /// ----------------------------------------------------------

  static Future<int> getQnATotalCount() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getInt('QnATotalCount');
  }

  /// ----------------------------------------------------------
  /// Method that returns the api broadcast page
  /// ----------------------------------------------------------
  static Future<int> getBroadcastPage() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getInt('broadcastPage');
  }

  /// ----------------------------------------------------------
  /// Method that saves the api broadcast  page
  /// ----------------------------------------------------------
  static Future<bool> setBroadcastPage(int value) async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.setInt('broadcastPage', value);
  }

  /// ----------------------------------------------------------
  /// Method that check show Help Screen before main fitur
  /// ----------------------------------------------------------
  static Future<bool> hasShowHelpQna() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getBool('showHelpQna') != null
        ? prefs.getBool('showHelpQna')
        : false;
  }

  /// ----------------------------------------------------------
  /// Method that saves the state show help Qna
  /// ----------------------------------------------------------
  static Future<void> setShowHelpQna(bool isShown) async {
    await Future.delayed(Duration(seconds: 1));

    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setBool('showHelpQna', isShown);
  }

  /// ----------------------------------------------------------
  /// Method that check show Help RW Activity before main feature
  /// ----------------------------------------------------------
  static Future<bool> hasShowHelpRwActivity() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getBool('showHelpRwActivity') != null
        ? prefs.getBool('showHelpRwActivity')
        : false;
  }

  /// ----------------------------------------------------------
  /// Method that saves the state show help Rw Activity
  /// ----------------------------------------------------------
  static Future<void> setShowHelpRwActivity(bool isShown) async {
    await Future.delayed(Duration(seconds: 1));

    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setBool('showHelpRwActivity', isShown);
  }

  /// ----------------------------------------------------------
  /// Method that check showcase Home
  /// ----------------------------------------------------------
  static Future<bool> hasShowcaseHome() async {
    final prefs = await SharedPreferences.getInstance();

    // because showcaseHome has been used before,
    // then set this to null for remove the value
    await prefs.setBool('showcaseHome', null);

    return prefs.getBool('showcaseHome1') != null
        ? prefs.getBool('showcaseHome1')
        : false;
  }

  /// ----------------------------------------------------------
  /// Method that saves the state showcase Home
  /// ----------------------------------------------------------
  static Future<void> setShowcaseHome(bool isShown) async {
    await Future.delayed(Duration(seconds: 1));

    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setBool('showcaseHome1', isShown);
  }

  /// ----------------------------------------------------------
  /// Method that check showcase Important Info
  /// ----------------------------------------------------------
  static Future<bool> hasShowcaseImportantInfo() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getBool('showcaseImportantInfo') != null
        ? prefs.getBool('showcaseImportantInfo')
        : false;
  }

  /// ----------------------------------------------------------
  /// Method that saves the state showcase Important Info
  /// ----------------------------------------------------------
  static Future<void> setShowcaseImportantInfo(bool isShown) async {
    await Future.delayed(Duration(seconds: 1));

    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setBool('showcaseImportantInfo', isShown);
  }

  /// ----------------------------------------------------------
  /// Method that return time education later
  /// ----------------------------------------------------------
  static Future<int> getTimeEducationLater() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getInt('timeEducationLater');
  }

  /// ----------------------------------------------------------
  /// Method that saves the time education later
  /// ----------------------------------------------------------
  static Future<void> setTimeEducationLater(int time) async {
    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setInt('timeEducationLater', time);
  }

  /// ----------------------------------------------------------
  /// Method that check showcase Edit Education
  /// ----------------------------------------------------------
  static Future<bool> hasShowcaseEditEducation() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getBool('showcaseEditEducation') != null
        ? prefs.getBool('showcaseEditEducation')
        : false;
  }

  /// ----------------------------------------------------------
  /// Method that saves the state showcase Edit Education
  /// ----------------------------------------------------------
  static Future<void> setShowcaseEditEducation(bool isShown) async {
    await Future.delayed(Duration(seconds: 1));

    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setBool('showcaseEditEducation', isShown);
  }

  /// ----------------------------------------------------------
  /// Method that return time rate later
  /// ----------------------------------------------------------
  static Future<int> getTimeRateLater() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getInt('timeRateLater');
  }

  /// ----------------------------------------------------------
  /// Method that saves the time rate later
  /// ----------------------------------------------------------
  static Future<void> setTimeRateLater(int time) async {
    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setInt('timeRateLater', time);
  }

  /// ----------------------------------------------------------
  /// Method that check has rated app
  /// ----------------------------------------------------------
  static Future<bool> hasRatedApp() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getBool('ratedApp') != null
        ? prefs.getBool('ratedApp')
        : false;
  }

  /// ----------------------------------------------------------
  /// Method that saves the state rated app
  /// ----------------------------------------------------------
  static Future<void> setHasRatedApp(bool isRated) async {
    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setBool('ratedApp', isRated);
  }

  /// ----------------------------------------------------------
  /// Method that return rate later count
  /// ----------------------------------------------------------
  static Future<int> getCountRateLater() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getInt('rateLaterCount') != null
        ? prefs.getInt('rateLaterCount')
        : 0;
  }

  /// ----------------------------------------------------------
  /// Method that saves the rate later count
  /// ----------------------------------------------------------
  static Future<void> setCountRateLater(int count) async {
    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setInt('rateLaterCount', count);
  }

  /// ----------------------------------------------------------
  /// Method that check has activation account
  /// ----------------------------------------------------------
  static Future<bool> hasActivationAccount() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getBool('activationAccount') != null
        ? prefs.getBool('activationAccount')
        : false;
  }

  /// ----------------------------------------------------------
  /// Method that saves the state activation account
  /// ----------------------------------------------------------
  static Future<void> setHasActivationAccount(bool activationAccount) async {
    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setBool('activationAccount', activationAccount);
  }

  /// ----------------------------------------------------------
  /// Method that check has token activation
  /// ----------------------------------------------------------
  static Future<String> hasTokenActivation() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getString('tokenActivation') != null
        ? prefs.getString('tokenActivation')
        : '';
  }

  /// ----------------------------------------------------------
  /// Method that saves the state token activation
  /// ----------------------------------------------------------
  static Future<void> setHasTokenActivation(String tokenActivation) async {
    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setString('tokenActivation', tokenActivation);
  }

  /// ----------------------------------------------------------
  /// Method that check has first verificationn account
  /// ----------------------------------------------------------
  static Future<bool> hasFirstVerificationAccount() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getBool('firstVerificationAccount') != null
        ? prefs.getBool('firstVerificationAccount')
        : false;
  }

  /// ----------------------------------------------------------
  /// Method that saves the state access token siap kerja
  /// ----------------------------------------------------------
  static Future<void> setAccessTokenSiapKerja(String accessToken) async {
    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setString('accessTokenSiapKerja', accessToken);
  }

  /// ----------------------------------------------------------
  /// Method that check get access Token siap kerja
  /// ----------------------------------------------------------
  static Future<String> getAccessTokenSiapKerja() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getString('accessTokenSiapKerja') != null
        ? prefs.getString('accessTokenSiapKerja')
        : '';
  }

  /// ----------------------------------------------------------
  /// Method that saves the state fisrst verification account
  /// ----------------------------------------------------------
  static Future<void> setHasFirstVerificationAccount(
      bool firstVerificationAccount) async {
    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setBool('firstVerificationAccount', firstVerificationAccount);
  }

  /// ----------------------------------------------------------
  /// Method that check count for resend OTP password
  /// ----------------------------------------------------------
  static Future<int> getCountResendOtpPassword() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getInt('countResendOtpPassword') != null
        ? prefs.getInt('countResendOtpPassword')
        : 0;
  }

  /// ----------------------------------------------------------
  /// Method that saves the state count for resend OTP password
  /// ----------------------------------------------------------
  static Future<void> setCountResendOtpPassword(int countResendOtp) async {
    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setInt('countResendOtpPassword', countResendOtp);
  }

  /// ----------------------------------------------------------
  /// Method for check date
  /// ----------------------------------------------------------
  static Future<int> getSaveDate() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getInt('saveDate') != null ? prefs.getInt('saveDate') : 0;
  }

  /// ----------------------------------------------------------
  /// Method that saves the date
  /// ----------------------------------------------------------
  static Future<void> setSaveDate(int saveDateTime) async {
    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setInt('saveDate', saveDateTime);
  }

  /// ----------------------------------------------------------
  /// Method get status approve term condition
  /// ----------------------------------------------------------
  static Future<bool> getStatusTermCondition() async {
    final prefs = await SharedPreferences.getInstance();

    return prefs.getBool('statusTermCondition') != null
        ? prefs.getBool('statusTermCondition')
        : false;
  }

  /// ----------------------------------------------------------
  /// Method for set status term condition
  /// ----------------------------------------------------------
  static Future<void> setStatusTermCondition(bool statusTermCondition) async {
    // obtain shared preferences
    final prefs = await SharedPreferences.getInstance();

    // set value
    await prefs.setBool('statusTermCondition', statusTermCondition);
  }
}
