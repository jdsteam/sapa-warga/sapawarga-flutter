import 'dart:convert';

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/models/UserInfoModel.dart';

class CookingOilDistributionHelper {
  static String paramCookingOil(
      UserInfoModel userInfo, RemoteConfig remoteConfig) {
    var packageValue =
        json.decode(remoteConfig.getString(FirebaseConfig.package));

    var codeMigorValue =
        json.decode(remoteConfig.getString(FirebaseConfig.code));

    return '?idRw=${userInfo.username}'
            '&kotakabupaten=${userInfo.kabkota.name}'
            '&kecamatan=${userInfo.kecamatan.name}'
            '&desakelurahan=${userInfo.kelurahan.name}'
            '&rw=${userInfo.rw}'
            '&paket=${packageValue.toString()}'
            '&alamat=${userInfo.address}'
            '&nama_rw=${userInfo.name}'
            '&no_hp_rw=${userInfo.phone}'
            '&code=${codeMigorValue.toString()}'
        .replaceAll(' ', '%20');
  }
}
