import 'package:sapawarga/constants/Dictionary.dart';

class Validations {
  static String nikValidation(String val) {
    if (val.length > 16) return Dictionary.errorMaximumNIK;

    if (val.length < 16) return Dictionary.errorMinimumNIK;

    if (val.isEmpty) return Dictionary.errorEmptyNIK;

    return null;
  }

  static String kabupatenValidation(String val) {
    if (val.isEmpty) return Dictionary.errorKabupaten;

    return null;
  }

  static String kecamatanValidation(String val) {
    if (val.isEmpty) return Dictionary.errorKecamatan;

    return null;
  }

  static String kelurahanValidation(String val) {
    if (val.isEmpty) return Dictionary.errorKecamatan;

    return null;
  }

  static String rwValidation(String val) {
    if (val.isEmpty) return Dictionary.errorRW;

    return null;
  }

  static String usernameValidationLogin(String val) {
    if (val.isEmpty) return Dictionary.errorEmptyUsername;

    if (val.length > 255) return Dictionary.errorMaximumUsername;

    return null;
  }

  static String usernameValidation(String val) {
    RegExp regex = RegExp(r'^[a-z0-9_./]+$');

    if (val.isEmpty) return Dictionary.errorEmptyUsername;

    if (val.length < 4) return Dictionary.errorMinimumUsername;

    if (val.length > 255) return Dictionary.errorMaximumUsername;

    if (!regex.hasMatch(val)) return Dictionary.errorInvalidUsername;

    return null;
  }

  static String passwordValidation(String val, bool isLogin) {
    // if you want use validation for check symbol, add this regex to below path (?=.*?[!@#$%^&*()_+\-=\[\]{};:\\|,.<>\/?])
    RegExp regex = RegExp(r'^(?=.*?[a-zA-Z])(?=.*?[0-9]).{8,}$');

    if (val.isEmpty) return Dictionary.errorEmptyPassword;

    if (!isLogin) {
      if (val.length < 8) return Dictionary.errorMinimumPassword;
    }

    if (val.length > 255) return Dictionary.errorMaximumPassword;

    if (!isLogin) {
      if (!regex.hasMatch(val)) return Dictionary.errorInvalidPassword;
    }

    return null;
  }

  static String confirmPasswordValidation(
      String password, String confirmPassword) {
    if (confirmPassword.isEmpty) return Dictionary.errorEmptyConfirmPassword;

    if (confirmPassword.length < 8) return Dictionary.errorMinimumPassword;

    if (confirmPassword.length > 255) return Dictionary.errorMaximumPassword;

    if (password != confirmPassword) return Dictionary.errorNotMatchPassword;

    return null;
  }

  static String repeatPasswordValidation(
      String password, String confirmPassword) {
    if (confirmPassword.isEmpty) {
      return Dictionary.errorEmptyRepeatPassword;
    } else if (password != confirmPassword) {
      return Dictionary.errorNotMatchRepeatPassword;
    }

    return null;
  }

  static String emailValidation(String val) {
    Pattern pattern =
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]{3,}@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$";
    RegExp regex = RegExp(pattern);

    if (val.isEmpty) return Dictionary.errorEmptyEmail;

    if (!regex.hasMatch(val)) return Dictionary.errorInvalidEmail;

    return null;
  }

  static String addressValidation(String val) {
    if (val.isEmpty) return Dictionary.errorEmptyAddress;

    if (val.length > 255) return Dictionary.errorMaximumAddress;

    return null;
  }

  static String rtValidation(String val) {
    bool valueValid = RegExp(r'^[0-9]+$').hasMatch(val);

    if (val.isEmpty) return Dictionary.errorEmptyRT;

    if (val.length < 3) return Dictionary.errorMaximumRT;

    if (val.length > 3) return Dictionary.errorMaximumRT;

    if (!valueValid) return Dictionary.errorInvalidRT;

    return null;
  }

  static String instagramValidation(String val) {
    bool valueValid =
        RegExp(r"^(?:@)?[a-z0-9_.^[\]a-z0-9.$+-]+$").hasMatch(val);

    if (val.isNotEmpty) {
      if (!valueValid) {
        return Dictionary.errorInvalidInstagram;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  static String twitterlidation(String val) {
    bool valueValid =
        RegExp(r"^(?:@)?[a-z0-9_.^[\]a-z0-9.$+-]+$").hasMatch(val);

    if (val.isNotEmpty) {
      if (!valueValid) {
        return Dictionary.errorInvalidTwitter;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  static String facebooklidation(String val) {
    bool valueValid = RegExp(
            r"(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?")
        .hasMatch(val);
    if (val.isNotEmpty) {
      if (!valueValid || val.length > 1 && val.length < 21) {
        return Dictionary.errorInvalidFacebook;
      }

      return null;
    } else {
      return null;
    }
  }

  static String nameValidation(String val) {
    Pattern pattern = r"^([a-zA-Z `'.]*)$";
    RegExp regex = RegExp(pattern);

    if (val.isEmpty) return Dictionary.errorEmptyName;

    if (val.length < 4) return Dictionary.errorMinimumName;

    if (val.length > 255) return Dictionary.errorMaximumName;

    if (!regex.hasMatch(val)) return Dictionary.errorInvalidName;

    return null;
  }

  static String phoneValidation(String val) {
    Pattern pattern = r"^(^62\s?|^0)(\d{5,13})$";

    RegExp regex = RegExp(pattern);

    if (val.isEmpty) return Dictionary.errorEmptyPhone;

    if (val.length < 3) return Dictionary.errorMinimumPhone;

    if (val.length > 13) return Dictionary.errorMaximumPhone;

    if (!regex.hasMatch(val)) return Dictionary.errorInvalidPhone;

    return null;
  }

  static String handPhoneValidation(String val) {
    Pattern pattern = r"^(^62\s?|^0)(\d{5,13})$";

    RegExp regex = RegExp(pattern);

    if (val.isEmpty) return Dictionary.errorEmptyHandPhone;

    if (val.length < 3) return Dictionary.errorMinimumHandPhone;

    if (val.length > 13) return Dictionary.errorMaximumHandPhone;

    if (!regex.hasMatch(val)) return Dictionary.errorInvalidHandPhone;

    return null;
  }

  static String whatsappNumberValidation(String val) {
    Pattern pattern = r"^(^62\s?|^0)(\d{5,13})$";

    RegExp regex = RegExp(pattern);

    if (val.isEmpty) return Dictionary.errorWhatsappNumber;

    if (val.length < 3) return Dictionary.errorMinimumWhatsappNumber;

    if (val.length > 13) return Dictionary.errorMaximumWhatsappNumber;

    if (!regex.hasMatch(val)) return Dictionary.errorInvalidWhatsappNumber;

    return null;
  }

  static String otpValidation(String val) {
    // Pattern pattern = r"^(^62\s?|^0)(\d{5,13})$";
    //
    // RegExp regex = RegExp(pattern);

    if (val.isEmpty) return Dictionary.errorOtp;

    if (val.length < 6) return Dictionary.errorMinimumOtp;

    if (val.length > 6) return Dictionary.errorMinimumOtp;

    // if (!regex.hasMatch(val)) return Dictionary.errorOtp;

    return null;
  }

  static String titleUsulanValidation(String val) {
    RegExp regex = RegExp(r'^[A-Za-z0-9\ /+$]+$');

    if (val.isEmpty) return Dictionary.errorEmptyTitleUsulan;

    if (val.length < 10) return Dictionary.errorMinimumTitleUsulan;

    if (val.length > 60) return Dictionary.errorMaximumTitleUsulan;

    if (!regex.hasMatch(val)) return Dictionary.errorTitleFormat;

    return null;
  }

  static String descUsulanValidation(String val) {
    if (val.isEmpty) return Dictionary.errorEmptyDescUsulan;

    return null;
  }

  static String commentValidation(String val) {
    if (val.isEmpty) return Dictionary.errorEmptyComment;

    if (val.length < 10) return Dictionary.errorMinimumComment;

    return null;
  }
}
