import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class NearByLocationsEvent extends Equatable {
  NearByLocationsEvent([List props = const []]);
}

class NearByLocationsLoad extends NearByLocationsEvent {
  final double latitude;
  final double longitude;

  NearByLocationsLoad({@required this.latitude, @required this.longitude});

  @override
  String toString() => 'Event NearByLocationsLoad';

  @override
  List<Object> get props => [latitude, longitude];
}
