import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/NearByLocationsModel.dart';

@immutable
abstract class NearByLocationsState extends Equatable {
  NearByLocationsState([List props = const []]);
}

class NearByLocationsInitial extends NearByLocationsState {
  @override
  String toString() => 'State NearByLocationsInitial';

  @override
  List<Object> get props => [];
}

class NearByLocationsLoading extends NearByLocationsState {
  @override
  String toString() => 'State NearByLocationsLoading';

  @override
  List<Object> get props => [];
}

class NearByLocationsLoaded extends NearByLocationsState {
  final List<NearByLocationsModel> records;

  NearByLocationsLoaded({@required this.records}) : super([records]);

  @override
  String toString() => 'State NearByLocationsLoaded';

  @override
  List<Object> get props => [records];
}

class NearByLocationsFailure extends NearByLocationsState {
  final String error;

  NearByLocationsFailure({@required this.error}) : super([error]);

  @override
  String toString() => 'State NearByLocationsFailure';

  @override
  List<Object> get props => [error];
}
