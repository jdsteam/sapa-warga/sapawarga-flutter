import 'dart:async';
import 'package:bloc/bloc.dart';
import './Bloc.dart';

class ShowcaseHomeBloc extends Bloc<ShowcaseHomeEvent, ShowcaseHomeState> {
  @override
  ShowcaseHomeState get initialState => ShowcaseHomeInitial();

  @override
  Stream<ShowcaseHomeState> mapEventToState(
    ShowcaseHomeEvent event,
  ) async* {
    if (event is LoadShowcaseHome) {
      yield ShowcaseHomeLoaded(disabled: event.disabled);
    }
  }
}
