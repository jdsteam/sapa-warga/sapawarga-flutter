import 'package:equatable/equatable.dart';

abstract class ShowcaseHomeEvent extends Equatable {
  const ShowcaseHomeEvent([List props = const <dynamic>[]]);
}

class LoadShowcaseHome extends ShowcaseHomeEvent {
  final bool disabled;

  LoadShowcaseHome({this.disabled});

  @override
  String toString() {
    return 'Event LoadShowcaseHome';
  }

  @override
  List<Object> get props => [];
}
