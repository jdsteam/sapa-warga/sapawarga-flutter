import 'package:equatable/equatable.dart';

abstract class ShowcaseHomeState extends Equatable {
  const ShowcaseHomeState();
}

class ShowcaseHomeInitial extends ShowcaseHomeState {
  @override
  List<Object> get props => [];
}


class ShowcaseHomeLoaded extends ShowcaseHomeState {
  final bool disabled;

  ShowcaseHomeLoaded({this.disabled});

  @override
  String toString() {
    return 'State ShowcaseHomeLoaded';
  }

  @override
  List<Object> get props => [];
}