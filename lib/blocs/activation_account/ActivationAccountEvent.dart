import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ActivationAccountEvent extends Equatable {
  ActivationAccountEvent([List props = const <dynamic>[]]);
}

class ActivationAccount extends ActivationAccountEvent {
  final String activationToken;

  ActivationAccount({
    @required this.activationToken,
  }) : assert(
          (activationToken != null),
        );

  @override
  String toString() {
    return 'Event ActivationAccount';
  }

  @override
  List<Object> get props => [activationToken];
}
