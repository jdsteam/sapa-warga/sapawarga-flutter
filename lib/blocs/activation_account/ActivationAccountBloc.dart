import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/blocs/activation_account/ActivationAccountEvent.dart';
import 'package:sapawarga/blocs/activation_account/ActivationAccountState.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/repositories/RegisterRepository.dart';

import './Bloc.dart';

class ActivationAccountBloc
    extends Bloc<ActivationAccountEvent, ActivationAccountState> {
  final RegisterRepository registerRepository;

  ActivationAccountBloc({@required this.registerRepository})
      : assert(registerRepository != null);

  @override
  ActivationAccountState get initialState => ActivationAccountInitial();

  @override
  Stream<ActivationAccountState> mapEventToState(
    ActivationAccountEvent event,
  ) async* {
    if (event is ActivationAccount) {
      yield ActivationAccountLoading();

      try {
        await registerRepository.activationAccount(
            activationToken: event.activationToken);
        yield ActivationAccountSuccess();
      } on ValidationException catch (error) {
        yield ValidationError(errors: error.errors);
      } catch (e) {
        yield ActivationAccountFailure(
            error: e.toString().replaceAll('Exception:', ''));
      }
    }
  }
}
