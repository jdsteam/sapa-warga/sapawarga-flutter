import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ActivationAccountState extends Equatable {
  ActivationAccountState([List props = const <dynamic>[]]);
}

class ActivationAccountInitial extends ActivationAccountState {
  @override
  List<Object> get props => [];
}

class ActivationAccountLoading extends ActivationAccountState {
  @override
  String toString() {
    return 'State ActivationAccountLoading';
  }

  @override
  List<Object> get props => [];
}

class ActivationAccountSuccess extends ActivationAccountState {
  @override
  String toString() {
    return 'State ActivationAccountSuccess';
  }

  @override
  List<Object> get props => [];
}

class ValidationError extends ActivationAccountState {
  final Map<String, dynamic> errors;

  ValidationError({@required this.errors}) : super([errors]);

  @override
  String toString() {
    return 'State ValidationError{errors: $errors}';
  }

  @override
  List<Object> get props => <Object>[errors];
}

class ActivationAccountFailure extends ActivationAccountState {
  final String error;

  ActivationAccountFailure({this.error});

  @override
  String toString() {
    return 'State ActivationAccountFailure{error: $error}';
  }

  @override
  List<Object> get props => <Object>[error];
}
