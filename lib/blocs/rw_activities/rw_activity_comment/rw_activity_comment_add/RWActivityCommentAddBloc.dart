import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/RWActivityCommentModel.dart';
import 'package:sapawarga/repositories/RWActivityRepository.dart';
import '../Bloc.dart';

class RWActivityCommentAddBloc extends Bloc<RWActivityCommentAddEvent, RWActivityCommentAddState> {

  final RWActivityRepository repository;


  RWActivityCommentAddBloc(this.repository);

  @override
  RWActivityCommentAddState get initialState => RWActivityCommentAddInitial();

  @override
  Stream<RWActivityCommentAddState> mapEventToState(
    RWActivityCommentAddEvent event,
  ) async* {
    if (event is RWActivityCommentAdd) {
      yield RWActivityCommentAddLoading();

      try {
        ItemRwActivityComment record = await repository.postComment(id: event.id, text: event.text);
        yield RWActivityCommentAdded(record);
      } catch (e) {
        yield RWActivityCommentAddFailure(error: CustomException.onConnectionException(e.toString()));
      }

    }
  }
}
