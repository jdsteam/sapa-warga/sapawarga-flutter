import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/RWActivityModel.dart';

abstract class RWActivityListState extends Equatable {
  const RWActivityListState([List props = const <dynamic>[]]);
}

class RWActivityListInitial extends RWActivityListState {
  @override
  List<Object> get props => [];
}

class RWActivityListLoading extends RWActivityListState {
  @override
  String toString() {
    return 'State RWActivityListLoading';
  }

  @override
  List<Object> get props => [];
}

class RWActivityListLoaded extends RWActivityListState {
  final RWActivityModel records;

  RWActivityListLoaded(this.records) : super([records]);

  @override
  String toString() {
    return 'State RWActivityListLoaded';
  }

  @override
  List<Object> get props => [records];
}

class RWActivityListFailure extends RWActivityListState {
  final String error;

  RWActivityListFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State RWActivityListFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}