import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/blocs/rw_activities/rw_activity_detail/RWActivityDetailEvent.dart';
import 'package:sapawarga/blocs/rw_activities/rw_activity_detail/RWActivityDetailState.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/RWActivityModel.dart';
import 'package:sapawarga/repositories/RWActivityRepository.dart';

class RWActivityDetailBloc
    extends Bloc<RWActivityDetailEvent, RWActivityDetailState> {

  final RWActivityRepository repository;

  RWActivityDetailBloc(this.repository);

  @override
  RWActivityDetailState get initialState => RWActivityDetailInitial();

  @override
  Stream<RWActivityDetailState> mapEventToState(
      RWActivityDetailEvent event,) async* {
    if (event is RWActivityDetailLoad) {
       yield RWActivityDetailLoading();
       ItemRWActivity records;

      try {
        if(event.id != null){
           records = await repository.getDetailRwActivities(
              id: event.id);
        }else{
          records = event.record;
        }
        yield RWActivityDetailLoaded(records);
      } catch (e) {
        yield RWActivityDetailFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
