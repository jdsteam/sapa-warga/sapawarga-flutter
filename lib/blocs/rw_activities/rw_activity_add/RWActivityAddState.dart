import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';
import 'package:sapawarga/models/RWActivityModel.dart';

abstract class RWActivityAddState extends Equatable {
  const RWActivityAddState([List props = const <dynamic>[]]);
}

class RWActivityAddInitial extends RWActivityAddState {
  @override
  List<Object> get props => [];
}

class RWActivityAddLoading extends RWActivityAddState {
  @override
  String toString() {
    return 'State RWActivityAddLoading';
  }

  @override
  List<Object> get props => [];
}

class RWActivityAdded extends RWActivityAddState {
  final ItemRWActivity record;

  RWActivityAdded(this.record) : super([record]);

  @override
  String toString() {
    return 'State RWActivityAdded';
  }

  @override
  List<Object> get props => [record];
}

class RWActivityAddFailure extends RWActivityAddState {
  final String error;

  RWActivityAddFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State RWActivityAddFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}

class RWActivityCategoryLoading extends RWActivityAddState {
  @override
  String toString() {
    return 'State RWActivityCategoryLoading';
  }

  @override
  List<Object> get props => [];
}

class RWActivityCategoryAdded extends RWActivityAddState {
  final List<MasterCategoryModel> records;

  RWActivityCategoryAdded(this.records) : super([records]);

  @override
  String toString() {
    return 'State RWActivityCategoryAdded';
  }

  @override
  List<Object> get props => [records];
}

class RWActivityCategoryFailure extends RWActivityAddState {
  final String error;

  RWActivityCategoryFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State RWActivityCategoryFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}