import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class VerifyOtpPasswordEvent extends Equatable {
  VerifyOtpPasswordEvent([List props = const <dynamic>[]]);
}

class VerifyOtpChangedPassword extends VerifyOtpPasswordEvent {
  final String phone;
  final String otp;

  VerifyOtpChangedPassword({@required this.phone, @required this.otp})
      : assert((phone != null) && (otp != null));

  @override
  String toString() {
    return 'Event VerifyOtpChangedPassword';
  }

  @override
  List<Object> get props => [phone, otp];
}
