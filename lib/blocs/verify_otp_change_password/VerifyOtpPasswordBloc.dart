import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/blocs/verify_otp_change_password/VerifyOtpPasswordEvent.dart';
import 'package:sapawarga/blocs/verify_otp_change_password/VerifyOtpPasswordState.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';

import './Bloc.dart';

class VerifyOtpPasswordBloc
    extends Bloc<VerifyOtpPasswordEvent, VerifyOtpPasswordState> {
  final AuthProfileRepository authProfileRepository;

  VerifyOtpPasswordBloc({@required this.authProfileRepository})
      : assert(authProfileRepository != null);

  @override
  VerifyOtpPasswordState get initialState => VerifyOtpPasswordInitial();

  @override
  Stream<VerifyOtpPasswordState> mapEventToState(
    VerifyOtpPasswordEvent event,
  ) async* {
    if (event is VerifyOtpChangedPassword) {
      yield VerifyOtpPasswordLoading();

      try {
        var verifyToken = await authProfileRepository.verifyOtpChangePassword(
            otp: event.otp, phone: event.phone);
        yield VerifyOtpPasswordSuccess(verifyToken: verifyToken);
      } on ValidationException catch (error) {
        yield ValidationVerifyOtpPasswordError(errors: error.errors);
      } catch (e) {
        yield VerifyOtpPasswordFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
