import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class VerifyOtpPasswordState extends Equatable {
  VerifyOtpPasswordState([List props = const <dynamic>[]]);
}

class VerifyOtpPasswordInitial extends VerifyOtpPasswordState {
  @override
  List<Object> get props => [];
}

class VerifyOtpPasswordLoading extends VerifyOtpPasswordState {
  @override
  String toString() {
    return 'State VerifyOtpPasswordLoading';
  }

  @override
  List<Object> get props => [];
}

class VerifyOtpPasswordSuccess extends VerifyOtpPasswordState {
  final String verifyToken;

  VerifyOtpPasswordSuccess({@required this.verifyToken}) : super([verifyToken]);

  @override
  String toString() {
    return 'State VerifyOtpPasswordSuccess verify_token: $verifyToken';
  }

  @override
  List<Object> get props => [];
}

class ValidationVerifyOtpPasswordError extends VerifyOtpPasswordState {
  final Map<String, dynamic> errors;

  ValidationVerifyOtpPasswordError({@required this.errors}) : super([errors]);

  @override
  String toString() {
    return 'State ValidationVerifyOtpPasswordError{errors: $errors}';
  }

  @override
  List<Object> get props => <Object>[errors];
}

class VerifyOtpPasswordFailure extends VerifyOtpPasswordState {
  final String error;

  VerifyOtpPasswordFailure({this.error});

  @override
  String toString() {
    return 'State VerifyOtpPasswordFailure{error: $error}';
  }

  @override
  List<Object> get props => <Object>[error];
}
