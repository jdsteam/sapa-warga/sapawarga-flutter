import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SendOtpPasswordEvent extends Equatable {
  SendOtpPasswordEvent([List props = const <dynamic>[]]);
}

class SendOtpChangedPassword extends SendOtpPasswordEvent {
  final String phone;

  SendOtpChangedPassword({@required this.phone}) : assert((phone != null));

  @override
  String toString() {
    return 'Event SendOtpChangedPassword';
  }

  @override
  List<Object> get props => [phone];
}
