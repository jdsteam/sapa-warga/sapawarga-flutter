import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/blocs/send_otp_change_password/SendOtpPasswordEvent.dart';
import 'package:sapawarga/blocs/send_otp_change_password/SendOtpPasswordState.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';

import './Bloc.dart';

class SendOtpPasswordBloc
    extends Bloc<SendOtpPasswordEvent, SendOtpPasswordState> {
  final AuthProfileRepository authProfileRepository;

  SendOtpPasswordBloc({@required this.authProfileRepository})
      : assert(authProfileRepository != null);

  @override
  SendOtpPasswordState get initialState => SendOtpPasswordInitial();

  @override
  Stream<SendOtpPasswordState> mapEventToState(
    SendOtpPasswordEvent event,
  ) async* {
    if (event is SendOtpChangedPassword) {
      yield SendOtpPasswordLoading();

      try {
        await authProfileRepository.sendOtpChangePassword(phone: event.phone);
        yield SendOtpPasswordSuccess();
      } on ValidationException catch (e) {
        yield ValidationSendPasswordOtpError(errors: e.errors);
      } catch (e) {
        yield SendOtpPasswordFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
