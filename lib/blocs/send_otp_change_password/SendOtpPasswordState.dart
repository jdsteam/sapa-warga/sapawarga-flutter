import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SendOtpPasswordState extends Equatable {
  SendOtpPasswordState([List props = const <dynamic>[]]);
}

class SendOtpPasswordInitial extends SendOtpPasswordState {
  @override
  List<Object> get props => [];
}

class SendOtpPasswordLoading extends SendOtpPasswordState {
  @override
  String toString() {
    return 'State SendOtpPasswordLoading';
  }

  @override
  List<Object> get props => [];
}

class SendOtpPasswordSuccess extends SendOtpPasswordState {
  @override
  String toString() {
    return 'State SendOtpPasswordSuccess';
  }

  @override
  List<Object> get props => [];
}

class ValidationSendPasswordOtpError extends SendOtpPasswordState {
  final Map<String, dynamic> errors;

  ValidationSendPasswordOtpError({@required this.errors}) : super([errors]);

  @override
  String toString() {
    return 'State ValidationSendPasswordOtpError{errors: $errors}';
  }

  @override
  List<Object> get props => <Object>[errors];
}

class SendOtpPasswordFailure extends SendOtpPasswordState {
  final String error;

  SendOtpPasswordFailure({this.error});

  @override
  String toString() {
    return 'State SendOtpPasswordFailure{error: $error}';
  }

  @override
  List<Object> get props => <Object>[error];
}
