import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/repositories/NotificationRepository.dart';
import '../Bloc.dart';

class NotificationBadgeBloc extends Bloc<NotificationBadgeEvent, NotificationBadgeState> {

  NotificationRepository notificationRepository;


  NotificationBadgeBloc({@required this.notificationRepository}):assert(notificationRepository!=null);

  @override
  NotificationBadgeState get initialState => NotificationBadgeInitial();

  @override
  Stream<NotificationBadgeState> mapEventToState(
    NotificationBadgeEvent event,
  ) async* {
    if (event is CheckNotificationBadge) {
      try {
        yield NotificationBadgeLoading();
        int unreadCount = await notificationRepository.unreadCount();
        if (unreadCount > 0) {
          yield NotificationBadgeShow(count: unreadCount);
        } else {
          yield NotificationBadgeHide();
        }
      } catch (e) {
        print(e.toString());
      }
    }
  }
}
