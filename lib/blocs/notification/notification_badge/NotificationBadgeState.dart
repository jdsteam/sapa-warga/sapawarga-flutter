import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class NotificationBadgeState extends Equatable {
  NotificationBadgeState([List props = const <dynamic>[]]);
}

class NotificationBadgeInitial extends NotificationBadgeState {
  @override
  List<Object> get props => [];
}

class NotificationBadgeLoading extends NotificationBadgeState {
  @override
  List<Object> get props => [];
}

class NotificationBadgeShow extends NotificationBadgeState {
  final int count;

  NotificationBadgeShow({this.count});

  @override
  String toString() {
    return 'State NotificationBadgeShow{count: $count}';
  }

  @override
  List<Object> get props => [count];
}

class NotificationBadgeHide extends NotificationBadgeState {
  @override
  String toString() {
    return 'State NotificationBadgeHide';
  }

  @override
  List<Object> get props => [];
}
