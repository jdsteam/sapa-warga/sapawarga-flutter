import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/NotificationModel.dart';
import 'package:sapawarga/repositories/NotificationRepository.dart';
import '../Bloc.dart';

class NotificationListBloc extends Bloc<NotificationListEvent, NotificationListState> {

  NotificationRepository notificationRepository;


  NotificationListBloc({@required this.notificationRepository});

  @override
  NotificationListState get initialState => NotificationListInitial();

  @override
  Stream<NotificationListState> mapEventToState(
    NotificationListEvent event,
  ) async* {
    if (event is NotificationListLoad) {
      yield NotificationListLoading();
      try {
        List<NotificationModel> records = await notificationRepository.getRecords(forceRefresh: true);

        yield NotificationListLoaded(records: records);
      } catch (e) {
        yield NotificationListFailure(error: CustomException.onConnectionException(e.toString()));
      }

    }

    if (event is NotificationListTap) {
      try {
        await notificationRepository.updateReadData(event.id);
        List<NotificationModel> records = await notificationRepository.getRecords();
        yield NotificationListLoaded(records: records);
      } catch (e) {
        yield NotificationListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is NotificationListReloadLocal) {
      yield NotificationListLoading();
      try {
        List<NotificationModel> records = await notificationRepository.getRecords();
        yield NotificationListLoaded(records: records);
      } catch (e) {
        yield NotificationListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
