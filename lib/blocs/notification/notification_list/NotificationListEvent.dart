import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class NotificationListEvent extends Equatable {
  NotificationListEvent([List props = const <dynamic>[]]);
}

class NotificationListLoad extends NotificationListEvent {
  @override
  String toString() {
    return 'Event NotificationListLoad';
  }

  @override
  List<Object> get props => [];
}

class NotificationListTap extends NotificationListEvent {
  final int id;

  NotificationListTap({@required this.id});

  @override
  String toString() {
    return 'Event NotificationListTap';
  }

  @override
  List<Object> get props => [id];
}

class NotificationListReloadLocal extends NotificationListEvent {
  @override
  String toString() {
    return 'Event NotificationListReloadLocal';
  }

  @override
  List<Object> get props => [];
}
