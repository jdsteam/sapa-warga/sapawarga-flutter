import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/repositories/PollingRepository.dart';

import './Bloc.dart';

class PollingDetailBloc extends Bloc<PollingDetailEvent, PollingDetailState> {
  final PollingRepository pollingRepository;

  PollingDetailBloc({@required this.pollingRepository}):assert(pollingRepository != null);

  @override
  PollingDetailState get initialState => PollingDetailInitial();

  @override
  Stream<PollingDetailState> mapEventToState(
    PollingDetailEvent event,
  ) async* {
    if (event is PollingDetailLoad) {
        yield PollingDetailLoaded(record: event.record);
    }

    if (event is PollingDetailSendVote) {
      yield PollingDetailLoading();

      try {
        int status = await pollingRepository.sendVote(
            pollingId: event.pollingId, answer: event.answerId);
        yield PollingDetailVoted(status: status);
      } catch (e) {
        yield PollingDetailFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is PollingDetailStatusVote) {
      yield PollingDetailLoading();
      try {
        bool isVoted =
            await pollingRepository.getVoteStatus(pollingId: event.pollingId);
        yield PollingDetailStatus(isVoted: isVoted);
      } catch (e) {
        yield PollingDetailFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
