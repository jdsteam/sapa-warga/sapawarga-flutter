import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/PollingModel.dart';

@immutable
abstract class PollingDetailEvent extends Equatable {
  PollingDetailEvent([List props = const <dynamic>[]]);
}

class PollingDetailLoad extends PollingDetailEvent {
  final PollingModel record;

  PollingDetailLoad({@required this.record}) : assert(record != null);

  @override
  String toString() {
    return 'Event PollingDetailLoad{record: $record}';
  }

  @override
  List<Object> get props => [record];
}

class PollingDetailSendVote extends PollingDetailEvent {
  final int pollingId;
  final int answerId;

  PollingDetailSendVote({@required this.pollingId, @required this.answerId})
      : assert(pollingId != null && answerId != null);

  @override
  String toString() {
    return 'Event PollingDetailSendVote{pollingId: $pollingId, answerId: $answerId}';
  }

  @override
  List<Object> get props => [pollingId, answerId];
}

class PollingDetailStatusVote extends PollingDetailEvent {
  final int pollingId;

  PollingDetailStatusVote({@required this.pollingId})
      : assert(pollingId != null);

  @override
  String toString() {
    return 'Event PollingDetailStatusVote{pollingId: $pollingId}';
  }

  @override
  List<Object> get props => [pollingId];
}
