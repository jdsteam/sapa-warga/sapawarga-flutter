import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/PollingModel.dart';

@immutable
abstract class PollingDetailState extends Equatable {
  PollingDetailState([List props = const <dynamic>[]]);
}

class PollingDetailInitial extends PollingDetailState {
  @override
  List<Object> get props => [];
}

class PollingDetailLoading extends PollingDetailState {
  @override
  List<Object> get props => [];
}

class PollingDetailLoaded extends PollingDetailState {
  final PollingModel record;

  PollingDetailLoaded({@required this.record}) : assert(record != null);

  @override
  String toString() {
    return 'State PollingDetailLoaded{record: $record}';
  }

  @override
  List<Object> get props => [record];
}

class PollingDetailVoted extends PollingDetailState {
  final int status;

  PollingDetailVoted({this.status});

  @override
  String toString() {
    return 'State PollingDetailVoted{status: $status}';
  }

  @override
  List<Object> get props => [status];
}

class PollingDetailStatus extends PollingDetailState {
  final bool isVoted;

  PollingDetailStatus({this.isVoted});

  @override
  String toString() {
    return 'State PollingDetailStatus{isVoted: $isVoted}';
  }

  @override
  List<Object> get props => [isVoted];
}

class PollingDetailFailure extends PollingDetailState {
  final String error;

  PollingDetailFailure({this.error});

  @override
  String toString() {
    return 'PollingDetailFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
