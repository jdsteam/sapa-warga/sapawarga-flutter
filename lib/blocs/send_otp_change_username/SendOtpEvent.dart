import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SendOtpEvent extends Equatable {
  SendOtpEvent([List props = const <dynamic>[]]);
}

class SendOtpChangedUsername extends SendOtpEvent {
  final String username;
  final String phone;

  SendOtpChangedUsername({@required this.username, @required this.phone})
      : assert((username != null) && (phone != null));

  @override
  String toString() {
    return 'Event SendOtpChangedUsername';
  }

  @override
  List<Object> get props => [username, phone];
}
