import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SendOtpState extends Equatable {
  SendOtpState([List props = const <dynamic>[]]);
}

class SendOtpInitial extends SendOtpState {
  @override
  List<Object> get props => [];
}

class SendOtpLoading extends SendOtpState {
  @override
  String toString() {
    return 'State SendOtpLoading';
  }

  @override
  List<Object> get props => [];
}

class SendOtpSuccess extends SendOtpState {
  @override
  String toString() {
    return 'State SendOtpSuccess';
  }

  @override
  List<Object> get props => [];
}

class ValidationSendOtpError extends SendOtpState {
  final Map<String, dynamic> errors;

  ValidationSendOtpError({@required this.errors}) : super([errors]);

  @override
  String toString() {
    return 'State ValidationSendOtpError{errors: $errors}';
  }

  @override
  List<Object> get props => <Object>[errors];
}

class SendOtpFailure extends SendOtpState {
  final String error;

  SendOtpFailure({this.error});

  @override
  String toString() {
    return 'State SendOtpFailure{error: $error}';
  }

  @override
  List<Object> get props => <Object>[error];
}
