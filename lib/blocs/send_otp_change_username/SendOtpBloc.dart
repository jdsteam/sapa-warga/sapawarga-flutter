import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';

import './Bloc.dart';

class SendOtpBloc extends Bloc<SendOtpEvent, SendOtpState> {
  final AuthProfileRepository authProfileRepository;

  SendOtpBloc({@required this.authProfileRepository})
      : assert(authProfileRepository != null);

  @override
  SendOtpState get initialState => SendOtpInitial();

  @override
  Stream<SendOtpState> mapEventToState(
    SendOtpEvent event,
  ) async* {
    if (event is SendOtpChangedUsername) {
      yield SendOtpLoading();

      try {
        await authProfileRepository.sendOtpChangeUsername(
            username: event.username, phone: event.phone);
        yield SendOtpSuccess();
      } on ValidationException catch (error) {
        yield ValidationSendOtpError(errors: error.errors);
      } catch (e) {
        yield SendOtpFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
