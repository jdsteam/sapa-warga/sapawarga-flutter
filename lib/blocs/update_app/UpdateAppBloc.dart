import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/repositories/UpdateAppRepository.dart';
import './Bloc.dart';

class UpdateAppBloc extends Bloc<UpdateAppEvent, UpdateAppState> {
  final UpdateAppRepository updateAppRepository;

  UpdateAppBloc({@required this.updateAppRepository}):assert(updateAppRepository != null);

  @override
  UpdateAppState get initialState => UpdateAppInitial();

  @override
  Stream<UpdateAppState> mapEventToState(
    UpdateAppEvent event,
  ) async* {
    if (event is CheckUpdate) {

      yield UpdateAppLoading();

      try {
        bool needUpdate = await updateAppRepository.checkUpdate();

        if (needUpdate) {
          yield UpdateAppRequired();
        } else {
          yield UpdateAppUpdated();
        }
      } catch (e) {
        yield UpdateAppFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
