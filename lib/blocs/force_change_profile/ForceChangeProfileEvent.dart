import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ForceChangeProfileEvent extends Equatable {
  ForceChangeProfileEvent([List props = const <dynamic>[]]);
}

class CheckForceChangeProfile extends ForceChangeProfileEvent {
  @override
  String toString() {
    return 'Event CheckForceChangeProfile';
  }

  @override
  List<Object> get props => [];
}
