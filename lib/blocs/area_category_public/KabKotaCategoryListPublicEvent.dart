import 'package:equatable/equatable.dart';

abstract class KabKotaCategoryListPublicEvent extends Equatable {
  const KabKotaCategoryListPublicEvent([List props = const <dynamic>[]]);
}

class KabKotaCategoryListPublicLoad extends KabKotaCategoryListPublicEvent {
  KabKotaCategoryListPublicLoad() : super([]);

  @override
  String toString() {
    return 'Event KabKotaCategoryListPublicLoad';
  }

  @override
  List<Object> get props => [];
}
