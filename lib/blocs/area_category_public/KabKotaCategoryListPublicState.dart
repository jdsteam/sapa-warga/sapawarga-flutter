import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/AreaModelCategory.dart';

abstract class KabKotaCategoryListPublicState extends Equatable {
  const KabKotaCategoryListPublicState([List props = const <dynamic>[]]);
}

class KabKotaCategoryListPublicInitial extends KabKotaCategoryListPublicState {
  @override
  List<Object> get props => [];
}

class KabKotaCategoryListPublicLoading extends KabKotaCategoryListPublicState {
  @override
  String toString() {
    return 'State KabKotaCategoryListPublicLoading';
  }

  @override
  List<Object> get props => [];
}

class KabKotaCategoryListPublicLoaded extends KabKotaCategoryListPublicState {
  final AreaModelCategory records;

  KabKotaCategoryListPublicLoaded(this.records) : super([records]);

  @override
  String toString() {
    return 'State KabKotaCategoryListPublicLoaded';
  }

  @override
  List<Object> get props => [records];
}

class KabKotaCategoryListPublicFailure extends KabKotaCategoryListPublicState {
  final String error;

  KabKotaCategoryListPublicFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State KabKotaCategoryListPublicFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
