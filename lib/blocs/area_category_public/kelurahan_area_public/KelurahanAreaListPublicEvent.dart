import 'package:equatable/equatable.dart';

abstract class KelurahanAreaListPublicEvent extends Equatable {
  const KelurahanAreaListPublicEvent([List props = const <dynamic>[]]);
}

class KelurahanAreaListPublicLoad extends KelurahanAreaListPublicEvent {
  final String id;

  KelurahanAreaListPublicLoad({this.id}) : super([]);

  @override
  String toString() {
    return 'Event KelurahanAreaListPublicLoad';
  }

  @override
  List<Object> get props => [id];
}
