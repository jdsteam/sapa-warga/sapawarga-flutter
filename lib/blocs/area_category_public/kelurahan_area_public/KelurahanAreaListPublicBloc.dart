import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/blocs/area_category_public/kelurahan_area_public/KelurahanAreaListPublicState.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/AreaModelCategory.dart';
import 'package:sapawarga/repositories/AreaRepository.dart';
import 'KelurahanAreaListPublicEvent.dart';

class KelurahanAreaListPublicBloc
    extends Bloc<KelurahanAreaListPublicEvent, KelurahanAreaListPublicState> {
  final AreaRepository repository;

  KelurahanAreaListPublicBloc(this.repository);

  @override
  KelurahanAreaListPublicState get initialState =>
      KelurahanAreaListPublicInitial();

  @override
  Stream<KelurahanAreaListPublicState> mapEventToState(
    KelurahanAreaListPublicEvent event,
  ) async* {
    if (event is KelurahanAreaListPublicLoad) {
      yield KelurahanAreaListPublicLoading();

      try {
        AreaModelCategory records = await repository.getCategoryListAreaPublic(
            depth: AreaRepository.urbanVillageCode.toString(), id: event.id);
        yield KelurahanAreaListPublicLoaded(records);
      } catch (e) {
        yield KelurahanAreaListPublicFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
