import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/AreaModelCategory.dart';

abstract class KelurahanAreaListPublicState extends Equatable {
  const KelurahanAreaListPublicState([List props = const <dynamic>[]]);
}

class KelurahanAreaListPublicInitial extends KelurahanAreaListPublicState {
  @override
  List<Object> get props => [];
}

class KelurahanAreaListPublicLoading extends KelurahanAreaListPublicState {
  @override
  String toString() {
    return 'State KelurahanAreaListPublicLoading';
  }

  @override
  List<Object> get props => [];
}

class KelurahanAreaListPublicLoaded extends KelurahanAreaListPublicState {
  final AreaModelCategory records;

  KelurahanAreaListPublicLoaded(this.records) : super([records]);

  @override
  String toString() {
    return 'State KelurahanAreaListPublicLoaded';
  }

  @override
  List<Object> get props => [records];
}

class KelurahanAreaListPublicFailure extends KelurahanAreaListPublicState {
  final String error;

  KelurahanAreaListPublicFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State KelurahanAreaListPublicFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
