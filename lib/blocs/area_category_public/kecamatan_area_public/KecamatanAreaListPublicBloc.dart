import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/AreaModelCategory.dart';
import 'package:sapawarga/repositories/AreaRepository.dart';

import 'KecamatanAreaListPublicEvent.dart';
import 'KecamatanAreaListPublicState.dart';

class KecamatanAreaListPublicBloc
    extends Bloc<KecamatanAreaListPublicEvent, KecamatanAreaListPublicState> {
  final AreaRepository repository;

  KecamatanAreaListPublicBloc(this.repository);

  @override
  KecamatanAreaListPublicState get initialState =>
      KecamatanAreaListPublicInitial();

  @override
  Stream<KecamatanAreaListPublicState> mapEventToState(
    KecamatanAreaListPublicEvent event,
  ) async* {
    if (event is KecamatanAreaListPublicLoad) {
      yield KecamatanAreaListPublicLoading();

      try {
        AreaModelCategory records = await repository.getCategoryListAreaPublic(
            depth: AreaRepository.subDistrictCode.toString(), id: event.id);
        yield KecamatanAreaListPublicLoaded(records);
      } catch (e) {
        yield KecamatanAreaListPublicFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
