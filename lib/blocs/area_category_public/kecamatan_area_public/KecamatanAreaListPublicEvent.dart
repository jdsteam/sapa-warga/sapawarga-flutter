import 'package:equatable/equatable.dart';

abstract class KecamatanAreaListPublicEvent extends Equatable {
  const KecamatanAreaListPublicEvent([List props = const <dynamic>[]]);
}

class KecamatanAreaListPublicLoad extends KecamatanAreaListPublicEvent {
  final String id;

  KecamatanAreaListPublicLoad({this.id}) : super([]);

  @override
  String toString() {
    return 'Event KecamatanAreaListPublicLoad';
  }

  @override
  List<Object> get props => [id];
}
