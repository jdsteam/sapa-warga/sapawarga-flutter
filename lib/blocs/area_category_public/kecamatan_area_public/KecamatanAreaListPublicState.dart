import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/AreaModelCategory.dart';

abstract class KecamatanAreaListPublicState extends Equatable {
  const KecamatanAreaListPublicState([List props = const <dynamic>[]]);
}

class KecamatanAreaListPublicInitial extends KecamatanAreaListPublicState {
  @override
  List<Object> get props => [];
}

class KecamatanAreaListPublicLoading extends KecamatanAreaListPublicState {
  @override
  String toString() {
    return 'State KecamatanAreaListPublicLoading';
  }

  @override
  List<Object> get props => [];
}

class KecamatanAreaListPublicLoaded extends KecamatanAreaListPublicState {
  final AreaModelCategory records;

  KecamatanAreaListPublicLoaded(this.records) : super([records]);

  @override
  String toString() {
    return 'State KecamatanAreaListPublicLoaded';
  }

  @override
  List<Object> get props => [records];
}

class KecamatanAreaListPublicFailure extends KecamatanAreaListPublicState {
  final String error;

  KecamatanAreaListPublicFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State KecamatanAreaListPublicFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
