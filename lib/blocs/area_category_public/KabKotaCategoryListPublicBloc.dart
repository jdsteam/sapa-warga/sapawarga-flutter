import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/blocs/area_category_public/KabKotaCategoryListPublicEvent.dart';
import 'package:sapawarga/blocs/area_category_public/KabKotaCategoryListPublicState.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/AreaModelCategory.dart';
import 'package:sapawarga/repositories/AreaRepository.dart';

class KabKotaCategoryListPublicBloc extends Bloc<KabKotaCategoryListPublicEvent,
    KabKotaCategoryListPublicState> {
  final AreaRepository repository;

  KabKotaCategoryListPublicBloc(this.repository);

  @override
  KabKotaCategoryListPublicState get initialState =>
      KabKotaCategoryListPublicInitial();

  @override
  Stream<KabKotaCategoryListPublicState> mapEventToState(
    KabKotaCategoryListPublicEvent event,
  ) async* {
    if (event is KabKotaCategoryListPublicLoad) {
      yield KabKotaCategoryListPublicLoading();

      try {
        AreaModelCategory records = await repository.getCategoryListAreaPublic(
            depth: AreaRepository.cityCode.toString());
        yield KabKotaCategoryListPublicLoaded(records);
      } catch (e) {
        yield KabKotaCategoryListPublicFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
