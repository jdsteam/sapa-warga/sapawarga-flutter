import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';

import 'UpdatePasswordEvent.dart';
import 'UpdatePasswordState.dart';

class UpdatePasswordBloc
    extends Bloc<UpdatePasswordEvent, UpdatePasswordState> {
  final AuthProfileRepository authProfileRepository;

  UpdatePasswordBloc({@required this.authProfileRepository})
      : assert(authProfileRepository != null);

  @override
  UpdatePasswordState get initialState => UpdatePasswordInitial();

  @override
  Stream<UpdatePasswordState> mapEventToState(
    UpdatePasswordEvent event,
  ) async* {
    if (event is UpdatePassword) {
      yield UpdatePasswordLoading();

      try {
        await authProfileRepository.changePasswordBeforeLogin(
            phone: event.phone,
            verificationToken: event.verifyToken,
            password: event.password,
            retypePassword: event.retypePassword);
        yield UpdatePasswordSuccess();
      } on ValidationException catch (error) {
        yield ValidationUpdatePasswordError(errors: error.errors);
      } catch (e) {
        yield UpdatePasswordFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
