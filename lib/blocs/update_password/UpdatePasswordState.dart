import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class UpdatePasswordState extends Equatable {
  UpdatePasswordState([List props = const <dynamic>[]]);
}

class UpdatePasswordInitial extends UpdatePasswordState {
  @override
  List<Object> get props => [];
}

class UpdatePasswordLoading extends UpdatePasswordState {
  @override
  String toString() {
    return 'State UpdatePasswordLoading';
  }

  @override
  List<Object> get props => [];
}

class UpdatePasswordSuccess extends UpdatePasswordState {
  @override
  String toString() {
    return 'State UpdatePasswordSuccess';
  }

  @override
  List<Object> get props => [];
}

class ValidationUpdatePasswordError extends UpdatePasswordState {
  final Map<String, dynamic> errors;

  ValidationUpdatePasswordError({@required this.errors}) : super([errors]);

  @override
  String toString() {
    return 'State ValidationUpdatePasswordError{errors: $errors}';
  }

  @override
  List<Object> get props => <Object>[errors];
}

class UpdatePasswordFailure extends UpdatePasswordState {
  final String error;

  UpdatePasswordFailure({this.error});

  @override
  String toString() {
    return 'State UpdatePasswordFailure{error: $error}';
  }

  @override
  List<Object> get props => <Object>[error];
}
