import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class UpdatePasswordEvent extends Equatable {
  UpdatePasswordEvent([List props = const <dynamic>[]]);
}

class UpdatePassword extends UpdatePasswordEvent {
  final String phone;
  final String password;
  final String retypePassword;
  final String verifyToken;

  UpdatePassword(
      {@required this.phone,
      @required this.verifyToken,
      @required this.password,
      @required this.retypePassword})
      : assert((phone != null) &&
            (verifyToken != null) &&
            (password != null) &&
            (retypePassword != null));

  @override
  String toString() {
    return 'Event UpdatePassword';
  }

  @override
  List<Object> get props => [phone, verifyToken, password, retypePassword];
}
