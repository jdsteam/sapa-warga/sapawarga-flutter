import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/QnAModel.dart';
import 'package:sapawarga/repositories/QnAGovernorRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import '../Bloc.dart';

class QnAGovernorListBloc extends Bloc<QnAGovernorListEvent, QnAGovernorListState> {
  final QnAGovernorRepository repository;

  QnAGovernorListBloc(this.repository);

  @override
  QnAGovernorListState get initialState => QnAGovernorListInitial();

  @override
  Stream<QnAGovernorListState> mapEventToState(
    QnAGovernorListEvent event,
  ) async* {

     if (event is QnAGovernorListLoad) {
       if (event.page == 1) yield QnAGovernorListLoading();

       try {
         List<QnAModel> records = await repository.fetchRecords(page: event.page);
         int maxLength = await Preferences.getQnATotalCount();
         yield QnAGovernorListLoaded(records, maxLength);
       } catch (e) {
         yield QnAGovernorListFailure(error: CustomException.onConnectionException(e.toString()));
       }

     }

     if (event is QnAGovernorListLike) {
       try {
         await repository.sendLike(id: event.id);
       } catch (e) {
         yield QnAGovernorListFailure(error: CustomException.onConnectionException(e.toString()));
       }
     }
  }
}
