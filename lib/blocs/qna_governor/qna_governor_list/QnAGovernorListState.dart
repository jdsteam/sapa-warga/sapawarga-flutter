import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/QnAModel.dart';

abstract class QnAGovernorListState extends Equatable {
  const QnAGovernorListState([List props = const <dynamic>[]]);
}

class QnAGovernorListInitial extends QnAGovernorListState {
  @override
  List<Object> get props => [];
}

class QnAGovernorListLoading extends QnAGovernorListState {
  @override
  String toString() {
    return 'State QnAGovernorListLoading';
  }

  @override
  List<Object> get props => [];
}

class QnAGovernorListLoaded extends QnAGovernorListState {
  final List<QnAModel> records;
  final int maxLength;

  QnAGovernorListLoaded(this.records, this.maxLength) : super([records, maxLength]);

  @override
  String toString() {
    return 'State QnAGovernorListLoaded';
  }

  @override
  List<Object> get props => [records];
}

class QnAGovernorListFailure extends QnAGovernorListState {
  final String error;

  QnAGovernorListFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State QnAGovernorListFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}