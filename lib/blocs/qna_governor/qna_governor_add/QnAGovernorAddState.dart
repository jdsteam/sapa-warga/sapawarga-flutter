import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/QnAModel.dart';

abstract class QnAGovernorAddState extends Equatable {
  const QnAGovernorAddState([List props = const <dynamic>[]]);
}

class QnAGovernorAddInitial extends QnAGovernorAddState {
  @override
  List<Object> get props => [];
}

class QnAGovernorAddLoading extends QnAGovernorAddState {
  @override
  String toString() {
    return 'State QnAGovernorAddLoading';
  }

  @override
  List<Object> get props => [];
}

class QnAGovernorAdded extends QnAGovernorAddState {
  final QnAModel record;

  QnAGovernorAdded(this.record) : super([record]);

  @override
  String toString() {
    return 'State QnAGovernorAdded';
  }

  @override
  List<Object> get props => [record];
}

class QnAGovernorAddFailure extends QnAGovernorAddState {
  final String error;

  QnAGovernorAddFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State QnAGovernorAddFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
