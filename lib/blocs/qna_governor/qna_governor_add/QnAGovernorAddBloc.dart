import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/QnAModel.dart';
import 'package:sapawarga/repositories/QnAGovernorRepository.dart';
import '../Bloc.dart';

class QnAGovernorAddBloc extends Bloc<QnAGovernorAddEvent, QnAGovernorAddState> {
  final QnAGovernorRepository repository;

  QnAGovernorAddBloc(this.repository);

  @override
  QnAGovernorAddState get initialState => QnAGovernorAddInitial();

  @override
  Stream<QnAGovernorAddState> mapEventToState(
    QnAGovernorAddEvent event,
  ) async* {
    if (event is QnAGovernorAdd) {
      yield QnAGovernorAddLoading();

      try {
        QnAModel record = await repository.addQuestion(question: event.question);
        yield QnAGovernorAdded(record);
      } catch (e) {
        yield QnAGovernorAddFailure(error: CustomException.onConnectionException(e.toString()));
      }

    }
  }
}
