import 'package:equatable/equatable.dart';

abstract class QnAGovernorDetailEvent extends Equatable {
  const QnAGovernorDetailEvent([List props = const <dynamic>[]]);
}

class QnAGovernorDetailLoad extends QnAGovernorDetailEvent {
  final int id;

  QnAGovernorDetailLoad(this.id) : super([id]);

  @override
  String toString() {
    return 'Event QnAGovernorDetailLoad{id: $id}';
  }

  @override
  List<Object> get props => [id];
}

class QnAGovernorDetailLike extends QnAGovernorDetailEvent {
  final int id;

  QnAGovernorDetailLike(this.id) : super([id]);

  @override
  String toString() {
    return 'Event QnAGovernorDetailLike{id: $id}';
  }

  @override
  List<Object> get props => [id];
}
