import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/QnAModel.dart';
import 'package:sapawarga/repositories/QnAGovernorRepository.dart';
import '../Bloc.dart';

class QnAGovernorDetailBloc
    extends Bloc<QnAGovernorDetailEvent, QnAGovernorDetailState> {
  final QnAGovernorRepository repository;

  QnAGovernorDetailBloc(this.repository);

  @override
  QnAGovernorDetailState get initialState => QnAGovernorDetailInitial();

  @override
  Stream<QnAGovernorDetailState> mapEventToState(
    QnAGovernorDetailEvent event,
  ) async* {
    if (event is QnAGovernorDetailLoad) {
      yield QnAGovernorDetailLoading();

      try {
        QnAModel record = await repository.getDetail(id: event.id);
        yield QnAGovernorDetailLoaded(record);
      } catch (e) {
        yield QnAGovernorDetailFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is QnAGovernorDetailLike) {
      try {
        await repository.sendLike(id: event.id);
      } catch (e) {
        yield QnAGovernorDetailFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
