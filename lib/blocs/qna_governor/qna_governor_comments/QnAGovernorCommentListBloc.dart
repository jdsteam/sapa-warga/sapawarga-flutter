import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/QnACommentModel.dart';
import 'package:sapawarga/repositories/QnAGovernorRepository.dart';
import '../Bloc.dart';

class QnAGovernorCommentListBloc extends Bloc<QnAGovernorCommentListEvent, QnAGovernorCommentListState> {
  QnAGovernorRepository repository;

  QnAGovernorCommentListBloc(this.repository);

  @override
  QnAGovernorCommentListState get initialState => QnAGovernorCommentListInitial();

  @override
  Stream<QnAGovernorCommentListState> mapEventToState(
    QnAGovernorCommentListEvent event,
  ) async* {
    if (event is QnAGovernorCommentListLoad) {
      if (event.page == 1) yield QnAGovernorCommentListLoading();

      try {
        List<QnACommentModel> records = await repository.fetchComments(questionId: event.questionId, page: event.page);
        yield QnAGovernorCommentListLoaded(records);
      } catch (e) {
        yield QnAGovernorCommentListFailure(error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
