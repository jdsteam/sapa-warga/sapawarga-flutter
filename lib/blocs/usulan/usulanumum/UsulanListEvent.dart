import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class UsulanListEvent extends Equatable {
  UsulanListEvent([List props = const <dynamic>[]]);
}

class UsulanListLoad extends UsulanListEvent {
  final int page;

  UsulanListLoad({@required this.page}) : super([page]);

  @override
  String toString() {
    return 'Event UsulanListLoad';
  }

  @override
  List<Object> get props => [page];
}

class UsulanListRefresh extends UsulanListEvent {
  @override
  String toString() {
    return 'Event UsulanListRefresh';
  }

  @override
  List<Object> get props => [];
}

class UsulanListLike extends UsulanListEvent {
  final int id;

  UsulanListLike({@required this.id}) : super([id]);

  @override
  String toString() {
    return 'Event UsulanListLike';
  }

  @override
  List<Object> get props => [id];
}
