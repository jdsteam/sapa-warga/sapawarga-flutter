import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/UsulanModel.dart';
import 'package:sapawarga/repositories/UsulanRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'Bloc.dart';


class UsulanListBloc extends Bloc<UsulanListEvent, UsulanListState> {
  final UsulanRepository usulanRepository;

  UsulanListBloc({@required this.usulanRepository}):assert(usulanRepository != null);

  @override
  UsulanListState get initialState => UsulanListInitial();

  @override
  Stream<UsulanListState> mapEventToState(
    UsulanListEvent event,
  ) async* {
    if (event is UsulanListLoad) {
      if (event.page == 1) yield UsulanListLoading();

      try {
        List<UsulanModel> records = await usulanRepository.getUsulan(page: event.page);
        int maxDatalength = await Preferences.getTotalCount();
        yield UsulanListLoaded(records: records, maxLengthData: maxDatalength, isLoad: true);
      } catch (e) {
        yield UsulanListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
    if (event is UsulanListRefresh) {
      yield UsulanListLoading();
      try {
        List<UsulanModel> records = await usulanRepository.getUsulan(page: 1);
        int maxDatalength = await Preferences.getTotalCount();
        yield UsulanListLoaded(records: records, maxLengthData: maxDatalength, isLoad: true);
      } catch (e) {
        yield UsulanListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
    if(event is UsulanListLike){
      try{
       await usulanRepository.likeUsulan(id: event.id);
      }catch(e){
        yield UsulanListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
