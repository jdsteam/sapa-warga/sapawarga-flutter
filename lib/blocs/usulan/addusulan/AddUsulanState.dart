import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/AddPhotoModel.dart';

@immutable
abstract class AddUsulantState extends Equatable {
  AddUsulantState([List props = const <dynamic>[]]);
}

class AddUsulanInitial extends AddUsulantState {
  @override
  String toString() => 'State AddUsulanInitial';

  @override
  List<Object> get props => [];
}

class AddUsulanLoading extends AddUsulantState {
  @override
  String toString() {
    return 'State AddUsulanLoading';
  }

  @override
  List<Object> get props => [];
}

class AddUsulanUpdated extends AddUsulantState {
  @override
  String toString() {
    return 'State AddUsulanUpdated';
  }

  @override
  List<Object> get props => [];
}

class AddUsulanPhotoLoading extends AddUsulantState {
  @override
  String toString() {
    return 'State AddUsulanPhotoLoading';
  }

  @override
  List<Object> get props => [];
}

class AddUsulanPhotoDone extends AddUsulantState {
  final AddPhotoModel addPhotoModel;

  AddUsulanPhotoDone({this.addPhotoModel}) : super([addPhotoModel]);

  @override
  String toString() {
    return 'State AddUsulanPhotoDone';
  }

  @override
  List<Object> get props => [addPhotoModel];
}

class AddUsulanFailure extends AddUsulantState {
  final String error;

  AddUsulanFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State AddUsulanFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}

class AddUsulanPhotoFailure extends AddUsulantState {
  final String error;

  AddUsulanPhotoFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State AddUsulanPhotoFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
