import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';

@immutable
abstract class AddCategoryUsulantState extends Equatable {
  AddCategoryUsulantState([List props = const <dynamic>[]]);
}

class AddCategoryUsulanInitial extends AddCategoryUsulantState {
  @override
  String toString() => 'State AddCategoryUsulanInitial';

  @override
  List<Object> get props => [];
}

class AddCategoryUsulanLoading extends AddCategoryUsulantState {
  @override
  String toString() {
    return 'State AddCategoryUsulanLoading';
  }

  @override
  List<Object> get props => [];
}

class AddCategoryUsulanLoaded extends AddCategoryUsulantState {
  final List<MasterCategoryModel> records;

  AddCategoryUsulanLoaded({@required this.records}) : super([records]);

  @override
  String toString() {
    return 'State AddCategoryUsulanLoaded';
  }

  @override
  List<Object> get props => [records];
}

class AddCategoryUsulanFailure extends AddCategoryUsulantState {
  final String error;

  AddCategoryUsulanFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State AddCategoryUsulanFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
