import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AddCategoryUsulanEvent extends Equatable {
  AddCategoryUsulanEvent([List props = const <dynamic>[]]);
}

class AddCategoryUsulanLoad extends AddCategoryUsulanEvent {
  final int page;

  AddCategoryUsulanLoad({@required this.page}) : super([page]);

  @override
  String toString() {
    return 'Event AddCategoryUsulanLoad';
  }

  @override
  List<Object> get props => [page];
}
