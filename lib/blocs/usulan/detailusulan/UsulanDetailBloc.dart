import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:sapawarga/blocs/usulan/detailusulan/Bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/UsulanModel.dart';
import 'package:sapawarga/repositories/UsulanRepository.dart';

class UsulanDetailBloc extends Bloc<UsulanDetailEvent, UsulanDetailState> {
  final UsulanRepository usulanRepository;

  UsulanDetailBloc({@required this.usulanRepository}):assert(usulanRepository != null);

  @override
  UsulanDetailState get initialState => UsulanDetailInitial();

  @override
  Stream<UsulanDetailState> mapEventToState(
    UsulanDetailEvent event,
  ) async* {
    if (event is UsulanDetailLoad) {
      yield UsulanDetailLoading();

      try {
        UsulanModel usulanDetail =
            await usulanRepository.getUsulanDetail(id: event.usulanId);
        var list = await usulanRepository.getRecordsCategoryUsulan();
        yield UsulanDetailLoaded(
            usulanModel: usulanDetail, categoryUsulan: list);
      } catch (e) {
        yield UsulanDetailFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is DeleteUsulan) {
      yield DeleteUsulanlLoading();

      try {
        await usulanRepository.deleteUsulan(idUsulan: event.usulanId);
        yield DeleteUsulanLoaded();
      } catch (e) {
        yield UsulanDetailFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if(event is UsulanDetailLike){
      try{
        await usulanRepository.likeUsulan(id: event.id);
      }catch(e){
        yield UsulanDetailFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
