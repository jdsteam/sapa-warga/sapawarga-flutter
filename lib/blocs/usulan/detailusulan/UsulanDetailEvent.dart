import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class UsulanDetailEvent extends Equatable {
  UsulanDetailEvent([List props = const <dynamic>[]]);
}

class UsulanDetailLoad extends UsulanDetailEvent {
  final int usulanId;

  UsulanDetailLoad({@required this.usulanId}) : super([usulanId]);

  @override
  String toString() {
    return 'Event UsulanDetailLoad{newsId: $usulanId}';
  }

  @override
  List<Object> get props => [usulanId];
}

class DeleteUsulan extends UsulanDetailEvent {
  final int usulanId;

  DeleteUsulan({@required this.usulanId}) : super([usulanId]);

  @override
  String toString() {
    return 'Event DeleteUsulan{usulanId: $usulanId}';
  }

  @override
  List<Object> get props => [usulanId];
}

class UsulanDetailLike extends UsulanDetailEvent {
  final int id;

  UsulanDetailLike({@required this.id}) : super([id]);

  @override
  String toString() {
    return 'Event UsulanDetailLike';
  }

  @override
  List<Object> get props => [id];
}
