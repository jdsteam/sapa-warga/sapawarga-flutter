import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';
import 'package:sapawarga/models/UsulanModel.dart';

@immutable
abstract class UsulanDetailState extends Equatable {
  UsulanDetailState([List props = const <dynamic>[]]);
}

class UsulanDetailInitial extends UsulanDetailState {
  @override
  List<Object> get props => [];
}

class UsulanDetailLoading extends UsulanDetailState {
  @override
  String toString() {
    return 'State UsulanDetailLoading';
  }

  @override
  List<Object> get props => [];
}

class UsulanDetailLoaded extends UsulanDetailState {
  final UsulanModel usulanModel;
  final List<MasterCategoryModel> categoryUsulan;

  UsulanDetailLoaded({this.usulanModel, this.categoryUsulan});

  @override
  String toString() {
    return 'State UsulanDetailLoaded';
  }

  @override
  List<Object> get props => [usulanModel, categoryUsulan];
}

class UsulanDetailFailure extends UsulanDetailState {
  final String error;

  UsulanDetailFailure({this.error});

  @override
  String toString() {
    return 'UsulanDetailFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}

class DeleteUsulanlLoading extends UsulanDetailState {
  @override
  String toString() {
    return 'State DeleteUsulanlLoading';
  }

  @override
  List<Object> get props => [];
}

class DeleteUsulanLoaded extends UsulanDetailState {
  @override
  String toString() {
    return 'State DeleteUsulanLoaded';
  }

  @override
  List<Object> get props => [];
}
