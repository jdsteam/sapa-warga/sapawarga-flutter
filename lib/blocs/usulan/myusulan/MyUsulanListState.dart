import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/UsulanModel.dart';

@immutable
abstract class MyUsulanListState extends Equatable {
  MyUsulanListState([List props = const <dynamic>[]]);
}

class MyUsulanListInitial extends MyUsulanListState {
  @override
  String toString() => 'State MyUsulanListInitial';

  @override
  List<Object> get props => [];
}

class MyUsulanListLoading extends MyUsulanListState {
  @override
  String toString() {
    return 'State MyUsulanListLoading';
  }

  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class MyUsulanListLoaded extends MyUsulanListState {
  final List<UsulanModel> records;
  final int maxData;
  bool isLoadData;

  MyUsulanListLoaded({@required this.records, @required this.maxData, @required this.isLoadData}) : super([records, maxData, isLoadData]);

  @override
  String toString() {
    return 'State MyUsulanListLoaded';
  }

  @override
  List<Object> get props => [records];
}

class MyUsulanListFailure extends MyUsulanListState {
  final String error;

  MyUsulanListFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State MyUsulanListFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
