import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/UsulanModel.dart';
import 'package:sapawarga/repositories/UsulanRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import 'Bloc.dart';

class MyUsulanListBloc extends Bloc<MyUsulanListEvent, MyUsulanListState> {
  final UsulanRepository usulanRepository;

  MyUsulanListBloc({@required this.usulanRepository}):assert(usulanRepository != null);

  @override
  MyUsulanListState get initialState => MyUsulanListInitial();

  @override
  Stream<MyUsulanListState> mapEventToState(
    MyUsulanListEvent event,
  ) async* {
    if (event is MyUsulanListLoad) {
      if (event.page == 1) yield MyUsulanListLoading();

      try {
        List<UsulanModel> records =
            await usulanRepository.getMyUsulan(page: event.page);
        int maxDatalength = await Preferences.getUsulanTotalCountMe();
        yield MyUsulanListLoaded(records: records, maxData: maxDatalength, isLoadData: true);
      } catch (e) {
        yield MyUsulanListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is MyUsulanListRefresh) {
      yield MyUsulanListLoading();
      try {
        List<UsulanModel> records = await usulanRepository.getMyUsulan(page: 1);
        int maxDatalength = await Preferences.getUsulanTotalCountMe();
        yield MyUsulanListLoaded(records: records, maxData: maxDatalength, isLoadData: true);
      } catch (e) {
        yield MyUsulanListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
