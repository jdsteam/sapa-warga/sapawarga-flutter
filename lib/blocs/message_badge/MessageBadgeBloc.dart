import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/repositories/BroadcastRepository.dart';
import './Bloc.dart';

class MessageBadgeBloc extends Bloc<MessageBadgeEvent, MessageBadgeState> {

  BroadcastRepository broadcastRepository;

  MessageBadgeBloc({@required this.broadcastRepository}):assert(broadcastRepository != null);

  @override
  MessageBadgeState get initialState => MessageBadgeInitial();

  @override
  Stream<MessageBadgeState> mapEventToState(
    MessageBadgeEvent event,
  ) async* {
    if (event is CheckMessageBadge) {
      yield MessageBadgeLoading();
      try {
        int unreadCount = await broadcastRepository.hasUnreadData();
        if (unreadCount > 0) {
          yield MessageBadgeShow(fromNotification: event.fromNotification, count: unreadCount);
        } else {
          yield MessageBadgeHide();
        }
      } catch (e) {
        print(e.toString());
      }
    }
  }
}
