import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MessageBadgeState extends Equatable {
  MessageBadgeState([List props = const <dynamic>[]]);
}

class MessageBadgeInitial extends MessageBadgeState {
  @override
  List<Object> get props => [];
}

class MessageBadgeLoading extends MessageBadgeState {
  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'State MessageBadgeLoading';
  }
}

class MessageBadgeShow extends MessageBadgeState {
  final bool fromNotification;
  final int count;

  MessageBadgeShow({this.fromNotification = false, @required this.count});

  @override
  String toString() {
    return 'State MessageBadgeShow';
  }

  @override
  List<Object> get props => [fromNotification];
}

class MessageBadgeHide extends MessageBadgeState {
  @override
  String toString() {
    return 'State MessageBadgeHide';
  }

  @override
  List<Object> get props => [];
}
