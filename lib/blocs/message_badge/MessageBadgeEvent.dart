import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MessageBadgeEvent extends Equatable {
  MessageBadgeEvent([List props = const <dynamic>[]]);
}

class CheckMessageBadge extends MessageBadgeEvent {
  final bool fromNotification;

  CheckMessageBadge({this.fromNotification = false});

  @override
  String toString() {
    return 'Event MessageBadgeCheck';
  }

  @override
  List<Object> get props => [fromNotification];
}
