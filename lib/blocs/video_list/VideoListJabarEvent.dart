import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class VideoListJabarEvent extends Equatable {
  VideoListJabarEvent([List props = const <dynamic>[]]);
}

class VideoListJabarLoad extends VideoListJabarEvent {
  @override
  String toString() {
    return 'Event VideoListJabarLoad';
  }

  @override
  List<Object> get props => [];
}
