import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/models/VideoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/VideoRepository.dart';

import 'package:sapawarga/blocs/video_list/Bloc.dart';

class VideoListKokabBloc
    extends Bloc<VideoListKokabEvent, VideoListKokabState> {
  final VideoRepository videoRepository;
  final AuthProfileRepository authProfileRepository;

  VideoListKokabBloc({@required this.videoRepository, this.authProfileRepository}) : assert(videoRepository != null);

  @override
  VideoListKokabState get initialState => VideoListKokabInitial();

  @override
  Stream<VideoListKokabState> mapEventToState(
    VideoListKokabEvent event,
  ) async* {
    if (event is VideoListKokabLoad) {
      yield VideoListKokabLoading();

      try {
        UserInfoModel userInfo = await authProfileRepository.getUserInfo();

        List<VideoModel> records =
            await videoRepository.fetchRecords(kokabId: userInfo.kabkotaId);
        yield VideoListKokabLoaded(
            records: records, kokab: userInfo.kabkota.name);
      } catch (e) {
        UserInfoModel userInfo = await authProfileRepository.getUserInfo();

        bool hasRecords = await videoRepository.hasVideosKokabLocal();
        if (hasRecords) {
          List<VideoModel> records =
              await videoRepository.getVideosKokabLocal();
          yield VideoListKokabLoaded(
              records: records, kokab: userInfo.kabkota.name);
        } else {
          print(e.toString());
          yield VideoListKokabFailure(
              error: CustomException.onConnectionException(e.toString()));
        }
      }
    }
  }
}
