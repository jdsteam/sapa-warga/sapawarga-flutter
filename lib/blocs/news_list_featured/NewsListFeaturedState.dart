import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/NewsModel.dart';

@immutable
abstract class NewsListFeaturedState extends Equatable {
  NewsListFeaturedState([List props = const <dynamic>[]]);
}

class NewsListFeaturedInitial extends NewsListFeaturedState {
  @override
  List<Object> get props => [];
}

class NewsListFeaturedLoading extends NewsListFeaturedState {
  @override
  String toString() {
    return 'State NewsListFeaturedLoading';
  }

  @override
  List<Object> get props => [];
}

class NewsListFeatuedLoaded extends NewsListFeaturedState {
  final List<NewsModel> listtNews;
  final String cityName;

  NewsListFeatuedLoaded({this.listtNews, this.cityName});

  @override
  String toString() {
    return 'State NewsListFeatuedLoaded';
  }

  @override
  List<Object> get props => [listtNews, cityName];
}

class NewsListFeaturedFailure extends NewsListFeaturedState {
  final String error;

  NewsListFeaturedFailure({this.error});

  @override
  String toString() {
    return 'NewsListFeaturedFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
