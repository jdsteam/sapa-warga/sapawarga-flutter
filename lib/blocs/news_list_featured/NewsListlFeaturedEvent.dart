import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class NewsListFeaturedEvent extends Equatable {
  NewsListFeaturedEvent([List props = const <dynamic>[]]);
}

class NewsListFeaturedLoad extends NewsListFeaturedEvent {
  final bool isIdKota;

  NewsListFeaturedLoad({@required this.isIdKota}) : super([isIdKota]);

  @override
  String toString() {
    return 'Event NewsListFeaturedLoad';
  }

  @override
  List<Object> get props => [isIdKota];
}
