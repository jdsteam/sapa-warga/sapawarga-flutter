import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/ImportantInfoCommentModel.dart';
import 'package:sapawarga/repositories/ImportantInfoRepository.dart';
import '../Bloc.dart';

class ImportantInfoCommentAddBloc extends Bloc<ImportantInfoCommentAddEvent, ImportantInfoCommentAddState> {

  ImportantInfoRepository repository;

  ImportantInfoCommentAddBloc(this.repository);

  @override
  ImportantInfoCommentAddState get initialState => ImportantInfoCommentAddInitial();

  @override
  Stream<ImportantInfoCommentAddState> mapEventToState(
    ImportantInfoCommentAddEvent event,
  ) async* {
    if (event is ImportantInfoCommentAdd) {
      yield ImportantInfoCommentAddLoading();

      try {
        ItemImportantInfoComment record = await repository.postComment(id: event.id, text: event.text);
        yield ImportantInfoCommentAdded(record);
      } catch (e) {
        yield ImportantInfoCommentAddFailure(error: CustomException.onConnectionException(e.toString()));
      }

    }
  }
}
