import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/ImportantInfoCommentModel.dart';

abstract class ImportantInfoCommentAddState extends Equatable {
  const ImportantInfoCommentAddState([List props = const <dynamic>[]]);
}

class ImportantInfoCommentAddInitial extends ImportantInfoCommentAddState {
  @override
  List<Object> get props => [];
}

class ImportantInfoCommentAddLoading extends ImportantInfoCommentAddState {
  @override
  String toString() {
    return 'State ImportantInfoCommentAddLoading';
  }

  @override
  List<Object> get props => [];
}

class ImportantInfoCommentAdded extends ImportantInfoCommentAddState {
  final ItemImportantInfoComment record;


  ImportantInfoCommentAdded(this.record):super([record]);

  @override
  String toString() {
    return 'State ImportantInfoCommentAdded';
  }

  @override
  List<Object> get props => [record];
}

class ImportantInfoCommentAddFailure extends ImportantInfoCommentAddState {
  final String error;

  ImportantInfoCommentAddFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State ImportantInfoCommentAddFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}