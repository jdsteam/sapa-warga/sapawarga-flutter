import 'package:equatable/equatable.dart';

abstract class ImportantInfoCommentListEvent extends Equatable {
  const ImportantInfoCommentListEvent([List props = const <dynamic>[]]);
}

class ImportantInfoCommentListLoad extends ImportantInfoCommentListEvent {
  final int id;
  final int page;

  ImportantInfoCommentListLoad(this.id, this.page):super([page]);


  @override
  String toString() {
    return 'Event ImportantInfoCommentListLoad{id: $id, page: $page}';
  }

  @override
  List<Object> get props => [id, page];

}
