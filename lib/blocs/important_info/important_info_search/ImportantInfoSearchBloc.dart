import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';
import 'package:sapawarga/repositories/ImportantInfoRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';
import './Bloc.dart';

class ImportantInfoSearchBloc extends Bloc<ImportantInfoSearchEvent, ImportantInfoSearchState> {
  ImportantInfoRepository repository;

  ImportantInfoSearchBloc(this.repository);

  @override
  ImportantInfoSearchState get initialState => ImportantInfoSearchInitial();

  @override
  Stream<ImportantInfoSearchState> mapEventToState(
    ImportantInfoSearchEvent event,
  ) async* {
    if (event is ImportantInfoSearch) {
      if (event.page == 1) yield ImportantInfoSearchLoading();

      try {
        List<ImportantInfoModel> records = await repository.searchRecords(keyword: event.keyword, page: event.page);
        int maxLength = await Preferences.getTotalCount();
        yield ImportantInfoSearchLoaded(records, maxLength);
      } catch (e) {
        yield ImportantInfoSearchFailure(error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
