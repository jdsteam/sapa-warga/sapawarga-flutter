import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ImportantInfoHomeEvent extends Equatable {
  ImportantInfoHomeEvent([List props = const <dynamic>[]]);
}

class ImportantInfoHomeLoad extends ImportantInfoHomeEvent {
  @override
  String toString() {
    return 'Event ImportantInfoHomeLoad';
  }

  @override
  List<Object> get props => [];
}
