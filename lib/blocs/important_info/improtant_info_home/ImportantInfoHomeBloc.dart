import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/repositories/ImportantInfoRepository.dart';
import './Bloc.dart';

class ImportantInfoHomeBloc extends Bloc<ImportantInfoHomeEvent, ImportantInfoHomeState> {

  final ImportantInfoRepository importantInfoRepository;

  ImportantInfoHomeBloc({@required this.importantInfoRepository}):assert(importantInfoRepository!=null);

  @override
  ImportantInfoHomeState get initialState => ImportantInfoHomeInitial();

  @override
  Stream<ImportantInfoHomeState> mapEventToState(
    ImportantInfoHomeEvent event,
  ) async* {
    if (event is ImportantInfoHomeLoad) {
      yield ImportantInfoHomeLoading();
      try {
        final records = await importantInfoRepository.fetchRecords(0, limit: 3);
        yield ImportantInfoHomeLoaded(records);
      } catch(e) {
        yield ImportantInfoHomeFailure(error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
