import 'package:equatable/equatable.dart';

abstract class ImportantInfoCategoriesEvent extends Equatable {
  const ImportantInfoCategoriesEvent([List props = const <dynamic>[]]);
}

class ImportantInfoCategoriesLoad extends ImportantInfoCategoriesEvent {

  @override
  String toString() {
    return 'Event ImportantInfoCategoriesLoad';
  }

  @override
  List<Object> get props => [];
}
