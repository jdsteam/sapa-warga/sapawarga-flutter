import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/MasterCategoryModel.dart';

abstract class ImportantInfoCategoriesState extends Equatable {
  const ImportantInfoCategoriesState([List props = const <dynamic>[]]);
}

class InitialImportantInfoCategoriesState extends ImportantInfoCategoriesState {
  @override
  List<Object> get props => [];
}

class ImportantInfoCategoriesLoading extends ImportantInfoCategoriesState {
  @override
  String toString() {
    return 'State ImportantInfoCategoriesLoading';
  }

  @override
  List<Object> get props => [];
}

class ImportantInfoCategoriesLoaded extends ImportantInfoCategoriesState {
  final List<MasterCategoryModel> records;

  ImportantInfoCategoriesLoaded({@required this.records}) : assert(records != null);

  @override
  String toString() {
    return 'State ImportantInfoCategoriesLoaded';
  }

  @override
  List<Object> get props => [records];
}

class ImportantInfoCategoriesFailure extends ImportantInfoCategoriesState {
  final String error;

  ImportantInfoCategoriesFailure({this.error});

  @override
  String toString() {
    return 'State ImportantInfoCategoriesFailure{error: $error}';
  }

  List<Object> get props => [error];
}