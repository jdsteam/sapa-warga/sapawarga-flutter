import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';
import 'package:sapawarga/repositories/ImportantInfoRepository.dart';
import './Bloc.dart';

class ImportantInfoListTopBloc
    extends Bloc<ImportantInfoListTopEvent, ImportantInfoListTopState> {
  final ImportantInfoRepository importantInfoRepository;

  final ImportantInfoListOthersBloc importantInfoListOthersBloc;

  ImportantInfoListTopBloc(
      {this.importantInfoRepository, this.importantInfoListOthersBloc});

  @override
  ImportantInfoListTopState get initialState => ImportantInfoListTopInitial();

  @override
  Stream<ImportantInfoListTopState> mapEventToState(
    ImportantInfoListTopEvent event,
  ) async* {
    if (event is ImportantInfoListTopLoad) {
      yield ImportantInfoListTopLoading();
      try {
        List<ImportantInfoModel> records =
            await importantInfoRepository.getTopFiveRecords(event.categoryId);

        yield ImportantInfoListTopLoaded(records: records);

        importantInfoListOthersBloc
            .add(ImportantInfoListOthersLoad(event.categoryId));
      } catch (e) {
        yield ImportantInfoListTopFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is ImportantInfoListTopLoadRefreshViewer) {
      try {
        List<ImportantInfoModel> records =
            await importantInfoRepository.getTopFiveRecords(event.categoryId);
        yield ImportantInfoListTopLoadingRefreshViewer();

        yield ImportantInfoListTopLoaded(records: records);

        importantInfoListOthersBloc
            .add(ImportantInfoListOthersLoadRefreshViewer(event.categoryId));
      } catch (e) {
        yield ImportantInfoListTopFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
