import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';

@immutable
abstract class ImportantInfoListTopState extends Equatable {
  ImportantInfoListTopState([List props = const <dynamic>[]]);
}

class ImportantInfoListTopInitial extends ImportantInfoListTopState {
  @override
  List<Object> get props => [];
}

class ImportantInfoListTopLoading extends ImportantInfoListTopState {
  @override
  String toString() {
    return 'State ImportantInfoListTopLoading';
  }

  @override
  List<Object> get props => [];
}

class ImportantInfoListTopLoadingRefreshViewer extends ImportantInfoListTopState {
  @override
  String toString() {
    return 'State ImportantInfoListTopLoadingRefreshViewer';
  }

  @override
  List<Object> get props => [];
}

class ImportantInfoListTopLoaded extends ImportantInfoListTopState {
  final List<ImportantInfoModel> records;

  ImportantInfoListTopLoaded({this.records});

  @override
  String toString() {
    return 'State ImportantInfoListTopLoaded';
  }

  @override
  List<Object> get props => [records];
}

class ImportantInfoListTopFailure extends ImportantInfoListTopState {
  final String error;

  ImportantInfoListTopFailure({this.error});

  @override
  String toString() {
    return 'State ImportantInfoListTopFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
