import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/ImprotantInfoModel.dart';

@immutable
abstract class ImportantInfoListOthersState extends Equatable {
  ImportantInfoListOthersState([List props = const <dynamic>[]]);
}

class ImportantInfoListOthersInitial extends ImportantInfoListOthersState {
  @override
  List<Object> get props => [];
}

class ImportantInfoListOthersLoading extends ImportantInfoListOthersState {
  @override
  String toString() {
    return 'State ImportantInfoListOthersLoading';
  }

  @override
  List<Object> get props => [];
}

class ImportantInfoListOthersLoadingRefreshViewer
    extends ImportantInfoListOthersState {
  @override
  String toString() {
    return 'State ImportantInfoListOthersLoadingRefreshViewer';
  }

  @override
  List<Object> get props => [];
}

class ImportantInfoListOthersLoaded extends ImportantInfoListOthersState {
  final List<ImportantInfoModel> records;

  ImportantInfoListOthersLoaded({this.records});

  @override
  String toString() {
    return 'State ImportantInfoListOthersLoaded';
  }

  @override
  List<Object> get props => [records];
}

class ImportantInfoListOthersFailure extends ImportantInfoListOthersState {
  final String error;

  ImportantInfoListOthersFailure({this.error});

  @override
  String toString() {
    return 'State ImportantInfoListOthersFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
