import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/BannerModel.dart';
import 'package:sapawarga/repositories/BannerRepository.dart';
import './Bloc.dart';

class BannerListBloc extends Bloc<BannerListEvent, BannerListState> {

  final BannerRepository bannerRepository;


  BannerListBloc({@required this.bannerRepository}):assert(bannerRepository != null);

  @override
  BannerListState get initialState => BannerListInitial();

  @override
  Stream<BannerListState> mapEventToState(
    BannerListEvent event,
  ) async* {
    if(event is BannerListLoad) {
      yield BannerListLoading();
      try {
        List<BannerModel> records = await bannerRepository.fetchRecords();
        yield BannerListLoaded(records: records);
      } catch (e) {
        yield BannerListFailure(error: CustomException.onConnectionException(e));
      }
    }
  }
}
