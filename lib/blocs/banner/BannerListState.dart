import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/BannerModel.dart';

@immutable
abstract class BannerListState extends Equatable {
  BannerListState([List props = const <dynamic>[]]);
}

class BannerListInitial extends BannerListState {
  @override
  List<Object> get props => [];
}

class BannerListLoading extends BannerListState {
  @override
  String toString() {
    return 'State BannerListLoading';
  }

  @override
  List<Object> get props => [];
}

class BannerListLoaded extends BannerListState {
  final List<BannerModel> records;

  BannerListLoaded({@required this.records});

  @override
  String toString() {
    return 'State BannerListLoaded';
  }

  @override
  List<Object> get props => [records];
}

class BannerListFailure extends BannerListState {
  final String error;

  BannerListFailure({this.error});

  @override
  String toString() {
    return 'State BannerListFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
