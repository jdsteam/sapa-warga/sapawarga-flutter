import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/PopupInformationModel.dart';

@immutable
abstract class PopupInformationState extends Equatable {
  PopupInformationState([List props = const <dynamic>[]]);
}

class PopupInformationInitial extends PopupInformationState {
  @override
  List<Object> get props => [];
}

class PopupInformationShow extends PopupInformationState {
  final PopupInformationModel record;

  PopupInformationShow({@required this.record});

  @override
  String toString() {
    return 'State PopupInformationShow';
  }

  @override
  List<Object> get props => [record];
}

class PopupInformationHasShown extends PopupInformationState {
  @override
  String toString() {
    return 'State PopupInformationHasShown';
  }

  @override
  List<Object> get props => [];
}
