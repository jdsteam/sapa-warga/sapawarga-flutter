import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/PopupInformationModel.dart';
import 'package:sapawarga/repositories/PopupInformationRepository.dart';

import './Bloc.dart';

class PopupInformationBloc
    extends Bloc<PopupInformationEvent, PopupInformationState> {
  final PopupInformationRepository repository;

  PopupInformationBloc({@required this.repository}):assert(repository != null);

  @override
  PopupInformationState get initialState => PopupInformationInitial();

  @override
  Stream<PopupInformationState> mapEventToState(
    PopupInformationEvent event,
  ) async* {
    if (event is CheckPopupInformation) {
        PopupInformationModel record = await repository.hasShownPopupInformation();

        if (record != null) {
          yield PopupInformationShow(record: record);
          await repository.setPopupInfoLastShown();
        } else {
          yield PopupInformationHasShown();
        }
    }
  }
}
