import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AdministrationDetailEvent extends Equatable {
  AdministrationDetailEvent([List props = const <dynamic>[]]);
}

class AdministrationDetailLoad extends AdministrationDetailEvent {
  final String instansi;

  AdministrationDetailLoad({@required this.instansi}) : super([instansi]);

  @override
  String toString() {
    return 'Event AdministrationDetailLoad{instansi: $instansi}';
  }

  @override
  List<Object> get props => [instansi];
}
