import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/PhoneBookModel.dart';

@immutable
abstract class AdministrationDetailState extends Equatable {
  AdministrationDetailState([List props = const <dynamic>[]]);
}

class AdministrationDetailInitial extends AdministrationDetailState {
  @override
  List<Object> get props => [];
}

class AdministrationDetailLoading extends AdministrationDetailState {
  @override
  String toString() {
    return 'State AdministrationDetailLoading';
  }

  @override
  List<Object> get props => [];
}

class AdministrationDetailLoaded extends AdministrationDetailState {
  final PhoneBookModel phoneBookModel;

  AdministrationDetailLoaded({this.phoneBookModel});

  @override
  String toString() {
    return 'State AdministrationDetailLoaded';
  }

  @override
  List<Object> get props => [phoneBookModel];
}

class AdministrationDetailFailure extends AdministrationDetailState {
  final String error;

  AdministrationDetailFailure({this.error});

  @override
  String toString() {
    return 'AdministrationDetailFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
