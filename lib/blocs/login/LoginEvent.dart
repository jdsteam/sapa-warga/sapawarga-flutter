import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class LoginEvent extends Equatable {
  LoginEvent([List props = const <dynamic>[]]);
}

class LoginButtonPressed extends LoginEvent {
  final String username;
  final String password;
  final String fcmToken;

  LoginButtonPressed({
    @required this.username,
    @required this.password,
    this.fcmToken,
  }) : super([username, password, fcmToken]);

  @override
  String toString() => 'LoginButtonPressed';

  @override
  List<Object> get props => [username, password, fcmToken];
}
