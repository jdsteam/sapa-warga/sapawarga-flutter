import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/SurveyModel.dart';
import 'package:sapawarga/repositories/SurveyRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';

import './Bloc.dart';

class SurveyListBloc extends Bloc<SurveyListEvent, SurveyListState> {
  final SurveyRepository surveyRepository;

  SurveyListBloc({@required this.surveyRepository}) : assert(surveyRepository != null);

  @override
  SurveyListState get initialState => SurveyListInitial();

  @override
  Stream<SurveyListState> mapEventToState(
    SurveyListEvent event,
  ) async* {
    if (event is SurveyListLoad) {
      yield SurveyListLoading();

      try {
        List<SurveyModel> records = await surveyRepository.fetchRecords(event.page);
        int maxDatalength = await Preferences.getTotalCount();
        yield SurveyListLoaded(records: records, maxData: maxDatalength, isLoad: true);
      } catch (e) {
        print(e.toString());
        yield SurveyListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
