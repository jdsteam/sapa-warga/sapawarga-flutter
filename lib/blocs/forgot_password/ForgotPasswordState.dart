import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ForgotPasswordState extends Equatable {
  ForgotPasswordState([List props = const <dynamic>[]]);
}

class ForgotPasswordInitial extends ForgotPasswordState {
  @override
  String toString() {
    return 'State ForgotPasswordInitial';
  }

  @override
  List<Object> get props => [];
}

class ForgotPasswordLoading extends ForgotPasswordState {
  @override
  String toString() {
    return 'State ForgotPasswordLoading';
  }

  @override
  List<Object> get props => [];
}

class ForgotPasswordRequested extends ForgotPasswordState {
  @override
  String toString() {
    return 'State ForgotPasswordRequested';
  }

  @override
  List<Object> get props => [];
}

class ForgotPasswordFailure extends ForgotPasswordState {
  final String error;

  ForgotPasswordFailure({this.error});

  @override
  String toString() {
    return 'State ForgotPasswordFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}

class ValidationError extends ForgotPasswordState {
  final Map<String, dynamic> errors;

  ValidationError({@required this.errors}) : super([errors]);

  @override
  String toString() => 'State ValidationError';

  @override
  List<Object> get props => [errors];
}
