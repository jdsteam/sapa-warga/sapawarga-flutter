import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import './Bloc.dart';

class ForgotPasswordBloc extends Bloc<ForgotPasswordEvent, ForgotPasswordState> {

  final AuthRepository authRepository;

  ForgotPasswordBloc({@required this.authRepository}):assert(authRepository != null);

  @override
  ForgotPasswordState get initialState => ForgotPasswordInitial();

  @override
  Stream<ForgotPasswordState> mapEventToState(
    ForgotPasswordEvent event,
  ) async* {
    if (event is RequestForgotPassword) {
      yield ForgotPasswordLoading();
      try {
        await authRepository.requestResetPassword(email: event.email);
        yield ForgotPasswordRequested();
      } on ValidationException catch (error) {
        yield ValidationError(errors: error.errors);
      } catch (e) {
        yield ForgotPasswordFailure(error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
