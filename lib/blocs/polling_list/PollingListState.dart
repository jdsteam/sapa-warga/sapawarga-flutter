import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/PollingModel.dart';

@immutable
abstract class PollingListState extends Equatable {
  PollingListState([List props = const <dynamic>[]]);
}

class PollingListInitial extends PollingListState {
  @override
  List<Object> get props => [];
}

class PollingListLoading extends PollingListState {
  @override
  String toString() {
    return 'State PollingListLoading';
  }

  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class PollingListLoaded extends PollingListState {
  final List<PollingModel> records;
  final int maxData;
  bool isLoad;

  PollingListLoaded({this.records, this.maxData, this.isLoad});

  @override
  String toString() {
    return 'State PollingListLoaded';
  }

  @override
  List<Object> get props => [records];
}

class PollingListStatusLoading extends PollingListState {
  @override
  String toString() {
    return 'State PollingListStatusLoading';
  }

  @override
  List<Object> get props => [];
}

class PollingListFailure extends PollingListState {
  final String error;

  PollingListFailure({this.error});

  @override
  String toString() {
    return 'State PollingListFailed{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
