import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class NewsDetailEvent extends Equatable {
  NewsDetailEvent([List props = const <dynamic>[]]);
}

class NewsDetailLoad extends NewsDetailEvent {
  final int newsId;

  NewsDetailLoad({@required this.newsId}) : super([newsId]);

  @override
  String toString() {
    return 'Event NewsDetailLoad{newsId: $newsId}';
  }

  @override
  List<Object> get props => [newsId];
}


class NewsDetailLike extends NewsDetailEvent {
  final int newsId;

  NewsDetailLike({@required this.newsId}) : super([newsId]);

  @override
  String toString() {
    return 'Event NewsDetailLike{newsId: $newsId}';
  }

  @override
  List<Object> get props => [newsId];
}
