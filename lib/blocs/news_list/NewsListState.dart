import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/NewsModel.dart';

@immutable
abstract class NewsListState extends Equatable {
  NewsListState([List props = const <dynamic>[]]);
}

class NewsListInitial extends NewsListState {
  @override
  List<Object> get props => [];
}

class NewsListLoading extends NewsListState {
  @override
  String toString() {
    return 'State NewsListLoading';
  }

  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class NewsListLoaded extends NewsListState {
  final List<NewsModel> listNews;
  final String cityName;
  final int maxData;
  bool isLoad;

  NewsListLoaded({@required this.listNews, this.cityName, @required this.maxData, @required this.isLoad}):assert(listNews != null);

  @override
  String toString() {
    return 'State NewsListLoaded';
  }

  @override
  List<Object> get props => [listNews, cityName];
}

class NewsListFailure extends NewsListState {
  final String error;

  NewsListFailure({this.error});

  @override
  String toString() {
    return 'NewsListFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
