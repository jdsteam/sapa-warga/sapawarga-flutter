import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class NewsListEvent extends Equatable {
  NewsListEvent([List props = const <dynamic>[]]);
}

class NewsListLoad extends NewsListEvent {
  final bool isIdKota;
  final int page;

  NewsListLoad({@required this.isIdKota, @required this.page}) : super([isIdKota]);

  @override
  String toString() {
    return 'Event NewsListLoad';
  }

  @override
  List<Object> get props => [isIdKota];
}
