import 'package:equatable/equatable.dart';

abstract class StatisticsEvent extends Equatable {
  const StatisticsEvent([List props = const <dynamic>[]]);
}

class StatisticsLoad extends StatisticsEvent {
  StatisticsLoad();

  @override
  String toString() {
    return 'Event StatisticsLoad';
  }

  @override
  List<Object> get props => [];
}
