import 'package:equatable/equatable.dart';
import 'package:sapawarga/models/statistics_pikobar_model.dart';

abstract class StatisticsState extends Equatable {
  const StatisticsState([List props = const <dynamic>[]]);
}

class InitialStatisticsState extends StatisticsState {
  @override
  List<Object> get props => [];
}

class StatisticsLoading extends StatisticsState {
  @override
  String toString() {
    return 'State StatisticsLoading';
  }

  @override
  List<Object> get props => [];
}

class StatisticsLoaded extends StatisticsState {
  final StatisticsPikobarModel record;

  StatisticsLoaded({this.record}) : super([record]);

  @override
  String toString() {
    return 'State StatisticsLoaded';
  }

  @override
  List<Object> get props => [record];
}
