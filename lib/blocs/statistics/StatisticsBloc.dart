import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/constants/FirebaseConfig.dart';
import 'package:sapawarga/models/statistics_pikobar_model.dart';
import 'package:sapawarga/repositories/FirebaseRepository.dart';
import 'package:sapawarga/repositories/StatisticsRepository.dart';
import './Bloc.dart';

class StatisticsBloc extends Bloc<StatisticsEvent, StatisticsState> {
  final StatisticsRepository statisticsRepository;

  StatisticsBloc({@required this.statisticsRepository});

  @override
  StatisticsState get initialState => InitialStatisticsState();

  @override
  Stream<StatisticsState> mapEventToState(
    StatisticsEvent event,
  ) async* {
    if (event is StatisticsLoad) {
      yield* _mapStatisticsLoadToState();
    }
  }

  Stream<StatisticsState> _mapStatisticsLoadToState() async* {
    yield StatisticsLoading();
    final RemoteConfig remoteConfig =
        await FirebaseRepository().connectRemoteConfig();

    final Map<String, dynamic> dataSecretKey =
        json.decode(remoteConfig.getString(FirebaseConfig.secretKeys));

    final String apiUrl = dataSecretKey['api_url_total_case_pikobar'];
    final String apiKey = dataSecretKey['api_key_total_case_pikobar'];

    final StatisticsPikobarModel data =
        await statisticsRepository.getStatisticsPikobar(apiUrl, apiKey);

    yield StatisticsLoaded(record: data);
  }
}
