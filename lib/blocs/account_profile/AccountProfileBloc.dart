import 'dart:async';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/blocs/account_profile/AccountProfileEvent.dart';
import 'package:sapawarga/blocs/account_profile/AccountProfileState.dart';

class AccountProfileBloc
    extends Bloc<AccountProfileEvent, AccountProfileState> {
  AuthProfileRepository authProfileRepository = AuthProfileRepository();

  AccountProfileBloc.profile({@required this.authProfileRepository})
      : assert(authProfileRepository != null);

  @override
  AccountProfileState get initialState => AccountProfileInitial();

  Stream<AccountProfileState> mapEventToState(
      AccountProfileEvent event) async* {
    if (event is AccountProfileLoad) {
      yield AccountProfileLoading();

      try {
        UserInfoModel record =
            await authProfileRepository.getUserInfo(forceFetch: true);

        yield AccountProfileLoaded(record: record);
      } catch (e) {
        yield AccountProfileFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is AccountProfileChanged) {
      yield AccountProfileLoading();

      try {
        await Future.delayed(Duration(seconds: 1));

        UserInfoModel record =
            await authProfileRepository.getUserInfo(forceFetch: true);

        yield AccountProfileLoaded(record: record);
      } catch (e) {
        yield AccountProfileFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
