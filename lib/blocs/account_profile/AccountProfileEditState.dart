import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class AccountProfileEditState extends Equatable {
  AccountProfileEditState([List props = const []]);
}

class AccountProfileEditInitial extends AccountProfileEditState {
  @override
  String toString() => 'State AccountProfileEditInitial';

  @override
  List<Object> get props => [];
}

class AccountProfileEditLoading extends AccountProfileEditState {
  @override
  String toString() => 'State AccountProfileEditLoading';

  @override
  List<Object> get props => [];
}

class AccountProfileEditValidationError extends AccountProfileEditState {
  final Map<String, dynamic> errors;

  AccountProfileEditValidationError({@required this.errors}) : super([errors]);

  @override
  String toString() => 'State AccountProfileEditValidationError';

  @override
  List<Object> get props => [errors];
}

class AccountProfileEditUpdated extends AccountProfileEditState {
  @override
  String toString() => 'State AccountProfileEditUpdated';

  @override
  List<Object> get props => [];
}

class AccountProfileEditFailure extends AccountProfileEditState {
  final String error;

  AccountProfileEditFailure({@required this.error}) : super([error]);

  @override
  String toString() => 'State AccountProfileEditFailure';

  @override
  List<Object> get props => [error];
}

class AccountProfileEditPhotoUpdated extends AccountProfileEditState {
  final image;

  AccountProfileEditPhotoUpdated({@required this.image}) : super([image]);

  @override
  String toString() => 'State AccountProfileEditPhotoUpdated';

  @override
  List<Object> get props => [image];
}

class AccountProfileEditPhotoLoading extends AccountProfileEditState {
  @override
  String toString() => 'State AccountProfileEditPhotoLoading';

  @override
  List<Object> get props => [];
}

class AccountProfileEditPhotoFailure extends AccountProfileEditState {
  final String error;

  AccountProfileEditPhotoFailure({@required this.error}) : super([error]);

  @override
  String toString() => 'State AccountProfileEditPhotoFailure';

  @override
  List<Object> get props => [error];
}
