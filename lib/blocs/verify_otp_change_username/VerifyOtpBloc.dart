import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/blocs/verify_otp_change_username/VerifyOtpEvent.dart';
import 'package:sapawarga/blocs/verify_otp_change_username/VerifyOtpState.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';

import './Bloc.dart';

class VerifyOtpBloc extends Bloc<VerifyOtpEvent, VerifyOtpState> {
  final AuthProfileRepository authProfileRepository;

  VerifyOtpBloc({@required this.authProfileRepository})
      : assert(authProfileRepository != null);

  @override
  VerifyOtpState get initialState => VerifyOtpInitial();

  @override
  Stream<VerifyOtpState> mapEventToState(
    VerifyOtpEvent event,
  ) async* {
    if (event is VerifyOtpChangedUsername) {
      yield VerifyOtpLoading();

      try {
        await authProfileRepository.verifyOtpChangeUsername(
            otp: event.otp, phone: event.phone);
        yield VerifyOtpSuccess();
      } on ValidationException catch (error) {
        yield ValidationVerifyOtpError(errors: error.errors);
      } catch (e) {
        yield VerifyOtpFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
