import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class VerifyOtpState extends Equatable {
  VerifyOtpState([List props = const <dynamic>[]]);
}

class VerifyOtpInitial extends VerifyOtpState {
  @override
  List<Object> get props => [];
}

class VerifyOtpLoading extends VerifyOtpState {
  @override
  String toString() {
    return 'State VerifyOtpLoading';
  }

  @override
  List<Object> get props => [];
}

class VerifyOtpSuccess extends VerifyOtpState {
  @override
  String toString() {
    return 'State VerifyOtpSuccess';
  }

  @override
  List<Object> get props => [];
}

class ValidationVerifyOtpError extends VerifyOtpState {
  final Map<String, dynamic> errors;

  ValidationVerifyOtpError({@required this.errors}) : super([errors]);

  @override
  String toString() {
    return 'State ValidationVerifyOtpError{errors: $errors}';
  }

  @override
  List<Object> get props => <Object>[errors];
}

class VerifyOtpFailure extends VerifyOtpState {
  final String error;

  VerifyOtpFailure({this.error});

  @override
  String toString() {
    return 'State VerifyOtpFailure{error: $error}';
  }

  @override
  List<Object> get props => <Object>[error];
}
