import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class VerifyOtpEvent extends Equatable {
  VerifyOtpEvent([List props = const <dynamic>[]]);
}

class VerifyOtpChangedUsername extends VerifyOtpEvent {
  final String phone;
  final String otp;

  VerifyOtpChangedUsername({@required this.phone, @required this.otp})
      : assert((phone != null) && (otp != null));

  @override
  String toString() {
    return 'Event VerifyOtpChangedUsername';
  }

  @override
  List<Object> get props => [phone, otp];
}
