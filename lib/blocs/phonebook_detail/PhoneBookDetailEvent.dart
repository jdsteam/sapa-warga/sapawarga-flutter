import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

import 'package:sapawarga/models/PhoneBookModel.dart';

abstract class PhoneBookDetailEvent extends Equatable {
  PhoneBookDetailEvent([List props = const []]);
}

class PhoneBookDetailLoad extends PhoneBookDetailEvent {
  final PhoneBookModel record;

  PhoneBookDetailLoad({
    @required this.record,
  }) : super([record]);

  @override
  String toString() => 'Event PhoneBookDetailLoad';

  @override
  List<Object> get props => [record];
}
