import 'package:bloc/bloc.dart';
import 'package:sapawarga/blocs/phonebook_detail/PhoneBookDetailEvent.dart';
import 'package:sapawarga/blocs/phonebook_detail/PhoneBookDetailState.dart';

class PhoneBookDetailBloc
    extends Bloc<PhoneBookDetailEvent, PhoneBookDetailState> {

  @override
  PhoneBookDetailState get initialState => PhoneBookDetailInitial();

  Stream<PhoneBookDetailState> mapEventToState(
      PhoneBookDetailEvent event) async* {

    if (event is PhoneBookDetailLoad) {
      yield PhoneBookDetailLoading();

      yield PhoneBookDetailLoaded();
    }
  }
}