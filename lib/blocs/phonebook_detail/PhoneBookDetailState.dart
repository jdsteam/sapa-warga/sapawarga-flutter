import 'package:equatable/equatable.dart';

abstract class PhoneBookDetailState extends Equatable {
  PhoneBookDetailState([List props = const []]);
}

class PhoneBookDetailInitial extends PhoneBookDetailState {
  @override
  String toString() => 'State PhoneBookDetailInitial';

  @override
  List<Object> get props => [];
}

class PhoneBookDetailLoading extends PhoneBookDetailState {
  @override
  String toString() => 'State PhoneBookDetailLoading';

  @override
  List<Object> get props => [];
}

class PhoneBookDetailLoaded extends PhoneBookDetailState {
  @override
  String toString() => 'State PhoneBookDetailLoaded';

  @override
  List<Object> get props => [];
}

class PhoneBookDetailFailure extends PhoneBookDetailState {
  @override
  String toString() => 'State PhoneBookDetailFailure';

  @override
  List<Object> get props => [];
}
