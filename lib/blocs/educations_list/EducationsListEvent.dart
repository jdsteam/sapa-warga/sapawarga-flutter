import 'package:equatable/equatable.dart';

abstract class EducationsListEvent extends Equatable {
  EducationsListEvent([List props = const []]);
}

class EducationsLoad extends EducationsListEvent {
  @override
  String toString() => 'Event EducationsLoad';

  @override
  List<Object> get props => [];
}
