import 'package:equatable/equatable.dart';

abstract class HumasJabarListEvent extends Equatable {
  HumasJabarListEvent([List props = const []]);
}

class HumasJabarLoad extends HumasJabarListEvent {
  @override
  String toString() => 'Event HumasJabarListLoaded';

  @override
  List<Object> get props => [];
}
