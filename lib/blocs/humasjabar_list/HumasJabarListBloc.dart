import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/HumasJabarModel.dart';
import 'package:sapawarga/repositories/HumasJabarRepository.dart';
import 'HumasJabarListEvent.dart';
import 'HumasJabarListState.dart';

class HumasJabarListBloc
    extends Bloc<HumasJabarListEvent, HumasJabarListState> {
  final HumasJabarRepository humasJabarRepository;

  HumasJabarListBloc({@required this.humasJabarRepository}) :assert(humasJabarRepository != null);

  @override
  HumasJabarListState get initialState => HumasJabarlInitial();

  Stream<HumasJabarListState> mapEventToState(
      HumasJabarListEvent event) async* {

    if (event is HumasJabarLoad) {
      yield HumasJabarLoading();
      try{
        List<HumasJabarModel> records = await humasJabarRepository.getRecordsDataHumasJabar(forceRefresh: true);

        yield HumasJabarLoaded(records: records);
      }catch(e){
        yield HumasJabarFailure(error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}