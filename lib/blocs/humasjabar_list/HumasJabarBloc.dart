import 'package:rxdart/rxdart.dart';
import 'package:sapawarga/models/HumasJabarModel.dart';
import 'package:sapawarga/repositories/HumasJabarRepository.dart';

class HumasJabarBloc{
  final _humasJabarRepository = HumasJabarRepository();
  final _humasJabarFecther = PublishSubject<List<HumasJabarModel>>();

  Observable<List<HumasJabarModel>> get allList => _humasJabarFecther.stream;

  fetchAllListHumasJabar() async{
    List<HumasJabarModel> listHumasJabar = await _humasJabarRepository.getRecordsDataHumasJabar(forceRefresh: true);
    _humasJabarFecther.sink.add(listHumasJabar);
  }

  dispose(){
    _humasJabarFecther.close();
  }

}

final humasJabarBloc = HumasJabarBloc();