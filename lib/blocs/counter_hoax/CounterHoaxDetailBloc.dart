import 'dart:async';

import 'package:bloc/bloc.dart';

import './Bloc.dart';

class CounterHoaxDetailBloc
    extends Bloc<CounterHoaxDetailEvent, CounterHoaxDetailState> {
  @override
  CounterHoaxDetailState get initialState => CounterHoaxDetailInitial();

  @override
  Stream<CounterHoaxDetailState> mapEventToState(
    CounterHoaxDetailEvent event,
  ) async* {
    if (event is CounterHoaxDetailLoad) {
      yield CounterHoaxDetailLoading();

      await Future.delayed(Duration(seconds: 1));
      yield CounterHoaxDetailLoaded(counterHoaxModel: event.record);
    }
  }
}
