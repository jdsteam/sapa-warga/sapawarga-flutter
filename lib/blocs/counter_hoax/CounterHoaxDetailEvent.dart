import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/CounterHoaxModel.dart';

@immutable
abstract class CounterHoaxDetailEvent extends Equatable {
  CounterHoaxDetailEvent([List props = const <dynamic>[]]);
}

class CounterHoaxDetailLoad extends CounterHoaxDetailEvent {
  final CounterHoaxModel record;

  CounterHoaxDetailLoad({@required this.record}) : assert(record != null);

  @override
  String toString() {
    return 'Event CounterHoaxDetailLoad';
  }

  @override
  List<Object> get props => [record];
}
