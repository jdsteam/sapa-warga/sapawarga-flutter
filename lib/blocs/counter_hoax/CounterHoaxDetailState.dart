import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/CounterHoaxModel.dart';

@immutable
abstract class CounterHoaxDetailState extends Equatable {
  CounterHoaxDetailState([List props = const <dynamic>[]]);
}

class CounterHoaxDetailInitial extends CounterHoaxDetailState {
  @override
  List<Object> get props => [];
}

class CounterHoaxDetailLoading extends CounterHoaxDetailState {
  @override
  String toString() {
    return 'State CounterHoaxDetailLoading';
  }

  @override
  List<Object> get props => [];
}

class CounterHoaxDetailLoaded extends CounterHoaxDetailState {
  final CounterHoaxModel counterHoaxModel;

  CounterHoaxDetailLoaded({@required this.counterHoaxModel}):assert(counterHoaxModel != null);

  @override
  String toString() {
    return 'State CounterHoaxDetailLoaded';
  }

  @override
  List<Object> get props => [counterHoaxModel];
}
