import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/CounterHoaxModel.dart';

@immutable
abstract class CounterHoaxListState extends Equatable {
  CounterHoaxListState([List props = const <dynamic>[]]);
}

class CounterHoaxListInitial extends CounterHoaxListState {
  @override
  String toString() {
    return 'State CounterHoaxListInitial';
  }

  @override
  List<Object> get props => [];
}

class CounterHoaxListLoading extends CounterHoaxListState {
  @override
  String toString() {
    return 'State CounterHoaxListLoading';
  }

  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class CounterHoaxListLoaded extends CounterHoaxListState {
  final List<CounterHoaxModel> records;
  final int maxLengthData;
  bool isLoad;

  CounterHoaxListLoaded({@required this.records, @required this.maxLengthData, @required this.isLoad});

  @override
  String toString() {
    return 'State CounterHoaxListLoaded';
  }

  @override
  List<Object> get props => [records];
}

class CounterHoaxListFailure extends CounterHoaxListState {
  final String error;

  CounterHoaxListFailure({this.error});

  @override
  String toString() {
    return 'State CounterHoaxListFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
