import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';

import './Bloc.dart';

class ChangeUsernameBloc
    extends Bloc<ChangeUsernameEvent, ChangeUsernameState> {
  final AuthProfileRepository authProfileRepository;

  ChangeUsernameBloc({@required this.authProfileRepository})
      : assert(authProfileRepository != null);

  @override
  ChangeUsernameState get initialState => ChangeUsernameInitial();

  @override
  Stream<ChangeUsernameState> mapEventToState(
    ChangeUsernameEvent event,
  ) async* {
    if (event is SendChangedUsername) {
      yield ChangeUsernameLoading();

      try {
        await authProfileRepository.changeUsername(
            username: event.username, phone: event.phone, otp: event.otp);
        yield ChangeUsernameSuccess();
      } on ValidationException catch (error) {
        yield ValidationError(errors: error.errors);
      } catch (e) {
        yield ChangeUsernameFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
