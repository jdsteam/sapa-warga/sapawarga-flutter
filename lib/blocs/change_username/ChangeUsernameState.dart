import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ChangeUsernameState extends Equatable {
  ChangeUsernameState([List props = const <dynamic>[]]);
}

class ChangeUsernameInitial extends ChangeUsernameState {
  @override
  List<Object> get props => [];
}

class ChangeUsernameLoading extends ChangeUsernameState {
  @override
  String toString() {
    return 'State ChangeUsernameLoading';
  }

  @override
  List<Object> get props => [];
}

class ChangeUsernameSuccess extends ChangeUsernameState {
  @override
  String toString() {
    return 'State ChangePasswordSuccess';
  }

  @override
  List<Object> get props => [];
}

class ValidationError extends ChangeUsernameState {
  final Map<String, dynamic> errors;

  ValidationError({@required this.errors}) : super([errors]);

  @override
  String toString() {
    return 'State ValidationError{errors: $errors}';
  }

  @override
  List<Object> get props => <Object>[errors];
}

class ChangeUsernameFailure extends ChangeUsernameState {
  final String error;

  ChangeUsernameFailure({this.error});

  @override
  String toString() {
    return 'State ChangeUsernameFailure{error: $error}';
  }

  @override
  List<Object> get props => <Object>[error];
}
