import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ChangeUsernameEvent extends Equatable {
  ChangeUsernameEvent([List props = const <dynamic>[]]);
}

class SendChangedUsername extends ChangeUsernameEvent {
  final String username;
  final String phone;
  final String otp;

  SendChangedUsername({@required this.username, @required this.phone, @required this.otp})
      : assert((username != null) && (phone != null));

  @override
  String toString() {
    return 'Event SendChangedUsername';
  }

  @override
  List<Object> get props => [username, phone];
}
