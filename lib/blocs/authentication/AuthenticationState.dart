import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AuthenticationState extends Equatable {
  AuthenticationState([List props = const <dynamic>[]]);
}

class AuthenticationUninitialized extends AuthenticationState {
  @override
  String toString() => 'AuthenticationUninitialized';

  @override
  List<Object> get props => [];
}

class AuthenticationAuthenticated extends AuthenticationState {
  @override
  String toString() => 'AuthenticationAuthenticated';

  @override
  List<Object> get props => [];
}

class AuthenticationUnauthenticated extends AuthenticationState {
  final bool hasOnBoarding;

  AuthenticationUnauthenticated({this.hasOnBoarding});

  @override
  String toString() => 'AuthenticationUnauthenticated';

  @override
  List<Object> get props => [hasOnBoarding];
}

class AuthenticationLoading extends AuthenticationState {
  @override
  String toString() => 'AuthenticationLoading';

  @override
  List<Object> get props => [];
}
