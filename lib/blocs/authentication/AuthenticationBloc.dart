import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/AuthRepository.dart';
import 'package:sapawarga/repositories/VideoRepository.dart';

import './Bloc.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthRepository authRepository;
  final AuthProfileRepository authProfileRepository;
  final VideoRepository videoRepository;

  AuthenticationBloc({@required this.authRepository, this.authProfileRepository, this.videoRepository}) : assert(authRepository != null);

  @override
  AuthenticationState get initialState => AuthenticationUninitialized();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      final bool hasToken = await authRepository.hasToken();
      final bool hasOnBoarding = await authRepository.hasOnboarding();

      if (hasToken) {
        yield AuthenticationAuthenticated();
      } else {
        yield AuthenticationUnauthenticated(hasOnBoarding: hasOnBoarding);
      }
    }

    if (event is LoggedIn) {
      yield AuthenticationLoading();
      await authRepository.persistToken(event.token);
      try {
        await AuthProfileRepository().getUserInfo(forceFetch: true);
      } catch (e) {
        print(e.toString());
      }

      yield AuthenticationAuthenticated();
    }

    if (event is LoggedOut) {
      yield AuthenticationLoading();
      await authRepository.unAuthenticate();
      yield AuthenticationUnauthenticated(hasOnBoarding: true);
    }
  }
}
