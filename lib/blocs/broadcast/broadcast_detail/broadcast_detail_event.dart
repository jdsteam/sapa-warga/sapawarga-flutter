import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class BroadcastDetailEvent extends Equatable {
  const BroadcastDetailEvent([List props = const <dynamic>[]]);
}

class BroadcastDetailLoad extends BroadcastDetailEvent {
  final int id;

  BroadcastDetailLoad({@required this.id})
      : super([id]);

  @override
  String toString() {
    return 'Event BroadcastDetailLoad';
  }

  @override
  List<Object> get props => [];
}