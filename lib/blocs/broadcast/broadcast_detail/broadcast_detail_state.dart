import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/BroadcastModel.dart';

abstract class BroadcastDetailState extends Equatable {
  const BroadcastDetailState([List props = const <dynamic>[]]);
}

class InitialBroadcastDetailState extends BroadcastDetailState {
  @override
  List<Object> get props => [];
}

class BroadcastDetailLoading extends BroadcastDetailState {
  @override
  String toString() {
    return 'BroadcastDetailLoading';
  }

  @override
  List<Object> get props => [];
}

class BroadcastDetailLoaded extends BroadcastDetailState {
  final BroadcastModel record;

  BroadcastDetailLoaded({@required this.record})
      : super([record]);

  @override
  String toString() {
    return 'BroadcastDetailLoaded';
  }

  @override
  List<Object> get props => [record];
}

class BroadcastDetailFailure extends BroadcastDetailState {
  final String error;

  BroadcastDetailFailure({this.error});

  @override
  String toString() {
    return 'BroadcastDetailFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}