import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/blocs/broadcast/broadcast_list/Bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/MessageModel.dart';
import 'package:sapawarga/repositories/BroadcastRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';

class BroadcastListBloc extends Bloc<BroadcastListEvent, BroadcastListState> {
  final BroadcastRepository broadcastRepository;

  BroadcastListBloc({@required this.broadcastRepository}):assert(broadcastRepository != null);

  @override
  BroadcastListState get initialState => InitialBroadcastListState();

  @override
  Stream<BroadcastListState> mapEventToState(
    BroadcastListEvent event,
  ) async* {
    if (event is BroadcastListLoad) {
      if (event.page == 1) yield BroadcastListLoading();
      try {
        List<MessageModel> records = await broadcastRepository.getRecords(
            forceRefresh: true, page: event.page);
        int maxDatalength = await Preferences.getTotalCount();
        yield BroadcastListLoaded(records: records, maxData: maxDatalength);
      } catch (e) {
        print(e.toString());
        yield BroadcastListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is BroadcastListTap) {
      try {
        await broadcastRepository.updateReadData(event.id);
        List<MessageModel> records = await broadcastRepository.getRecords();
        yield BroadcastListLoaded(records: records, isDeleteSuccess: false);
      } catch (e) {
        print(e.toString());
        yield BroadcastListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is BroadcastListDelete) {
      try {
        await broadcastRepository.delete(event.id);
        List<MessageModel> records = await broadcastRepository.getRecords();
        yield BroadcastListLoaded(records: records, isDeleteSuccess: false);
      } catch (e) {
        print(e.toString());
        yield BroadcastListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is BroadcastMultipleDelete) {
      yield BroadcastListLoading();
      try {
        await broadcastRepository.deleteListRecord(event.record);
        List<MessageModel> records = await broadcastRepository.getRecords();
        int maxDatalength = await Preferences.getTotalCount();
        yield BroadcastListLoaded(records: records, isDeleteSuccess: true, maxData: maxDatalength);
      } catch (e) {
        yield BroadcastListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
