import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/MessageModel.dart';

abstract class BroadcastListEvent extends Equatable {
  BroadcastListEvent([List props = const <dynamic>[]]);
}

class BroadcastListLoad extends BroadcastListEvent {
  final int page;

  BroadcastListLoad({@required this.page})
      : super([page]);

  @override
  String toString() {
    return 'Event BroadcastListLoad';
  }

  @override
  List<Object> get props => [];
}

class BroadcastListTap extends BroadcastListEvent {
  final String id;

  BroadcastListTap({@required this.id});

  @override
  String toString() {
    return 'Event BroadcastListTap';
  }

  @override
  List<Object> get props => [id];
}

class BroadcastListDelete extends BroadcastListEvent {
  final String id;

  BroadcastListDelete({@required this.id});

  @override
  String toString() {
    return 'Event BroadcastListDelete';
  }

  @override
  List<Object> get props => [id];
}

class BroadcastMultipleDelete extends BroadcastListEvent {
  final List<MessageModel> record;

  BroadcastMultipleDelete({@required this.record});

  @override
  String toString() {
    return 'Event BroadcastMultipleDelete';
  }

  @override
  List<Object> get props => [record];
}
