import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/MessageModel.dart';

abstract class BroadcastListState extends Equatable {
  BroadcastListState([List props = const <dynamic>[]]);
}

class InitialBroadcastListState extends BroadcastListState {
  @override
  List<Object> get props => [];
}

class BroadcastListLoading extends BroadcastListState {
  @override
  String toString() {
    return 'BroadcastListLoading';
  }

  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class BroadcastListLoaded extends BroadcastListState {
  final List<MessageModel> records;
  final int maxData;
  bool isDeleteSuccess;

  BroadcastListLoaded({@required this.records, this.isDeleteSuccess, this.maxData})
      : super([records]);

  @override
  String toString() {
    return 'BroadcastListLoaded';
  }

  @override
  List<Object> get props => [records, isDeleteSuccess];
}

class BroadcastListFailure extends BroadcastListState {
  final String error;

  BroadcastListFailure({this.error});

  @override
  String toString() {
    return 'BroadcastListFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
