import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RegisterEvent extends Equatable {
  RegisterEvent([List props = const <dynamic>[]]);
}

class SendRegister extends RegisterEvent {
  final String name;
  final String password;
  final String email;
  final String phone;
  final String kabkotaId;
  final String kecId;
  final String kelId;
  final String rw;
  final String role;

  SendRegister(
      {@required this.name,
      @required this.password,
      @required this.email,
      @required this.phone,
      @required this.kabkotaId,
      @required this.kecId,
      @required this.kelId,
      @required this.rw,
      @required this.role})
      : assert((name != null) &&
            (password != null) &&
            (email != null) &&
            (phone != null) &&
            (kecId != null) &&
            (kelId != null) &&
            (rw != null) &&
            (role != null) &&
            (kabkotaId != null));

  @override
  String toString() {
    return 'Event SendRegister';
  }

  @override
  List<Object> get props =>
      [name, password, email, phone, kabkotaId, kecId, kelId, rw, role];
}
