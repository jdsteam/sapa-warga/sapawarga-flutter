import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RegisterState extends Equatable {
  RegisterState([List props = const <dynamic>[]]);
}

class RegisterInitial extends RegisterState {
  @override
  List<Object> get props => [];
}

class RegisterLoading extends RegisterState {
  @override
  String toString() {
    return 'State RegisterLoading';
  }

  @override
  List<Object> get props => [];
}

class RegisterSuccess extends RegisterState {
  @override
  String toString() {
    return 'State RegisterSuccess';
  }

  @override
  List<Object> get props => [];
}

class ValidationError extends RegisterState {
  final Map<String, dynamic> errors;

  ValidationError({@required this.errors}) : super([errors]);

  @override
  String toString() {
    return 'State ValidationError{errors: $errors}';
  }

  @override
  List<Object> get props => <Object>[errors];
}

class RegisterFailure extends RegisterState {
  final String error;

  RegisterFailure({this.error});

  @override
  String toString() {
    return 'State RegisterFailure{error: $error}';
  }

  @override
  List<Object> get props => <Object>[error];
}
