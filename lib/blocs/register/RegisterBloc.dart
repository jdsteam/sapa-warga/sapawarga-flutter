import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/blocs/register/RegisterEvent.dart';
import 'package:sapawarga/blocs/register/RegisterState.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/repositories/RegisterRepository.dart';

import './Bloc.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final RegisterRepository registerRepository;

  RegisterBloc({@required this.registerRepository})
      : assert(registerRepository != null);

  @override
  RegisterState get initialState => RegisterInitial();

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    if (event is SendRegister) {
      yield RegisterLoading();

      try {
        await registerRepository.sendRegister(
            name: event.name,
            password: event.password,
            email: event.email,
            phone: event.phone,
            kabkotaId: event.kabkotaId,
            kecId: event.kecId,
            kelId: event.kelId,
            rw: event.rw,
            role: event.role);
        yield RegisterSuccess();
      } on ValidationException catch (error) {
        yield ValidationError(errors: error.errors);
      } catch (e) {
        yield RegisterFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
