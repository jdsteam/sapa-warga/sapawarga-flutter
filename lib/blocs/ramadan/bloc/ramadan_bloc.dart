import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/constants/Dictionary.dart';
import 'package:sapawarga/models/UserInfoModel.dart';
import 'package:sapawarga/models/ramadan_model.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';
import 'package:sapawarga/repositories/ramadan_repository.dart';

part 'ramadan_event.dart';
part 'ramadan_state.dart';

class RamadanBloc extends Bloc<RamadanEvent, RamadanState> {
  final RamadanRepository ramadanRepository;

  RamadanBloc({@required this.ramadanRepository});

  @override
  RamadanState get initialState => RamadanInitial();

  @override
  Stream<RamadanState> mapEventToState(
    RamadanEvent event,
  ) async* {
    if (event is RamadanLoad) {
      yield RamadanLoading();
      try {
        final UserInfoModel userInfo =
            await AuthProfileRepository().getUserInfo();

        Item item = await ramadanRepository.getRamadan(
            userInfo?.kabkota?.name, event.url);
        if (item != null) {
          yield RamadanLoaded(item);
        } else {
          yield RamadanFailure(error: Dictionary.empty);
        }
      } catch (e) {
        yield RamadanFailure(error: e);
      }
    }
  }
}
