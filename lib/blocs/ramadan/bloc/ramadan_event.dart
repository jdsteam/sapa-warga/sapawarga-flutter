part of 'ramadan_bloc.dart';

abstract class RamadanEvent extends Equatable {
  const RamadanEvent();

  @override
  List<Object> get props => [];
}

class RamadanLoad extends RamadanEvent {
  final String url;

  RamadanLoad({@required this.url});

  @override
  List<Object> get props => [];
}
