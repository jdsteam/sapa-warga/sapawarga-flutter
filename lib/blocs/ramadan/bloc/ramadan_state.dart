part of 'ramadan_bloc.dart';

abstract class RamadanState extends Equatable {
  const RamadanState();

  @override
  List<Object> get props => [];
}

class RamadanInitial extends RamadanState {}

class RamadanLoading extends RamadanState {}

class RamadanLoaded extends RamadanState {
  final Item item;

  RamadanLoaded(this.item);

  @override
  List<Object> get props => [item];
}

class RamadanFailure extends RamadanState {
  final String error;

  RamadanFailure({this.error});

  @override
  List<Object> get props => [error];
}
