import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/UserInfoModel.dart';

@immutable
abstract class AccessTokenEvent extends Equatable {
  AccessTokenEvent([List props = const <dynamic>[]]);
}

class GetAccessToken extends AccessTokenEvent {
  final UserInfoModel userInfoModel;
  final String tOtpToken;
  final Map<String, dynamic> secretEnvSiapKerja;

  GetAccessToken(
      {@required this.userInfoModel,
      @required this.tOtpToken,
      @required this.secretEnvSiapKerja});

  @override
  String toString() {
    return 'Event GetAccessToken';
  }

  @override
  List<Object> get props => [];
}
