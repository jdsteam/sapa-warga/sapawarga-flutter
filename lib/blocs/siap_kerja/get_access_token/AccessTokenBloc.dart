import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/blocs/siap_kerja/get_access_token/AccessTokenEvent.dart';
import 'package:sapawarga/blocs/siap_kerja/get_access_token/AccessTokenState.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/repositories/SiapKerjaRepository.dart';

import './Bloc.dart';

class AccessTokenBloc extends Bloc<AccessTokenEvent, AccessTokenState> {
  final SiapKerjaRepository siapKerjaRepository;

  AccessTokenBloc({@required this.siapKerjaRepository})
      : assert(siapKerjaRepository != null);

  @override
  AccessTokenState get initialState => AccessTokenInitial();

  @override
  Stream<AccessTokenState> mapEventToState(
    AccessTokenEvent event,
  ) async* {
    if (event is GetAccessToken) {
      yield AccessTokenLoading();

      try {
        String accessToken = await siapKerjaRepository.loginOrSignUp(
            userInfoModel: event.userInfoModel,
            tOtpToken: event.tOtpToken,
            envSiapKerja: event.secretEnvSiapKerja);
        yield AccessTokenSuccess(accessToken: accessToken);
      } on ValidationException catch (error) {
        yield ValidationError(errors: error.errors);
      } catch (e) {
        yield AccessTokenFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
