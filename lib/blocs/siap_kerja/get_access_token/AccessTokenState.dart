import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AccessTokenState extends Equatable {
  AccessTokenState([List props = const <dynamic>[]]);
}

class AccessTokenInitial extends AccessTokenState {
  @override
  List<Object> get props => [];
}

class AccessTokenLoading extends AccessTokenState {
  @override
  String toString() {
    return 'State AccessTokenLoading';
  }

  @override
  List<Object> get props => [];
}

class AccessTokenSuccess extends AccessTokenState {
  final String accessToken;

  AccessTokenSuccess({@required this.accessToken});

  @override
  String toString() {
    return 'State AccessTokenSuccess token : $accessToken';
  }

  @override
  List<Object> get props => [];
}

class ValidationError extends AccessTokenState {
  final Map<String, dynamic> errors;

  ValidationError({@required this.errors}) : super([errors]);

  @override
  String toString() {
    return 'State ValidationError{errors: $errors}';
  }

  @override
  List<Object> get props => <Object>[errors];
}

class AccessTokenFailure extends AccessTokenState {
  final String error;

  AccessTokenFailure({this.error});

  @override
  String toString() {
    return 'State AccessTokenFailure{error: $error}';
  }

  @override
  List<Object> get props => <Object>[error];
}
