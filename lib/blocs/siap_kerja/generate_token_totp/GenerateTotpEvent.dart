import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class GenerateTotpEvent extends Equatable {
  GenerateTotpEvent([List props = const <dynamic>[]]);
}

class GenerateTotp extends GenerateTotpEvent {
  final Map<String, dynamic> secretEnvSiapKerja;

  GenerateTotp({@required this.secretEnvSiapKerja});

  @override
  String toString() {
    return 'Event GenerateTotp';
  }

  @override
  List<Object> get props => [];
}
