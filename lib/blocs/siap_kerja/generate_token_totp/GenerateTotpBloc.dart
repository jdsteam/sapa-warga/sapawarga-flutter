import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/blocs/siap_kerja/generate_token_totp/GenerateTotpEvent.dart';
import 'package:sapawarga/blocs/siap_kerja/generate_token_totp/GenerateTotpState.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/repositories/SiapKerjaRepository.dart';

import './Bloc.dart';

class GenerateTotpBloc extends Bloc<GenerateTotpEvent, GenerateTotpState> {
  final SiapKerjaRepository siapKerjaRepository;

  GenerateTotpBloc({@required this.siapKerjaRepository})
      : assert(siapKerjaRepository != null);

  @override
  GenerateTotpState get initialState => GenerateTotpInitial();

  @override
  Stream<GenerateTotpState> mapEventToState(
    GenerateTotpEvent event,
  ) async* {
    if (event is GenerateTotp) {
      yield GenerateTotpLoading();

      try {
        String tOtpToken = await siapKerjaRepository.generateTokenTotp(
            envSiapKerja: event.secretEnvSiapKerja);
        yield GenerateTotpSuccess(tOtpToken: tOtpToken);
      } on ValidationException catch (error) {
        yield ValidationError(errors: error.errors);
      } catch (e) {
        yield GenerateTotpFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
