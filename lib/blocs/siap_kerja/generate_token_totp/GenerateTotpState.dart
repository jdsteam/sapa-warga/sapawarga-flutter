import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class GenerateTotpState extends Equatable {
  GenerateTotpState([List props = const <dynamic>[]]);
}

class GenerateTotpInitial extends GenerateTotpState {
  @override
  List<Object> get props => [];
}

class GenerateTotpLoading extends GenerateTotpState {
  @override
  String toString() {
    return 'State GenerateTotpLoading';
  }

  @override
  List<Object> get props => [];
}

class GenerateTotpSuccess extends GenerateTotpState {
  final String tOtpToken;

  GenerateTotpSuccess({@required this.tOtpToken});

  @override
  String toString() {
    return 'State GenerateTotpSuccess token : $tOtpToken';
  }

  @override
  List<Object> get props => [];
}

class ValidationError extends GenerateTotpState {
  final Map<String, dynamic> errors;

  ValidationError({@required this.errors}) : super([errors]);

  @override
  String toString() {
    return 'State ValidationError{errors: $errors}';
  }

  @override
  List<Object> get props => <Object>[errors];
}

class GenerateTotpFailure extends GenerateTotpState {
  final String error;

  GenerateTotpFailure({this.error});

  @override
  String toString() {
    return 'State GenerateTotpFailure{error: $error}';
  }

  @override
  List<Object> get props => <Object>[error];
}
