import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/blocs/siap_kerja/send_profile_user/SendProfileUserEvent.dart';
import 'package:sapawarga/blocs/siap_kerja/send_profile_user/SendProfileUserState.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/exceptions/ValidationException.dart';
import 'package:sapawarga/repositories/SiapKerjaRepository.dart';

import './Bloc.dart';

class SendProfileUserBloc
    extends Bloc<SendProfileUserEvent, SendProfileUserState> {
  final SiapKerjaRepository siapKerjaRepository;

  SendProfileUserBloc({@required this.siapKerjaRepository})
      : assert(siapKerjaRepository != null);

  @override
  SendProfileUserState get initialState => SendProfileUserInitial();

  @override
  Stream<SendProfileUserState> mapEventToState(
    SendProfileUserEvent event,
  ) async* {
    if (event is SendProfileUser) {
      yield SendProfileUserLoading();

      try {
        bool isUpdate = await siapKerjaRepository.sendProfile(
            userInfoModel: event.userInfoModel,
            envSiapKerja: event.secretEnvSiapKerja);
        yield SendProfileUserSuccess(isUpdate: isUpdate);
      } on ValidationException catch (error) {
        yield ValidationError(errors: error.errors);
      } catch (e) {
        yield SendProfileUserFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
