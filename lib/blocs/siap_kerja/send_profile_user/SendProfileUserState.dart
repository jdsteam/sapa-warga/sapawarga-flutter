import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SendProfileUserState extends Equatable {
  SendProfileUserState([List props = const <dynamic>[]]);
}

class SendProfileUserInitial extends SendProfileUserState {
  @override
  List<Object> get props => [];
}

class SendProfileUserLoading extends SendProfileUserState {
  @override
  String toString() {
    return 'State SendProfileUserLoading';
  }

  @override
  List<Object> get props => [];
}

class SendProfileUserSuccess extends SendProfileUserState {
  final bool isUpdate;

  SendProfileUserSuccess({@required this.isUpdate});

  @override
  String toString() {
    return 'State SendProfileUserSuccess : $isUpdate';
  }

  @override
  List<Object> get props => [];
}

class ValidationError extends SendProfileUserState {
  final Map<String, dynamic> errors;

  ValidationError({@required this.errors}) : super([errors]);

  @override
  String toString() {
    return 'State ValidationError{errors: $errors}';
  }

  @override
  List<Object> get props => <Object>[errors];
}

class SendProfileUserFailure extends SendProfileUserState {
  final String error;

  SendProfileUserFailure({this.error});

  @override
  String toString() {
    return 'State SendProfileUserFailure{error: $error}';
  }

  @override
  List<Object> get props => <Object>[error];
}
