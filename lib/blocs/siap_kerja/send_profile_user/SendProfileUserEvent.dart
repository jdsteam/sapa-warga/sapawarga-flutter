import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/UserInfoModel.dart';

@immutable
abstract class SendProfileUserEvent extends Equatable {
  SendProfileUserEvent([List props = const <dynamic>[]]);
}

class SendProfileUser extends SendProfileUserEvent {
  final UserInfoModel userInfoModel;
  final Map<String, dynamic> secretEnvSiapKerja;

  SendProfileUser(
      {@required this.userInfoModel, @required this.secretEnvSiapKerja});

  @override
  String toString() {
    return 'Event SendProfileUser';
  }

  @override
  List<Object> get props => [];
}
