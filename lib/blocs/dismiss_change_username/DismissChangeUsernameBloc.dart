import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/repositories/AuthProfileRepository.dart';

import './Bloc.dart';

class DismissChangeUsernameBloc
    extends Bloc<DismissChangeUsernameEvent, DismissChangeUsernameState> {
  final AuthProfileRepository authProfileRepository;

  DismissChangeUsernameBloc({@required this.authProfileRepository})
      : assert(authProfileRepository != null);

  @override
  DismissChangeUsernameState get initialState => DismissChangeUsernameInitial();

  @override
  Stream<DismissChangeUsernameState> mapEventToState(
    DismissChangeUsernameEvent event,
  ) async* {
    if (event is DismissSendChangedUsername) {
      try {
        await authProfileRepository.scheduleChangeUsername();
        yield DismissChangeUsernameSuccess();
      } catch (e) {
        yield DismissChangeUsernameFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
