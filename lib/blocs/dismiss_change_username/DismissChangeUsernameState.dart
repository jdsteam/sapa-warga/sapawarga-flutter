import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DismissChangeUsernameState extends Equatable {
  DismissChangeUsernameState([List props = const <dynamic>[]]);
}

class DismissChangeUsernameInitial extends DismissChangeUsernameState {
  @override
  List<Object> get props => [];
}

class DismissChangeUsernameSuccess extends DismissChangeUsernameState {
  @override
  String toString() {
    return 'State DismissChangeUsernameSuccess';
  }

  @override
  List<Object> get props => [];
}

class DismissChangeUsernameFailure extends DismissChangeUsernameState {
  final String error;

  DismissChangeUsernameFailure({this.error});

  @override
  String toString() {
    return 'State DismissChangeUsernameFailure{error: $error}';
  }

  @override
  List<Object> get props => <Object>[error];
}
