import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DismissChangeUsernameEvent extends Equatable {
  DismissChangeUsernameEvent([List props = const <dynamic>[]]);
}

class DismissSendChangedUsername extends DismissChangeUsernameEvent {
  @override
  String toString() {
    return 'Event DismissChangeUsernameEvent';
  }

  @override
  List<Object> get props => [];
}
