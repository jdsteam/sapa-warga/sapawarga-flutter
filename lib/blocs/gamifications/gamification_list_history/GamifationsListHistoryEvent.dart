import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class GamifationsListHistoryEvent extends Equatable {
  GamifationsListHistoryEvent([List props = const <dynamic>[]]);
}

class GamificationsHistoryListLoad extends GamifationsListHistoryEvent {
  final int page;

  GamificationsHistoryListLoad({@required this.page}) : super([page]);

  @override
  String toString() {
    return 'Event GamificationsHistoryListLoad';
  }

  @override
  List<Object> get props => [page];
}
