import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/GamificationOnprogressModel.dart';

@immutable
abstract class GamificationsListHistoryState extends Equatable {
  GamificationsListHistoryState([List props = const <dynamic>[]]);
}

class GamificationsListHistoryInitial extends GamificationsListHistoryState {
  @override
  String toString() => 'State GamificationsListHistoryInitial';

  @override
  List<Object> get props => [];
}

class GamificationsListHistoryLoading extends GamificationsListHistoryState {
  @override
  String toString() {
    return 'State GamificationsListHistoryLoading';
  }

  @override
  List<Object> get props => [];
}


class GamificationsListHistoryLoaded extends GamificationsListHistoryState {
  final GamificationOnprogressModel records;

  GamificationsListHistoryLoaded({@required this.records}) : super([records]);

  @override
  String toString() {
    return 'State GamificationsListHistoryLoaded';
  }

  @override
  List<Object> get props => [records];
}

class GamificationsListHistoryFailure extends GamificationsListHistoryState {
  final String error;

  GamificationsListHistoryFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State GamificationsListHistoryFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}

