import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class GamifationsDetailMissionEvent extends Equatable {
  GamifationsDetailMissionEvent([List props = const <dynamic>[]]);
}


class GamificationsDetailMissionLoad extends GamifationsDetailMissionEvent {
  final int id;

  GamificationsDetailMissionLoad({@required this.id}) : super([id]);

  @override
  String toString() {
    return 'Event GamificationsDetailMissionLoad';
  }

  @override
  List<Object> get props => [id];
}
