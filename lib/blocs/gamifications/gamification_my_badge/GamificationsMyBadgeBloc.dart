import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/GamificationOnprogressModel.dart';
import 'package:sapawarga/repositories/GamificationsRepository.dart';
import 'Bloc.dart';

class GamificationsMyBadgeBloc extends Bloc<
    GamifationsMyBadgeEvent, GamificationsMyBadgeState> {
  final GamificationsRepository gamificationsRepository;

  GamificationsMyBadgeBloc({@required this.gamificationsRepository});

  @override
  GamificationsMyBadgeState get initialState =>
      GamificationsMyBadgeInitial();

  @override
  Stream<GamificationsMyBadgeState> mapEventToState(
    GamifationsMyBadgeEvent event,
  ) async* {
    if (event is GamificationsMyBadgeLoad) {
      if (event.page == 1) yield GamificationsMyBadgeLoading();

      try {
        GamificationOnprogressModel records = await gamificationsRepository
            .getMyBadge(page: event.page);
        yield GamificationsMyBadgeLoaded(records: records);
      } catch (e) {
        yield GamificationsMyBadgeFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
