import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/GamificationOnprogressModel.dart';

@immutable
abstract class GamificationsMyBadgeState extends Equatable {
  GamificationsMyBadgeState([List props = const <dynamic>[]]);
}

class GamificationsMyBadgeInitial extends GamificationsMyBadgeState {
  @override
  String toString() => 'State GamificationsMyBadgeInitial';

  @override
  List<Object> get props => [];
}

class GamificationsMyBadgeLoading extends GamificationsMyBadgeState {
  @override
  String toString() {
    return 'State GamificationsMyBadgeLoading';
  }

  @override
  List<Object> get props => [];
}


class GamificationsMyBadgeLoaded extends GamificationsMyBadgeState {
  final GamificationOnprogressModel records;

  GamificationsMyBadgeLoaded({@required this.records}) : super([records]);

  @override
  String toString() {
    return 'State GamificationsMyBadgeLoaded';
  }

  @override
  List<Object> get props => [records];
}

class GamificationsMyBadgeFailure extends GamificationsMyBadgeState {
  final String error;

  GamificationsMyBadgeFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State GamificationsMyBadgeFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
