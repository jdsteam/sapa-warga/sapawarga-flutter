import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/GamificationOnprogressModel.dart';

@immutable
abstract class GamificationsListOnProgressState extends Equatable {
  GamificationsListOnProgressState([List props = const <dynamic>[]]);
}

class GamificationsListOnProgressInitial extends GamificationsListOnProgressState {
  @override
  String toString() => 'State GamificationsListOnProgressInitial';

  @override
  List<Object> get props => [];
}

class GamificationsListOnProgressLoading extends GamificationsListOnProgressState {
  @override
  String toString() {
    return 'State GamificationsListOnProgressLoading';
  }

  @override
  List<Object> get props => [];
}


class GamificationsListOnProgressLoaded extends GamificationsListOnProgressState {
  final GamificationOnprogressModel records;

  GamificationsListOnProgressLoaded({@required this.records}) : super([records]);

  @override
  String toString() {
    return 'State GamificationsListOnProgressLoaded';
  }

  @override
  List<Object> get props => [records];
}

class GamificationsListOnProgressFailure extends GamificationsListOnProgressState {
  final String error;

  GamificationsListOnProgressFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State GamificationsListOnProgressFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
