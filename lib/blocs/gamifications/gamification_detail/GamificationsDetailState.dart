import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/GamificationsModel.dart';

@immutable
abstract class GamificationsDetailState extends Equatable {
  GamificationsDetailState([List props = const <dynamic>[]]);
}

class GamificationsDetailInitial extends GamificationsDetailState {
  @override
  String toString() => 'State GamificationsDetailInitial';

  @override
  List<Object> get props => [];
}

class GamificationsDetailLoading extends GamificationsDetailState {
  @override
  String toString() {
    return 'State GamificationsDetailLoading';
  }

  @override
  List<Object> get props => [];
}


class GamificationsDetailLoaded extends GamificationsDetailState {
  final ItemGamification records;

  GamificationsDetailLoaded({@required this.records}) : super([records]);

  @override
  String toString() {
    return 'State GamificationsDetailLoaded';
  }

  @override
  List<Object> get props => [records];
}

class GamificationsDetailFailure extends GamificationsDetailState {
  final String error;

  GamificationsDetailFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State GamificationsDetailFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}

class GamificationsTakeMissionlLoading extends GamificationsDetailState {
  @override
  String toString() {
    return 'State GamificationsTakeMissionlLoading';
  }

  @override
  List<Object> get props => [];
}


class GamificationsTakeMissionLoaded extends GamificationsDetailState {
  final bool isTakeMission;

  GamificationsTakeMissionLoaded({@required this.isTakeMission}) : super([isTakeMission]);

  @override
  String toString() {
    return 'State GamificationsTakeMissionLoaded';
  }

  @override
  List<Object> get props => [isTakeMission];
}

class GamificationsTakeMissionFailure extends GamificationsDetailState {
  final String error;

  GamificationsTakeMissionFailure({this.error}) : super([error]);

  @override
  String toString() {
    return 'State GamificationsTakeMissionFailure{error: $error}';
  }

  @override
  List<Object> get props => [error];
}
