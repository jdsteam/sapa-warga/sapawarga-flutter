import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/GamificationsModel.dart';
import 'package:sapawarga/repositories/GamificationsRepository.dart';
import 'Bloc.dart';


class GamificationsDetailBloc extends Bloc<GamifationDetailEvent, GamificationsDetailState> {
  final GamificationsRepository gamificationsRepository;

  GamificationsDetailBloc({@required this.gamificationsRepository}):assert(gamificationsRepository != null);

  @override
  GamificationsDetailState get initialState => GamificationsDetailInitial();

  @override
  Stream<GamificationsDetailState> mapEventToState(
    GamifationDetailEvent event,
  ) async* {
    if (event is GamificationsDetailLoad) {
       yield GamificationsDetailLoading();

      try {
        ItemGamification records = await gamificationsRepository.getNewMissionDetail(id: event.id);
        yield GamificationsDetailLoaded(records: records);
      } catch (e) {
        yield GamificationsDetailFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
    if (event is GamificationsTakeMission) {
       yield GamificationsTakeMissionlLoading();

      try {
        var isTakeMission = await gamificationsRepository.takeMission(id: event.id);
        yield GamificationsTakeMissionLoaded(isTakeMission: isTakeMission);
      } catch (e) {
        yield GamificationsTakeMissionFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
