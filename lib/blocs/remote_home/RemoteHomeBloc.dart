import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/repositories/FirebaseRepository.dart';
import './Bloc.dart';

class RemoteHomeBloc extends Bloc<RemoteHomeEvent, RemoteHomeState> {
  final FirebaseRepository firebaseRepository;

  RemoteHomeBloc({@required this.firebaseRepository});

  @override
  RemoteHomeState get initialState => InitialRemoteHomeState();

  @override
  Stream<RemoteHomeState> mapEventToState(
    RemoteHomeEvent event,
  ) async* {
    if (event is RemoteHomeLoad) {
      yield RemoteHomeLoading();

      final RemoteConfig remoteConfig =
          await firebaseRepository.connectRemoteConfig();

      yield RemoteHomeLoaded(remoteConfig);
    }
  }
}
