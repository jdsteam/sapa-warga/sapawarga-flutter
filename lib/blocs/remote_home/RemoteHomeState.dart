import 'package:equatable/equatable.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';

abstract class RemoteHomeState extends Equatable {
  const RemoteHomeState([List props = const <dynamic>[]]);
}

class InitialRemoteHomeState extends RemoteHomeState {
  @override
  List<Object> get props => [];
}

class RemoteHomeLoading extends RemoteHomeState {
  @override
  String toString() {
    return 'State RemoteHomeLoading';
  }

  @override
  List<Object> get props => [];
}

class RemoteHomeLoaded extends RemoteHomeState {
  final RemoteConfig remoteConfig;

  RemoteHomeLoaded(this.remoteConfig) : super([remoteConfig]);

  @override
  String toString() {
    return 'State RemoteHomeLoaded';
  }

  @override
  List<Object> get props => [remoteConfig];
}
