import 'package:equatable/equatable.dart';

abstract class RemoteHomeEvent extends Equatable {
  const RemoteHomeEvent([List props = const <dynamic>[]]);
}

class RemoteHomeLoad extends RemoteHomeEvent {

  RemoteHomeLoad();

  @override
  String toString() {
    return 'Event RemoteHomeLoad';
  }

  @override
  List<Object> get props => [];

}