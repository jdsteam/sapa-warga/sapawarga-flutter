import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PhoneBookListEvent extends Equatable {
  PhoneBookListEvent([List props = const []]);
}

class PhoneBookListLoad extends PhoneBookListEvent {
  final int page;
  final bool isFirstLoad;

  PhoneBookListLoad({@required this.page, this.isFirstLoad = false})
      : super([page]);

  @override
  String toString() => 'Event PhoneBookListLoad';

  @override
  List<Object> get props => [page, isFirstLoad];
}

class PhoneBookListRefresh extends PhoneBookListEvent {
  @override
  String toString() => 'Event PhoneBookListRefresh';

  @override
  List<Object> get props => [];
}

class PhoneBookListSearch extends PhoneBookListEvent {
  final String keyword;
  final int page;
  final bool isFirstLoad;

  PhoneBookListSearch(
      {@required this.keyword, this.page, this.isFirstLoad = false})
      : super([keyword]);

  @override
  String toString() => 'Event PhoneBookListSearch';

  @override
  List<Object> get props => [keyword, page, isFirstLoad];
}

class PhoneBookListFilter extends PhoneBookListEvent {
  final String keyword;

  PhoneBookListFilter({@required this.keyword}) : super([keyword]);

  @override
  String toString() => 'Event PhoneBookListFilter';

  @override
  List<Object> get props => [keyword];
}
