import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/models/PhoneBookModel.dart';

@immutable
abstract class PhoneBookListState extends Equatable {
  PhoneBookListState([List props = const []]);
}

class PhoneBookListInitial extends PhoneBookListState {
  @override
  String toString() => 'State PhoneBookListInitial';

  @override
  List<Object> get props => [];
}

class PhoneBookListLoading extends PhoneBookListState {
  @override
  String toString() => 'State PhoneBookListLoading';

  @override
  List<Object> get props => [];
}

class PhoneBookListLoaded extends PhoneBookListState {
  final List<PhoneBookModel> records;
  final int maxData;

  PhoneBookListLoaded({@required this.records, this.maxData}) : super([records]);

  @override
  String toString() => 'State PhoneBookListLoaded';

  @override
  List<Object> get props => [records];
}

class PhoneBookListFailure extends PhoneBookListState {
  final String error;

  PhoneBookListFailure({@required this.error}) : super([error]);

  @override
  String toString() => 'State PhoneBookListFailure';

  @override
  List<Object> get props => [error];
}
