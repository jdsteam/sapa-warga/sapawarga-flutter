import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sapawarga/blocs/phonebook_list/Bloc.dart';
import 'package:sapawarga/exceptions/CustomException.dart';
import 'package:sapawarga/models/PhoneBookModel.dart';
import 'package:sapawarga/repositories/PhoneBookRepository.dart';
import 'package:sapawarga/utilities/SharedPreferences.dart';

class PhoneBookListBloc extends Bloc<PhoneBookListEvent, PhoneBookListState> {
  final PhoneBookRepository phoneBookRepository;

  PhoneBookListBloc({@required this.phoneBookRepository}):assert(phoneBookRepository != null);

  @override
  PhoneBookListState get initialState => PhoneBookListInitial();

  Stream<PhoneBookListState> mapEventToState(PhoneBookListEvent event) async* {
    if (event is PhoneBookListLoad) {
      if (event.page == 1 && event.isFirstLoad) yield PhoneBookListLoading();

      try {
        List<PhoneBookModel> records = await phoneBookRepository.getRecords(
            page: event.page, forceRefresh: event.page == 1 ? true : false);
        int maxDatalength = await Preferences.getTotalCount();
        yield PhoneBookListLoaded(records: records, maxData: maxDatalength);
      } catch (e) {
        print('failure phonebook '+e.toString());
        yield PhoneBookListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is PhoneBookListRefresh) {
      yield PhoneBookListLoading();

      try {
        List<PhoneBookModel> records =
            await phoneBookRepository.getRecords(page: 1, forceRefresh: true);
        int maxDatalength = await Preferences.getTotalCount();

        yield PhoneBookListLoaded(records: records, maxData: maxDatalength);
      } catch (e) {
        yield PhoneBookListFailure(
            error: CustomException.onConnectionException(e.toString()));

        // Load dari local database jika gagal fetch dari server
        List<PhoneBookModel> records = await phoneBookRepository.getRecords();

        yield PhoneBookListLoaded(records: records);
      }
    }

    if (event is PhoneBookListSearch) {
      if (event.page == 1 && event.isFirstLoad) yield PhoneBookListLoading();

      try {
        List<PhoneBookModel> records = await phoneBookRepository.getRecords(
            keyword: event.keyword, page: event.page);
        int maxDatalength = await Preferences.getTotalCount();

        yield PhoneBookListLoaded(records: records, maxData: maxDatalength);
      } catch (e) {
        yield PhoneBookListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }

    if (event is PhoneBookListFilter) {
      yield PhoneBookListLoading();

      try {
        List<PhoneBookModel> records =
            await phoneBookRepository.getRecords(filter: event.keyword);
        int maxDatalength = await Preferences.getTotalCount();

        yield PhoneBookListLoaded(records: records, maxData: maxDatalength);
      } catch (e) {
        yield PhoneBookListFailure(
            error: CustomException.onConnectionException(e.toString()));
      }
    }
  }
}
