import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sapawarga/configs/FlavorConfig.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  DBProvider._();

  static final _dbName = FlavorConfig.instance.values.databaseName;
  static final DBProvider db = DBProvider._();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _dbName);
    return await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE PhoneBooks ("
          "pid INTEGER PRIMARY KEY,"
          "id INTEGER UNIQUE,"
          "name TEXT,"
          "address TEXT,"
          "description TEXT,"
          "seq INTEGER,"
          "kabkota_id INTEGER,"
          "kec_id INTEGER,"
          "kel_id INTEGER,"
          "latitude TEXT,"
          "longitude TEXT,"
          "cover_image_path TEXT,"
//          "cover_image_url TEXT,"
          "phone_numbers TEXT"
//          "category TEXT,"
//          "category_id INTEGER"
          ")");

      await db.execute("CREATE TABLE HumasJabar ("
          "id INTEGER PRIMARY KEY,"
          "postTitle TEXT,"
          "thumbnail TEXT,"
          "slug TEXT,"
          "tglPublish TEXT"
          ")");

      await db.execute("CREATE TABLE Messages ("
          "id TEXT PRIMARY KEY,"
          "type TEXT,"
          "message_id INTEGER UNIQUE,"
          "sender_id INTEGER,"
          "sender_name TEXT,"
          "recipient_id INTEGER,"
          "category_name TEXT,"
          "title TEXT,"
          "excerpt TEXT,"
          "content TEXT,"
          "status INTEGER,"
          "meta TEXT,"
          "read_at INTEGER,"
          "created_at INTEGER,"
          "updated_at INTEGER"
          ")");

      await db.execute("CREATE TABLE Notifications ("
          "id INTEGER PRIMARY KEY,"
          "title TEXT,"
          "target TEXT,"
          "meta TEXT,"
          "read_at INTEGER"
          ")");

      await db.execute("CREATE TABLE CategoryUsulan ("
          "id INTEGER PRIMARY KEY,"
          "type TEXT,"
          "name TEXT,"
          "meta TEXT,"
          "status INTEGER,"
          "status_label TEXT,"
          "created_at INTEGER,"
          "updated_at INTEGER"
          ")");

      await db.execute("CREATE TABLE PopupInformation ("
          "id INTEGER PRIMARY KEY,"
          "last_shown TEXT"
          ")");
    });
  }
}
